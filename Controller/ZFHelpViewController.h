//
//  ZFTimeBonusViewController.h
//  Ziferblat
//
//  Created by Terry Michel on 08/04/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "ZFHelpView.h"

@interface ZFHelpViewController : UIViewController {
    ZFHelpView *contentView;
    NSArray *venues;
}

- (instancetype)initWithVenues:(NSArray *)venuesList;

@end
