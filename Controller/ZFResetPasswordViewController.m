//
//  ZFResetPasswordViewController.m
//  Ziferblat
//
//  Created by Terry Michel on 23/07/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "ZFResetPasswordViewController.h"
#import "ZFBackButton.h"
#import "ZFButton.h"
#import "ZFForgotPasswordRequest.h"
#import "ZFPasswordEmailViewController.h"

#define kBackgroundKeyboardVisibleShift 38
#define kResetPasswordLabelFrame        (UI_USER_INTERFACE_IDIOM() !=UIUserInterfaceIdiomPad ? CGRectMake(53, 41, 213, 31) : CGRectMake(234, 70, 300, 60))
#define kEmailLabelFrame                (UI_USER_INTERFACE_IDIOM() !=UIUserInterfaceIdiomPad ? CGRectMake(self.view.center.x-224/2, 176, 224, 30) : CGRectMake(self.view.center.x-350/2, 340, 350, 40))
#define kEmailTextFieldFrame            (UI_USER_INTERFACE_IDIOM() !=UIUserInterfaceIdiomPad ? CGRectMake(62, 210, 208, 38) : CGRectMake(200, 420, 365, 68))
#define kResetButtonTopMargin           (UI_USER_INTERFACE_IDIOM() !=UIUserInterfaceIdiomPad ? 276 : 552)

static NSString *const crossImage = @"ZFLoginBadgeX";
static NSString *const checkImage = @"ZFLoginBadgeV";

@interface ZFResetPasswordViewController()

@property (strong, nonatomic) UIImageView *cross;
@property (strong, nonatomic) UIImageView *done;
@property (strong, nonatomic) NSString *canonicalTitle;

@end

@implementation ZFResetPasswordViewController

@synthesize done;
@synthesize cross;

- (void)loadView {
    ZFBaseView *aView = [[ZFBaseView alloc] init];
    self.view = aView;
    
    self.canonicalTitle = NSLocalizedString(@"Please enter your email:",nil);
    
    backgroundImageView = [UIImageView imageViewWithImageNamed:@"ZFResetPasswordBackground"];
    [kScrollView addSubview:backgroundImageView];
    
    backButton = [ZFBackButton backButtonWithTitle:NSLocalizedString(@"BACK TO LOG IN PAGE", nil)];
    [kScrollView addSubview:backButton];
    TARGET_TOUCH_UP(backButton, @selector(backButtonPressed))
    
    resetPasswordLabel = [[UILabel alloc]initWithFrame:kResetPasswordLabelFrame];
    resetPasswordLabel.text = NSLocalizedString(@"RESET PASSWORD",nil);
    resetPasswordLabel.textColor = Colour_Black;
    resetPasswordLabel.textAlignment = NSTextAlignmentCenter;
    resetPasswordLabel.font = (UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPad ? FontRockWell(17) : FontRockWell(30));
    resetPasswordLabel.adjustsFontSizeToFitWidth = YES;
    resetPasswordLabel.minimumScaleFactor = 0.5;
    [kScrollView addSubview:resetPasswordLabel];
    
    emailLabel = [[UILabel alloc]initWithFrame:kEmailLabelFrame];
    emailLabel.numberOfLines = 0;
    emailLabel.text = self.canonicalTitle;
    emailLabel.textColor = Colour_White;
    emailLabel.textAlignment = NSTextAlignmentCenter;
    emailLabel.font = (UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPad ? FontRockWell(14) : FontRockWell(28));
    emailLabel.adjustsFontSizeToFitWidth = YES;
    emailLabel.minimumScaleFactor = 0.5;
    [kScrollView addSubview:emailLabel];
    
    emailTextField = [UITextField textfieldWithFrame:kEmailTextFieldFrame
                                     placeholderText:NSLocalizedString(@"Email", nil)
                                           textColor:Colour_Black
                                            textFont:(UI_USER_INTERFACE_IDIOM() !=UIUserInterfaceIdiomPad ? FontRockWell(13) : FontRockWell(24))
                                        keyboardMode:UIKeyboardTypeEmailAddress
                                           clearMode:UITextFieldViewModeAlways
                                       returnKeyMode:UIReturnKeyNext
                                  capitalizationMode:UITextAutocapitalizationTypeNone
                                     autoCorrectMode:UITextAutocorrectionTypeNo
                                                 tag:0
                                            isSecure:NO
                                           addToView:kScrollView];
    [emailTextField setAdjustsFontSizeToFitWidth:TRUE];
    [emailTextField setMinimumFontSize:3];
    [emailTextField addTarget:self action:@selector(checkStatus) forControlEvents:UIControlEventEditingChanged];
    
    resetButton  = [ZFButton buttonWithTitle:NSLocalizedString(@"SEND CODE", nil)];
    resetButton.enabled = NO;
    [kScrollView addSubview:resetButton];
    resetButton.top = kResetButtonTopMargin;
    [resetButton centerHorizontallyInSuperView];
    TARGET_TOUCH_UP(resetButton, @selector(resetButtonPressed))
    
    UITapGestureRecognizer *tapGesture = [UITapGestureRecognizer gestureWithTarget:self
                                                                          selector:@selector(tapGestureDetected:)
                                                                         addToView:self.view];
    tapGesture.delegate = self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.cross = [UIImageView imageViewWithImageNamed:crossImage];
    self.cross.hidden = YES;
    self.done = [UIImageView imageViewWithImageNamed:checkImage];
    self.done.hidden = YES;
    [kScrollView addSubview:cross];
    [kScrollView addSubview:done];
}

#pragma mark - 
#pragma mark Buttons actions

- (void)backButtonPressed {
    [self.view endEditing:YES];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)resetButtonPressed {
    [self.view endEditing:TRUE];
    [ZFForgotPasswordRequest requestWithEmail:emailTextField.text
                                   completion:^(LORESTRequest *aRequest) {
                                       
                                       [SVProgressHUD dismiss];
                                       self.view.userInteractionEnabled = YES;
                                       
                                       if(aRequest.resultStatus == LORESTRequestResultStatusSucceeded) {
                                           ZFPasswordEmailViewController *passwordEmailViewController = [ZFPasswordEmailViewController new];
                                           passwordEmailViewController.email = emailTextField.text;
                                           [self.navigationController pushViewController:passwordEmailViewController animated:YES];
                                       }
                                       else if(aRequest.resultStatus == LORESTRequestResultStatusNotFound) {
                                           [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"Email not found",nil)];
                                           emailLabel.frame = CGRectMake(CGRectGetMinX(kEmailLabelFrame) - 20, CGRectGetMidY(kEmailLabelFrame) - 50, CGRectGetWidth(kEmailLabelFrame) + 20, CGRectGetHeight(kEmailLabelFrame) + 40);
                                           [emailLabel centerHorizontallyInSuperView];
                                           emailLabel.text = NSLocalizedString(@"Sorry we don't have a record for that email! Please try to enter an email you gave us at registration", nil);
                                       }
                                       else {
                                           [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"Authentication failed",nil)];
                                       }
                                   }];
}

#pragma mark -
#pragma mark UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self checkStatus];
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range
replacementString:(NSString *)string {
    [self checkStatus];
    return YES;
}

#pragma mark -
#pragma mark Gestures

- (void)tapGestureDetected:(UITapGestureRecognizer *)aTapGesture {
    [self.view endEditing:YES];
}

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer {
    CGPoint location = [gestureRecognizer locationInView:kScrollView];
    return (!CGRectContainsPoint(resetButton.frame, location) && !CGRectContainsPoint(backButton.frame, location));
}

#pragma mark -
#pragma mark Utils

- (void)checkStatus {
    BOOL validEmail = NO;
    
    if (emailTextField.text.isValidEmailAddress) {
        validEmail = YES;
        if ([emailTextField isFirstResponder]) {
            [self annotateTextField:emailTextField asValidated:validEmail];
        }
    } else {
        validEmail = NO;
        emailLabel.text = NSLocalizedString(@"Email is not correct.", @"Reset Password title");
        emailLabel.textColor = [UIColor lightGrayColor];
        [self annotateTextField:emailTextField asValidated:validEmail];
    }
    
    if (validEmail) {
        emailLabel.text = NSLocalizedString(@"Email is correct.", @"Reset Password title");
        emailLabel.textColor = Colour_Hex(@"A5CE43");
    }
    
    resetButton.enabled = validEmail;
}

- (void)annotateTextField:(UITextField *)field asValidated:(BOOL)valid {
    
    static int leftPadding = 26;
    
    cross.center = CGPointMake(leftPadding, field.center.y);
    done.center = cross.center;
    
    if (valid) {
        cross.hidden = YES;
        done.hidden = NO;
    } else {
        cross.hidden = NO;
        done.hidden = YES;
    }
}

@end
