//
//  ZFBalanceHistoryViewController.h
//  Ziferblat
//
//  Created by Terry Michel on 08/04/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "ZFBaseView.h"
#import "ZFViewController.h"

@class LOTableView;

@interface ZFBalanceHistoryViewController : ZFViewController<UITableViewDataSource, UITableViewDelegate> {
    NSMutableArray *balanceHistory;
    LOTableView *tableView;
}

@end
