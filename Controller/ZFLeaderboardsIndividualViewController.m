//
//  ZFLeaderboardsIndividualViewController.m
//  Ziferblat
//
//  Created by Terry Michel on 22/04/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "ZFLeaderboardsIndividualViewController.h"
#import "ZFLeaderTableViewCell.h"

@interface ZFLeaderboardsIndividualViewController()

@property (strong, nonatomic) NSArray<ZFLeaderBoard *> *leaderboard;
@property (strong, nonatomic) NSString *subTitle;
@property (strong, nonatomic) UITableView *tableView;
@property CGFloat keyboardHeight;
@property (strong, nonatomic) UIView *footer;
@property (strong, nonatomic) UITextField *searchField;
@property CGPoint keyboardOffset;
@property ZFLeaderBoardsSortType selectedTab;

@end

@implementation ZFLeaderboardsIndividualViewController

@synthesize contentView;

#define kTablePositionY (notIPad ? 397:953)
#define kRowHeight (notIPad ? 40:96)
#define kRowWidth (notIPad ? 320:768)
#define kFooterHeight (notIPad ? 56:134) // Search field

#pragma mark - 
#pragma mark Lifecycle

- (instancetype)initWithContent:(NSArray<ZFLeaderBoard *> *)aLeaderBoard
                        andType:(ZFLeaderBoardType)aType
                  personalScore:(ZFLeaderBoardCount *)aPersonalScore
                          venue:(NSNumber *)venueId
                            tab:(ZFLeaderBoardsSortType)selectedTab{
    
    self = [super init];
    if (self) {
        
        self.myType = aType;
        self.selectedTab = selectedTab;
        
        NSString *subtitle;
        switch (aType) {
            case Experince:
                subtitle = NSLocalizedString(@"MOST EXPERIENCE POINTS", @"LeaderBoard subtitle");
                break;
            case Time:
                subtitle = NSLocalizedString(@"MOST TIME SPENT AT ZIFERBLAT", @"LeaderBoard subtitle");
                break;
            case Task:
                subtitle = NSLocalizedString(@"MOST TASKS COMPLETED SCORE", @"LeaderBoard subtitle");
                break;
            default:
                break;
        }
        
        self.subTitle = subtitle;
        
        contentView = [[ZFLeaderboardsIndividualView alloc] initWithLeaderBoard:aLeaderBoard
                                                                    andSubTitle:subtitle
                                                                  personalScore:aPersonalScore
                                                                          venue:venueId];
        self.view = contentView;
        
        // Setup Table
        self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, kTablePositionY, [UIScreen mainScreen].bounds.size.width, 0)];
        [self.tableView registerClass:[ZFLeaderTableViewCell class] forCellReuseIdentifier:@"ZFLeaderBoardsCell"];
        self.tableView.delegate = self;
        self.tableView.dataSource = self;
        self.tableView.scrollEnabled = NO;
        self.tableView.separatorStyle = UITableViewCellSelectionStyleNone;
        [contentView.scrollView addSubview:self.tableView];
        
        
        [self refreshWithContent:aLeaderBoard
                         andType:aType
                   personalScore:aPersonalScore];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(keyboardDidShow:)
                                                     name: UIKeyboardDidShowNotification
                                                   object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(keyboardDidHide:)
                                                     name: UIKeyboardDidHideNotification
                                                   object:nil];
        [self setupTouchTargets];
        
        if (self.selectedTab == ZFLeaderBoardsSortType_AllTime) {
            [self performSelector:@selector(switchToSecondTab)
                       withObject:nil
                       afterDelay:0.5];
        }
    }
    return self;
}

- (void)switchToSecondTab {
    [contentView.allTimeToggleButton setSelected:YES];
    [contentView.lastDaysToggleButton setSelected:NO];
    [contentView updateForSortType:ZFLeaderBoardsSortType_AllTime];
}

- (void)refreshWithContent:(NSArray<ZFLeaderBoard *> *)aLeaderBoard
                   andType:(ZFLeaderBoardType)aType
             personalScore:(ZFLeaderBoardCount *)aPersonalScore {
    
    self.leaderboard = aLeaderBoard;
    
    [contentView refreshWithpersonalScore:aPersonalScore];
    
    contentView.delegate = self;
    
    [self updateTable];
    [self setupTouchTargets];
}

- (void)setupTouchTargets {
    TARGET_TOUCH_UP(contentView.backButton, @selector(backButtonTapped))
    TARGET_TOUCH_UP(contentView.lastDaysToggleButton, @selector(lastDaysToggleButtonTapped:))
    TARGET_TOUCH_UP(contentView.allTimeToggleButton, @selector(allTimeToggleButtonTapped:))
    TARGET_TOUCH_UP(contentView.ziferblatSelector, @selector(switchZiferblatButtonTapped:));
}

- (void)dealloc {
    contentView.isAllTimeTabSelected = NO;
    
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

-(void)updateTable {
    [self.tableView reloadData];
    self.tableView.height = self.tableView.contentSize.height;
    contentView.scrollView.contentSize = CGSizeMake(contentView.frame.size.width, self.contentView.backgroundImageView.size.height + self.tableView.contentSize.height + (notIPad ? 0:60));
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSInteger total = self.leaderboard.count;
    
    return total;
}

- (ZFLeaderTableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    ZFLeaderTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ZFLeaderBoardsCell"];
    
    if (indexPath.row == 0) {
        // First cell doesn't need separator on top.
        cell.backgroundView = nil;
    }
    
    // Setup data

    // Data source object
    ZFLeaderBoard *leader = self.leaderboard[indexPath.row];
    
    int leaderPosition = leader.position.intValue;
    cell.indexLabel.text = [NSString stringWithFormat:@"#%d", leaderPosition];
    
    NSString *firstName = @"";
    NSString *lastName = @"";
    
    firstName = leader.firstName;
    lastName = leader.lastName;
    
    cell.username.text = [NSString stringWithFormat:NSLocalizedString(@"%@ %@", @"Full name"), firstName, lastName];
    cell.pointsLabel.text = leader.valueCount.stringValue;
    
    // Align username
    [cell.username sizeToFit];
    cell.username.center = CGPointMake(kRowWidth/ 2, kRowHeight/ 2);
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat height = kRowHeight;
    return height;
}

- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    if (!self.footer) {
        
        self.footer = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, kFooterHeight)];
        UIImageView *backgroundView = [UIImageView imageViewWithImageNamed:@"ZFLeaderboardsSearch"];
        [self.footer addSubview:backgroundView];
        
        self.searchField = [UITextField textfieldWithFrame:(notIPad ? CGRectMake(47, 18, 212, 22):CGRectMake(113, 35, 509, 53))
                                           placeholderText:nil
                                                 textColor:[UIColor blackColor]
                                                  textFont:FontRockWell((notIPad ? 12:29))
                                              keyboardMode:UIKeyboardTypeDefault
                                                 clearMode:UITextFieldViewModeNever
                                             returnKeyMode:UIReturnKeySearch
                                        capitalizationMode:UITextAutocapitalizationTypeNone
                                           autoCorrectMode:UITextAutocorrectionTypeNo
                                                       tag:0
                                                  isSecure:NO
                                                 addToView:self.footer];
        self.searchField.delegate = self;
        self.searchField.placeholder = NSLocalizedString(@"Search...", @"Leaderboards charts");
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setFrame:(notIPad ? CGRectMake(260, 12, 32, 32):CGRectMake(624, 26, 77, 77))];
        [self.footer addSubview:button];
        
        [button addTarget:self
                   action:@selector(searchButtonTapped)
         forControlEvents:UIControlEventTouchUpInside];
    }
    
    return self.footer;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return kFooterHeight;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    ZFLeaderBoard *record = self.leaderboard[indexPath.row];
    [self.delegate openProfileForUserId:record.memberId from:self];
}

#pragma mark - Buttons actions

- (void)lastDaysToggleButtonTapped:(UIButton *)aButton {
    if(!aButton.isSelected) {
        [aButton setSelected:YES];
        [contentView.allTimeToggleButton setSelected:NO];
        [contentView updateForSortType:ZFLeaderBoardsSortType_LastDays];
        self.selectedTab = ZFLeaderBoardsSortType_LastDays;
    }
}

- (void)allTimeToggleButtonTapped:(UIButton *)aButton {
    if(!aButton.isSelected) {
        [aButton setSelected:YES];
        [contentView.lastDaysToggleButton setSelected:NO];
        [contentView updateForSortType:ZFLeaderBoardsSortType_AllTime];
        self.selectedTab = ZFLeaderBoardsSortType_AllTime;
    }
}

- (void)backButtonTapped {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)switchZiferblatButtonTapped:(UIButton *)sender {
    [contentView ziferblatSelectionButtonTapped];
}

- (void)searchButtonTapped {
    [self.delegate makeIndividualRequestWithVenue:contentView.ziferblatSelector.selectedVenue.uuid
                                      resultItems:LOMaxItemCount
                                           search:self.searchField.text
                                          andType:self.myType];
}

#pragma mark - Other

//method to move the view up/down whenever the keyboard is shown/dismissed
-(void)setViewMovedUp:(BOOL)movedUp {
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3]; // if you want to slide up the view
    
    CGRect rect = contentView.frame;
    if (movedUp) {
        // 1. move the view's origin up so that the text field that will be hidden come above the keyboard
        // 2. increase the size of the view so that the area behind the keyboard is covered up.
        rect.origin.y -= self.keyboardHeight;
        rect.size.height += self.keyboardHeight;
    }
    else {
        // revert back to the normal state.
        rect.origin.y += self.keyboardHeight;
        rect.size.height -= self.keyboardHeight;
    }
    contentView.frame = rect;
    
    [UIView commitAnimations];
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [self searchButtonTapped];
    [textField resignFirstResponder];
    
    return YES;
}

#pragma mark - Notifications

- (void)keyboardDidHide:(NSNotification *)notification {

    if (contentView.frame.origin.y >= 0) {
        [self setViewMovedUp:YES];
    }
    else if (contentView.frame.origin.y < 0) {
        [self setViewMovedUp:NO];
    }
}

- (void)keyboardDidShow:(NSNotification *)notification {
    self.keyboardHeight = [[notification.userInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size.height;
    
    // Animate the current view out of the way
    if (contentView.frame.origin.y >= 0) {
        [self setViewMovedUp:YES];
    }
    else if (contentView.frame.origin.y < 0) {
        [self setViewMovedUp:NO];
    }
}

#pragma mark - ZFLeaderBoardsViewDelegate

- (void)makeRequestsWith:(NSNumber *)venueId {
    [self.delegate makeIndividualRequestWithVenue:venueId
                                      resultItems:LOMaxItemCount
                                          andType:_myType];
}

@end
