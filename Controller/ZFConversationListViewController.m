//
//  ZFDiscussionViewController.m
//  Ziferblat
//
//  Created by Terry Michel on 08/04/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "ZFConversationListViewController.h"
#import "ZFBackButton.h"
#import "ZFBaseView.h"
#import "LOTableView.h"
#import "ZFMessageSummary.h"
#import "ZFGetConversationsListRequest.h"
#import "ZFConversationDetailViewController.h"
#import "ZFSearchView.h"
#import "ZFFriendsSearchTableViewCell.h"
#import "ZFOtherProfileViewController.h"
#import "ZFGetMemberVenueDetailsRequest.h"
#import "ZFGetMemberFriendsListRequest.h"
#import "ZFVenue.h"

#define kStartConversationButtonFrame   CGRectMake(98, 79, 123, 33)
#define kHeaderLabelFrame               (notIPad ? CGRectMake(47, 13, 224, 28):CGRectMake(113, 31, 538, 67))
#define kNewConversationLabelFrame      CGRectMake(132, 55, 75, 20)

typedef NS_ENUM(NSUInteger, ZFConversationList_TableViewType) {
    ZFConversationList_TableViewType_Conversations = 0,
    ZFConversationList_TableViewType_Search
};

@interface ZFConversationListViewController()<UITableViewDataSource, UITableViewDelegate, ZFSearchViewDelegate, ZFFriendSearchTableViewCellDelegate, UIScrollViewDelegate>

@end

@implementation ZFConversationListViewController {
    NSMutableArray *conversationsSummaryList;
    LOTableView *conversationsTableView;
    UIView *overlayView;
    ZFSearchView *searchView;
    NSArray *friendsList;
    NSArray *searchFriendsList;
    BOOL isFetchingNextPage;
    NSUInteger currentPage;
}

#pragma mark -
#pragma mark Lifecycle

- (instancetype)initWithMessages:(NSArray *)messageList andFriendsList:(NSArray *)aFriendList {
    self = [super init];
    if(self) {
        conversationsSummaryList = [[NSMutableArray alloc] initWithArray:[messageList sortedArrayUsingComparator:^NSComparisonResult(ZFMessageSummary *obj1, ZFMessageSummary *obj2) {
            return [self comparisonResultForMessageSummary1:obj1 messageSummary2:obj2];
        }]];
        friendsList = aFriendList;
        searchFriendsList = aFriendList;
        currentPage = 1;
    }
    return self;
}

- (void)loadView {
    self.view = [[ZFBaseView alloc] init];
    
    ZFBackButton *backButton = [ZFBackButton backButtonWithTitle:NSLocalizedString(@"BACK TO ROOM", nil)];
    [self.view addSubview:backButton];
    TARGET_TOUCH_UP(backButton, @selector(backButtonTapped))
    
    UIImageView *headerImageView = [UIImageView imageViewWithImageNamed:@"ConversationListHeader"];
    [kScrollView addSubview:headerImageView];
    headerImageView.top = backButton.bottom - 1;
    
    UILabel *headerLabel = [[UILabel alloc] initWithFrame:kHeaderLabelFrame];
    headerLabel.textAlignment = NSTextAlignmentCenter;
    headerLabel.text = NSLocalizedString(@"CONVERSATION LIST",nil);
    headerLabel.font = FontRockWell(notIPad ? 20:48);
    headerLabel.adjustsFontSizeToFitWidth = YES;
    headerLabel.minimumScaleFactor = 0.5;
    [headerImageView addSubview:headerLabel];
    
    searchView = [ZFSearchView searchViewOfType:ZFSearchViewType_Conversations delegate:self tableViewTag:ZFConversationList_TableViewType_Search];
    
    [self.view addSubview:searchView];
    [searchView centerHorizontallyInSuperView];
    searchView.top = (notIPad ? 90:200);
    
    conversationsTableView = [LOTableView tableViewWithFrame:CGRectMake(0,
                                                                        headerImageView.bottom,
                                                                        kScrollView.width,
                                                                        kScrollView.height-headerImageView.bottom)
                                               noContentView:nil
                                               noContentText:NSLocalizedString(@"No conversations",nil)
                                                  canRefresh:NO
                                                   addToView:kScrollView];
    conversationsTableView.backgroundColor = Colour_Clear;
    conversationsTableView.tableView.tag =ZFConversationList_TableViewType_Conversations;
    conversationsTableView.tableView.backgroundColor = Colour_Clear;
    conversationsTableView.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [conversationsTableView setController:self];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    Notification_Observe(kNotification_ConversationsListRefreshed, conversationsListRefreshed);
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [NetworkService pollForType:ZFPollingRequestType_ConversationsList memberId:UserService.loggedInUser.uuid venueId:nil summary:YES];
    [NetworkService refreshConversationList];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [self dismissSearchView];
}

#pragma mark -
#pragma mark Buttons actions

// Override
- (void)backButtonTapped {
    [self.view endEditing:YES];
	[super backButtonTapped];
}

#pragma mark -
#pragma mark TableView

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (tableView.tag) {
        case ZFConversationList_TableViewType_Conversations:
        {
            ZFConversationListCell *cell = [tableView dequeueReusableCellWithIdentifier:[ZFConversationListCell reuseIdentifier]];
            if(!cell) {
                cell = [ZFConversationListCell cell];
                [cell setDelegate:self];
            }
            [cell setupWithMessageSummary:[conversationsSummaryList objectAtIndex:indexPath.row]];
            return cell;
        }
            
        case ZFConversationList_TableViewType_Search:
        {
            NSString *identifier = NSStringFromClass([ZFFriendsSearchTableViewCell class]);
            ZFFriendsSearchTableViewCell *cell = (ZFFriendsSearchTableViewCell *)[tableView dequeueReusableCellWithIdentifier:identifier];
            if (!cell) {
                cell = [[ZFFriendsSearchTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
                cell.delegate = self;
            }
            ZFUser *friend = [searchFriendsList objectAtIndex:indexPath.row];
#warning CLARIFY WHEN TO DISPLAY GREEN OR YELLOW DOT
            [cell setupWithUser:friend isLast:(indexPath.row == searchFriendsList.count-1) isCheckIn:(friend.currentVenueId.integerValue == CurrentUser.currentVenueId.integerValue) isInSameZiferblat:NO];
            return cell;
        }
            
        default:
            return nil;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    ZFUser *user;
    switch (tableView.tag) {
        case ZFConversationList_TableViewType_Conversations:
        {
            ZFMessageSummary *message = [conversationsSummaryList objectAtIndex:indexPath.row];
            user = ([message.fromMember isEqual:UserService.loggedInUser] ? message.toMember : message.fromMember);
        }
            break;
            
        case ZFConversationList_TableViewType_Search:
            user = [searchFriendsList objectAtIndex:indexPath.row];
            break;
            
        default:
            break;
    }
    [self.view endEditing:YES];
    ZFConversationDetailViewController *conversationDetailViewController = [[ZFConversationDetailViewController alloc] initWithUser:user];
    [self.navigationController pushViewController:conversationDetailViewController animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (tableView.tag) {
        case ZFConversationList_TableViewType_Conversations:
            return [ZFConversationListCell height];
            
        case ZFConversationList_TableViewType_Search:
            return [ZFFriendsSearchTableViewCell height];
            
        default:
            return 0;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    switch (tableView.tag) {
        case ZFConversationList_TableViewType_Conversations:
            return conversationsSummaryList.count;
            
        case ZFConversationList_TableViewType_Search:
            return searchFriendsList.count;
            
        default:
            return 0;
    }
}

#pragma mark -
#pragma mark Notifications

- (void)conversationsListRefreshed
{
    
    if (!conversationsSummaryList.firstObject) {
        // Attempt to Fix crash [__NSArrayM objectAtIndex:]: index 0 beyond bounds for empty array
        return;
    }
    
    NSArray *refreshedConversations = [NetworkService.messagesSummaryList sortedArrayUsingComparator:^NSComparisonResult(ZFMessageSummary *obj1, ZFMessageSummary *obj2) {
        return [self comparisonResultForMessageSummary1:obj1 messageSummary2:obj2];
    }];
    /*if (![[conversationsSummaryList firstObject] isEqual:[refreshedConversations firstObject]]) {
        [conversationsTableView.tableView beginUpdates];
        NSMutableArray *newIndexPaths = [NSMutableArray new];
        NSUInteger i = 0;
        for(ZFMessageSummary *messageSummary in refreshedConversations) {
            if (![messageSummary isEqual:[conversationsSummaryList objectAtIndex:i]]) {
                [conversationsSummaryList insertObject:messageSummary atIndex:i];
                [newIndexPaths addObject:[NSIndexPath indexPathForItem:i inSection:0]];
            } else {
                break;
            }
            i++;
        }
        [conversationsTableView.tableView insertRowsAtIndexPaths:newIndexPaths withRowAnimation:UITableViewRowAnimationAutomatic];
        [conversationsTableView.tableView endUpdates];
    } else {
        for(ZFMessageSummary *messageSummary in refreshedConversations) {
            for(ZFMessageSummary *existingSummary in conversationsSummaryList) {
                if([existingSummary isEqual:messageSummary]) {
                    existingSummary.text = messageSummary.text;
                    existingSummary.messageRead = messageSummary.messageRead;
                }
            }
        }*/
    
    conversationsSummaryList = (NSMutableArray *)refreshedConversations;
    [conversationsTableView.tableView reloadData];
}

#pragma mark -
#pragma mark ZFFriendsSearchTableViewCell delegate

- (void)friendSearchCellDidRequestToPresentProfile:(ZFUser *)aUser {
    ZFOtherProfileViewController *otherProfileViewController = [[ZFOtherProfileViewController alloc] initWithUser:aUser];
    otherProfileViewController.canBeDismissed = YES;
    [self presentViewController:otherProfileViewController animated:YES completion:nil];
}

#pragma mark -
#pragma mark ZFSearchViewDelegate

- (void)searchViewWillExpand {
    if (!overlayView) {
        overlayView = [UIView viewWithColor:Colour_Black frame:self.view.frame addToView:nil];
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissSearchView)];
        [overlayView addGestureRecognizer:tapGesture];
    }
    
    overlayView.alpha = 0.0;
    [kScrollView addSubview:overlayView];
    [searchView bringToFront];
    [UIView animateWithDuration:0.3 animations:^{
        overlayView.alpha = 0.5;
    } completion:nil];
}

- (void)searchViewWillCompact {
    [UIView animateWithDuration:0.3 animations:^{
        overlayView.alpha = 0.0;
    } completion:^(BOOL finished) {
        [overlayView removeFromSuperview];
    }];
}

- (void)searchViewDidSearchForText:(NSString *)aText {
    if(aText.isValidString) {
        searchFriendsList = [friendsList filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"fullName contains[c] %@", aText]];
    } else {
        searchFriendsList = friendsList;
    }
    [searchView reloadData];
}

#pragma mark - Search View

- (void)dismissSearchView {
    [searchView compact];
}

#pragma mark -
#pragma mark ScrollView

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if([scrollView isEqual:conversationsTableView.tableView]) {
        if(scrollView.contentOffset.y > 0 && !isFetchingNextPage) {
            [self getConversationsSummaryList];
            isFetchingNextPage = YES;
        }
    }
}

#pragma mark -
#pragma mark Request Methods

- (void)getConversationsSummaryList {
    [ZFGetConversationsListRequest requestWithPage:@(currentPage) completion:^(LORESTRequest *aRequest) {
        if(aRequest.resultStatus == LORESTRequestResultStatusSucceeded) {
            ZFPagedResults *pagedResult = aRequest.result;
            NSArray *sortedResults = [pagedResult.result sortedArrayUsingComparator:^NSComparisonResult(ZFMessageSummary *obj1, ZFMessageSummary *obj2) {
                return [self comparisonResultForMessageSummary1:obj1 messageSummary2:obj2];
            }];
            NSMutableIndexSet *indexes = [NSMutableIndexSet new];
            NSMutableArray *indexPaths = [NSMutableArray new];
            for(int i=0; i<sortedResults.count; i++) {
                [indexes addIndex:conversationsSummaryList.count+i];
                [indexPaths addObject:[NSIndexPath indexPathForItem:conversationsSummaryList.count+i inSection:0]];
            }
            if(![[conversationsSummaryList lastObject] isEqual:[sortedResults lastObject]]) {
                [conversationsTableView.tableView beginUpdates];
                [conversationsSummaryList insertObjects:sortedResults atIndexes:indexes];
                [conversationsTableView.tableView insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationAutomatic];
                [conversationsTableView.tableView endUpdates];
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [UIView animateWithDuration:0.3 delay:0.0 options:UIViewAnimationOptionCurveLinear animations:^{
                        [conversationsTableView.tableView scrollToRowAtIndexPath:[indexPaths lastObject] atScrollPosition:UITableViewScrollPositionBottom animated:NO];
                    } completion:^(BOOL finished) {
                        isFetchingNextPage = NO;
                    }];
                });
            } else {
                isFetchingNextPage = NO;
            }
            if(pagedResult.hasNext) {
                currentPage++;
            }
        }
    }];
}
                                     
#pragma mark -
#pragma mark Utils

- (NSComparisonResult)comparisonResultForMessageSummary1:(ZFMessageSummary *)aMessageSummary1 messageSummary2:(ZFMessageSummary *)aMessageSummary2 {
    return [aMessageSummary2.dateSent compare:aMessageSummary1.dateSent];
}

#pragma mark -
#pragma mark Cell Delegate Methods

- (void)showProfile:(ZFUser *)user {
    [SVProgressHUD showWithStatus:NSLocalizedString(@"Loading member...",nil) maskType:SVProgressHUDMaskTypeGradient];
    [ZFGetMemberFriendsListRequest requestWithMemberId:user.uuid andVenueId:CurrentUser.selectedVenue.uuid completion:^(LORESTRequest *aRequest) {
        if (aRequest.resultStatus == LORESTRequestResultStatusSucceeded) {
            user.friendsList = aRequest.result;
            user.friendsLoaded = TRUE;
            [self checkLoaded:user];
        } else {
            Show_ConnectionError
        }
    }];
    
    [ZFGetMemberVenueDetailsRequest requestWithMemberId:user.uuid venueId:CurrentUser.selectedVenue.uuid completion:^(LORESTRequest *aRequest) {
        if (aRequest.resultStatus == LORESTRequestResultStatusSucceeded) {
            user.venueDetails = aRequest.result;
            [self checkLoaded:user];
        } else {
            Show_ConnectionError
        }
    }];
}

- (void)checkLoaded:(ZFUser *)aUser {
    if((aUser.venueDetails == nil) || (!aUser.friendsLoaded)) {
        return;
    }
    
    Hide_HudAndPerformBlock(^{
        if([aUser.uuid isEqualToNumber:CurrentUser.uuid]) {
            ZFProfileViewController *profileViewController = [[ZFProfileViewController alloc] initWithUser:CurrentUser
                                                                                               backMessage:NSLocalizedString(@"BACK TO CONVERSATION", @"Back button title for conversations")
                                                                                                 andFilter:NO
                                                                                                forVenueId:nil];
            profileViewController.canBeDismissed = YES;
            [self presentViewController:profileViewController
                               animated:YES
                             completion:nil];
        } else {
            ZFOtherProfileViewController *otherProfileViewController = [[ZFOtherProfileViewController alloc ] initWithUser:aUser
                                                                                               backMessage:NSLocalizedString(@"BACK TO CONVERSATION", @"Back button title for conversations")
                                                                                                 andFilter:NO
                                                                                                forVenueId:nil];
            otherProfileViewController.canBeDismissed = YES;
            [self presentViewController:otherProfileViewController
                               animated:YES
                             completion:nil];
        }
    });
}

@end
