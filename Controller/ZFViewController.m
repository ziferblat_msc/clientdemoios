//
//  ZFViewController.m
//  Ziferblat
//
//  Created by Boris Yurkevich on 24/12/2015.
//  Copyright © 2015 Locassa. All rights reserved.
//

#import "ZFViewController.h"

@implementation ZFViewController

- (void)backButtonTapped {
	if (_canBeDismissed) {
		[self dismissViewControllerAnimated:YES completion:nil];
	} else {
		[self.navigationController popViewControllerAnimated:YES];
	}
}

@end
