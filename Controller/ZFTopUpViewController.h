//
//  ZFPiastresTopUpViewController.h
//  Ziferblat
//
//  Created by Terry Michel on 08/04/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "ZFTopUpType.h"
#import "ZFKeyboard.h"
#import "ZFTreasuryZiferblatSelector.h"
#import "PayPalMobile.h"

@class ZFButton;
@class ZFBaseView;

@interface ZFTopUpViewController : UIViewController<UIScrollViewDelegate, ZFKeyboardDelegate, ZFTreasuryZiferblatSelectorDelegate,
PayPalPaymentDelegate> {
    ZFTopUpType type;
    BOOL shouldPopWithDirection;
    ZFVenue *selectedVenue;
    NSArray *venues;
    ZFBaseView *contentView;
    UIButton *piastresButton;
    UIButton *timeButton;
    ZFKeyboard *keyboard;
    NSNumber *amount;
    UILabel *moneyToSpendLabel;
    ZFButton *yesButton;
    UILabel *piastresLabel;
    UIButton *subscriptionButton;
    UIScrollView *venuesScrollView;
    ZFTreasuryZiferblatSelector *ziferblatSelector;
    PayPalConfiguration *payPalConfiguration;
    
    UILabel *backLabel;
    UILabel *headerLabel;
    UILabel *timeLabel;
    UILabel *youHaveLabel;
    UILabel *addLabel;
    UILabel *getMoreLabel;
    UILabel *autorefillLabel;
    UILabel *iWantLabel;
    UILabel *subscriptionLabel;
}

#pragma mark - 
#pragma mark Instance

@property BOOL presentedFromNotification;

- (instancetype)initWithTopUpType:(ZFTopUpType)aType venuesList:(NSArray *)venues;

@end
