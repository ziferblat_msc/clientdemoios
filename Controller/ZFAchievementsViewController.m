//
//  ZFAchievementsViewController.m
//  Ziferblat
//
//  Created by Terry Michel on 08/04/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "ZFAchievementsViewController.h"
#import "ZFAchievementsInfoViewController.h"
#import "ZFLeaderBoardsViewController.h"
#import "ZFExperiencePointsViewController.h"
#import "ZFActivitiesViewController.h"
#import "LOSeparatorImageView.h"
#import "ZFBackButton.h"

#define kAchievementsLabelFrame         (notIPad ? CGRectMake(81, 113, 156, 19):CGRectMake(194, 284, 374, 46))
#define kLeaderboardsButtonFrame        (notIPad ? CGRectMake(83, 160, 156, 36):CGRectMake(199, 384, 374, 86))
#define kScoreButtonFrame               (notIPad ? CGRectMake(40, 188, 103, 36):CGRectMake(96, 451, 247, 86))
#define kActivitiesButtonFrame          (notIPad ? CGRectMake(179, 188, 103, 36):CGRectMake(430, 451, 247, 86))
#define kAchievementsNumberLabelFrame   (notIPad ? CGRectMake(128.0, 61.0, 63.0,34.0):CGRectMake(307, 160, 151, 82))
#define kExperiencesPointsLabelFrame    (notIPad ? CGRectMake(77.0, 186.0, 50.0, 22.0):CGRectMake(185, 459, 120, 53))
#define kNumberOfTasksLabelFrame        (notIPad ? CGRectMake(232.0, 186.0, 50.0, 22.0):CGRectMake(557, 459, 120, 53))
#define kLeaderboardPositionLabelFrame  (notIPad ? CGRectMake(109.0, 153.0, 117.0, 20.0):CGRectMake(262, 378, 281, 48))
#define kAchievementsTableViewFrame     (notIPad ? CGRectMake(0.0, 223.0, 320.0, 355.0):CGRectMake(0.0, 535, 768, 852))
#define kProfilePictureFrame            (notIPad ? CGRectMake(81.0, 152.5, 22.0, 22.0):CGRectMake(194, 377, 56, 56))

@interface ZFAchievementsViewController ()

@property (nonatomic, strong) UIImageView *footerImageView;

@end

@implementation ZFAchievementsViewController

#pragma mark -
#pragma mark Lifecycle

- (instancetype)initWithUser:(ZFUser *)aUser
{
    self = [super init];
    if(self)
    {
        user = aUser;
        backButtonMessage = NSLocalizedString(@"BACK TO ROOM", nil);
    }
    return self;
}

- (instancetype)initWithUser:(ZFUser *)aUser backMessage:(NSString *)backMessage
{
    if (self = [self initWithUser:aUser])
    {
        backButtonMessage = backMessage;
    }
    return self;
}

- (void)loadView {
    ZFBaseView *aView = [[ZFBaseView alloc] init];
    self.view = aView;
    
    
    UIImageView *topImageView = [UIImageView imageViewWithImageNamed:RESOURCE_ACHIEVEMENT_HEADER
                                                              origin:CGPointMake(0, 0)
                                                           addToView:kScrollView];
    
    ZFBackButton *backButton = [ZFBackButton backButtonWithTitle:NSLocalizedString(backButtonMessage, nil)];
    [backButton setExclusiveTouch:YES];
    [self.view addSubview:backButton];
    topImageView.top = backButton.bottom;
    TARGET_TOUCH_UP(backButton, @selector(backButtonTapped))
    
    UILabel *youHaveLabel = [[UILabel alloc]initWithFrame:(notIPad ? CGRectMake(96, backButton.bottom, 130, 22):CGRectMake(230, backButton.bottom + 20, 312, 53))];
    if(user.uuid.intValue == CurrentUser.uuid.intValue)
    {
        youHaveLabel.text = NSLocalizedString(@"YOU HAVE",nil);
    }
    else
    {
        youHaveLabel.text = [NSString stringWithFormat:NSLocalizedString(@"%@ HAS",nil), [user.firstName uppercaseString]];
    }
    youHaveLabel.textAlignment = NSTextAlignmentCenter;
    youHaveLabel.font = FontRockWell(notIPad ? 16:38);
    youHaveLabel.adjustsFontSizeToFitWidth = YES;
    youHaveLabel.minimumScaleFactor = 0.5;
    [kScrollView addSubview:youHaveLabel];
    
    achievementsLabel = [[UILabel alloc]initWithFrame:kAchievementsLabelFrame];
    achievementsLabel.text = NSLocalizedString(@"ACHIEVEMENTS",nil);
    achievementsLabel.textColor = Colour_White;
    achievementsLabel.textAlignment = NSTextAlignmentCenter;
    achievementsLabel.font = FontRockWell(notIPad ? 14:34);
    achievementsLabel.adjustsFontSizeToFitWidth = YES;
    achievementsLabel.minimumScaleFactor = 0.5;
    [kScrollView addSubview:achievementsLabel];
    
    UIButton *leaderboardsButton = [[UIButton alloc]initWithFrame:kLeaderboardsButtonFrame];
    [leaderboardsButton setExclusiveTouch:YES];
    [kScrollView addSubview:leaderboardsButton];
    TARGET_TOUCH_UP(leaderboardsButton, @selector(leaderboardsButtonTapped))
    
    UIButton *scoreButton = [[UIButton alloc]initWithFrame:kScoreButtonFrame];
    [scoreButton setShowsTouchWhenHighlighted:NO];
    [kScrollView addSubview:scoreButton];
    TARGET_TOUCH_UP(scoreButton, @selector(scoreButtonTapped))
    
    UIButton *activitiesButton = [[UIButton alloc]initWithFrame:kActivitiesButtonFrame];
    [activitiesButton setExclusiveTouch:YES];
    [kScrollView addSubview:activitiesButton];
    TARGET_TOUCH_UP(activitiesButton, @selector(activitiesButtonTapped))
    
    achievementsNumberLabel = [[UILabel alloc]initWithFrame:kAchievementsNumberLabelFrame];
    [achievementsNumberLabel setFont:FontRockWell(notIPad ? 42:100)];
    achievementsNumberLabel.adjustsFontSizeToFitWidth = YES;
    [achievementsNumberLabel setTextColor:Colour_Black];
    [achievementsNumberLabel setTextAlignment:NSTextAlignmentCenter];
    [kScrollView addSubview:achievementsNumberLabel];
    
    experiencePointsLabel = [[UILabel alloc]initWithFrame:kExperiencesPointsLabelFrame];
    [experiencePointsLabel setFont:FontRockWell(notIPad ? 16:38)];
    [experiencePointsLabel setTextColor:Colour_Black];
    [experiencePointsLabel setTextAlignment:NSTextAlignmentLeft];
    [kScrollView addSubview:experiencePointsLabel];
    
    numberOfTasksLabel = [[UILabel alloc]initWithFrame:kNumberOfTasksLabelFrame];
    [numberOfTasksLabel setFont:FontRockWell(notIPad ? 16:38)];
    numberOfTasksLabel.text = @"0";
    [numberOfTasksLabel setTextColor:Colour_Black];
    [numberOfTasksLabel setTextAlignment:NSTextAlignmentLeft];
    [kScrollView addSubview:numberOfTasksLabel];
    
    UILabel *leaderboardPositionLabel = [[UILabel alloc]initWithFrame:kLeaderboardPositionLabelFrame];
    [leaderboardPositionLabel setText:[NSString stringWithFormat:NSLocalizedString(@"#%@ on leaderboard",nil),[user.venueDetails.rank stringValue]]];
    [leaderboardPositionLabel setFont:FontRockWell(notIPad ? 11:26)];
    [leaderboardPositionLabel setTextColor:Colour_Black];
    [leaderboardPositionLabel setTextAlignment:NSTextAlignmentCenter];
    [kScrollView addSubview:leaderboardPositionLabel];
    
    achievementsTableView = [[UITableView alloc] initWithFrame:kAchievementsTableViewFrame];
    [kScrollView addSubview:achievementsTableView];
    achievementsTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    achievementsTableView.bounces = NO;
    achievementsTableView.backgroundColor = Colour_Clear;
    achievementsTableView.delegate = self;
    achievementsTableView.dataSource = self;
    [achievementsTableView registerClass:[ZFAchievementsCell class] forCellReuseIdentifier:NSStringFromClass([ZFAchievementsCell class])];
    
    
    UIImageView *profilePictureImageView = [[UIImageView alloc]initWithFrame:kProfilePictureFrame];
    NSString *url = user.imageUrl;
    NSURL *imageURL = [NSURL URLWithString:url];
    NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
    UIImage *profileImage = [UIImage imageWithData:imageData];
    [profilePictureImageView setImage:profileImage];
    profilePictureImageView.layer.cornerRadius = (notIPad ? 11:26);
    profilePictureImageView.clipsToBounds = YES;
    [kScrollView addSubview:profilePictureImageView];
    
    self.footerImageView = [UIImageView imageViewWithImageNamed:RESOURCE_ACHIEVEMENT_FOOTER
                                                                 origin:CGPointMake(0, achievementsTableView.bottom + kMediumPadding)
                                                              addToView:kScrollView];
    
    kScrollView.contentSize = CGSizeMake(self.view.width, self.footerImageView.bottom + (notIPad ? 10:24));
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self updateData];
}

- (void)updateAchievementTableHeight
{
    achievementsTableView.height = achievementsTableView.contentSize.height;
    self.footerImageView.top = achievementsTableView.bottom + kMediumPadding;
    kScrollView.contentSize = CGSizeMake(self.view.width, self.footerImageView.bottom + (notIPad ? 10:24));
}

- (void)updateData {
    achievementArray = user.venueDetails.achievements;
    achievementsNumberLabel.text  = [NSString stringWithFormat:@"%lu", (unsigned long)achievementArray.count];
    
    if(achievementArray.count == 1)
    {
        achievementsLabel.text = NSLocalizedString(@"ACHIEVEMENT",nil);
    }
    else
    {
        if(user.uuid.intValue == CurrentUser.uuid.intValue)
        {
            achievementsLabel.text = NSLocalizedString(@"ACHIEVEMENTS",nil);
        }
        else
        {
            achievementsLabel.text = NSLocalizedString(@"SEVERAL ACHIEVEMENTS",nil);
        }
    }
    
    experiencePointsLabel.text = [user.venueDetails.experiencePoints stringValue];
    numberOfTasksLabel.text = [user.venueDetails.tasks stringValue];
    [achievementsTableView reloadData];
    [self updateAchievementTableHeight];
    [achievementsNumberLabel setNeedsDisplay];
}

- (void)leaderboardsButtonTapped {
    ZFLeaderBoardsViewController *leaderboardsViewController = [[ZFLeaderBoardsViewController alloc] initWithTitle:NSLocalizedString(@"BACK TO ACHIEVEMENTS", @"Leaderboards opened from history of time")
                                                                                                           venueId:user.selectedVenue.uuid];
    [self presentViewController:leaderboardsViewController animated:YES completion:nil];
}

- (void)scoreButtonTapped {
    ZFExperiencePointsViewController *scoreViewController = [[ZFExperiencePointsViewController alloc] init];
    scoreViewController.canBeDismissed = YES;
    [self presentViewController:scoreViewController animated:YES completion:nil];
}

- (void)activitiesButtonTapped {
    ZFActivitiesViewController *activitiesViewController = [ZFActivitiesViewController new];
    activitiesViewController.canBeDismissed = YES;
    [self presentViewController:activitiesViewController animated:YES completion:nil];
}

#pragma mark -
#pragma mark UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return achievementArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [ZFAchievementsCell height];
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *identifier = NSStringFromClass([ZFAchievementsCell class]);
    ZFAchievementsCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    cell.delegate = self;
    
    ZFAchievementLevel *achievementLevel = [achievementArray objectAtIndex:indexPath.row];
    
    if (indexPath.row % 2 == 0) {
        [cell configureWithAchievementLevel:achievementLevel position:tableViewPositioneven];
    } else {
        [cell configureWithAchievementLevel:achievementLevel position:tableViewPositionOdd];
        
    }
    
    if(indexPath.row != achievementArray.count-1){
        LOSeparatorImageView *separator = [LOSeparatorImageView separatorWithLeft:0
                                                                              top:[ZFAchievementsCell height] - 1
                                                                            width:cell.width
                                                                      addedToView:cell
                                                                        imageName:RESOURCE_ACHIEVEMENT_DIVIDER];
        separator.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    }
    
    return cell;
}

#pragma mark -
#pragma mark Achievement Cell Delegate methods

- (void)achievementCellShouldShowDetail:(ZFAchievementsCell *)aCell {
    NSUInteger index = [achievementsTableView indexPathForCell:aCell].row;
    ZFAchievementLevel *achievementSelected = [achievementArray objectAtIndex:index];
    NSInteger integerToSend = [achievementSelected.achievement.uuid integerValue];
    ZFAchievementsInfoViewController *achievementsListController = [[ZFAchievementsInfoViewController alloc] init];
    achievementsListController.selectedAchievementIdentifier = integerToSend;
    [self presentViewController:achievementsListController animated:YES completion:nil];
}

@end


