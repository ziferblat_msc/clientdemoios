//
//  ZFSplashScreenViewController.m
//  Ziferblat
//
//  Created by Terry Michel on 08/04/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "ZFSplashScreenViewController.h"
#import "ZFLoginViewController.h"
#import "ZFHomeViewController.h"
#import "ZFLoginRequest.h"
#import "LORESTRequestService.h"
#import "ZFGetAccountBalanceRequest.h"

@implementation ZFSplashScreenViewController

#pragma mark -
#pragma mark Lifecycle

- (void)loadView {
    contentView = [[ZFSplashScreenView alloc] init];
    self.view = contentView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (![[NSUserDefaults standardUserDefaults] boolForKey:kTutorialPresented]) {
        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Tutorial" bundle:nil];
        ZFTutorialViewController *tutorialVC = (ZFTutorialViewController *)[sb instantiateViewControllerWithIdentifier:@"TutorialViewController"];
        tutorialVC.delegate = self;
        
        [self presentViewController:tutorialVC animated:YES completion:nil];
        
    } else {
        [self startLogin];
    }
}

#pragma mark
#pragma mark ZFTutorialViewControllerDelegate

-(void)startLogin {
    
//    self.view = contentView;
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        ZFUser *previousUser = UserService.previousUser;
        
        [self tryLoginWithEmail:previousUser.email password:previousUser.password];
    });
    [contentView showActivity];
}

#pragma mark -
#pragma mark Login

- (void)tryLoginWithEmail:(NSString *)email password:(NSString *)password {
    NSMutableArray *controllers = @[[ZFLoginViewController new]].mutableCopy;
    if (email.isValidEmailAddress && password) {
        [ZFLoginRequest requestWithUsername:email
                                   password:password
                                 completion:^(LORESTRequest *aRequest) {
                                     ZFUser *user = aRequest.result;
                                     user.password = password;
                                     [UserService setLoggedInUser:aRequest.result];
                                     if(aRequest.resultStatus == LORESTRequestResultStatusSucceeded) {
                                         [UserDefaults setValue:[NSDate date] forKey:kLastContentUpdateDateKey];
                                         [ZFGetAccountBalanceRequest requestWithId:CurrentUser.uuid
                                                                        completion:^(LORESTRequest *aRequest) {
                                                                            
                                                                            if(aRequest.resultStatus == LORESTRequestResultStatusSucceeded) {
                                                                                CurrentUser.accountBalance = aRequest.result;
                                                                                Notification_Post(kNotification_AccountBalanceRefreshed);
                                                                                [controllers addObject:[ZFHomeViewController new]];
                                                                            }
                                                                            [self setViewControllersAnimated:controllers];
                                                                        }];
                                     } else {
                                         [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"Login details incorrect.",nil)];
                                         [self setViewControllersAnimated:controllers];
                                     }
                                 }];
    } else {
        [self setViewControllersAnimated:controllers];
    }
}

#pragma mark -
#pragma mark Util

- (void)setViewControllersAnimated:(NSArray *)viewControllers {
    [UIView transitionWithView:self.navigationController.view
                      duration:0.75
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
                        [self.navigationController setViewControllers:viewControllers];
                    } completion:nil];
}

@end
