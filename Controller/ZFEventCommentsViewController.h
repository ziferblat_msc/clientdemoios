//
//  ZFEventCommentsViewController.h
//  Ziferblat
//
//  Created by Jose Fernandez on 26/08/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZFEventViewController.h"

@interface ZFEventCommentsViewController : UIViewController
@property (nonatomic, strong) ZFEvents *event;
@property (nonatomic, strong) NSNumber *venueId;
@property (nonatomic, strong) NSLayoutConstraint *containerHeightConstraint;
@property (nonatomic, strong) NSString *replyCommentId;

@property (weak, nonatomic) ZFEventViewController *parent;
@end
