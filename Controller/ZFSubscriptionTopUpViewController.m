//
//  ZFSubscriptionTopUpViewController.m
//  Ziferblat
//
//  Created by Terry Michel on 08/04/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "ZFSubscriptionTopUpViewController.h"
#import "ZFSubscriptionSelectionViewController.h"
#import "ZFBaseView.h"
#import "ZFButton.h"
#import "ZFVenue.h"
#import "ZFLocalisedImageDefinitions.h"
#import "ZFBackButton.h"

@implementation ZFSubscriptionTopUpViewController

#define kPiastresTop     (notIPad ? 120:288)

#pragma mark - 
#pragma mark Lifecycle

- (void)loadView {
    contentView = [[ZFBaseView alloc] init];
    self.view = contentView;
    UIImage *image = [UIImage imageNamed:@"ZFSubscriptionTopUpView"];
    contentView.scrollView.contentSize = CGSizeMake(image.size.width, image.size.height);
    contentView.backgroundImageView.frame = CGRectMake(0, (notIPad ? 0:-7), image.size.width, image.size.height);
    contentView.backgroundImageView.image = image;
    
    ZFBackButton *backButton = [ZFBackButton backButtonWithTitle:NSLocalizedString(@"BACK TO TOP-UP",nil)];
    [kScrollView addSubview:backButton];
    TARGET_TOUCH_UP(backButton, @selector(backButtonPressed));
    
    UILabel *titlLabel = [[UILabel alloc] init];
    [titlLabel setText:NSLocalizedString(@"SUBSCRIPTIONS",nil)];
    [titlLabel setFont:FontRockWell(notIPad ? 22 : 53)];
    [titlLabel setTextAlignment:NSTextAlignmentCenter];
    titlLabel.frame = (notIPad ? CGRectMake(46, 40, 223, 28):CGRectMake(110, 83, 535, 67));
    [contentView.scrollView addSubview:titlLabel];
    
    UILabel *youHaveLabel = [[UILabel alloc] init];
    [youHaveLabel setText:NSLocalizedString(@"YOU HAVE",nil)];
    [youHaveLabel setFont:FontRockWell(notIPad ? 18 : 43)];
    [youHaveLabel setTextAlignment:NSTextAlignmentCenter];
    youHaveLabel.frame = (notIPad ? CGRectMake(92, 91, 138, 28):CGRectMake(221, 205, 331, 67));
    [contentView.scrollView addSubview:youHaveLabel];
    
    piastresButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    ZFLocalisedImageDefinitions *imageDefinitions = [ZFLocalisedImageDefinitions sharedInstance];
    [piastresButton setImage:imageDefinitions.resourceSharePiastres selectedImage:imageDefinitions.resourceSharePiastresSelected];
    
    piastresButton.left = kLargePadding;
    piastresButton.top = kPiastresTop;
    piastresButton.userInteractionEnabled = NO;
    [kScrollView addSubview:piastresButton];
    
    piastresLabel = [[UILabel alloc] initWithFrame:(notIPad ? CGRectMake(46, 136, 100, 20):CGRectMake(110, 326, 240, 48))];
    piastresLabel.textAlignment = NSTextAlignmentCenter;
    piastresLabel.font = FontHelvetica(notIPad ? 16 : 38);
    piastresLabel.textColor = Colour_Black;
    piastresLabel.adjustsFontSizeToFitWidth = YES;
    [kScrollView addSubview:piastresLabel];

    timeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [timeButton setImage:imageDefinitions.resourceShareTime selectedImage:imageDefinitions.resourceShareTimeSelected];
    timeButton.right = self.view.width - kLargePadding;
    timeButton.top = piastresButton.top - (notIPad ? 3 : 7.2);
    timeButton.userInteractionEnabled = NO;
    [kScrollView addSubview:timeButton];
    
    CGFloat timeLabelSize = (notIPad ? 48 : 115);
    timeLabel = [[UILabel alloc] initWithFrame:(notIPad ? CGRectMake(217, 144, timeLabelSize, timeLabelSize):CGRectMake(521, 346, timeLabelSize, timeLabelSize))];
    timeLabel.textAlignment = NSTextAlignmentCenter;
    timeLabel.font = FontLobster(notIPad ? 22 : 53);
    timeLabel.textColor = Colour_Black;
    timeLabel.adjustsFontSizeToFitWidth = YES;
    timeLabel.layer.cornerRadius = timeLabel.height / 2;
    timeLabel.clipsToBounds = YES;
    [kScrollView addSubview:timeLabel];
    
    ZFButton *unlimitedButton = [ZFButton buttonWithTitle:NSLocalizedString(@" WANNA ",nil)];
    unlimitedButton.left = kLargePadding + (notIPad ? 10:104);
    [contentView.scrollView addSubview:unlimitedButton];
    TARGET_TOUCH_UP(unlimitedButton, @selector(unlimitedButtonPressed));
    
    unlimitedButton.enabled = NO;
    
    if([CurrentUser.accountBalance respondsToSelector:@selector(hasSubscription)])
    {
        unlimitedButton.enabled = !CurrentUser.accountBalance.hasSubscription;
    }
    
    ZFButton *workingButton = [ZFButton buttonWithTitle:NSLocalizedString(@" NEEDA ",nil)];
    workingButton.right = contentView.scrollView.width - (kLargePadding+(notIPad ? 10:114));
    [contentView.scrollView addSubview:workingButton];
    TARGET_TOUCH_UP(workingButton, @selector(workingButtonPressed));
    workingButton.enabled = unlimitedButton.isEnabled;
    
    UILabel *unlimitedPassTitleLabel = [[UILabel alloc] init];
    [unlimitedPassTitleLabel setText:NSLocalizedString(@"UNLIMITED\nPASS",nil)];
    [unlimitedPassTitleLabel setFont:FontRockWell(notIPad ? 14:34)];
    [unlimitedPassTitleLabel setTextColor:Colour_Black];
    [unlimitedPassTitleLabel setTextAlignment:NSTextAlignmentCenter];
    unlimitedPassTitleLabel.numberOfLines = 0;
    [unlimitedPassTitleLabel sizeToFit];
    unlimitedPassTitleLabel.top = (notIPad ? 252:605);
    [contentView.scrollView addSubview:unlimitedPassTitleLabel];
    [unlimitedPassTitleLabel setCenterX:unlimitedButton.center.x];
    
    UILabel *unlimitedPassDescriptionLabel = [[UILabel alloc] init];
    [unlimitedPassDescriptionLabel setText:NSLocalizedString(@"This is so amazingly cute to be able to come at any time and "
                                                             "not to think about at all///This is so ******* cute to be able to come at any time", @"unlimitedPassDescriptionLabel")];
    unlimitedPassDescriptionLabel.frame = (notIPad ? CGRectMake(workingButton.left, 423, workingButton.width, 0):CGRectMake(workingButton.left - 80, 1015, workingButton.width + 160, 0));
    unlimitedPassDescriptionLabel.numberOfLines = 0;
    [unlimitedPassDescriptionLabel setFont:FontRockWell(notIPad ? 10:24)];
    [unlimitedPassDescriptionLabel sizeToWidthIsAttributed:NO];
    [contentView.scrollView addSubview:unlimitedPassDescriptionLabel];
    
    UILabel *workingPassTitleLabel = [[UILabel alloc] init];
    [workingPassTitleLabel setText:NSLocalizedString(@"WORKING\nPASS",nil)];
    workingPassTitleLabel.numberOfLines = 0;
    [workingPassTitleLabel setTextAlignment:NSTextAlignmentCenter];
    [workingPassTitleLabel setFont:unlimitedPassTitleLabel.font];
    [workingPassTitleLabel sizeToFit];
    workingPassTitleLabel.top = unlimitedPassTitleLabel.top;
    [contentView.scrollView addSubview:workingPassTitleLabel];
    [workingPassTitleLabel setCenterX:workingButton.center.x];
    
    UILabel *workingPassDescriptionLabel = [[UILabel alloc] init];
    [workingPassDescriptionLabel setText:NSLocalizedString(@"Like to work early in the morning. "
                                                           "Fresh coffee, nothing to distract, jazz Like to work early in the morning. "
                                                           "Fresh coffee, nothing to distract, jazz", @"Full subscription description - workingPassDescriptionLabel")];
    workingPassDescriptionLabel.frame = (notIPad ? CGRectMake(unlimitedButton.left, unlimitedPassDescriptionLabel.top, unlimitedButton.width, 0):CGRectMake(unlimitedButton.left - 80, unlimitedPassDescriptionLabel.top, unlimitedButton.width + 160, 0));
    workingPassDescriptionLabel.numberOfLines = 0;
    [workingPassDescriptionLabel setFont:unlimitedPassDescriptionLabel.font];
    [workingPassDescriptionLabel sizeToWidthIsAttributed:NO];
    [contentView.scrollView addSubview:workingPassDescriptionLabel];
    
    CGFloat buttonTop = (unlimitedPassDescriptionLabel.bottom > workingPassDescriptionLabel.bottom ?
                         unlimitedPassDescriptionLabel.bottom + (notIPad ? 10:24) : workingPassDescriptionLabel.bottom + (notIPad ? 10:24));
    unlimitedButton.top = buttonTop;
    workingButton.top = buttonTop;
    
    [kScrollView setContentSize:CGSizeMake(kScrollView.width, workingButton.bottom + kLargePadding)];
    [self updateBalance];

    [NetworkService pollForType:ZFPollingRequestType_AccountBalance memberId:CurrentUser.uuid venueId:nil summary:YES];
    Notification_Observe(kNotification_AccountBalanceRefreshed, updateBalance)
}

- (void)dealloc {
    Notification_RemoveObserver
}

#pragma mark - 
#pragma mark Buttons actions

- (void)backButtonPressed {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)unlimitedButtonPressed {
    [self pushSubscriptionSelectionViewControllerForType:ZFSubscriptionType_Unlimited];
}

- (void)workingButtonPressed {
    [self pushSubscriptionSelectionViewControllerForType:ZFSubscriptionType_Working];
}

#pragma mark - 
#pragma mark Utils

- (void)updateBalance
{
    NSUInteger piastresToDisplay = 0;
    
    if([CurrentUser.accountBalance respondsToSelector:@selector(piastres)])
    {
        piastresToDisplay = CurrentUser.accountBalance.piastres.integerValue;
    }
    
    NSString *piastresTitle = [NSString stringWithFormat:@"%ld", (long)piastresToDisplay];
    piastresLabel.text = piastresTitle;
    
    NSUInteger minutes = 0;
    
    if([CurrentUser.accountBalance respondsToSelector:@selector(baseMinutes)])
    {
        minutes = roundf(CurrentUser.accountBalance.baseMinutes.floatValue * CurrentUser.selectedVenue.baseMinutesFactor.floatValue);
    }
    
    NSString *minutesTitle = [NSString stringWithFormat:@"%ld",(unsigned long)minutes];
    timeLabel.text = minutesTitle;
}

- (void)pushSubscriptionSelectionViewControllerForType:(ZFSubscriptionType)aType {
    ZFSubscriptionSelectionViewController *subscriptionSelectionViewController = [[ZFSubscriptionSelectionViewController alloc] initWithType:aType];
    [self.navigationController pushViewController:subscriptionSelectionViewController animated:YES];
}

@end
