//
//  ZFPasswordEmailViewController.h
//  Ziferblat
//
//  Created by Jose Fernandez on 30/07/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZFPasswordEmailViewController : UIViewController
@property (nonatomic, strong) NSString *email;

//Constraints
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topNavigationControlHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *ResetPasswordLabelYConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *ResetPasswordLabelWidthConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *ResetPasswordLabelHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *instructionsLabelYConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *instructionsLabelWidthConstraint;


@end
