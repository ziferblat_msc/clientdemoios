//
//  ZFNewsViewController.m
//  Ziferblat
//
//  Created by Terry Michel on 08/04/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "ZFNewsViewController.h"
#import "ZFNewsItemCell.h"
#import "ZFNews.h"
#import "ZFNewsView.h"
#import "ZFNewsArticleViewController.h"
#import "ZFBackButton.h"
#import "ZFGetVenueNewsRequest.h"
#import "LOImageCache.h"
#import "LOImageDownloader.h"

static CGFloat const ZFScrollViewLoadModeThreshold = 100;
static NSInteger const ZFFetchSize = 5;

@interface ZFNewsViewController () {
    NSNumber *venueId;
}

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *newsItems;
@property (nonatomic) NSInteger currentPage;
@property (nonatomic) BOOL isTableViewOffset;
@property (nonatomic) BOOL hasFetchedAllNews;

@end

@implementation ZFNewsViewController

#define kHeaderLabelFrame       CGRectMake(83, 53, 154, 28)

#pragma mark - 
#pragma mark Lifecycle

- (ZFNewsViewController *)initWithVenueId:(NSNumber *)aVenueId {
    self = [super init];
    if (self) {
        venueId = aVenueId;
        _newsItems = [NSMutableArray new];
        _hasFetchedAllNews = NO;
        _isTableViewOffset = NO;
    }
    return self;
}

- (void)loadView {
    contentView = [[ZFNewsView alloc] init];
    self.view = contentView;
    self.tableView = contentView.tableView.tableView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.tableView registerNib:[UINib nibWithNibName:[ZFNewsItemCell className] bundle:nil]
         forCellReuseIdentifier:[ZFNewsItemCell reuseIdentifier]];
    contentView.scrollView.delegate = self;
    TARGET_TOUCH_UP(contentView.backButton, @selector(backButtonTapped));
    [contentView.tableView setController:self];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    _currentPage = 0;
    [self.newsItems removeAllObjects];
    [self fetchNewsItems];
}

#pragma mark - Requests

- (void)fetchNewsItems {
    Show_Hud(nil);
    [ZFGetVenueNewsRequest requestForNewsItemsAtVenueId:venueId page:self.currentPage fetchSize:ZFFetchSize completion:^(LORESTRequest *aRequest) {
        Hide_HudAndPerformBlock(^{
            if (aRequest.resultStatus == LORESTRequestResultStatusSucceeded) {
                if ([aRequest.result isKindOfClass:[ZFPagedResults class]]) {
                    ZFPagedResults *pagedResults = aRequest.result;
                    if ([pagedResults.result isKindOfClass:[NSArray class]]) {
                        NSArray *newsArray = (NSArray *)pagedResults.result;
                        self.hasFetchedAllNews = ([newsArray count] != ZFFetchSize);
                        [self.newsItems addObjectsFromArray:newsArray];
                        [contentView updateLayoutForNumberOfRows:[self.newsItems count] withRowHeight:[ZFNewsItemCell cellHeight]];
                        
                        [self reloadData];
                    } else {
                        // Fail gracefully
                        Show_Error(NSLocalizedString(@"Error parsing news", nil));
                    }
                } else {
                    // Fail gracefully
                    Show_Error(NSLocalizedString(@"Error parsing news", nil));
                }
            } else {
                Show_Error(NSLocalizedString(@"Error occurred while fetching news", nil));
            }
        });
    }];
}

#pragma mark - Helpers

- (void)reloadData {
    [self.tableView reloadData];
}

#pragma mark - 
#pragma mark UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.newsItems count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ZFNewsItemCell *cell = [tableView dequeueReusableCellWithIdentifier:[ZFNewsItemCell reuseIdentifier]];
    if (self.newsItems.count > 0) {
        ZFNews *news = [self.newsItems objectAtIndex:indexPath.row];
        [cell setupWithNews:news];
    }
    return cell;
}

#pragma mark - 
#pragma mark UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [ZFNewsItemCell cellHeight];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    ZFNews *news = [self.newsItems objectAtIndex:indexPath.row];
    ZFNewsArticleViewController *newsArticleViewController = [[ZFNewsArticleViewController alloc] initWithNibName:[ZFNewsArticleViewController className]
                                                                                                           bundle:nil];
    newsArticleViewController.selectedNews = news;
    newsArticleViewController.venueId = venueId;
    [self.navigationController pushViewController:newsArticleViewController
                                         animated:YES];
}


# pragma mark -
# pragma mark UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    // if scrolled to within 100 pixels of the bottom of the content
    // set flag to cause next page of notifications to be loaded.
    CGFloat contentOffsetBottom = scrollView.contentOffset.y + scrollView.contentInset.top + scrollView.frame.size.height;
    CGFloat contentHeight = scrollView.contentSize.height + scrollView.contentInset.top + scrollView.contentInset.bottom;
    
    self.isTableViewOffset = (contentOffsetBottom > contentHeight - ZFScrollViewLoadModeThreshold);
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    if (self.isTableViewOffset && !self.hasFetchedAllNews) {
        self.currentPage++;
        [self fetchNewsItems];
    }
}

@end
