//
//  ZFTreasuryViewController.m
//  Ziferblat
//
//  Created by Terry Michel on 08/04/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "ZFTreasuryViewController.h"
#import "ZFShareViewController.h"
#import "ZFTopUpViewController.h"
#import "ZFSetupAutopaymentViewController.h"
#import "ZFTopUpViewController.h"
#import "ZFTimeHistoryViewController.h"
#import "ZFSubscriptionTopUpViewController.h"
#import "ZFHelpViewController.h"
#import "ZFBalanceHistoryViewController.h"
#import "ZFGetAccountBalanceRequest.h"
#import "ZFGetSubscriptionForVenueRequest.h"
#import "ZFVenue.h"
#import "ZFGuestService.h"

@interface ZFTreasuryViewController()
@property BOOL paypalConfigured; // API key and secret in admin app
@end

@implementation ZFTreasuryViewController

#pragma mark - Lifecycle

- (instancetype)initWithSelectedVenue:(ZFVenue *)aVenue
                        andVenuesList:(NSArray *)venuesList
                          backMessage:(NSString *)message {
    self = [super init];
    if(self) {
        selectedVenue = aVenue;
        // Some view controllers which this class creates are initialised with the venues array. These view controllers poll will
        // poll for venues which results in a refreshed venues array sorted by it's default sort order (by venue ID). There is a
        // inconsistency unless we sort the venues list here by the same sort order.
        venues = [venuesList sortedArrayUsingComparator:^NSComparisonResult(ZFVenue* _Nonnull venue1, ZFVenue*  _Nonnull venue2) {
            return [venue1.uuid compare:venue2.uuid];
        }];
        backMessage = message;
    }
    return self;
}

- (void)loadView {
    contentView = [[ZFTreasuryView alloc] initWithBackMessage:backMessage];
    self.view = contentView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSString *venueIdentifier = selectedVenue.internalIdentifier;
    if (venueIdentifier && venueIdentifier.length > 0) {
        self.paypalConfigured = YES;
    } else {
        self.paypalConfigured = NO;
    }
    
    TARGET_TOUCH_UP(contentView.backButton, @selector(backButtonTapped));
    TARGET_TOUCH_UP(contentView.getPiastresButton, @selector(getPiastresButtonTapped));
    TARGET_TOUCH_UP(contentView.givePiastresButton, @selector(givePiastresButtonTapped));
    TARGET_TOUCH_UP(contentView.autoRefillButton, @selector(autorefillButtonTapped));
    TARGET_TOUCH_UP(contentView.ziferblatSelector, @selector(switchZiferblatButtonTapped:));
    TARGET_TOUCH_UP(contentView.getMoreTimeButton, @selector(getMoreTimeButtonTapped));
    TARGET_TOUCH_UP(contentView.shareTimeButton, @selector(shareTimeButtonTapped));
    TARGET_TOUCH_UP(contentView.timeHistoryButton, @selector(timeHistoryButtonTapped));
    TARGET_TOUCH_UP(contentView.subscriptionTopUpButton, @selector(subscriptionTopUpButtonTapped));
    TARGET_TOUCH_UP(contentView.helpButton, @selector(helpButtonTapped));
    TARGET_TOUCH_UP(contentView.balanceHistoryButton, @selector(balanceHistoryButtonTapped));
    [contentView updateWithSelectedVenue:selectedVenue];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    Show_Hud(NSLocalizedString(@"Fetching data...",nil))
    [ZFGetAccountBalanceRequest requestWithId:CurrentUser.uuid completion:^(LORESTRequest *aRequest) {
        if(aRequest.resultStatus == LORESTRequestResultStatusSucceeded) {
            CurrentUser.accountBalance = aRequest.result;
        }
        
        [ZFGetSubscriptionForVenueRequest requestWithVenueId:contentView.ziferblatSelector.selectedVenue.uuid completion:^(LORESTRequest *aRequest) {
            Hide_HudAndPerformBlock(^{
                [contentView updateWithSelectedVenue:contentView.ziferblatSelector.selectedVenue];
            });
        }];
    }];
    
}

#pragma mark - Buttons actions

- (void)backButtonTapped {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)getPiastresButtonTapped {
    if ([[ZFGuestService sharedInstance] makeSureUserIsNotAGuestFromViewController:self]) {
        return;
    }
    
    if (!self.paypalConfigured) {
        UIAlertController *paymentAlert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Purchase directly", @"Treasury")
                                                                              message:NSLocalizedString(@"PayPal payments have not yet been integrated for this Ziferblat. Please purchase the minutes directly from the administrator of this Ziferblat.", @"Treasury")
                                                                       preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okayOption = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", @"Treasury purchase directly")
                                                             style:UIAlertActionStyleDefault
                                                           handler:nil];
        [paymentAlert addAction:okayOption];
        [self presentViewController:paymentAlert
                           animated:YES
                         completion:nil];
        return;
    }
    
    ZFTopUpViewController *topUpPiastresViewController = [[ZFTopUpViewController alloc] initWithTopUpType:ZFTopUpTypePiastres venuesList:venues];
    [self.navigationController pushViewController:topUpPiastresViewController withDirection:LODirection_Right];
}

- (void)givePiastresButtonTapped {
    if ([[ZFGuestService sharedInstance] makeSureUserIsNotAGuestFromViewController:self]) {
        return;
    }
    ZFShareViewController *sharePiastresViewController = [[ZFShareViewController alloc] initWitShareType:ZFShareType_Piastres venuesList:venues];
    [self.navigationController pushViewController:sharePiastresViewController animated:YES];
}

- (void)autorefillButtonTapped {
    if ([[ZFGuestService sharedInstance] makeSureUserIsNotAGuestFromViewController:self]) {
        return;
    }
    ZFSetupAutopaymentViewController *autoPaymentViewController = [[ZFSetupAutopaymentViewController alloc] init];
    [self.navigationController pushViewController:autoPaymentViewController animated:YES];
}

- (void)switchZiferblatButtonTapped:(UIControl *)aControl {
    [contentView ziferblatSelectionButtonTapped];
}

- (void)getMoreTimeButtonTapped {
    if ([[ZFGuestService sharedInstance] makeSureUserIsNotAGuestFromViewController:self]) {
        return;
    }
    ZFTopUpViewController *timeToUpViewController = [[ZFTopUpViewController alloc] initWithTopUpType:ZFTopUpTypeMinutes venuesList:venues];
    [self.navigationController pushViewController:timeToUpViewController animated:YES];
}

- (void)shareTimeButtonTapped {
    if ([[ZFGuestService sharedInstance] makeSureUserIsNotAGuestFromViewController:self]) {
        return;
    }
    ZFShareViewController *shareViewController = [[ZFShareViewController alloc] initWitShareType:ZFShareType_Time venuesList:venues];
    [self.navigationController pushViewController:shareViewController animated:YES];
}

- (void)timeHistoryButtonTapped {
    ZFTimeHistoryViewController *timeHistoryViewController = [[ZFTimeHistoryViewController alloc] init];
    [self.navigationController pushViewController:timeHistoryViewController animated:YES];
}

- (void)subscriptionTopUpButtonTapped {
    if ([[ZFGuestService sharedInstance] makeSureUserIsNotAGuestFromViewController:self]) {
        return;
    }
    ZFSubscriptionTopUpViewController *subscriptionTopUpViewController = [[ZFSubscriptionTopUpViewController alloc] init];
    [self.navigationController pushViewController:subscriptionTopUpViewController animated:YES];
}

- (void)helpButtonTapped {
    ZFHelpViewController *timeBonusViewController = [[ZFHelpViewController alloc] initWithVenues:venues];
    [self.navigationController pushViewController:timeBonusViewController animated:YES];
}

- (void)balanceHistoryButtonTapped {
    ZFBalanceHistoryViewController *balanceHistoryViewController = [[ZFBalanceHistoryViewController alloc] init];
    [self.navigationController pushViewController:balanceHistoryViewController animated:YES];
}

@end
