//
//  ZFEventsViewController.m
//  Ziferblat
//
//  Created by Jose Fernandez on 25/08/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "ZFEventsViewController.h"
#import "ZFEventsCell.h"
#import "ZFEventsArchiveViewController.h"
#import "ZFEventsRequest.h"
#import "ZFEventRequest.h"
#import "ZFEvents.h"
#import "ZFVenue.h"
#import "ZFEventViewController.h"

static CGFloat const ZFScrollViewLoadModeThreshold = 100;
#define kRowHeight (notIPad ? 150:310)

@interface ZFEventsViewController() <ZFEventsCellDelegate>
@property (nonatomic, strong) ZFVenue *selectedVenue;
@property (nonatomic, strong) NSMutableArray *soonEvents;
@property (nonatomic, strong) NSMutableArray *willEvents;
@property (nonatomic, strong) NSMutableArray *wentEvents;
@property (nonatomic) NSInteger currentSoonPage;
@property (nonatomic) NSInteger currentWillPage;
@property (nonatomic) NSInteger currentWentPage;
@property (nonatomic) BOOL shouldGetMoreSoonEvents;
@property (nonatomic) BOOL shouldGetMoreWillEvents;
@property (nonatomic) BOOL shouldGetMoreWentEvents;
@property (nonatomic) BOOL isTableViewOffset;
@property (nonatomic, strong) UITableView *tableView;
@end

@implementation ZFEventsViewController

- (instancetype)initWithSelectedVenue:(ZFVenue *)aVenue; {
    self = [super init];
    if(self) {
        _selectedVenue = aVenue;
        _soonEvents = @[].mutableCopy;
        _willEvents = @[].mutableCopy;
        _wentEvents = @[].mutableCopy;
        _currentSoonPage = 0;
        _currentWillPage = 0;
        _currentWentPage = 0;
        _shouldGetMoreSoonEvents = NO;
        _shouldGetMoreWillEvents = NO;
        _shouldGetMoreWentEvents = NO;
        _isTableViewOffset = NO;
    }
    
    return self;
}

- (void)loadView {
    contentView = [[ZFEventsView alloc] init];
    self.view = contentView;
    self.tableView = contentView.tableView.tableView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"ZFEventsCell" bundle:nil] forCellReuseIdentifier:[ZFEventsCell reuseIdentifier]];
    
    contentView.scrollView.delegate = self;
    
    TARGET_TOUCH_UP(contentView.backButton, @selector(backButtonTapped))
    TARGET_TOUCH_UP(contentView.tab1.tabButton, @selector(tabButtonTapped:))
    TARGET_TOUCH_UP(contentView.tab2.tabButton, @selector(tabButtonTapped:))
    TARGET_TOUCH_UP(contentView.tab3.tabButton, @selector(tabButtonTapped:))
    TARGET_TOUCH_UP(contentView.archiveButton, @selector(archiveButtonTapped))
    [contentView.tableView setController:self];
    self.tableView.tag = EventsTypeSoon;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [[self getEventsArrayOfType:self.tableView.tag] removeAllObjects];
    [self fetchEventsOfType:self.tableView.tag];
}

# pragma mark -
# pragma mark Requests

- (void)fetchEventsOfType:(EventsType)eventType {
    Show_Hud(nil);
    [ZFEventsRequest requestWithEventType:eventType venueId:self.selectedVenue.uuid page:[self getCurrentPageNumberOfType:eventType] completion:^(LORESTRequest *aRequest) {
        Hide_HudAndPerformBlock(^{
            if(aRequest.resultStatus == LORESTRequestResultStatusSucceeded) {
                ZFPagedResults *pagedResults = aRequest.result;
                [self setShouldGetMoreEventsOfType:eventType value:pagedResults.hasNext];
                [[self getEventsArrayOfType:eventType] addObjectsFromArray:pagedResults.result];
                
                [self setEventsArrayOfType:eventType value:[[self getEventsArrayOfType:eventType] sortedArrayUsingComparator:^NSComparisonResult(ZFEvents *obj1, ZFEvents *obj2) {
                    return [obj1.eventDate compare:obj2.eventDate] == NSOrderedDescending;
                }]];
            }
            
            [contentView updateLayoutForNumberOfRows:[self getEventsArrayOfType:eventType].count withRowHeight:kRowHeight];
            [self reloadData];
        })
    }];
}

- (void)fetchEventWithId:(NSNumber *)uuid completion:(LORESTRequestCompletion)aCompletion {
    Show_Hud(nil);
    [ZFEventRequest requestWithVenueId:self.selectedVenue.uuid eventId:uuid completion:aCompletion];
}

#pragma mark -
#pragma mark Buttons actions

- (void)tabButtonTapped:(UIButton *)sender {
    self.tableView.tag = sender.tag;
    [[self getEventsArrayOfType:self.tableView.tag] removeAllObjects];
    
    [contentView.tab1 setSelected:sender.tag == EventsTypeSoon];
    [contentView.tab2 setSelected:sender.tag == EventsTypeWill];
    [contentView.tab3 setSelected:sender.tag == EventsTypeWent];

    [self fetchEventsOfType:sender.tag];
}

- (void)archiveButtonTapped {
    ZFEventsArchiveViewController *archivesViewController = [ZFEventsArchiveViewController new];
    [self.navigationController presentViewController:archivesViewController animated:YES completion:nil];
}

# pragma mark -
# pragma mark Helpers

- (void)reloadData {
    [self.tableView reloadData];
}

- (void)setEventsArrayOfType:(EventsType)eventType value:(NSArray *)array {
    switch (eventType) {
        case EventsTypeSoon:
            self.soonEvents = array.mutableCopy;
            break;
        case EventsTypeWill:
            self.willEvents = array.mutableCopy;
            break;
        case EventsTypeWent: // fall through
        default:
            self.wentEvents = array.mutableCopy;
            break;
    }
}

- (NSMutableArray *)getEventsArrayOfType:(EventsType)eventType {
    switch (eventType) {
        case EventsTypeSoon:
            return self.soonEvents;
        case EventsTypeWill:
            return self.willEvents;
        case EventsTypeWent:
            return self.wentEvents;
        default:
            return nil;
    }
}

- (NSInteger)getCurrentPageNumberOfType:(EventsType)eventType {
    switch (eventType) {
        case EventsTypeSoon:
            return self.currentSoonPage;
        case EventsTypeWill:
            return self.currentWillPage;
        case EventsTypeWent: // fall through
        default:
            return self.currentWentPage;
    }
}

- (void)increaseCurrentPageNumberOfType:(EventsType)eventType {
    switch (eventType) {
        case EventsTypeSoon:
            self.currentSoonPage++;
            break;
        case EventsTypeWill:
            self.currentWillPage++;
            break;
        case EventsTypeWent: // fall through
        default:
            self.currentWentPage++;
            break;
    }
}
            
- (void)setShouldGetMoreEventsOfType:(EventsType)eventType value:(BOOL)value {
    switch (eventType) {
        case EventsTypeSoon:
            self.shouldGetMoreSoonEvents = value;
            break;
        case EventsTypeWill:
            self.shouldGetMoreWillEvents = value;
            break;
        case EventsTypeWent: // fall through
        default:
            self.shouldGetMoreWentEvents = value;
            break;
    }
}

- (BOOL)getShouldGetMoreEventsOfType:(EventsType)eventType {
    switch (eventType) {
        case EventsTypeSoon:
            return self.shouldGetMoreSoonEvents;
        case EventsTypeWill:
            return self.shouldGetMoreWillEvents;
        case EventsTypeWent: // fall through
        default:
            return self.shouldGetMoreWentEvents;
    }
}

#pragma mark -
#pragma mark UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self getEventsArrayOfType:tableView.tag].count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ZFEventsCell *cell = [tableView dequeueReusableCellWithIdentifier:[ZFEventsCell reuseIdentifier]];
    cell.delegate = self;
    [cell setupCellWithEvent:[self getEventsArrayOfType:tableView.tag][indexPath.row] indexPath:indexPath];
    
    return cell;
}

#pragma mark - 
#pragma mark UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {    
    return kRowHeight;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    dispatch_async(dispatch_get_main_queue(), ^{
        
        __block ZFEvents *event = [self getEventsArrayOfType:self.tableView.tag][indexPath.row];
        [self fetchEventWithId:event.uuid completion:^(LORESTRequest *aRequest) {
            Hide_HudAndPerformBlock(^{
                if (aRequest.resultStatus == LORESTRequestResultStatusSucceeded) {
                    ZFEventViewController *eventViewController = [[ZFEventViewController alloc] initWithNibName:@"ZFEventViewController" bundle:nil];
                    eventViewController.venueId = self.selectedVenue.uuid;
                    eventViewController.event = aRequest.result;
                    [self.navigationController pushViewController:eventViewController animated:YES];
                }
            })
        }];
    });
}

# pragma mark -
# pragma mark UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    // if scrolled to within 100 pixels of the bottom of the content
    // set flag to cause next page of notifications to be loaded.
    CGFloat contentOffsetBottom = scrollView.contentOffset.y + scrollView.contentInset.top + scrollView.frame.size.height;
    CGFloat contentHeight = scrollView.contentSize.height + scrollView.contentInset.top + scrollView.contentInset.bottom;
    
    self.isTableViewOffset = (contentOffsetBottom > contentHeight - ZFScrollViewLoadModeThreshold);
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    if (self.isTableViewOffset && [self getShouldGetMoreEventsOfType:self.tableView.tag]) {
        [self increaseCurrentPageNumberOfType:self.tableView.tag];
        [self fetchEventsOfType:self.tableView.tag];
    }
}

#pragma mark -
#pragma mark ZFEventsCellDelegate

- (void)interestingButtonWasTappedWithIndex:(NSInteger)index {
//    TO_BE_IMPLEMENTED
}

@end
