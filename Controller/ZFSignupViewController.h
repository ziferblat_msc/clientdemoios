//
//  ZFLoginViewController.h
//  Ziferblat
//
//  Created by Terry Michel on 08/04/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "ZFBaseView.h"
@class ZFButton;

@interface ZFSignUpViewController : UIViewController<UITextFieldDelegate, UIGestureRecognizerDelegate> {
    UIImageView *backgroundImageView;
    UITextField *emailTextField;
    UITextField *passwordTextField;
    UITextField *confirmPasswordTextField;
    UITextField *secretWordTextField;
    ZFButton *signInButton;
    UIButton *skipItButton;
    UIButton *backButton;
    UILabel *backLabel;
    UILabel *headerLabel;
    UILabel *secretMessageLabel;
    UILabel *skipItLabel;
    UILabel *justWannaSeeLabel;
}

@end
