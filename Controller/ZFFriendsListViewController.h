//
//  ZFFriendsListViewController.h
//  Ziferblat
//
//  Created by Terry Michel on 08/04/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "ZFFriendsSearchTableViewCell.h"
#import "ZFSearchView.h"
#import "ZFFriendRequestTableViewCell.h"

@class ZFFilterTextButton, ZFFriendRequestView;

@interface ZFFriendsListViewController : UIViewController

- (instancetype)initWithFriendsList:(NSArray *)friends;

- (void)displayFriendRequests;

@end
