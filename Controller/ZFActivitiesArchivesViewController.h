//
//  ZFActivitiesArchivesViewController.h
//  Ziferblat
//
//  Created by Terry Michel on 16/04/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "ZFActivitiesArchivesView.h"

@interface ZFActivitiesArchivesViewController : UIViewController {
    ZFActivitiesArchivesView *contentView;
}

@end
