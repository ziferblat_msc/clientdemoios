//
//  ZFActivityDetailViewController.m
//  Ziferblat
//
//  Created by Peter Su on 28/10/2015.
//  Copyright © 2015 Locassa. All rights reserved.
//

#import "ZFActivityDetailViewController.h"
#import "ZFActivity.h"
#import "ZFAttendees.h"
#import "ZFAttendeesCell.h"
#import "ZFComment.h"
#import "ZFCommentsCell.h"
#import "ZFButton.h"
#import "ZFActivityRequest.h"
#import "ZFActivityLikeRequest.h"
#import "ZFActivityCommentsRequest.h"
#import "ZFActivityAttendRequest.h"
#import "ZFActivityJoinQueueRequest.h"
#import "ZFActivityCommentRequest.h"
#import "ZFActivityCommentLikeRequest.h"
#import "ZFGuestService.h"
#import "LOImageDownloader.h"

#define kCommentPlaceholderString NSLocalizedString(@"Comment...", nil)
#define kTextLimit 255
#define kReplyToViewHeight (notIPad ? 30:62)

@interface ZFActivityDetailViewController () <UITableViewDataSource, UITableViewDelegate, UICollectionViewDataSource, UICollectionViewDelegate, UITextViewDelegate, ZFCommentsCellDelegate>

@property (weak, nonatomic) IBOutlet UIControl *topNavigationView;
@property (weak, nonatomic) IBOutlet UILabel *backButtonLabel;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

// Top View
@property (weak, nonatomic) IBOutlet UIView *topView;
@property (weak, nonatomic) IBOutlet UILabel *activityTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *activityDateLabel;
@property (weak, nonatomic) IBOutlet UIImageView *activityImageView;
@property (weak, nonatomic) IBOutlet UILabel *activityContentLabel;
@property (weak, nonatomic) IBOutlet UIView *commentsAndLikesView;
@property (weak, nonatomic) IBOutlet UILabel *numberOfCommentsLabel;
@property (weak, nonatomic) IBOutlet UILabel *numberOfLikesLabel;
@property (weak, nonatomic) IBOutlet UIImageView *divider;
@property (weak, nonatomic) IBOutlet UIImageView *likeImageView;
@property (weak, nonatomic) IBOutlet UILabel *likeLabel;
@property (weak, nonatomic) IBOutlet UIView *commentsHeaderView;

// Attendees Sections

@property (weak, nonatomic) IBOutlet UILabel *attendeesLabel;
@property (weak, nonatomic) IBOutlet UILabel *neededLabel;
@property (weak, nonatomic) IBOutlet UICollectionView *attendedCollectionView;
@property (weak, nonatomic) IBOutlet UICollectionView *neededCollectionView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *collectionViewsHeightConstraint;

@property (weak, nonatomic) IBOutlet UIView *buttonContainer;

// Comment Table View
@property (weak, nonatomic) IBOutlet UITableView *commentsTableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *commentsTableHeight;

// Submit Comment view
@property (weak, nonatomic) IBOutlet UIView *submitCommentView;
@property (weak, nonatomic) IBOutlet UITextView *commentTextView;
@property (weak, nonatomic) IBOutlet UIView *replyToView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *replyToViewHeightConstraint;
@property (weak, nonatomic) IBOutlet UILabel *replyToLabel;
@property (weak, nonatomic) IBOutlet UILabel *replyToPersonLabel;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *keyboardPaddingHeight;

@property (nonatomic, strong) UITapGestureRecognizer *tapGestureRecognizer;
@property (nonatomic, strong) NSMutableArray *comments;
@property (nonatomic, copy) NSString *replyComment;

@end

@implementation ZFActivityDetailViewController {
    ZFButton *attendButton;
    ZFButton *postItButton;
}

@synthesize selectedActivity;
@synthesize comments;

#pragma mark - Life cycle

- (void)dealloc {
    Notification_RemoveObserver
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setupViewSizes];
    
    comments = [NSMutableArray new];
    self.backButtonLabel.text = NSLocalizedString(@"BACK TO ACTIVITIES", nil);
    
    // register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    
    Notification_Observe(kNotification_SingleActivityRefreshed, receivedSingleActivityRefreshed:);
    Notification_Observe(kNotification_SingleActivityCommentsRefreshed, receivedSingleActivityCommentsRefreshed:);
    
    [[ZFNetworkService sharedInstance] pollForType:ZFPollingRequestType_SingleActivity|ZFPollingRequestType_SingleActivityComments
                                        activityId:selectedActivity.uuid
                                           venueId:self.venueId
                                           summary:YES];
    
    self.tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTapOnScrollView:)];
    self.tapGestureRecognizer.cancelsTouchesInView = NO;
    [self.scrollView addGestureRecognizer:self.tapGestureRecognizer];
    [self setupStyle];
    [self setupCollectionViews];
    [self setupAttendButton];
    [self setupCommentsTable];
    [self setupSubmitCommentView];
    [self updateContentForSelectedActivity];
    
    self.attendeesLabel.text = NSLocalizedString(@"Attendees:", nil);
    self.neededLabel.text = NSLocalizedString(@"Needed:", nil);
    self.likeLabel.text = NSLocalizedString(@"Like", nil);
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self fetchComments];
    [self updateHeightForCollectionViews];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.view endEditing:YES];
}

- (void)setupViewSizes
{
    if(isIPad)
    {
        self.backButtonLabel.font = [UIFont fontWithName:self.backButtonLabel.font.fontName size:26];
        self.activityTitleLabel.font = [UIFont fontWithName:self.activityTitleLabel.font.fontName size:41];
        self.activityDateLabel.font = [UIFont fontWithName:self.activityDateLabel.font.fontName size:34];
        self.activityContentLabel.font = [UIFont fontWithName:self.activityContentLabel.font.fontName size:34];
        self.numberOfCommentsLabel.font = [UIFont fontWithName:self.numberOfCommentsLabel.font.fontName size:31];
        self.numberOfLikesLabel.font = [UIFont fontWithName:self.numberOfLikesLabel.font.fontName size:34];
        self.likeLabel.font = [UIFont fontWithName:self.likeLabel.font.fontName size:36];
        self.replyToLabel.font = [UIFont fontWithName:self.replyToLabel.font.fontName size:36];
        self.replyToPersonLabel.font = [UIFont fontWithName:self.replyToPersonLabel.font.fontName size:36];
        self.commentTextView.font = [UIFont fontWithName:self.commentTextView.font.fontName size:34];
        self.attendeesLabel.font = [UIFont fontWithName:self.attendeesLabel.font.fontName size:34];
        self.neededLabel.font = [UIFont fontWithName:self.neededLabel.font.fontName size:34];
        
        self.topNavigationView.constraints[0].constant = 48;
        
        for(NSLayoutConstraint *constraint in self.topView.constraints)
        {
            constraint.constant *= 2.4;
        }
        
        for (UIView *subview in self.topView.subviews )
        {
            for(NSLayoutConstraint *constraint in subview.constraints)
            {
                constraint.constant *= 2.4;
            }
        }
        
        for (UIView *subview in self.commentsAndLikesView.subviews )
        {
            for(NSLayoutConstraint *constraint in subview.constraints)
            {
                constraint.constant *= 2.4;
            }
        }
        
        for(NSLayoutConstraint *constraint in self.commentsHeaderView.constraints)
        {
            constraint.constant *= 2.4;
        }
        
        for (UIView *subview in self.commentsHeaderView.subviews )
        {
            for(NSLayoutConstraint *constraint in subview.constraints)
            {
                constraint.constant *= 2.4;
            }
        }
        
        for(NSLayoutConstraint *constraint in self.submitCommentView.constraints)
        {
            constraint.constant *= 2.4;
        }
        
        for (UIView *subview in self.submitCommentView.subviews )
        {
            for(NSLayoutConstraint *constraint in subview.constraints)
            {
                constraint.constant *= 2.4;
            }
        }
        
        for (UIView *subview in self.replyToView.subviews )
        {
            for(NSLayoutConstraint *constraint in subview.constraints)
            {
                constraint.constant *= 2.4;
            }
        }
        
        self.commentsAndLikesView.constraints[5].constant = 7;
        self.commentsAndLikesView.constraints[7].constant = 7;
    }
}

#pragma mark - Setup Comments Table

- (void)setupStyle {
    self.activityDateLabel.textColor = Colour_Hex(@"B3B3B3");
    
    UIImage *dividerImage = [[UIImage imageNamed:@"Divider"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.divider setImage:dividerImage];
    [self.divider setTintColor:[UIColor colorWithRed:179/255.0f green:179/255.0f blue:179/255.0f alpha:1.0f]];
}

- (void)setupCommentsTable {
    [self.commentsTableView registerNib:[UINib nibWithNibName:[ZFCommentsCell className] bundle:nil] forCellReuseIdentifier:[ZFCommentsCell reuseIdentifier]];
    self.commentsTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.commentsTableView.delegate = self;
    self.commentsTableView.dataSource = self;
    [self updateCommentsTableViewHeightForNumberOfRows:0];
}

- (void)setupCollectionViews {
    [self.attendedCollectionView registerNib:[UINib nibWithNibName:@"ZFAttendeesCell" bundle:nil]
                  forCellWithReuseIdentifier:[ZFAttendeesCell reuseIdentifier]];
    self.attendedCollectionView.delegate = self;
    self.attendedCollectionView.dataSource = self;
    
    [self.neededCollectionView registerNib:[UINib nibWithNibName:@"ZFAttendeesCell" bundle:nil]
                forCellWithReuseIdentifier:[ZFAttendeesCell reuseIdentifier]];
    self.neededCollectionView.delegate = self;
    self.neededCollectionView.dataSource = self;
    
    
}

- (void)setupAttendButton {
    attendButton = [[ZFButton alloc] initWithTitle:NSLocalizedString(@"I WILL GO TO THIS EVENT SUPER LONG TEXTT YEAH HHHHH BOY!", nil)
                                      contentWidth:180
                                     contentHeight:66
                                         showMarks:YES
                                        showBorder:NO];
    CGFloat width = attendButton.width;
    CGFloat height = attendButton.height;
    attendButton.translatesAutoresizingMaskIntoConstraints = NO;
    [self.buttonContainer addSubview:attendButton];
    
    NSLayoutConstraint *buttonTopConstraint = [NSLayoutConstraint constraintWithItem:attendButton
                                                                           attribute:NSLayoutAttributeTop
                                                                           relatedBy:NSLayoutRelationEqual
                                                                              toItem:self.buttonContainer
                                                                           attribute:NSLayoutAttributeTop
                                                                          multiplier:1.0
                                                                            constant: (notIPad ? 6:14)];
    NSLayoutConstraint *buttonHeightConstraint = [NSLayoutConstraint constraintWithItem:attendButton
                                                                              attribute:NSLayoutAttributeHeight
                                                                              relatedBy:NSLayoutRelationEqual
                                                                                 toItem:nil
                                                                              attribute:NSLayoutAttributeNotAnAttribute
                                                                             multiplier:1.0
                                                                               constant:height];
    NSLayoutConstraint *buttonWidthConstraint = [NSLayoutConstraint constraintWithItem:attendButton
                                                                              attribute:NSLayoutAttributeWidth
                                                                              relatedBy:NSLayoutRelationEqual
                                                                                 toItem:nil
                                                                              attribute:NSLayoutAttributeNotAnAttribute
                                                                             multiplier:1.0
                                                                               constant:width];
    NSLayoutConstraint *buttonBottomConstraint = [NSLayoutConstraint constraintWithItem:self.buttonContainer
                                                                              attribute:NSLayoutAttributeBottom
                                                                              relatedBy:NSLayoutRelationEqual
                                                                                 toItem:attendButton
                                                                              attribute:NSLayoutAttributeBottom
                                                                             multiplier:1.0
                                                                               constant:(notIPad ? 6:14)];
    NSLayoutConstraint *buttonCenterXConstraint = [NSLayoutConstraint constraintWithItem:attendButton
                                                                               attribute:NSLayoutAttributeCenterX
                                                                               relatedBy:NSLayoutRelationEqual
                                                                                  toItem:self.buttonContainer
                                                                               attribute:NSLayoutAttributeCenterX
                                                                              multiplier:1.0
                                                                                constant:0];
    
    [self.buttonContainer addConstraints:@[buttonTopConstraint,
                                           buttonHeightConstraint,
                                           buttonWidthConstraint,
                                           buttonBottomConstraint,
                                           buttonCenterXConstraint]];
    
    TARGET_TOUCH_UP(attendButton, @selector(attendButtonWasTapped))
}

#pragma mark - Setup Submit Comment

- (void)setupSubmitCommentView {
    [self setReplyToVisible:NO];
    self.commentTextView.delegate = self;
    self.commentTextView.layer.borderColor = Colour_Black.CGColor;
    self.commentTextView.layer.borderWidth = 2.0f;
    [self resetCommentTextView];
    
    [self setupPostItButton];
}

- (void)resetCommentTextView {
    self.commentTextView.text = kCommentPlaceholderString;
    self.commentTextView.textColor = Colour_Hex(@"727374");
    
    self.replyComment = nil;
    [self setReplyToVisible:NO];
}

- (void)setupPostItButton {
    postItButton = [ZFButton buttonWithTitle:NSLocalizedString(@"POST IT!", nil) showMarks:NO];
    CGFloat width = postItButton.width;
    postItButton.translatesAutoresizingMaskIntoConstraints = NO;
    postItButton.enabled = NO;
    [self.submitCommentView addSubview:postItButton];
    
    NSLayoutConstraint *postItButtonTopConstraint = [NSLayoutConstraint constraintWithItem:postItButton
                                                                                 attribute:NSLayoutAttributeTop
                                                                                 relatedBy:NSLayoutRelationEqual
                                                                                    toItem:self.commentTextView
                                                                                 attribute:NSLayoutAttributeBottom
                                                                                multiplier:1.0
                                                                                  constant:(notIPad ?15:25)];
    NSLayoutConstraint *postItButtonCenterXConstraint = [NSLayoutConstraint constraintWithItem:postItButton
                                                                                     attribute:NSLayoutAttributeCenterX
                                                                                     relatedBy:NSLayoutRelationEqual
                                                                                        toItem:self.submitCommentView
                                                                                     attribute:NSLayoutAttributeCenterX
                                                                                    multiplier:1.0
                                                                                      constant:0];
    NSLayoutConstraint *postItButtonWidthConstraint = [NSLayoutConstraint constraintWithItem:postItButton
                                                                                   attribute:NSLayoutAttributeWidth
                                                                                   relatedBy:NSLayoutRelationEqual
                                                                                      toItem:nil
                                                                                   attribute:NSLayoutAttributeNotAnAttribute
                                                                                  multiplier:1.0
                                                                                    constant:width];
    NSLayoutConstraint *postItButtonHeightConstraint = [NSLayoutConstraint constraintWithItem:postItButton
                                                                                    attribute:NSLayoutAttributeHeight
                                                                                    relatedBy:NSLayoutRelationEqual
                                                                                       toItem:nil
                                                                                    attribute:NSLayoutAttributeNotAnAttribute
                                                                                   multiplier:1.0
                                                                                     constant:44];
    NSLayoutConstraint *postItButtonBottomConstraint = [NSLayoutConstraint constraintWithItem:self.submitCommentView
                                                                                    attribute:NSLayoutAttributeBottom
                                                                                    relatedBy:NSLayoutRelationEqual
                                                                                       toItem:postItButton
                                                                                    attribute:NSLayoutAttributeBottom
                                                                                   multiplier:1.0
                                                                                     constant:15];
    
    [self.submitCommentView addConstraints:@[postItButtonTopConstraint,
                                             postItButtonCenterXConstraint,
                                             postItButtonWidthConstraint,
                                             postItButtonHeightConstraint,
                                             postItButtonBottomConstraint]];
    
    TARGET_TOUCH_UP(postItButton, @selector(postItButtonWasTapped))
}

#pragma mark - Keyboard

- (void)keyboardWillShow:(NSNotification *)notification {
    UIView *view = self.view;
    
    CGRect keyboardFrame = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue];
    keyboardFrame = [view convertRect:keyboardFrame fromView:self.view.window];
    
    CGFloat duration = [[[notification userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    NSInteger animationCurve = [[[notification userInfo] objectForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue];
    
    self.keyboardPaddingHeight.constant = CGRectGetHeight(keyboardFrame);
    [self.view setNeedsUpdateConstraints];
    
    [UIView animateWithDuration:duration delay:0.0f options:animationCurve animations:^{
        [self.view layoutIfNeeded];
        CGPoint bottomOffset = CGPointMake(0, self.scrollView.contentSize.height - self.scrollView.bounds.size.height);
        [self.scrollView setContentOffset:bottomOffset animated:NO];
    } completion:^(BOOL finished) {
    }];
}

- (void)keyboardWillHide:(NSNotification *)notification
{
    CGFloat duration = [[[notification userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    NSInteger animationCurve = [[[notification userInfo] objectForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue];
    
    self.keyboardPaddingHeight.constant = 0.0f;
    [self.view setNeedsUpdateConstraints];
    
    [UIView animateWithDuration:duration delay:0.0f options:animationCurve animations:^{
        [self.view layoutIfNeeded];
    } completion:nil];
}


#pragma mark - Actions

- (IBAction)backButtonWasTapped:(id)sender {
    [super backButtonTapped];
}

- (IBAction)likeButtonWasTapped:(id)sender {
    if ([[ZFGuestService sharedInstance] makeSureUserIsNotAGuestFromViewController:self]) {
        return;
    }
    [self likeNews];
}

- (void)postItButtonWasTapped {
    [self.view endEditing:YES];
    NSString *comment = self.commentTextView.text;
    
    if (![comment isEqualToString:@""] && ![comment isEqualToString:kCommentPlaceholderString]) {
        [self submitComment:comment];
    } else {
        Show_Error(NSLocalizedString(@"Please enter a comment",nil));
    }
}

- (IBAction)cancelReplyWasTapped:(UIButton *)sender {
    self.replyComment = nil;
    self.replyToPersonLabel.text = @"";
    
    [self.commentTextView resignFirstResponder];
    [self setReplyToVisible:NO];
}

- (void)attendButtonWasTapped {
	if ([[ZFGuestService sharedInstance] makeSureUserIsNotAGuestFromViewController:self]) {
        return;
    }

    if (self.selectedActivity.full)
    {
        [self queueForActivity];
    } else {
        [self attendActivity];
    }
}

- (void)receivedSingleActivityRefreshed:(NSNotification *)notification {
    selectedActivity = [ZFNetworkService sharedInstance].activity;
    [self updateContentForSelectedActivity];
}

- (void)receivedSingleActivityCommentsRefreshed:(NSNotification *)notification {
    // Only reload the data in table if user is not typing a message since it causes a lag for user's input
    if (![self.commentTextView isFirstResponder]) {
        [self.comments removeAllObjects];
        [self flattenCommentsArray:[ZFNetworkService sharedInstance].activityComments];
        [self.commentsTableView reloadData];
        [self updateCommentsTableViewHeightForNumberOfRows:self.comments.count];
    }
}

- (void)didTapOnScrollView:(UIGestureRecognizer *)recognizer {
    [self.view endEditing:YES];
}

#pragma mark - Requests

- (void)updateActivity {
    [ZFActivityRequest requestWithVenueId:self.venueId
                                     activityId:self.selectedActivity.uuid
                                     completion:^(LORESTRequest *aRequest) {
                                         if (aRequest.resultStatus == LORESTRequestResultStatusSucceeded) {
                                             
                                             Hide_HudAndPerformBlock(^{
                                                 if ([aRequest.result isKindOfClass:[ZFActivity class]]) {
                                                     selectedActivity = aRequest.result;
                                                     [self updateContentForSelectedActivity];
                                                     [self fetchComments];
                                                 } else {
                                                     // Failed to update comment, return user to news list
                                                 }
                                             });
                                         }
                                     }];
}

- (void)fetchComments {
    [self.comments removeAllObjects];
    Show_Hud(nil);
    [ZFActivityCommentsRequest requestWithVenueId:self.venueId
                                           activityId:selectedActivity.uuid
                                           completion:^(LORESTRequest *aRequest) {
                                               Hide_HudAndPerformBlock(^{
                                                   if (aRequest.resultStatus == LORESTRequestResultStatusSucceeded) {
                                                       if ([aRequest.result isKindOfClass:[NSArray class]]) {
                                                           NSArray *parentComments = aRequest.result;
                                                           [self flattenCommentsArray:parentComments];
                                                       }
                                                   } else {
                                                       Show_Error(NSLocalizedString(@"Error fetching comments",nil));
                                                   }
                                                   [self.commentsTableView reloadData];
                                                   [self updateCommentsTableViewHeightForNumberOfRows:self.comments.count];
                                               });
                                           }];
}

- (void)likeNews {
    Show_Hud(nil);
    [ZFActivityLikeRequest requestWithVenueId:self.venueId
                                   activityId:selectedActivity.uuid
                                         like:!selectedActivity.liked
                                   completion:^(LORESTRequest *aRequest) {
                                       if (aRequest.resultStatus == LORESTRequestResultStatusSucceeded) {
                                           [self updateActivity];
                                       } else {
                                           Hide_HudAndPerformBlock(^{
                                               Show_Error(NSLocalizedString(@"Error liking news", nil));
                                           });
                                       }
                                   }];
}

- (void)submitComment:(NSString *)comment {
    Show_Hud(nil);
    if (self.replyComment) {
        [ZFActivityCommentRequest requestWithVenueId:self.venueId
                                               activityId:self.selectedActivity.uuid
                                                 parentId:self.replyComment
                                              commentText:comment
                                               completion:^(LORESTRequest *aRequest) {
                                                   if (aRequest.resultStatus == LORESTRequestResultStatusSucceeded) {
                                                       [self resetCommentTextView];
                                                       [self updateActivity];
                                                   } else {
                                                       Hide_HudAndPerformBlock(^{
                                                           Show_Error(NSLocalizedString(@"Error submitting comment", nil));
                                                       });
                                                   }
                                               }];
    } else {
        [ZFActivityCommentRequest requestWithVenueId:self.venueId
                                               activityId:self.selectedActivity.uuid
                                              commentText:comment
                                               completion:^(LORESTRequest *aRequest) {
                                                   if (aRequest.resultStatus == LORESTRequestResultStatusSucceeded) {
                                                       [self resetCommentTextView];
                                                       [self updateActivity];
                                                   } else {
                                                       Hide_HudAndPerformBlock(^{
                                                           Show_Error(NSLocalizedString(@"Error submitting comment",nil));
                                                       });
                                                   }
                                               }];
    }
}

- (void)attendActivity {
    Show_Hud(nil);
    [ZFActivityAttendRequest requestWithVenueId:self.venueId
                                     activityId:selectedActivity.uuid
                                         attend:!selectedActivity.attended
                                     completion:^(LORESTRequest *aRequest) {
                                         if (aRequest.resultStatus == LORESTRequestResultStatusSucceeded) {
                                             NSLog(@"Result response: %@", aRequest.result);
                                             [self updateActivity];
                                         } else {
                                             Hide_HudAndPerformBlock(^{
                                                 Show_Error(NSLocalizedString(@"Error during request", nil));
                                             });
                                         }
                                     }];
}

- (void)queueForActivity {
    Show_Hud(nil);
    [ZFActivityJoinQueueRequest requestWithVenueId:self.venueId
                                        activityId:selectedActivity.uuid
                                            attend:!selectedActivity.joined
                                        completion:^(LORESTRequest *aRequest) {
                                            if (aRequest.resultStatus == LORESTRequestResultStatusSucceeded) {
                                                NSLog(@"Result response: %@", aRequest.result);
                                                [self updateActivity];
                                            } else {
                                                Hide_HudAndPerformBlock(^{
                                                    Show_Error(NSLocalizedString(@"Error during request", nil));
                                                });
                                            }
                                        }];
}

#pragma mark - Helpers

- (void)flattenCommentsArray:(NSArray *)array {
    for (ZFComment *comment in array) {
        // Polling causing issues with fetch since both poll result and actual fetch request delivering results to flattenCommentsArray
        // at the same time which results in the duplicate comments and replies. Predicate used to only add unique comments to display array
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"uuid == %@", comment.uuid];
        if ([comments filteredArrayUsingPredicate:predicate].count == 0) {
            [comments addObject:comment];
        }
        
        if ([comment.replies count] > 0) {
            [self flattenCommentsArray:comment.replies];
        }
    }
}

- (void)updateContentForSelectedActivity {
    if (selectedActivity) {
        self.activityTitleLabel.text = selectedActivity.title;
        
        NSDateFormatter *dateFormatter = [NSDateFormatter new];
        dateFormatter.dateStyle = NSDateFormatterMediumStyle;
        dateFormatter.timeStyle = NSDateFormatterShortStyle;
        NSString *dateString = [dateFormatter stringFromDate:selectedActivity.deadLine];
        if (dateString) {
            self.activityDateLabel.text = [NSString stringWithFormat:@"%@ %@", NSLocalizedString(@"Deadline:", @"Deadline title label"), dateString];
        } else {
            self.activityDateLabel.text = [NSString stringWithFormat:NSLocalizedString(@"No deadline", @"Deadline title label")];
        }
        self.activityContentLabel.text = selectedActivity.detail;
        
        UIImage *image = [LOImageCache imageFromCache:selectedActivity.imageUrl];
        if (!image) {
            self.activityImageView.image = [UIImage imageNamed:@"ZFPlaceholderOfflineTask 320pt width"];
            [LOImageDownloader downloadImageFromPath:selectedActivity.imageUrl
                                withCompletionHander:^(UIImage *downloadedImage) {
                                    [UIView transitionWithView:self.activityImageView
                                                      duration:0.5
                                                       options:UIViewAnimationOptionTransitionCrossDissolve
                                                    animations:^{
                                                        self.activityImageView.image = downloadedImage;
                                                    } completion:nil];
                                }];
        } else {
            self.activityImageView.image = image;
        }
        
        NSNumber *numberOfLikes = (selectedActivity.likeCount) ? selectedActivity.likeCount : @0;
        self.numberOfLikesLabel.text = [NSString stringWithFormat:@"%@", numberOfLikes];
        
        NSNumber *numberOfComments = (selectedActivity.commentCount) ? selectedActivity.commentCount : @0;
        self.numberOfCommentsLabel.text = [[NSString stringWithFormat:NSLocalizedString(@"%d comments", nil), numberOfComments.integerValue] uppercaseString];
        
        NSString *likeImagePath = (selectedActivity.liked) ? @"LikeIconRed" : @"NewsLikeIconGrey";
        self.likeImageView.image = [UIImage imageNamed:likeImagePath];
        
        [attendButton updateTitle:(selectedActivity.attended) ? NSLocalizedString(@"I WON'T !", @"Won't attend activity") : NSLocalizedString(@"I WILL", @"Will attend activity")];
        BOOL inProgress = (!selectedActivity.completed && [[NSDate date] isLaterThanDate:selectedActivity.deadLine] && selectedActivity.attended);
        if (inProgress) {
            [attendButton updateTitle:NSLocalizedString(@"IN PROGRESS", @"Activity in progress")];
        } else if (selectedActivity.completed) {
            [attendButton updateTitle:NSLocalizedString(@"COMPLETED", @"Activity has been completed")];
            attendButton.enabled = NO;
        }
        else if (selectedActivity.full)
        {
            if (!selectedActivity.joined)
            {
                [attendButton updateTitle:NSLocalizedString(@"JOIN QUEUE", @"Join the queue for activity")];
            }
            else
            {
                [attendButton updateTitle:NSLocalizedString(@"LEAVE QUEUE", @"Join the queue for activity")];
            }
        }
        
        [self.attendedCollectionView reloadData];
        [self.neededCollectionView reloadData];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self updateHeightForCollectionViews];
        });
    }
}

- (void)updateCommentsTableViewHeightForNumberOfRows:(NSInteger)numberOfRows {
    CGFloat totalRowHeight = 0;
    for (NSInteger index = 0; index < numberOfRows; index++) {
        totalRowHeight += [self heightForCommentCellAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0]];
    }
    self.commentsTableHeight.constant = totalRowHeight;
    
    [self.view setNeedsUpdateConstraints];
    
    [UIView animateWithDuration:0.3 animations:^{
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {
        
    }];
}

- (void)updateHeightForCollectionViews {
    CGFloat attendedHeight = self.attendedCollectionView.contentSize.height;
    CGFloat neededHeight = self.neededCollectionView.contentSize.height;
    
    if (attendedHeight > neededHeight) {
        self.collectionViewsHeightConstraint.constant = attendedHeight + kLargePadding;
    } else {
        self.collectionViewsHeightConstraint.constant = neededHeight + kLargePadding;
    }

    [UIView animateWithDuration:0.3 animations:^{
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {
        
    }];
}

- (void)setReplyToVisible:(BOOL)visible {
    if (visible) {
        self.replyToView.alpha = 1.0;
        self.replyToViewHeightConstraint.constant = kReplyToViewHeight;
    } else {
        self.replyToView.alpha = 0.0;
        self.replyToViewHeightConstraint.constant = 0;
    }
    [self.view setNeedsUpdateConstraints];
    
    [UIView animateWithDuration:0.3 animations:^{
        [self.view layoutIfNeeded];
        if (visible) {
            CGPoint bottomOffset = CGPointMake(0, self.scrollView.contentSize.height - self.scrollView.bounds.size.height);
            [self.scrollView setContentOffset:bottomOffset animated:NO];
        }
    }];
}

#pragma mark - UITableViewDataSource / UITableViewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return comments.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [self heightForCommentCellAtIndexPath:indexPath];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ZFCommentsCell *commentsCell = [tableView dequeueReusableCellWithIdentifier:[ZFCommentsCell reuseIdentifier] forIndexPath:indexPath];
    commentsCell.commentDelegate = self;
    ZFComment *comment = [comments objectAtIndex:indexPath.row];
    [commentsCell setupCellForComment:comment];
    
    return commentsCell;
}

#pragma mark - UITableView Helper

- (CGFloat)heightForCommentCellAtIndexPath:(NSIndexPath *)indexPath {
    static ZFCommentsCell *sizingCell = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sizingCell = [self.commentsTableView dequeueReusableCellWithIdentifier:[ZFCommentsCell reuseIdentifier]];
    });
    
    ZFComment *comment = [comments objectAtIndex:indexPath.row];
    [sizingCell setupCellForComment:comment];
    return [self calculateHeightForConfiguredSizingCell:sizingCell];
}

- (CGFloat)calculateHeightForConfiguredSizingCell:(UITableViewCell *)sizingCell {
    sizingCell.bounds = CGRectMake(0.0f, 0.0f, CGRectGetWidth(self.commentsTableView.frame), CGRectGetHeight(sizingCell.bounds));
    [sizingCell setNeedsLayout];
    [sizingCell layoutIfNeeded];
    
    CGSize size = [sizingCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
    return size.height;
}

#pragma mark - ZFCommentsCellDelegate

- (void)replyCommentWithCommentId:(NSString *)uuid username:(NSString *)username {
    self.replyComment = uuid;
    self.replyToPersonLabel.text = username;
    [self setReplyToVisible:YES];
}

- (void)commentWithId:(NSString *)commentId like:(BOOL)like {
    [ZFActivityCommentLikeRequest requestWithVenueId:self.venueId
                                          activityId:selectedActivity.uuid
                                           commentId:commentId
                                                like:like
                                          completion:nil];
}


# pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if (collectionView == self.attendedCollectionView) {
        return self.selectedActivity.attendees.count;
    } else {
        return [self.selectedActivity.peopleNeeded integerValue] - [self.selectedActivity.attendeeCount integerValue];
    }
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return (notIPad ? CGSizeMake(27, 27):CGSizeMake(65, 65));
}

# pragma mark - UICollectionViewDelegate

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    ZFAttendeesCell *attendeesCell = [collectionView dequeueReusableCellWithReuseIdentifier:[ZFAttendeesCell reuseIdentifier] forIndexPath:indexPath];
    
    if (collectionView == self.attendedCollectionView) {
        ZFAttendees *attendee = self.selectedActivity.attendees[indexPath.row];
        [attendeesCell layoutForAttendee:attendee];
    } else {
        [attendeesCell layoutForPersonNeeded];
    }
    return attendeesCell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (collectionView == self.attendedCollectionView) {
        ZFAttendees *attendee = self.selectedActivity.attendees[indexPath.row];
        [[ZFPresenter sharedInstanceWithNavigationController:nil] presentProfileForMemberId:attendee.uuid
                                                                          andOptionalMember:attendee];
    }
}

#pragma mark - UITextViewDelegate

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView {
    BOOL canEdit = YES;
    if ([[ZFGuestService sharedInstance] makeSureUserIsNotAGuestFromViewController:self]) {
        canEdit = NO;
    }
    
    return canEdit;
}

- (void)textViewDidBeginEditing:(UITextView *)textView {
    if ([textView.text isEqualToString:kCommentPlaceholderString]) {
        textView.text = @"";
        textView.textColor = Colour_Black;
    }
}

- (void)textViewDidEndEditing:(UITextView *)textView {
    if ([textView.text isEqualToString:@""]) {
        [self resetCommentTextView];
    }
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        
        return NO;
    }
    
    BOOL aux = YES;
    NSString *output = [textView.text stringByReplacingCharactersInRange:range withString:text];
    aux = output.length < kTextLimit;
    
    if (output.length == 0 || [output stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length == 0) {
        postItButton.enabled = NO;
    } else {
        postItButton.enabled = YES;
    }
    
    return aux;
}

@end
