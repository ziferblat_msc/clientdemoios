//
//  ZFLoginViewController.m
//  Ziferblat
//
//  Created by Terry Michel on 08/04/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "ZFLoginViewController.h"
#import "ZFSignUpViewController.h"
#import "ZFHomeViewController.h"
#import "ZFLoginRequest.h"
#import "LORESTRequestService.h"
#import "ZFGetAccountBalanceRequest.h"
#import "ZFButton.h"
#import "ZFResetPasswordViewController.h"
#import "ZFGuestService.h"

#define kBackgroundKeyboardVisibleShift 38
#define kEmailTextFieldFrame            (UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPad ? CGRectMake(62, 198, 208, 38) : CGRectMake(213, 317, 355, 68))
#define kPasswordTextFieldFrame         (UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPad ? CGRectMake(62, 255, 208, 38) : CGRectMake(213, 409, 355, 68))
#define kResetPasswordButtonFrame       (UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPad ? CGRectMake(172, 296, 208, 38) : CGRectMake(429, 480, 150, 38))
#define kLoginButtonTopOrigin           (UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPad ? 333 : 560)
#define kSkipItButtonFrame              (UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPad ? CGRectMake(85, 404, 149, 29) : CGRectMake(234, 638, 300, 58))
#define kBecomeACitizenButtonFrame      (UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPad ? CGRectMake(55, 494, 208, 75) : CGRectMake(204, 915, 360, 50))
#define kHeaderLabelFrame               (UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPad ? CGRectMake(48, 45, 225, 30) : CGRectMake(234, 65, 300, 60))
#define kComeBackLabelFrame             (UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPad ? CGRectMake(79, 136, 170, 50) : CGRectMake(234, 200, 280, 120))
#define kResetPasswordLabelFrame        (UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPad ? CGRectMake(185, 295, 90, 20) : CGRectMake(439, 485 , 130, 30))
#define kSkipItLabelFrame               (UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPad ? CGRectMake(122, 404, 75, 20) : CGRectMake(309, 628, 150, 40))
#define kJustWannaSeeLabelFrame         (UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPad ? CGRectMake(60, skipItLabel.bottom, 200, 15) : CGRectMake(184, 668, 400, 45))
#define kBecomeACitizenLabelFrame       (UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPad ? CGRectMake(72 ,507, 174, 28) : CGRectMake(250 ,910, 280, 56))

typedef NS_ENUM(NSInteger, ZFLoginView_FieldsTag) {
    ZFLoginView_FieldsTag_Email,
    ZFLoginView_FieldsTag_Password
};

@implementation ZFLoginViewController

#pragma mark -
#pragma mark Lifecycle

- (void)loadView {
    ZFBaseView *aView = [[ZFBaseView alloc] init];
    self.view = aView;
    
    backgroundImageView = [UIImageView imageViewWithImageNamed:@"ZFLoginView"];
    [kScrollView addSubview:backgroundImageView];
    kScrollView.contentSize = backgroundImageView.size;
    
    headerLabel = [[UILabel alloc]initWithFrame:kHeaderLabelFrame];
    headerLabel.text = NSLocalizedString(@"LOG IN TO ZIFERBLAT",nil);
    headerLabel.textColor = Colour_White;
    headerLabel.textAlignment = NSTextAlignmentCenter;
    headerLabel.font = (UI_USER_INTERFACE_IDIOM() !=UIUserInterfaceIdiomPad ? FontRockWell(17) : FontRockWell(27));
    headerLabel.adjustsFontSizeToFitWidth = YES;
    headerLabel.minimumScaleFactor = 0.5;
    [kScrollView addSubview:headerLabel];
    
    comeBackLabel = [[UILabel alloc]initWithFrame:kComeBackLabelFrame];
    comeBackLabel.text = NSLocalizedString(@"Come back to our magic world",nil);
    comeBackLabel.numberOfLines = 2;
    comeBackLabel.font = (UI_USER_INTERFACE_IDIOM() !=UIUserInterfaceIdiomPad ? FontRockWell(16) : FontRockWell(25));
    comeBackLabel.textAlignment = NSTextAlignmentCenter;
    comeBackLabel.textColor = Colour_White;
    comeBackLabel.adjustsFontSizeToFitWidth = YES;
    comeBackLabel.minimumScaleFactor = 0.5;
    [kScrollView addSubview:comeBackLabel];
    
    emailTextField = [UITextField textfieldWithFrame:kEmailTextFieldFrame
                                     placeholderText:NSLocalizedString(@"Email", nil)
                                           textColor:Colour_Black
                                            textFont:(UI_USER_INTERFACE_IDIOM() !=UIUserInterfaceIdiomPad ? FontRockWell(13) : FontRockWell(24))
                                        keyboardMode:UIKeyboardTypeEmailAddress
                                           clearMode:UITextFieldViewModeAlways
                                       returnKeyMode:UIReturnKeyNext
                                  capitalizationMode:UITextAutocapitalizationTypeNone
                                     autoCorrectMode:UITextAutocorrectionTypeNo
                                                 tag:ZFLoginView_FieldsTag_Email
                                            isSecure:NO
                                           addToView:kScrollView];
    [emailTextField setAdjustsFontSizeToFitWidth:TRUE];
    [emailTextField setMinimumFontSize:3];
    
    ZFUser *previousUser = UserService.previousUser;
    if(previousUser) {
        emailTextField.text = previousUser.email;
    }
    
    passwordTextField = [UITextField textfieldWithFrame:kPasswordTextFieldFrame
                                        placeholderText:NSLocalizedString(@"Password", nil)
                                              textColor:Colour_Black
                                               textFont: (UI_USER_INTERFACE_IDIOM() !=UIUserInterfaceIdiomPad ? FontRockWell(13) : FontRockWell(24))
                                           keyboardMode:UIKeyboardTypeDefault
                                              clearMode:UITextFieldViewModeAlways
                                          returnKeyMode:UIReturnKeyDone
                                     capitalizationMode:UITextAutocapitalizationTypeNone
                                        autoCorrectMode:UITextAutocorrectionTypeNo
                                                    tag:ZFLoginView_FieldsTag_Password
                                               isSecure:YES
                                              addToView:kScrollView];
    [passwordTextField setAdjustsFontSizeToFitWidth:TRUE];
    [passwordTextField setMinimumFontSize:3];
    
    resetPasswordLabel = [[UILabel alloc]initWithFrame:kResetPasswordLabelFrame];
    resetPasswordLabel.text = NSLocalizedString(@"Forgot password",nil);
    resetPasswordLabel.textColor = Colour_White;
    resetPasswordLabel.textAlignment = NSTextAlignmentCenter;
    resetPasswordLabel.font = (UI_USER_INTERFACE_IDIOM() !=UIUserInterfaceIdiomPad ? FontRockWell(11.5) : FontRockWell(23));
    resetPasswordLabel.adjustsFontSizeToFitWidth = YES;
    resetPasswordLabel.minimumScaleFactor = 0.5;
    [kScrollView addSubview:resetPasswordLabel];
    
    resetPasswordButton = [UIButton buttonWithFrame:kResetPasswordButtonFrame
                                         isExclusive:YES
                               showsTouchOnHighlight:NO
                                                font:nil
                                         normalTitle:nil
                                   normalTitleColour:nil
                                       selectedTitle:nil
                                 selectedTitleColour:nil
                                     normalImageName:nil
                                   selectedImageName:nil
                           normalBackgroundImageName:nil
                         selectedBackgroundImageName:nil
                                           addToView:kScrollView];
    
    loginButton = [ZFButton buttonWithTitle:NSLocalizedString(@"ENTER", nil) showBorder:YES];
    loginButton.top = kLoginButtonTopOrigin;
    loginButton.enabled = NO;
    [kScrollView addSubview:loginButton];
    [loginButton centerHorizontallyInSuperView];
    
    skipItLabel = [[UILabel alloc]initWithFrame:kSkipItLabelFrame];
    skipItLabel.text = NSLocalizedString(@"Skip it",nil);
    skipItLabel.textAlignment = NSTextAlignmentCenter;
    skipItLabel.textColor = Colour_Black;
    skipItLabel.font = (UI_USER_INTERFACE_IDIOM() !=UIUserInterfaceIdiomPad ? FontRockWell(15) : FontRockWell(30));
    skipItLabel.adjustsFontSizeToFitWidth = YES;
    skipItLabel.minimumScaleFactor = 0.5;
    [kScrollView addSubview:skipItLabel];
    
    justWannaSeeLabel = [[UILabel alloc]initWithFrame:kJustWannaSeeLabelFrame];
    justWannaSeeLabel.text = NSLocalizedString(@"(just wanna see what's there)",nil);
    justWannaSeeLabel.numberOfLines = 0;
    justWannaSeeLabel.textAlignment = NSTextAlignmentCenter;
    justWannaSeeLabel.font = (UI_USER_INTERFACE_IDIOM() !=UIUserInterfaceIdiomPad ? FontRockWell(11) : FontRockWell(22));
    justWannaSeeLabel.adjustsFontSizeToFitWidth = YES;
    justWannaSeeLabel.minimumScaleFactor = 0.5;
    [kScrollView addSubview:justWannaSeeLabel];
    
    skipItButton = [UIButton buttonWithFrame:kSkipItButtonFrame
                                 isExclusive:YES
                       showsTouchOnHighlight:NO
                                        font:nil
                                 normalTitle:nil
                           normalTitleColour:nil
                               selectedTitle:nil
                         selectedTitleColour:nil
                             normalImageName:nil
                           selectedImageName:nil
                   normalBackgroundImageName:nil
                 selectedBackgroundImageName:nil
                                   addToView:kScrollView];
    
    signupLabel = [[UILabel alloc]initWithFrame:kBecomeACitizenLabelFrame];
    signupLabel.text = NSLocalizedString(@"Become a citizen",nil);
    signupLabel.textAlignment = NSTextAlignmentCenter;
    signupLabel.textColor = Colour_Black;
    signupLabel.font = (UI_USER_INTERFACE_IDIOM() !=UIUserInterfaceIdiomPad ? FontRockWell(15) : FontRockWell(30));
    signupLabel.adjustsFontSizeToFitWidth = YES;
    signupLabel.minimumScaleFactor = 0.5;
    [kScrollView addSubview:signupLabel];
    
    signupButton = [UIButton buttonWithFrame:kBecomeACitizenButtonFrame
                                         isExclusive:YES
                               showsTouchOnHighlight:NO
                                                font:nil
                                         normalTitle:nil
                                   normalTitleColour:nil
                                       selectedTitle:nil
                                 selectedTitleColour:nil
                                     normalImageName:nil
                                   selectedImageName:nil
                           normalBackgroundImageName:nil
                         selectedBackgroundImageName:nil
                                           addToView:kScrollView];
    
    UITapGestureRecognizer *tapGesture = [UITapGestureRecognizer gestureWithTarget:self
                                     selector:@selector(tapGestureDetected:)
                                    addToView:self.view];
    tapGesture.delegate = self;
    
    Notification_Observe(UIKeyboardWillChangeFrameNotification, updateLayoutForKeyboard:)
}

- (void)viewDidLoad {
    [super viewDidLoad];
    TARGET_TOUCH_UP(loginButton, @selector(loginButtonPressed))
    TARGET_TOUCH_UP(resetPasswordButton, @selector(forgotPasswordButtonPressed))
    TARGET_TOUCH_UP(skipItButton, @selector(skipItButtonPressed))
    TARGET_TOUCH_UP(signupButton, @selector(becomeACitizenButtonPressed))
    [emailTextField addTarget:self action:@selector(checkStatus) forControlEvents:UIControlEventEditingChanged];
    [passwordTextField addTarget:self action:@selector(checkStatus) forControlEvents:UIControlEventEditingChanged];

    emailTextField.delegate = self;
    passwordTextField.delegate = self;
}

- (void)dealloc {
    Notification_RemoveObserver
}

#pragma mark -
#pragma mark Button actions

- (void)loginButtonPressed {
    [self.view endEditing:YES];
    self.view.userInteractionEnabled = NO;
    
    [self tryLoginWithEmail:emailTextField.text password:passwordTextField.text];
}

- (void)forgotPasswordButtonPressed {
    ZFResetPasswordViewController *controller = [[ZFResetPasswordViewController alloc] init];
    [self.navigationController pushViewController:controller animated:YES];
}

- (void)skipItButtonPressed
{
    [self.view endEditing:YES];
    
    self.view.userInteractionEnabled = NO;
    
    [self tryLoginWithEmail:kGuestUsername
                   password:kGuestPassword];
}

- (void)becomeACitizenButtonPressed {
    ZFSignUpViewController *signupController = [[ZFSignUpViewController alloc] init];
    [self.navigationController pushViewController:signupController animated:YES];
}

#pragma mark -
#pragma mark Gestures

- (void)tapGestureDetected:(UITapGestureRecognizer *)aTapGesture {
    [self.view endEditing:YES];
}

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer {
    CGPoint location = [gestureRecognizer locationInView:kScrollView];
    return !(CGRectContainsPoint(loginButton.frame, location));
}

#pragma mark -
#pragma mark UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    switch (textField.tag) {
        case ZFLoginView_FieldsTag_Email:
            [passwordTextField becomeFirstResponder];
            break;
        case ZFLoginView_FieldsTag_Password:
            [self.view endEditing:YES];
            break;
        default:
            break;
    }
    [self checkStatus];
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range
replacementString:(NSString *)string {
    [self checkStatus];
    return YES;
}

#pragma mark -
#pragma mark Login

- (void)tryLoginWithEmail:(NSString *)email password:(NSString *)password
{
    [ZFLoginRequest requestWithUsername:email
                               password:password
                             completion:^(LORESTRequest *aRequest) {
                                 ZFUser *user = aRequest.result;
                                 user.password = password;
                                 [UserService setLoggedInUser:aRequest.result];
                                 self.view.userInteractionEnabled = YES;
                                 
                                 if(aRequest.resultStatus == LORESTRequestResultStatusSucceeded) {
                                     [UserDefaults setValue:[NSDate date] forKey:kLastContentUpdateDateKey];
                                     
                                     [ZFGetAccountBalanceRequest requestWithId:CurrentUser.uuid
                                                                    completion:^(LORESTRequest *aRequest) {
                                                                        
                                                                        if(aRequest.resultStatus == LORESTRequestResultStatusSucceeded) {
                                                                            CurrentUser.accountBalance = aRequest.result;
                                                                            [self pushHomeViewController];
                                                                        } else {
                                                                            [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"Authentication failed",nil)];
                                                                        }
                                                                    }];
                                 } else {
                                     [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"Authentication failed",nil)];
                                 }
                             }];
}

#pragma mark -
#pragma mark Utils

- (void)pushHomeViewController
{
    ZFHomeViewController *homeViewController = [[ZFHomeViewController alloc] initIsLaunchFromLogin:YES];
    [self.navigationController pushViewController:homeViewController animated:YES];
    passwordTextField.text = @"";
}

- (void)checkStatus {
    BOOL loginEnabled = emailTextField.text.isValidEmailAddress;
    loginEnabled &= passwordTextField.text.isValidString;
    loginEnabled &= passwordTextField.text.length >= TEXT_FIELD_PASSWORD_MIN_LENGTH;
    
    loginButton.enabled = loginEnabled;
}

#pragma mark -
#pragma mark Notifications

- (void)updateLayoutForKeyboard:(NSNotification *)notification {
    BOOL showingKeyboard = [notification keyboardHeight] > 0;
    [kScrollView setScrollEnabled:!showingKeyboard];
    [kScrollView setContentOffset:CGPointMake(0, showingKeyboard ? kBackgroundKeyboardVisibleShift : 0)];
}

@end
