//
//  ZFPiastresTopUpViewController.m
//  Ziferblat
//
//  Created by Terry Michel on 08/04/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "ZFTopUpViewController.h"
#import "ZFSetupAutopaymentViewController.h"
#import "ZFSubscriptionTopUpViewController.h"
#import "ZFVenue.h"
#import "ZFButton.h"
#import "ZFBaseView.h"
#import "ZFPurchase.h"
#import "ZFTopUpRequest.h"
#import "ZFBackButton.h"
#import "ZFLocalisedImageDefinitions.h"
#import "ZFGetGlobalData.h"
#import "ZFGlobalData.h"

#define kLabelSize (notIPad ? 14:34)

@interface ZFTopUpViewController()

@property double piastredPerDollarConversionRate;

@end

@implementation ZFTopUpViewController

#define kHeaderLabelFrame               (notIPad ? CGRectMake(48, 40, 219, 26):CGRectMake(115, 86, 526, 62))
#define kYouHaveLabelFrame              (notIPad ? CGRectMake(102, 90, 120, 25):CGRectMake(245, 210, 288, 50))
#define kGetMorePiastresLabelFrame      (notIPad ? CGRectMake(59.5, 250, 200, 55):CGRectMake(143, 610, 480, 132))
#define kGetMoreTimeLabelFrame          (notIPad ? CGRectMake(59, 254, 200, 30):CGRectMake(142, 610, 480, 72))
#define kAddLabelFrame                  (notIPad ? CGRectMake(30, 325, 132, 20):CGRectMake(72, 780, 317, 48))
#define kVenueScrollViewFrame           (notIPad ? CGRectMake(0, 509, 248.0, 44):CGRectMake(0, 1222, 595, 96))
#define kPiastresTop                    (notIPad ? 120:288)
#define kKeyboardTopPiastresTopUp       (notIPad ? 348:835)
#define kKeyboardTopTimeTopUp           (notIPad ? 350:840)
#define kZiferblatSelectorTop           (notIPad ? 286:686)
#define kMoneyToSpendWidth              (notIPad ? 260:664)
#define kYesButtonTopPiastresTopUp      (notIPad ? 496:1190)
#define kYesButtonTopTimeTopUp          (notIPad ? 556:1334)
#define kSubscriptionButtonTopOrigin    (notIPad ? 646:1550)
#define kAutorefillLabelFrame           (notIPad ? CGRectMake(87, 728, 146, 24):CGRectMake(209, 1747, 350, 58))
#define kAutorefillButtonFrame          (notIPad ? CGRectMake(70, 604, 182, 156):CGRectMake(168, 1450, 437, 374))
#define kIWantLabelFrame                (notIPad ? CGRectMake(127, 686, 140, 30):CGRectMake(305, 1646, 336, 72))
#define kSubscriptionLabelFrame         (notIPad ? CGRectMake(134.5, 720, 130, 25):CGRectMake(323, 1728, 312, 60))

#pragma mark -
#pragma mark Lifecycle

- (instancetype)initWithTopUpType:(ZFTopUpType)aType venuesList:(NSArray *)venuesList {
    self = [super init];
    if(self) {
        venues = venuesList;
        amount = 0;
        type = aType;
        selectedVenue = CurrentUser.selectedVenue;
        shouldPopWithDirection = (type == ZFTopUpTypePiastres);
        
        NSString *venueIdentifier = selectedVenue.internalIdentifier;
        
        if (venueIdentifier && venueIdentifier.length > 0) {
            [PayPalMobile initializeWithClientIdsForEnvironments:@{PayPalEnvironmentProduction : venueIdentifier,
                                                                   PayPalEnvironmentSandbox : venueIdentifier}];
        } else {
            [PayPalMobile initializeWithClientIdsForEnvironments:@{PayPalEnvironmentProduction : @"YOUR_CLIENT_ID_FOR_PRODUCTION",
                                                                   PayPalEnvironmentSandbox : @"0"}];
        }
    }
    return self;
}

- (void)loadView {
    contentView = [[ZFBaseView alloc] init];
    [contentView.scrollView setDelegate:self];
    self.view = contentView;
    
    [self updateBackgroundImageWithImageName:RESOURCE_PIASTRES_TOP_UP];
    
    ZFBackButton *backButton = [ZFBackButton backButtonWithTitle:NSLocalizedString(@"BACK TO MY TREASURY", @"Top Up")];
    [self.view addSubview:backButton];
    TARGET_TOUCH_UP(backButton, @selector(backButtonPressed));
    
    headerLabel = [[UILabel alloc]initWithFrame:kHeaderLabelFrame];
    headerLabel.text = NSLocalizedString(@"PIASTRES TOP-UP", @"Top Up");
    headerLabel.textAlignment = NSTextAlignmentCenter;
    headerLabel.font = FontRockWell(notIPad ? 24:58);
    headerLabel.adjustsFontSizeToFitWidth = YES;
    headerLabel.minimumScaleFactor = 0.5;
    [kScrollView addSubview:headerLabel];
    
    youHaveLabel = [[UILabel alloc]initWithFrame:kYouHaveLabelFrame];
    youHaveLabel.text =NSLocalizedString(@"YOU HAVE", @"Top Up");
    youHaveLabel.textAlignment = NSTextAlignmentCenter;
    youHaveLabel.font = FontRockWell(notIPad ? 19:46);
    youHaveLabel.adjustsFontSizeToFitWidth = YES;
    youHaveLabel.minimumScaleFactor = 0.5;
    [kScrollView addSubview:youHaveLabel];
    
    getMoreLabel = [[UILabel alloc]initWithFrame:kGetMorePiastresLabelFrame];
    getMoreLabel.textAlignment = NSTextAlignmentCenter;
    getMoreLabel.font = FontRockWell(notIPad ? 19:46);
    getMoreLabel.numberOfLines = 0;
    getMoreLabel.adjustsFontSizeToFitWidth = YES;
    getMoreLabel.minimumScaleFactor = 0.5;
    [kScrollView addSubview:getMoreLabel];
    
    addLabel = [[UILabel alloc]initWithFrame:kAddLabelFrame];
    addLabel.textAlignment = NSTextAlignmentLeft;
    addLabel.font = FontRockWell(notIPad ? 13.5:32);
    addLabel.adjustsFontSizeToFitWidth = YES;
    addLabel.minimumScaleFactor = 0.5;
    [kScrollView addSubview:addLabel];
    
    ZFLocalisedImageDefinitions *imageDefinitions = [ZFLocalisedImageDefinitions sharedInstance];
    
    piastresButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [piastresButton setImage:imageDefinitions.resourceSharePiastres selectedImage:imageDefinitions.resourceSharePiastresSelected];
    piastresButton.left = kLargePadding;
    piastresButton.top = kPiastresTop;
    [piastresButton.titleLabel setFont:FontHelvetica(notIPad ? 16:38)];
    [piastresButton setTitleColor:Colour_Black forState:UIControlStateNormal];
    [piastresButton setTitle:@"0" forState:UIControlStateNormal];
    [piastresButton setTitleEdgeInsets:UIEdgeInsetsMake((notIPad ? -61:-146), 0, 0, 0)];
    [kScrollView addSubview:piastresButton];
    TARGET_TOUCH_UP(piastresButton, @selector(piastresButtonPressed:));
    
    timeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [timeButton setImage:imageDefinitions.resourceShareTime selectedImage:imageDefinitions.resourceShareTimeSelected];
    timeButton.right = self.view.width - kLargePadding;
    timeButton.top = piastresButton.top - 3.0;
    [kScrollView addSubview:timeButton];
    TARGET_TOUCH_UP(timeButton, @selector(timeButtonPressed:));
    
    CGFloat timeLabelSize = (notIPad ? 48:115);
    CGFloat x = (notIPad ? 217:521);
    CGFloat y = (notIPad ? 145:352);
    timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(x, y, timeLabelSize, timeLabelSize)];
    timeLabel.font = FontLobster(notIPad ? 22:53);
    timeLabel.textColor = Colour_Black;
    timeLabel.adjustsFontSizeToFitWidth = YES;
    timeLabel.minimumScaleFactor = 0.25;
    timeLabel.clipsToBounds = YES;
    timeLabel.layer.cornerRadius = timeLabelSize / 2;
    timeLabel.text = @"0";
    timeLabel.textAlignment = NSTextAlignmentCenter;
    [kScrollView addSubview:timeLabel];
    
    keyboard = [[ZFKeyboard alloc] init];
    [keyboard setDelegate:self];
    [contentView.scrollView addSubview:keyboard];
    keyboard.top = kKeyboardTopPiastresTopUp;
    
    ziferblatSelector = [[ZFTreasuryZiferblatSelector alloc] init];
    ziferblatSelector.delegate = self;
    [contentView.scrollView addSubview:ziferblatSelector];
    ziferblatSelector.top = kZiferblatSelectorTop;
    [ziferblatSelector centerHorizontallyInSuperView];
    TARGET_TOUCH_UP(ziferblatSelector, @selector(ziferblatSelectionButtonPressed));
    
    moneyToSpendLabel = [[UILabel alloc] init];
    moneyToSpendLabel.numberOfLines = 0;
    [moneyToSpendLabel setFont:FontRockWell(kLabelSize)];
    moneyToSpendLabel.adjustsFontSizeToFitWidth = YES;
    [moneyToSpendLabel setTextAlignment:NSTextAlignmentLeft];
    moneyToSpendLabel.top = keyboard.bottom + kPadding;
    [contentView.scrollView addSubview:moneyToSpendLabel];
    
    yesButton = [ZFButton buttonWithTitle:NSLocalizedString(@"YEEESSSS !!", @"Top Up")];
    [contentView.scrollView addSubview:yesButton];
    [yesButton centerHorizontallyInSuperView];
    yesButton.top = kYesButtonTopPiastresTopUp;
    TARGET_TOUCH_UP(yesButton, @selector(yesButtonPressed));
    yesButton.enabled = NO;
    
    piastresLabel = [[UILabel alloc] init];
    [piastresLabel setFont:FontRockWell(kLabelSize)];
    piastresLabel.adjustsFontSizeToFitWidth = YES;
    [contentView.scrollView addSubview:piastresLabel];
    
    autorefillLabel = [[UILabel alloc]initWithFrame:kAutorefillLabelFrame];
//    autorefillLabel.text = NSLocalizedString(@"SET AUTOREFILL", @"Top Up"); - I commented this out because the kettle button is disabled. 3 of 4
    autorefillLabel.textAlignment = NSTextAlignmentCenter;
    autorefillLabel.font = FontRockWell(notIPad ? 16:38);
    autorefillLabel.adjustsFontSizeToFitWidth = YES;
    autorefillLabel.minimumScaleFactor = 0.5;
    [kScrollView addSubview:autorefillLabel];
    
    UIButton *autorefillButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [autorefillButton setFrame:kAutorefillButtonFrame];
    [contentView.scrollView addSubview:autorefillButton];
//    TARGET_TOUCH_UP(autorefillButton, @selector(autoRefillButtonPressed)); - I commented this out because the kettle button is disabled. 4 of 4
    
    venuesScrollView = [[UIScrollView alloc] initWithFrame:kVenueScrollViewFrame];
    [contentView.scrollView addSubview:venuesScrollView];
    [venuesScrollView centerHorizontallyInSuperView];
    venuesScrollView.left += 1;
    [venuesScrollView setPagingEnabled:TRUE];
    [venuesScrollView setDelegate:self];
    [venuesScrollView setShowsHorizontalScrollIndicator:NO];
    
    UIImage *image = [UIImage imageNamed:RESOURCE_TIME_TO_UP];
    subscriptionButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [contentView.scrollView addSubview:subscriptionButton];
    subscriptionButton.frame = CGRectMake(0, kSubscriptionButtonTopOrigin, contentView.width, image.size.height-kSubscriptionButtonTopOrigin);
    TARGET_TOUCH_UP(subscriptionButton, @selector(subscriptionButtonPressed));
    
    piastresButton.selected = (type == ZFTopUpTypePiastres);
    timeButton.selected = (type == ZFTopUpTypeMinutes);
    subscriptionButton.alpha = (type == ZFTopUpTypeMinutes ? 1.0 : 0.0);
    venuesScrollView.alpha = (type == ZFTopUpTypeMinutes ? 1.0 : 0.0);
    ziferblatSelector.alpha = (type == ZFTopUpTypeMinutes ? 1.0 : 0.0);
    
    iWantLabel = [[UILabel alloc]initWithFrame:kIWantLabelFrame];
    iWantLabel.text = NSLocalizedString(@"NO, I WANT A", @"Top Up");
    iWantLabel.textAlignment = NSTextAlignmentRight;
    iWantLabel.font = FontRockWell(notIPad ? 20:48);
    iWantLabel.adjustsFontSizeToFitWidth = YES;
    iWantLabel.minimumScaleFactor = 0.5;
    [kScrollView addSubview:iWantLabel];
    
    subscriptionLabel = [[UILabel alloc]initWithFrame:kSubscriptionLabelFrame];
    subscriptionLabel.text = NSLocalizedString(@"SUBSCRIBTION", @"Top Up");
    subscriptionLabel.textAlignment = NSTextAlignmentRight;
    subscriptionLabel.textColor = Colour_White;
    subscriptionLabel.font = FontRockWell(notIPad ? 17.5:42);
    subscriptionLabel.adjustsFontSizeToFitWidth = YES;
    subscriptionLabel.minimumScaleFactor = 0.5;
    [kScrollView addSubview:subscriptionLabel];
    
    [self resetForType:type];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [NetworkService pollForType:ZFPollingRequestType_VenuesList|ZFPollingRequestType_AccountBalance memberId:CurrentUser.uuid venueId:nil summary:YES];
    Notification_Observe(kNotification_AccountBalanceRefreshed, updateBalance)
    Notification_Observe(kNotification_VenuesListRefreshed, venuesListRefreshed)
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [PayPalMobile preconnectWithEnvironment:PayPalEnvironmentProduction];
}

- (void)dealloc {
    Notification_RemoveObserver
}

#pragma mark Notifications

- (void)venuesListRefreshed {
    venues = NetworkService.venuesList;
    [self updateAmount];
}

#pragma mark -
#pragma mark - Buttons actions

- (void)ziferblatSelectionButtonPressed {
    CGRect frame = CGRectMake(0, ziferblatSelector.top-kPadding, contentView.scrollView.width, contentView.scrollView.height);
    [contentView.scrollView scrollRectToVisible:frame animated:YES];
    
    if(ziferblatSelector.isSelected) {
        [UIView animateWithDuration:0.3 animations:^{
            [ziferblatSelector setSelected:NO];
            [ziferblatSelector compact];
        } completion:^(BOOL finished) {
            [contentView.scrollView setScrollEnabled:YES];
        }];
    } else {
        [contentView.scrollView setScrollEnabled:NO];
        [UIView animateWithDuration:0.3 animations:^{
            [ziferblatSelector setSelected:YES];
            [ziferblatSelector expand];
        }];
    }
}

- (void)backButtonPressed {
    
    if (_presentedFromNotification) {
        [self dismissViewControllerAnimated:YES completion:nil];
    } else {
        if(shouldPopWithDirection) {
            [self.navigationController popViewControllerWithDirection:LODirection_Left];
        } else {
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
}

// Buy button pressed
- (void)yesButtonPressed
{
    switch (type) {
        case ZFTopUpTypePiastres:
        {
			
            NSString *shortDescription = [NSString stringWithFormat:NSLocalizedString(@"%@: refill", @"Top Up"), CurrentUser.fullName];
            double amountToPay = amount.doubleValue / _piastredPerDollarConversionRate;

            NSDecimalNumber *decimalAmount = [[NSDecimalNumber alloc] initWithDouble:amountToPay];
            NSDecimalNumberHandler *behavior = [NSDecimalNumberHandler decimalNumberHandlerWithRoundingMode:NSRoundPlain
                                                                                                      scale:2
                                                                                           raiseOnExactness:NO
                                                                                            raiseOnOverflow:NO
                                                                                           raiseOnUnderflow:NO
                                                                                        raiseOnDivideByZero:NO];
            NSDecimalNumber *roundedDecimalAmount = [decimalAmount decimalNumberByRoundingAccordingToBehavior:behavior];

            if(!payPalConfiguration)
            {
                payPalConfiguration = [[PayPalConfiguration alloc] init];
            }
            
            PayPalPayment *payment = [[PayPalPayment alloc] init];
            payment.amount = roundedDecimalAmount;
            payment.currencyCode = @"USD";
            payment.shortDescription = shortDescription;
            payment.intent = PayPalPaymentIntentAuthorize;
            
            if (payment.processable) {
                PayPalPaymentViewController *paymentViewController;
                paymentViewController = [[PayPalPaymentViewController alloc] initWithPayment:payment
                                                                               configuration:payPalConfiguration
                                                                                    delegate:self];
                
                [self presentViewController:paymentViewController animated:YES completion:nil];
            }
			break;
        }
            
        case ZFTopUpTypeMinutes:
        {
			
            ZFPurchase *purchase = [[ZFPurchase alloc] init];
            purchase.amount = [NSNumber numberWithDouble:MAX(1, amount.doubleValue / selectedVenue.baseMinutesFactor.doubleValue)];
            purchase.paymentDate = @(roundf([NSDate date].timeIntervalSince1970/1000));
            purchase.venueId = selectedVenue.uuid;
            [self validatePurchase:purchase paymentViewController:nil];
			break;
        }
        default: {
            break;
        }
    }
}

- (void)autoRefillButtonPressed {
    ZFSetupAutopaymentViewController *autoPaymentViewController = [[ZFSetupAutopaymentViewController alloc] init];
    [self.navigationController pushViewController:autoPaymentViewController animated:YES];
}

- (void)piastresButtonPressed:(UIButton *)aButton {
    if(aButton.selected) {
        return;
    }
    
    timeButton.selected = NO;
    aButton.selected = YES;
    [self resetForType:ZFTopUpTypePiastres];
}

- (void)timeButtonPressed:(UIButton *)aButton {
    if(aButton.selected) {
        return;
    }
    
    piastresButton.selected = NO;
    aButton.selected = YES;
    [self resetForType:ZFTopUpTypeMinutes];
}

- (void)subscriptionButtonPressed {
    ZFSubscriptionTopUpViewController *subscriptionTopUpViewController = [[ZFSubscriptionTopUpViewController alloc] init];
    [self.navigationController pushViewController:subscriptionTopUpViewController animated:YES];
}

#pragma mark -
#pragma mark Utils

- (void)validatePurchase:(ZFPurchase *)aPurchase
   paymentViewController:(UIViewController *)aPaymentViewController {
    
    [SVProgressHUD showWithStatus:NSLocalizedString(@"Validating purchase...", @"Top Up") maskType:SVProgressHUDMaskTypeGradient];
    [ZFTopUpRequest requestWithPurchase:aPurchase
                                   type:type
                             completion:^(LORESTRequest *aRequest) {
                                 
        if (aRequest.resultStatus == LORESTRequestResultStatusSucceeded) {
            Hide_HudAndPerformBlock(^{
                CurrentUser.accountBalance = aRequest.result;
                [self resetForType:type];
                [aPaymentViewController dismissViewControllerAnimated:YES completion:nil];
            })
        } else {
            [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"Cannot validate purchase", @"Top Up")];
            [aPaymentViewController dismissViewControllerAnimated:YES completion:nil];
        }
    }];
}

- (void)resetForType:(ZFTopUpType)aType {
    type = aType;
    [keyboard reset];
    amount = 0;
    yesButton.top = (type == ZFTopUpTypePiastres ? kYesButtonTopPiastresTopUp : kYesButtonTopTimeTopUp);
    subscriptionButton.alpha = (type == ZFTopUpTypePiastres ? 0.0 : 1.0);
    autorefillLabel.alpha = (type == ZFTopUpTypePiastres ? 1.0 : 0.0);
    iWantLabel.alpha = (type == ZFTopUpTypePiastres ? 0.0 : 1.0);
    subscriptionLabel.alpha = (type == ZFTopUpTypePiastres ? 0.0 : 1.0);
    venuesScrollView.alpha = (type == ZFTopUpTypePiastres ? 0.0 : 1.0);
    ziferblatSelector.alpha = (type == ZFTopUpTypePiastres ? 0.0 : 1.0);
    
    [self updateBackgroundImageWithImageName:(type == ZFTopUpTypePiastres ? RESOURCE_PIASTRES_TOP_UP : RESOURCE_TIME_TO_UP)];
    [self refreshLabels];
    [self checkStatus];
    [self updateBalance];
    [self updateAmount];
}

- (void)updateAmount
{
    [venuesScrollView removeSubviews];
    double base = [selectedVenue.baseMinutesFactor doubleValue];
    double baseMinutes = amount.doubleValue / base;
    
    for(ZFVenue *venue in venues) {
        UILabel *venueLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, venuesScrollView.width - 30, 30)];
        venueLabel.textAlignment = NSTextAlignmentCenter;
        [venueLabel setFont:FontRockWell(notIPad ? 14:26)];
        [venuesScrollView addSubview:venueLabel];
        
        int minutes = round(venue.baseMinutesFactor.doubleValue * baseMinutes);
        venueLabel.text =[NSString stringWithFormat:NSLocalizedString(@"Equal to %d min at %@", @"Top Up"), minutes, venue.name];
        venueLabel.adjustsFontSizeToFitWidth = YES;
        venueLabel.minimumScaleFactor = 0.25;

        [venueLabel centerInSuperView];
        venueLabel.left += (venuesScrollView.width * [venues indexOfObject:venue]) - 4;
    }
    
    [venuesScrollView setContentSize:CGSizeMake(venues.count * venuesScrollView.width, venuesScrollView.height)];
    [self checkStatus];
}

- (void)checkStatus {
    // Disable buy minutes button so user can't top up with zero balance
    double baseMinutes = amount.doubleValue / selectedVenue.baseMinutesFactor.doubleValue;
    int piastresToSpend = MAX(1, round(baseMinutes / selectedVenue.piastresFactor.doubleValue));
    int userBalance = 0;
    
    if([CurrentUser.accountBalance respondsToSelector:@selector(piastres)])
    {
        userBalance = CurrentUser.accountBalance.piastres.intValue;
    }
    
    // Amount is numbers typed in a keybaord
    if (amount.intValue > 0 && userBalance >= piastresToSpend) {
        yesButton.enabled = YES;
    } else {
        yesButton.enabled = NO;
    }
}

- (void)updateBalance
{
	int piastres = CurrentUser.accountBalance.piastres.intValue;
    // Red chest
    [piastresButton setTitle:[NSString stringWithFormat:@"%d", piastres]
                    forState:UIControlStateNormal];
	
    int minutes = round(CurrentUser.accountBalance.baseMinutes.doubleValue * selectedVenue.baseMinutesFactor.doubleValue);
    NSString *minutesTitle = [NSString stringWithFormat:@"%d", minutes];
    timeLabel.text = minutesTitle;
}

- (void)updateBackgroundImageWithImageName:(NSString *)aImageName {
    UIImage *image = [UIImage imageNamed:aImageName];
    contentView.scrollView.contentSize = CGSizeMake(image.size.width, image.size.height);
    
    contentView.backgroundImageView.frame = CGRectMake(0, 0, image.size.width, image.size.height);
    contentView.backgroundImageView.image = image;
}

- (void)refreshLabels {
    NSString *moneyText;
    NSString *piastresText;
    CGRect moneyLabelCenterFrame;
    
    switch (type) {
        case ZFTopUpTypePiastres: {
            
            // Set placeholders while data loading.
            getMoreLabel.text = NSLocalizedString(@"GET MORE PIASTRES\nFOR YOUR TREASURY", @"Top Up");
            getMoreLabel.frame = kGetMorePiastresLabelFrame;
            addLabel.text = NSLocalizedString(@"PIASTRES TO ADD", @"Top Up");
            CGRect topUpMoneyLabelCenterFrame = CGRectMake(0, keyboard.bottom, yesButton.top, yesButton.top-keyboard.bottom);
            
            [self updateLabelsWithTextForMoneyLabel:NSLocalizedString(@"MONEY TO SPEND: ...", @"Top Up")
                            andTextForPiastresLabel:[NSString stringWithFormat:NSLocalizedString(@"(buy %d piastres for ...)", @"Do not translate"), amount.intValue]
                           andMoneyLabelCenterFrame:topUpMoneyLabelCenterFrame];
            
            if (amount.doubleValue == 0) {
                return;
            }
            
            yesButton.enabled = NO;
            
            [ZFGetGlobalData requestWithCompletion:^(LORESTRequest *globalDataRequest) {
                if (globalDataRequest.resultStatus != LORESTRequestResultStatusSucceeded) {
                    Show_ConnectionError
                    return;
                }
                
                ZFGlobalData *data = globalDataRequest.result;
                _piastredPerDollarConversionRate = data.piastresPerDollar.doubleValue;
                double moneyToSpend = amount.doubleValue / _piastredPerDollarConversionRate;
                
                NSString *moneyText = [NSString stringWithFormat:NSLocalizedString(@"MONEY TO SPEND: $%.2f", @"Top Up"), moneyToSpend];
                NSString *piastresText = [NSString localizedStringWithFormat:NSLocalizedString(@"(buy %d piastres for $%.2f)", @"Do not translate"), amount.intValue, moneyToSpend];
                CGRect moneyLabelCenterFrame = CGRectMake(0, keyboard.bottom, yesButton.top, yesButton.top-keyboard.bottom);
                [self updateLabelsWithTextForMoneyLabel:moneyText
                                andTextForPiastresLabel:piastresText
                               andMoneyLabelCenterFrame:moneyLabelCenterFrame];
                
                yesButton.enabled = YES;
            }];
            break;
        }
            
        case ZFTopUpTypeMinutes: {
            double baseMinutes = amount.doubleValue / selectedVenue.baseMinutesFactor.doubleValue;
            int piastresToSpend = MAX(1, round(baseMinutes / selectedVenue.piastresFactor.doubleValue));
            if(amount.intValue == 0) {
                piastresToSpend = 0;
            }
            
            moneyText =[NSString stringWithFormat:NSLocalizedString(@"PIASTRES TO SPEND: %d", @"Top Up"), piastresToSpend];
            piastresText =[NSString localizedStringWithFormat:NSLocalizedString(@"(buy %d minutes for %d P)", @"Do not translate"), amount.intValue, piastresToSpend];
            CGFloat scrollViewTopOrigin = (notIPad  ? 510:1224);
            moneyLabelCenterFrame = CGRectMake(0, keyboard.bottom, scrollViewTopOrigin, scrollViewTopOrigin - keyboard.bottom);
            getMoreLabel.text = NSLocalizedString(@"GET MORE TIME AT", @"Top Up");
            getMoreLabel.frame = kGetMoreTimeLabelFrame;
            addLabel.text = NSLocalizedString(@"MINUTES TO ADD", @"Top Up");
            
            [self updateLabelsWithTextForMoneyLabel:moneyText
                            andTextForPiastresLabel:piastresText
                           andMoneyLabelCenterFrame:moneyLabelCenterFrame];
            break;
        }
            
        default: {
            break;
        }
    }
}
- (void)updateLabelsWithTextForMoneyLabel:(NSString *)moneyText
                  andTextForPiastresLabel:(NSString *)piastresText
                 andMoneyLabelCenterFrame:(CGRect)moneyFrame {
    
    moneyToSpendLabel.text = moneyText;
    [moneyToSpendLabel sizeTextToFitWidth:kMoneyToSpendWidth];
    [moneyToSpendLabel centerInFrame:moneyFrame];
    moneyToSpendLabel.left = (kWidth - kMoneyToSpendWidth)/2;
    
    piastresLabel.text = piastresText;
    piastresLabel.top = yesButton.bottom + kSmallPadding;
    piastresLabel.font = FontRockWell(kLabelSize);
    [piastresLabel sizeToFit];
    [piastresLabel centerHorizontallyInSuperView];
}

#pragma mark -
#pragma mark ScrollView Delegate Methods

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    if(!decelerate) {
        [self scrollViewDidEndDecelerating:scrollView];
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    if([scrollView isEqual:kScrollView]) {
        return;
    }
    
	//	WAITING FOR CLIENT FEEDBACK ON BEHAVIOUR EXPECTED
    //  NSUInteger index = scrollView.contentOffset.x / scrollView.width;
    //  [venues objectAtIndex:index];
}

#pragma mark -
#pragma mark ZFKeyboardDelegateMethods

- (BOOL)keyboardCanInsertAmount:(NSNumber *)anAmount {

    BOOL canInsertAmount;
    int piastresToSpend;
    
    switch (type) {
        case ZFTopUpTypeMinutes: {
            double baseMinutes = anAmount.doubleValue / selectedVenue.baseMinutesFactor.doubleValue;
            piastresToSpend = MAX(1, baseMinutes / selectedVenue.piastresFactor.doubleValue);
            canInsertAmount = CurrentUser.accountBalance.piastres.floatValue >= piastresToSpend;
        }
        default: {
            canInsertAmount = YES;
            break;
        }
    }
    
    if (!canInsertAmount) {
        CGFloat piastresDecimal = CurrentUser.accountBalance.piastres.doubleValue;
        CGFloat piastresFactor = selectedVenue.piastresFactor.doubleValue;
        CGFloat maximumMinsAmount = piastresDecimal*piastresFactor;
        CGFloat baseMinuttesFactorDecimal = selectedVenue.baseMinutesFactor.doubleValue;
        maximumMinsAmount = maximumMinsAmount * baseMinuttesFactorDecimal;
        [keyboard updateAmount:@(floor(maximumMinsAmount))];
    }
    
    return canInsertAmount;
}

- (void)keyboardAmountUpdated:(NSNumber *)aAmount {
    amount = aAmount;
    [self refreshLabels];
    [self updateAmount];
}

#pragma mark -
#pragma mark ZiferblatSelector delegate

- (void)ziferblatSelectorDidSelectVenue:(ZFVenue *)venue {
    amount = 0;
    [keyboard reset];
    [self ziferblatSelectionButtonPressed];
    selectedVenue = venue;
    [self refreshLabels];
    [self updateAmount];
    [self updateBalance];
}
                                                             
#pragma mark -
#pragma mark PayPalPaymentDelegate

- (void)payPalPaymentDidCancel:(PayPalPaymentViewController *)paymentViewController {
    [paymentViewController dismissViewControllerAnimated:YES completion:nil];
}

- (void)payPalPaymentViewController:(PayPalPaymentViewController *)paymentViewController didCompletePayment:(PayPalPayment *)completedPayment {
    NSDictionary *response = [completedPayment.confirmation valueForKey:@"response"];
    
    ZFPurchase *purchase = [[ZFPurchase alloc] init];
    purchase.amount = amount;
    NSDate *creationDate = [NSDate dateFromUtcDateString:[response valueForKey:@"create_time"] format:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
    purchase.paymentDate = @(creationDate.timeIntervalSince1970*1000);
    purchase.paymentId = [response valueForKey:@"authorization_id"];
    purchase.venueId = selectedVenue.uuid;
    
    [self validatePurchase:purchase paymentViewController:paymentViewController];
}

@end
