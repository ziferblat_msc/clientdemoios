//
//  ZFTreasuryViewController.h
//  Ziferblat
//
//  Created by Terry Michel on 08/04/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "ZFTreasuryView.h"

@interface ZFTreasuryViewController : UIViewController {
    ZFTreasuryView *contentView;
    NSArray *venues;
    ZFVenue *selectedVenue;
    NSString *backMessage;
}

#pragma mark - Instance

- (instancetype)initWithSelectedVenue:(ZFVenue *)selectedVenue
                        andVenuesList:(NSArray *)venuesList
                          backMessage:(NSString *)message;

@end
