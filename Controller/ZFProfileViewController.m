//
//  ZFProfileViewController.m
//  Ziferblat
//
//  Created by Terry Michel on 08/04/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "ZFProfileViewController.h"

#import "ZFSettingsViewController.h"
#import "ZFTimeHistoryViewController.h"
#import "ZFLeaderBoardsViewController.h"
#import "ZFExperiencePointsViewController.h"
#import "ZFAchievementsViewController.h"
#import "ZFOtherProfileViewController.h"

#import "LOHorizontalCollectionViewLayout.h"
#import "ZFAchievementCollectionViewCell.h"
#import "ZFActivityProfileCell.h"
#import "ZFInformationCell.h"
#import "ZFProfileImageCollectionViewCell.h"
#import "ZFProfileImage.h"
#import "ZFTimeView.h"
#import "ZFVenue.h"
#import "LOImageCache.h"
#import "LOImageDownloader.h"

#import "ZFGetMemberVenueDetailsRequest.h"
#import "ZFGetMemberFriendsListRequest.h"
#import "ZFGetExperiencePointsPositionByVenue.h"
#import "ZFUpdateMemberRequest.h"
#import "ZFBackButton.h"
#import "ZFOtherProfileViewController.h"

@interface ZFProfileViewController ()

@property (nonatomic, strong) NSString *backMessage;
@property BOOL isFilterEnabled;
@property (nonatomic, strong) NSNumber *venueIdPickedByParent;
@property (nonatomic, strong) UIView *expContainer;

@end

@implementation ZFProfileViewController

@synthesize backMessage;

#define kBottomOffset                       8
#define kScrollViewWidth                    270
#define kScrollViewHeight                   67
#define kFriendsLabelHeight                 24

#define kActivityLabelTopOrigin             112
#define kActivitiesTableViewTopOrigin       137

#define kLargeImageViewFrame                (notIPad ? CGRectMake(74 ,84, 178, 178):CGRectMake(178 ,187, 427, 427))
#define kNameLabelFrame                     (notIPad ? CGRectMake(55, 37, 170, 35):CGRectMake(132, 74, 408, 84))
#define kRankLabelFrame                     (notIPad ? CGRectMake(57, 280, 98, 25):CGRectMake(137, 657, 235, 60))
#define kLevelLabelFrame                    (notIPad ? CGRectMake(217, 280, 72, 25):CGRectMake(521, 657, 173, 60))
#define kScoreLabelFrame                    (notIPad ? CGRectMake(143, 229, 120, 38):CGRectMake(343, 535, 288, 91))
#define kTimeHistoryButtonFrame             (notIPad ? CGRectMake(100, 317, 132, 57):CGRectMake(240, 748, 317, 137))
#define kSettingsButtonFrame                (notIPad ? CGRectMake(275, 34, 44, 40):CGRectMake(660, 71, 106, 96))
#define kLeaderboardsButtonFrame            (notIPad ? CGRectMake(19, 272, 153, 40):CGRectMake(46, 633, 367, 96))
#define kScoreButtonFrame                   (notIPad ? CGRectMake(57, 227, 209, 42):CGRectMake(137, 535, 502, 101))

#define kTimeViewOrigin                     (notIPad ? CGPointMake(127, 327):CGPointMake(305, 770))
#define kSmallImageViewCenter               (notIPad ? CGPointMake(37, 292):CGPointMake(89, 687))

#define kAchievementsFrame                  CGRectMake(kLargePadding, 2, self.view.width - (kLargePadding * 2), 24)

#pragma mark -
#pragma mark Lifecycle

- (instancetype)initWithUser:(ZFUser *)aUser {
    return [self initWithUser:aUser
                  backMessage:NSLocalizedString(@"BACK TO ROOM", @"Default back button title")
                    andFilter:NO
                   forVenueId:nil];
}

- (instancetype)initWithUser:(ZFUser *)aUser
                 backMessage:(NSString *)aBackMessage
                   andFilter:(BOOL)isFilterEnabled
                  forVenueId:(NSNumber * _Nullable)venuedId;{
    self = [super init];
    if (self) {
        user = aUser;
        backMessage = aBackMessage;
        self.isFilterEnabled = isFilterEnabled;
        self.venueIdPickedByParent = venuedId;
    }
    return self;
}

- (void)loadView {
    ZFBaseView *aView = [[ZFBaseView alloc] init];
    [aView.scrollView setDelegate:self];
    self.view = aView;
    
    // Header items
    topImageView = [UIImageView imageViewWithImageNamed:[self topImage]];
    [aView addElement:topImageView addPadding:FALSE];

    largeImageView = [[UIImageView alloc] initWithFrame:kLargeImageViewFrame];
    [kScrollView insertSubview:largeImageView belowSubview:aView.elementsContainer];
    largeImageView.contentMode = UIViewContentModeScaleAspectFill;
    
    nameLabel = [self labelWithFrame:kNameLabelFrame font:FontRockWell((notIPad ? 18:43)) center:TRUE];
    [nameLabel centerHorizontallyInSuperView];
    
    rankLabel = [self labelWithFrame:kRankLabelFrame font:FontRockWell((notIPad ? 13:31)) center:TRUE];
    
    levelLabel = [self labelWithFrame:kLevelLabelFrame font:FontRockWell((notIPad ? 13:31)) center:TRUE];
    [levelLabel setTextColor:Colour_White];
    
    self.expContainer = [[UIView alloc] initWithFrame:kScoreLabelFrame];
    [kScrollView addSubview:self.expContainer];
    
    scoreLabel = [self labelWithFrame:kScoreLabelFrame font:FontRockWell((notIPad ? 24:58)) center:FALSE];
    scoreLabel.textColor = Colour_White;
    
    smallImageView = [ZFProfileImage normalSmallProfile];
    [kScrollView addSubview:smallImageView];
    smallImageView.center = kSmallImageViewCenter;

    timeView = [[ZFTimeView alloc] init];
    [kScrollView addSubview:timeView];
    timeView.origin = kTimeViewOrigin;
    
    // Friends
    friendHeaderView = [aView addHeader:NSLocalizedString(@"Friends",nil) colour:Colour_Hex(@"ededed")];
    friendsView = [[ZFFriendsHorizontalScrollView alloc] init];
    [friendsView setFriendsDelegate:self];
    [aView addElement:friendsView addPadding:FALSE];
    
    // User information
    [aView addHeader:NSLocalizedString(@"Information",nil) colour:Colour_Hex(@"fbe9c2")];
    userDetailsView = [[ZFUserDetailsView alloc] init];
    [userDetailsView setDelegate:self];
    [aView addElement:userDetailsView];
    
    // Achievements
    [aView addHeader:NSLocalizedString(@"Achievements",nil) colour:Colour_Hex(@"f7ce81")];
    achievementsView = [[ZFAchievementsHorizontalScrollView alloc] init];
    [achievementsView setAchievementsDelegate:self];
    [aView addElement:achievementsView addPadding:FALSE];
    
    /*// Activity
    [aView addHeader:NSLocalizedString(@"Recent Activity",nil) colour:Colour_Hex(@"f1b550")];
    //activitiesTableView = [self tableViewWithFrame:CGRectMake(0, 0, kScrollView.width, 300)];
    //[self addElement:activitiesTableView];
    
    UILabel *noData = [[UILabel alloc] init];
    noData.width = kScrollView.width;
    [noData setText:NSLocalizedString(@"No recent activity.",nil)];
    [noData setFont:FontRockWell((notIPad ? 14: 34))];
    [noData sizeToWidthIsAttributed:FALSE];
    [noData setTextAlignment:NSTextAlignmentCenter];
    [aView addElement:noData padding:kLargePadding * 2.0];*/
    
    // Buttons
    backButton = [ZFBackButton backButtonWithTitle:self.backMessage];
    [self.view addSubview:backButton];
    TARGET_TOUCH_UP(backButton, @selector(backButtonTapped))
    
    [self buttonWithFrame:kLeaderboardsButtonFrame selector:@selector(leaderboardsButtonTapped)];
    scoreButton = [self buttonWithFrame:kScoreButtonFrame selector:@selector(scoreButtonTapped)];
    settingsButton = [self buttonWithFrame:kSettingsButtonFrame selector:@selector(settingsButtonTapped)];
    timeHistoryButton = [self buttonWithFrame:kTimeHistoryButtonFrame selector:@selector(timeHistoryButtonTapped)];
    
    [self setupFavoriteVanue];

    [aView layoutElements];
    [self setupWithUser:user];

    Notification_Observe(kNotification_FriendsListUpdated, friendsListUpdated:)
    Notification_Observe(kNotification_SingleMemberDetailsRefreshed, memberUpdated)
    Notification_Observe(kNotification_MemberVenueDetailsRefreshed, memberUpdated)
    
    if (user.uuid && CurrentUser.selectedVenue.uuid && !self.canBeDismissed && [user.uuid isEqualToNumber:CurrentUser.selectedVenue.uuid]) {
        // When viewing another person profile, it start to poll their user details by providing their uuid. However the user could have left
        // and when viewing their own profile, the notification gets called to update the UI giving them the wrong details.
		// Update:
		// 1) This is not called when view presented from notifications (canBeDismissed is true)
        // to prevent the crash with CurrentUser.selectedVenue is nil
        // 2) Add user.uuid && CurrentUser.selectedVenue.uuid to prevent compare:nil crash
        [NetworkService pollForType:ZFPollingRequestType_SingleMemberRequest|ZFPollingRequestType_MemberVenueDetails
                           memberId:CurrentUser.uuid
                            venueId:CurrentUser.selectedVenue.uuid
                            summary:YES];
    }
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
   
    [self fetchExperience];
}

- (void)dealloc {
    Notification_RemoveObserver;
}

- (void)setupFavoriteVanue
{
    //For vanueName showing
    if(user.favouriteVenueId)
    {
        for(ZFVenue *anyVenue in ZiferblatService.allZiferblats)
        {
            if([anyVenue.uuid isEqualToNumber:user.favouriteVenueId])
            {
                user.favoriteVenue = anyVenue;
            }
        }
    }
}

#pragma mark - 
#pragma mark Notifications

- (void)friendsListUpdated:(NSNotification *)sender {
    ZFUser *sourceUser = [sender.userInfo objectForKey:kNotificationKey_FriendSource];
    ZFUser *destinationUser = [sender.userInfo objectForKey:kNotificationKey_FriendDestination];
    
    if(([user isEqual:sourceUser]) || ([user isEqual:destinationUser])) {
        [self updateFriendsCount];
    }
}

- (void)memberUpdated {
    if([user.uuid isEqualToNumber:CurrentUser.uuid] && !userDetailsView.editingUserDetails) {
        user = CurrentUser;
        [self setupWithUser:user];
    }
}

#pragma mark -
#pragma mark Instance specific elements

- (NSString *)topImage {
    return RESOURCE_PROFILE_VIEW_TOP;
}

#pragma mark -
#pragma mark Helper Methods

- (UIButton *)buttonWithFrame:(CGRect)aFrame selector:(SEL)aSelector {
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setFrame:aFrame];
    [kScrollView addSubview:button];
    TARGET_TOUCH_UP(button, aSelector);
    return button;
}

- (UILabel *)labelWithFrame:(CGRect)aFrame font:(UIFont *)aFont center:(BOOL)center {
    UILabel *label = [[UILabel alloc] initWithFrame:aFrame];
    [label setFont:aFont];
    [kScrollView addSubview:label];
    label.adjustsFontSizeToFitWidth = YES;
    
    if(center) {
        [label setTextAlignment:NSTextAlignmentCenter];
    }
    
    return label;
}

- (UITableView *)tableViewWithFrame:(CGRect)aFrame {
    UITableView *tableView = [[UITableView alloc] initWithFrame:aFrame];
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
//    tableView.delegate = self;
//    tableView.dataSource = self;
    [kScrollView addSubview:tableView];
    return tableView;
}

- (void)settingsButtonTapped {
    ZFSettingsViewController *settingsViewController = [[ZFSettingsViewController alloc] initWithUser:CurrentUser];
    [self presentViewController:settingsViewController animated:YES completion:nil];
}

- (void)timeHistoryButtonTapped {
    ZFTimeHistoryViewController *timeHistoryViewController = [[ZFTimeHistoryViewController alloc] init];
    [self.navigationController pushViewController:timeHistoryViewController animated:YES];
}

- (void)leaderboardsButtonTapped {
    ZFLeaderBoardsViewController *leaderboardsViewController = [[ZFLeaderBoardsViewController alloc] initWithTitle:NSLocalizedString(@"BACK TO PROFILE", @"Leaderboards opened from Profile View controller")
                                                                                                           venueId:CurrentUser.selectedVenue.uuid];
    [self presentViewController:leaderboardsViewController animated:YES completion:nil];
}

- (void)scoreButtonTapped {
    ZFExperiencePointsViewController *experienceViewController = [ZFExperiencePointsViewController new];
    [self.navigationController pushViewController:experienceViewController
                                         animated:YES];
}

#pragma mark -
#pragma mark Request Methods

- (void)makeUpdateRequest {
    [SVProgressHUD showWithStatus:NSLocalizedString(@"Updating user data...",nil) maskType:SVProgressHUDMaskTypeGradient];
    [ZFUpdateMemberRequest requestWithUser:user completion:^(LORESTRequest *aRequest) {
        if(aRequest.resultStatus == LORESTRequestResultStatusSucceeded) {
            Hide_HudAndPerformBlock(^{
                CurrentUser = (ZFUser *)aRequest.result;
            })
        } else {
            Show_ConnectionError
        }
    }];
}

- (void)fetchExperience {
    if ([self isMemberOfClass:[ZFOtherProfileViewController class]])
    {
        return;
    }
    if (UserService.loggedInUser == nil)
    {
        return;
    }
    
    // Please use to enable condition
    // disableFilter: !self.isFilterEnabled (bang is required)
    //       venueId: self.venueIdPickedByParent
    
    [SVProgressHUD showWithStatus:NSLocalizedString(@"Fetching data...", @"Profile view") maskType:SVProgressHUDMaskTypeGradient];
    [ZFGetExperiencePointsPositionByVenue requestWithCompletion:^(LORESTRequest *request) {
        Hide_HudAndPerformBlock((^{
            if (request.resultStatus != LORESTRequestResultStatusSucceeded) {
                Show_ConnectionError
                return;
			} else
            {
                
				ZFLeaderBoardCount *expiriencePosition = request.result;
				scoreLabel.text = [NSString stringWithFormat:@"%d",expiriencePosition.valueCount.intValue];
				rankLabel.text = [NSString stringWithFormat:NSLocalizedString(@"You are #%d", ""), expiriencePosition.position.intValue];
			}
        }));
		
    } disableFilter:YES
            venueId:nil // Can be nil, then it is global
        forMemberId:CurrentUser.uuid];
}

#pragma mark -
#pragma mark Public setup

- (void)setupWithUser:(ZFUser *)aUser
{
    user = aUser;
    
    [userDetailsView setUser:aUser];
    [friendsView setUser:aUser];
    [achievementsView setUser:aUser];
    
    UIImage *image = [LOImageCache imageFromCache:aUser.imageUrl];
    if (!image) {
        largeImageView.image = [UIImage imageNamed:@"ZFPlaceHolderZorro"];
        [LOImageDownloader downloadImageFromPath:aUser.imageUrl
                            withCompletionHander:^(UIImage *downloadedImage) {
                                [UIView transitionWithView:largeImageView
                                                  duration:0.5
                                                   options:UIViewAnimationOptionTransitionCrossDissolve
                                                animations:^{
                                                    largeImageView.image = downloadedImage;
                                                } completion:nil];
                            }];
    } else {
        largeImageView.image = image;
    }
    
    [smallImageView setupWithImageAtUrlString:aUser.imageUrl firstName:nil andLastName:nil];
    
    levelLabel.text =[NSString stringWithFormat:NSLocalizedString(@"Level %@",nil), aUser.venueDetails.level];
    nameLabel.text = aUser.fullName;
    
    [self updateFriendsCount];
    [timeView setNumber:nil]; // Shows total time spent on the clock
    //[activitiesTableView reloadData];
}

- (void)updateFriendsCount {
    if(user.friendsList.count > 0) {
        
        [friendHeaderView setCaption:[NSString localizedStringWithFormat:NSLocalizedString(@"%lu friend", @"Do not translate"), user.friendsList.count]];
    } else {
        
        [friendHeaderView setCaption:NSLocalizedString(@"Friends",nil)];
    }
}

#pragma mark -
#pragma mark Friends Scroll View Delegate Methods

- (void)didSelectFriend:(ZFUser *)aUser {
    [SVProgressHUD showWithStatus:NSLocalizedString(@"Loading member...",nil) maskType:SVProgressHUDMaskTypeGradient];
    
    [ZFGetMemberFriendsListRequest requestWithMemberId:aUser.uuid andVenueId:CurrentUser.selectedVenue.uuid completion:^(LORESTRequest *aRequest) {
        if(aRequest.resultStatus == LORESTRequestResultStatusSucceeded) {
            aUser.friendsList = aRequest.result;
            aUser.friendsLoaded = TRUE;
            [self checkLoaded:aUser];
        } else {
            Show_ConnectionError
        }
    }];
    
    [ZFGetMemberVenueDetailsRequest requestWithMemberId:aUser.uuid venueId:CurrentUser.selectedVenue.uuid completion:^(LORESTRequest *aRequest) {
        if(aRequest.resultStatus == LORESTRequestResultStatusSucceeded) {
            aUser.venueDetails = aRequest.result;
            [self checkLoaded:aUser];
        } else {
            Show_ConnectionError
        }
    }];
}

- (void)checkLoaded:(ZFUser *)aUser {
    if((aUser.venueDetails == nil) || (!aUser.friendsLoaded)) {
        return;
    }
    
    Hide_HudAndPerformBlock(^{
        
        if([aUser.uuid isEqualToNumber:CurrentUser.uuid]) {
            ZFProfileViewController *profileViewController = [[ZFProfileViewController alloc] initWithUser:CurrentUser
                                                                                               backMessage:NSLocalizedString(@"BACK TO PROFILE", @"Back button title for profile")
                                                                                                 andFilter:NO
                                                                                                forVenueId:nil];
            profileViewController.canBeDismissed = YES;
            [self presentViewController:profileViewController animated:YES completion:nil];
        } else {
            ZFOtherProfileViewController *otherProfileViewController = [[ZFOtherProfileViewController alloc ] initWithUser:aUser
                                                                                                               backMessage:NSLocalizedString(@"BACK TO PROFILE", @"Back button title for profile")
                                                                                                                 andFilter:NO
                                                                                                                forVenueId:nil];
            otherProfileViewController.canBeDismissed = YES;
            [self presentViewController:otherProfileViewController animated:YES completion:nil];
        }
    });
}

#pragma mark -
#pragma mark Achievements Scroll View Delegate Methods

- (void)didSelectAchievementLevel:(ZFAchievementLevel *)aLevel
{
    ZFAchievementsViewController *achievementsViewController = [[ZFAchievementsViewController alloc] initWithUser:user];
    achievementsViewController.canBeDismissed = YES;
    [self presentViewController:achievementsViewController
                       animated:YES
                     completion:nil];
}

#pragma mark -
#pragma mark User Details Delegate Methods

- (void)isEditingTextView:(UITextView *)aTextView {
    CGRect frame = [kScrollView convertRect:aTextView.frame fromView:aTextView.superview];
    // Y offset needs to take back button height since the scroll view is not under the back button
    [kScrollView setContentOffset:CGPointMake(0, frame.origin.y - (kLargePadding + CGRectGetHeight(backButton.frame))) animated:TRUE];
}

- (void)userDetailsDidChange {
    [((ZFBaseView *)self.view) layoutElements];
}

#pragma mark -
#pragma mark Scroll View Delegate Methods

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    [self.view endEditing:TRUE];
}

@end
