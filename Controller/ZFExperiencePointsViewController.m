//
//  ZFExperiencePointsViewController.m
//  Ziferblat
//
//  Created by Terry Michel on 08/04/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "ZFExperiencePointsViewController.h"
#import "ZFVenue.h"
#import "ZFBackButton.h"
#import "LOTableView.h"
#import "ZFExpereinceHistoryItem.h"
#import "ZFGetExpereinceHistory.h"
#import "ZFExpereinceHistoryTableViewCell.h"
#import "ZFExperienceHistoryHeader.h"
#import "ZFGetExperiencePointsPositionByVenue.h"
#import "ZFLeaderBoardCount.h"

#define kTableRowHeight (notIPad ? 48:115)
#define kContainerPaddingTop (notIPad ? 270:648)
#define kTableHeaderHeight (notIPad ? 30:72)
#define kTableViewHeight (notIPad ? 298:397)
static NSString *const kCellIdentifier = @"ZFExperienceTableViewCell";
static NSString *const kCellNibName = @"ZFExpereinceHistoryTableViewCell";
static NSString *const kHeaderIdentifier = @"ZFExperienceTableViewHeader";
static NSString *const kHeaderNibName = @"ZFExpereinceHistoryHader";
static NSString *const kEventTypeVisit = @"VISIT";
static NSString *const kEventTypeTask = @"TASK";
static NSString *const kEventTypeAchivement = @"ACHIEVEMENT";
static NSString *const kHistoryItemPictureVisit = @"ExperienceTimeIcon2";
static NSString *const kHistoryItemPictureTask = @"ExperienceTaskIcon";
static NSString *const kHistoryItemPictureAchivement = @"ExperienceAchievelentIcon";

@interface ZFExperiencePointsViewController ()

@property (nonatomic, copy) NSString *backButtonMessage;
@property (nonatomic, strong) UILabel *placeholder;
@property (nonatomic, strong) UITableView *tableView;
@property BOOL historyTabSelected;
@property (nonatomic, strong) UIView *blueRibbonsContainer;

@property (nonatomic, strong) UILabel *timeBadgeValue;
@property (nonatomic, strong) UILabel *cupBadgeValue;
@property (nonatomic, strong) UILabel *taskBadgeValue;

@property (nonatomic, strong) NSArray<ZFExpereinceHistoryItem*> *expereinceList;
@property (strong, nonatomic) NSMutableDictionary<NSDate *, NSMutableArray<ZFExpereinceHistoryItem *>*> *sections; // Data Source
@property (strong, nonatomic) NSArray *sortedDays;

@property (strong, nonatomic) NSDateFormatter *sectionDateFormatter;
@property (strong, nonatomic) NSDateFormatter *cellDateFormatter;

@end

@implementation ZFExperiencePointsViewController

@synthesize sectionDateFormatter;
@synthesize cellDateFormatter;

#define kYouGainedLabelFrame                    (notIPad ? CGRectMake(120, 72, 83, 20):CGRectMake(288, 173, 199, 48))
#define kPointsLabelFrame                       (notIPad ? CGRectMake(117, kYouGainedLabelFrame.origin.y+kYouGainedLabelFrame.size.height, 83, 28):CGRectMake(286, kYouGainedLabelFrame.origin.y+kYouGainedLabelFrame.size.height, 199, 67))
#define kHeaderLabelFrame                       (notIPad ? CGRectMake(74, 123, 168, 30):CGRectMake(178, 300, 403, 72))
#define kLevelLabelFrame                        (notIPad ? CGRectMake(138, 167, 43, 20):CGRectMake(337, 406, 103, 48))
#define kAllButtonFrame                         (notIPad ? CGRectMake(69, 210, 64, 20) : CGRectMake(166, 504, 154, 48))
#define kHistoryButtonFrame                     (notIPad ? CGRectMake(158, kAllButtonFrame.origin.y, 90, kAllButtonFrame.size.height):CGRectMake(379, kAllButtonFrame.origin.y, 216, kAllButtonFrame.size.height))
#define kExperiencePointsViewAllImageName       @"ExperiencePointsViewAll"
#define kExperiencePointsViewHistoryImageName   @"ExperiencePointsViewHistory"

#pragma mark - 
#pragma mark Lifecycle

- (instancetype)init {
    if (self = [super init]) {
        self.backButtonMessage = NSLocalizedString(@"BACK TO ROOM", nil);
    }
    return self;
}

- (instancetype)initWithBackMessage:(NSString *)backMessage {
    if (self = [super init]) {
        self.backButtonMessage = backMessage;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    ZFBaseView *aView = [ZFBaseView new];
    self.view = aView;
    
    ZFBackButton *backButton = [ZFBackButton backButtonWithTitle:self.backButtonMessage];
    [self.view addSubview:backButton];
    TARGET_TOUCH_UP(backButton, @selector(backButtonTapped))
    
    topImageView = [UIImageView imageViewWithImageNamed:kExperiencePointsViewAllImageName];
    [kScrollView addSubview:topImageView];
    topImageView.top = backButton.bottom - 1;
    topImageView.userInteractionEnabled = YES;
    
    headerLabel = [[UILabel alloc]initWithFrame:kHeaderLabelFrame];
    headerLabel.textAlignment = NSTextAlignmentCenter;
    headerLabel.text = [NSString stringWithFormat:NSLocalizedString(@"%d EXPERIENCE POINTS", @"Do not translate"), 0];
    headerLabel.font = FontRockWell(notIPad ? 20:48);
    headerLabel.textColor = Colour_White;
    headerLabel.adjustsFontSizeToFitWidth = YES;
    headerLabel.minimumScaleFactor = 0.5;
    [topImageView addSubview:headerLabel];
    
    UILabel *youGainedLabel = [[UILabel alloc]initWithFrame:kYouGainedLabelFrame];
    youGainedLabel.textAlignment = NSTextAlignmentCenter;
    youGainedLabel.text = NSLocalizedString(@"YOU GAINED",nil);
    youGainedLabel.font = FontRockWell(notIPad ? 11:26);
    youGainedLabel.adjustsFontSizeToFitWidth = YES;
    headerLabel.minimumScaleFactor = 0.5;
    [topImageView addSubview:youGainedLabel];
    
    pointsLabel = [UILabel labelWithText:nil
                                    font:FontRockWell(notIPad ? 26:62)
                                  colour:Colour_Black
                           textAlignment:NSTextAlignmentCenter
                           numberOfLines:1
                                   frame:kPointsLabelFrame
                              resizeType:UILabelResizeType_none
                               addToView:topImageView];
    pointsLabel.adjustsFontSizeToFitWidth = YES;
    
    levelLabel = [UILabel labelWithText:nil
                                   font:FontRockWell(notIPad ? 11:34)
                                 colour:Colour_Black
                          textAlignment:NSTextAlignmentCenter
                          numberOfLines:1
                                  frame:kLevelLabelFrame
                             resizeType:UILabelResizeType_none
                              addToView:topImageView];
    levelLabel.adjustsFontSizeToFitWidth = YES;
    
    allButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [allButton setTitle:NSLocalizedString(@"ALL", @"Button in balance history") forState:UIControlStateNormal];
    allButton.frame = kAllButtonFrame;
    allButton.titleLabel.font = FontRockWell(notIPad ? 16:38);
    [allButton setSelected:YES];
    allButton.titleLabel.textColor = Colour_Black;
    [allButton setTitleColor:Colour_AppLightGray forState:UIControlStateNormal];
    [allButton setTitleColor:Colour_Black forState:UIControlStateSelected];
    [topImageView addSubview:allButton];
    TARGET_TOUCH_UP(allButton, @selector(toggleButtonPressed:))
    
    historyButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [historyButton setTitle:NSLocalizedString(@"HISTORY", @"Tab header") forState:UIControlStateNormal];
    historyButton.frame = kHistoryButtonFrame;
    historyButton.titleLabel.font = FontRockWell(notIPad ? 16:38);
    [historyButton setTitleColor:Colour_AppLightGray forState:UIControlStateNormal];
    [historyButton setTitleColor:Colour_Black forState:UIControlStateSelected];
    [topImageView addSubview:historyButton];
    TARGET_TOUCH_UP(historyButton, @selector(toggleButtonPressed:))
    
    Notification_Observe(kNotification_MemberVenueDetailsRefreshed, memberVenueDetailsRefreshed)
    [NetworkService pollForType:ZFPollingRequestType_MemberVenueDetails memberId:CurrentUser.uuid venueId:CurrentUser.selectedVenue.uuid summary:YES];
    
    [self setupWithUser:CurrentUser];
    [self setupPlaceholderForEmtyTable];
    
    [self setupHeaders]; // Three items, 3 blue ribbons AKA banners
    [self setupTable];
    [self update];
}

- (void)dealloc {
    Notification_RemoveObserver
}

#pragma mark - Setup UI

- (void)setupPlaceholderForEmtyTable {
    
    self.placeholder = [UILabel labelWithText:NSLocalizedString(@"You do not have any experience points.", @"Expereince History - when there's no data")
                                             font:FontRockWell(notIPad ? 20:48)
                                           colour:[UIColor blackColor]
                                    textAlignment:NSTextAlignmentCenter
                                    numberOfLines:1
                                        frame:(notIPad ? CGRectMake(0, 0, 220, 20):CGRectMake(0, 0, 528, 48))
                                       resizeType:UILabelResizeType_none
                                        addToView:self.view];
    
    self.placeholder.adjustsFontSizeToFitWidth = YES;
    self.placeholder.center = CGPointMake([UIScreen mainScreen].bounds.size.width/2, (notIPad ? 380:712));
}

- (void)setupHeaders {
    
    // Add three blue ribbons on container
    self.blueRibbonsContainer = [UIView viewWithColor:[UIColor whiteColor]
                                                frame:CGRectMake(0, kContainerPaddingTop, [UIScreen mainScreen].bounds.size.width, (notIPad ? 220:528))
                                            addToView:self.view];
    int blueRibbonPosition = (notIPad ? 20:48);
    int blueRibbonTopMargin = (notIPad ? 40:96);
    
    for (int i = 0; i < 3; i++) {
        UIImageView *blueRibbon = [UIImageView imageViewWithImageNamed:@"ExperienceAchievementBanner"];
        blueRibbon.center = CGPointMake([UIScreen mainScreen].bounds.size.width/2, blueRibbonPosition + (blueRibbonTopMargin * i));
        [self.blueRibbonsContainer addSubview:blueRibbon];
        
        switch (i) {
            case 0: {
                UIImageView *timeBadge = [UIImageView imageViewWithImageNamed:@"ExperienceTimeIcon"];
                timeBadge.center = (notIPad ? CGPointMake(84, 16):CGPointMake(202, 38)); // have no idea why blueRibbon.center.y didn't work, using 16 instead
                [blueRibbon addSubview:timeBadge];
                
                UILabel *label = [UILabel labelWithText:NSLocalizedString(@"For being at Ziferblat", @"Expereince History - Blue Ribbon Label")
                                                   font:FontRockWell(notIPad ? 14:34)
                                                 colour:[UIColor blackColor]
                                          textAlignment:NSTextAlignmentLeft
                                          numberOfLines:1
                                                  frame:(notIPad ? CGRectMake(0, 0, 200, 22):CGRectMake(0, 0, 480, 53))
                                             resizeType:UILabelResizeType_none
                                              addToView:blueRibbon];
                label.center = (notIPad ? CGPointMake(200, 14):CGPointMake(480, 34));
                
                self.timeBadgeValue = [UILabel labelWithText:@"0"
                                                        font:FontRockWell(notIPad ? 14:34)
                                                      colour:[UIColor blackColor]
                                               textAlignment:NSTextAlignmentRight
                                               numberOfLines:1
                                                       frame:(notIPad ? CGRectMake(0, 0, 50, 20):CGRectMake(0, 0, 120, 48))
                                                  resizeType:UILabelResizeType_none
                                                   addToView:blueRibbon];
                self.timeBadgeValue.center = (notIPad ? CGPointMake(40, 14):CGPointMake(96, 34));
                
                break;
            }
            case 1: {
                UIImageView *cupBadge = [UIImageView imageViewWithImageNamed:@"ExperienceAchievelentIcon"];
                cupBadge.center = (notIPad ? CGPointMake(84, 16):CGPointMake(202, 38)); // have no idea why blueRibbon.center.y didn't work
                [blueRibbon addSubview:cupBadge];
                
                UILabel *label = [UILabel labelWithText:NSLocalizedString(@"As a prize for achievements", @"Expereince History - Blue Ribbon Label")
                                                   font:FontRockWell(notIPad ? 14:34)
                                                 colour:[UIColor blackColor]
                                          textAlignment:NSTextAlignmentLeft
                                          numberOfLines:1
                                                  frame:(notIPad ? CGRectMake(0, 0, 200, 22):CGRectMake(0, 0, 480, 53))
                                             resizeType:UILabelResizeType_none
                                              addToView:blueRibbon];
                label.center = (notIPad ? CGPointMake(200, 14):CGPointMake(480, 34));
                
                self.cupBadgeValue = [UILabel labelWithText:@"0"
                                                        font:FontRockWell(notIPad ? 14:34)
                                                      colour:[UIColor blackColor]
                                               textAlignment:NSTextAlignmentRight
                                               numberOfLines:1
                                                       frame:(notIPad ? CGRectMake(0, 0, 50, 20):CGRectMake(0, 0, 120, 48))
                                                  resizeType:UILabelResizeType_none
                                                   addToView:blueRibbon];
                self.cupBadgeValue.center = (notIPad ? CGPointMake(40, 14):CGPointMake(96, 34));
                
                break;
            }
            case 2: {
                UIImageView *cupBadge = [UIImageView imageViewWithImageNamed:@"ExperienceTaskIcon"];
                cupBadge.center = (notIPad ? CGPointMake(84, 16):CGPointMake(202, 38)); // have no idea why blueRibbon.center.y didn't work
                [blueRibbon addSubview:cupBadge];
                
                UILabel *label = [UILabel labelWithText:NSLocalizedString(@"For completed tasks", @"Expereince History - Blue Ribbon Label")
                                                   font:FontRockWell(notIPad ? 14:34)
                                                 colour:[UIColor blackColor]
                                          textAlignment:NSTextAlignmentLeft
                                          numberOfLines:1
                                                  frame:(notIPad ? CGRectMake(0, 0, 200, 22):CGRectMake(0, 0, 480, 53))
                                             resizeType:UILabelResizeType_none
                                              addToView:blueRibbon];
                label.center = (notIPad ? CGPointMake(200, 14):CGPointMake(480, 34));
                
                self.taskBadgeValue = [UILabel labelWithText:@"0"
                                                       font:FontRockWell(notIPad ? 14:34)
                                                     colour:[UIColor blackColor]
                                              textAlignment:NSTextAlignmentRight
                                              numberOfLines:1
                                                       frame:(notIPad ? CGRectMake(0, 0, 50, 20):CGRectMake(0, 0, 120, 48))
                                                 resizeType:UILabelResizeType_none
                                                  addToView:blueRibbon];
                self.taskBadgeValue.center =(notIPad ? CGPointMake(40, 14):CGPointMake(96, 34));
                
                break;
            }
            default:
                break;
        }
    }
}

- (void)setupTable {
    
    self.sectionDateFormatter = [NSDateFormatter new];
    [self.sectionDateFormatter setDateStyle:NSDateFormatterLongStyle];
    [self.sectionDateFormatter setTimeStyle:NSDateFormatterNoStyle];
    
    self.cellDateFormatter = [NSDateFormatter new];
    [self.cellDateFormatter setDateStyle:NSDateFormatterNoStyle];
    [self.cellDateFormatter setTimeStyle:NSDateFormatterShortStyle];
    
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, kContainerPaddingTop - (notIPad ? 0:20), [UIScreen mainScreen].bounds.size.width, 0)
                                                  style:UITableViewStyleGrouped];
    
    UINib *nib = [UINib nibWithNibName:kCellNibName bundle:nil];
    [self.tableView registerNib:nib forCellReuseIdentifier:kCellIdentifier];
    UINib *headerNib = [UINib nibWithNibName:kHeaderNibName bundle:nil];
    [self.tableView registerNib:headerNib forHeaderFooterViewReuseIdentifier:kHeaderIdentifier];
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.backgroundColor = [UIColor whiteColor];
    self.tableView.allowsSelection = NO;
    
    [self.view addSubview:self.tableView];
}

- (void)update {
    [self updateDependingOnTabState];
    
    [SVProgressHUD showWithStatus:NSLocalizedString(@"Fetching data...",nil) maskType:SVProgressHUDMaskTypeGradient];
    // Get expereince points
    [ZFGetExperiencePointsPositionByVenue requestWithCompletion:^(LORESTRequest *expereinceRequest) {
        if (expereinceRequest.resultStatus != LORESTRequestResultStatusSucceeded) {
            [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"Could not read in data",nil)];
            return;
        }
        // Fetech the data for history table and total
        [ZFGetExpereinceHistory requestWithCompletion:^(LORESTRequest *historyRequest) {
            if (historyRequest.resultStatus != LORESTRequestResultStatusSucceeded) {
                [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"Could not read in data",nil)];
                return;
            }
            Hide_HudAndPerformBlock((^{
                // Toggle Placeholder
                if (self.expereinceList.count && self.expereinceList.count > 0) {
                    self.placeholder.hidden = YES;
                } else {
                    self.placeholder.hidden = NO;
                }
                
                self.expereinceList = historyRequest.result;
                [self rebuildTable];
                [self calculateTotalValues];
                
                ZFLeaderBoardCount *expiriencePosition = expereinceRequest.result;
                pointsLabel.text = [NSString stringWithFormat:@"%d",expiriencePosition.valueCount.intValue];
                headerLabel.text = [NSString stringWithFormat:NSLocalizedString(@"%d EXPERIENCE POINTS",@"Do not translate"), expiriencePosition.valueCount.intValue];
                
            }));
        }];
        
    } disableFilter:YES venueId:nil forMemberId:nil];
}

- (void)rebuildTable {
    
    self.sections = [NSMutableDictionary dictionary];
    for (ZFExpereinceHistoryItem *item in self.expereinceList) {
        NSTimeInterval interval = item.date.doubleValue / 1000;
        NSDate *itemDate = [NSDate dateWithTimeIntervalSince1970:interval];
        
        // For every new date insert new section
        
        // Reduce event start date to date components (year, month, day)
        NSDate *dateRepresentingThisDay = [self dateAtBeginningOfDayForDate:itemDate];
        
        // If we don't yet have an array to hold the events for this day, create one
        NSMutableArray *eventsOnThisDay = [self.sections objectForKey:dateRepresentingThisDay];
        if (eventsOnThisDay == nil) {
            eventsOnThisDay = [NSMutableArray array];
            
            // Use the reduced date as dictionary key to later retrieve the event list this day
            [self.sections setObject:eventsOnThisDay forKey:dateRepresentingThisDay];
        }
        
        // Add the event to the list for this day
        [eventsOnThisDay addObject:item];
    }
    
    if (self.sections.count && self.sections.count != 0) {
        // Create a sorted list of days and Revers to for ASC
        NSArray *unsortedDays = [self.sections allKeys];
        self.sortedDays = [[[unsortedDays sortedArrayUsingSelector:@selector(compare:)] reverseObjectEnumerator] allObjects];
        
        // Ucmomment this if you need dynamic table height
        // CGFloat tableViewHeight = (kTableRowHeight * self.expereinceList.count) + (kTableHeaderHeight * self.sections.count);
        self.tableView.height = kTableViewHeight;
        [self.tableView reloadData];
    }
}
- (NSDate *)dateByAddingYears:(NSInteger)numberOfYears toDate:(NSDate *)inputDate {
    
    // Use the user's current calendar
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    NSDateComponents *dateComps = [[NSDateComponents alloc] init];
    [dateComps setYear:numberOfYears];
    
    NSDate *newDate = [calendar dateByAddingComponents:dateComps toDate:inputDate options:0];
    return newDate;
}
- (NSDate *)dateAtBeginningOfDayForDate:(NSDate *)inputDate {
    
    // Use the user's current calendar and time zone
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSTimeZone *timeZone = [NSTimeZone systemTimeZone];
    [calendar setTimeZone:timeZone];
    
    // Selectively convert the date components (year, month, day) of the input date
    NSDateComponents *dateComps = [calendar components:NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit fromDate:inputDate];
    
    // Set the time components manually
    [dateComps setHour:0];
    [dateComps setMinute:0];
    [dateComps setSecond:0];
    
    // Convert back
    NSDate *beginningOfDay = [calendar dateFromComponents:dateComps];
    return beginningOfDay;
}

- (void)calculateTotalValues {
    int timeTotal = 0;
    int cupTotal = 0;
    int taskTotal = 0;
    
    for (ZFExpereinceHistoryItem *item in self.expereinceList) {
        int amountToAdd = item.amount.intValue;
        if ([item.type isEqualToString:kEventTypeVisit]) {
            timeTotal += amountToAdd;
        } else if ([item.type isEqualToString:kEventTypeAchivement]) {
            cupTotal += amountToAdd;
        } else if ([item.type isEqualToString:kEventTypeTask]) {
            taskTotal += amountToAdd;
        }
    }
    
    self.timeBadgeValue.text = [NSString stringWithFormat:@"%d", timeTotal];
    self.cupBadgeValue.text = [NSString stringWithFormat:@"%d", cupTotal];
    self.taskBadgeValue.text = [NSString stringWithFormat:@"%d", taskTotal];
}

- (void)updateDependingOnTabState {
    if (self.historyTabSelected) {
        self.tableView.hidden = NO;
        self.blueRibbonsContainer.hidden = YES;
    } else {
        self.tableView.hidden = YES;
        self.blueRibbonsContainer.hidden = NO;
    }
}

#pragma mark - Notifications

- (void)memberVenueDetailsRefreshed {
    [self setupWithUser:CurrentUser];
}

- (void)toggleButtonPressed:(UIButton *)aButton {
    if(!aButton.isSelected) {
        if([aButton isEqual:allButton]) {
            topImageView.image = [UIImage imageNamed:kExperiencePointsViewAllImageName];
            [historyButton setSelected:NO];
            historyButton.userInteractionEnabled = YES;
            [aButton setSelected:YES];
            aButton.userInteractionEnabled = NO;
            self.historyTabSelected = NO;
            [self update];
        } else if([aButton isEqual:historyButton]) {
            topImageView.image = [UIImage imageNamed:kExperiencePointsViewHistoryImageName];
            [allButton setSelected:NO];
            allButton.userInteractionEnabled = YES;
            [aButton setSelected:YES];
            aButton.userInteractionEnabled = NO;
            self.historyTabSelected = YES;
            [self update];
        }
    }
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    NSDate *dateRepresentingThisDay = [self.sortedDays objectAtIndex:section];
    NSArray *eventsOnThisDay = [self.sections objectForKey:dateRepresentingThisDay];
    
    return [eventsOnThisDay count];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    NSInteger sections = self.sections.count;
    
    return sections;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    ZFExpereinceHistoryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellIdentifier
                                                                             forIndexPath:indexPath];
    
    NSDate *dateRepresentingThisDay = [self.sortedDays objectAtIndex:indexPath.section];
    
    NSArray *eventsOnThisDay = [self.sections objectForKey:dateRepresentingThisDay];
    ZFExpereinceHistoryItem *event = [eventsOnThisDay objectAtIndex:indexPath.row];
    
    NSString *activityType = nil;

    if ([event.type isEqualToString:kEventTypeVisit]) {
        
        NSString *ziferblatPlaceholder = NSLocalizedString(@"Ziferblat", @"Experence Points View - placeholder for Visit (%d for being at %@)");
        if (event.associatedText && event.associatedText.length > 0) {
            ziferblatPlaceholder = event.associatedText;
        }
        activityType = [NSString stringWithFormat:NSLocalizedString(@"%d for being at %@", @"Expereince Points Table row"), event.amount.intValue, ziferblatPlaceholder];
        cell.icon.image = [UIImage imageNamed:kHistoryItemPictureVisit];
        
    } else if ([event.type isEqualToString:kEventTypeTask]) {
        
        NSString *taskPlaceholder = NSLocalizedString(@"compliting task", @"Experence Points View - placeholder for Task");
        if (event.associatedText && event.associatedText.length > 0) {
            taskPlaceholder = event.associatedText;
        }
        activityType = [NSString stringWithFormat:NSLocalizedString(@"%d for %@", @"Expereince Points Table row"), event.amount.intValue, taskPlaceholder];
        cell.icon.image = [UIImage imageNamed:kHistoryItemPictureTask];
        
    } else if ([event.type isEqualToString:kEventTypeAchivement]) {
        
        NSString *achivementPlaceholder = NSLocalizedString(@"being volonteer", @"Experence Points View - placeholder for Achivement");
        if (event.associatedText && event.associatedText.length > 0) {
            achivementPlaceholder = event.associatedText;
        }
        activityType = [NSString stringWithFormat:NSLocalizedString(@"%d for %@", @"Expereince Points Table row"), event.amount.intValue, achivementPlaceholder];
        cell.icon.image = [UIImage imageNamed:kHistoryItemPictureAchivement];
    }
    
    cell.label.text = activityType;
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return kTableRowHeight;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return  kTableHeaderHeight;
}

- (nullable UIView *)tableView:(UITableView *)tableView
        viewForHeaderInSection:(NSInteger)section {
    ZFExperienceHistoryHeader *header = [self.tableView dequeueReusableHeaderFooterViewWithIdentifier:kHeaderIdentifier];
    
    NSDate *dateRepresentingThisDay = [self.sortedDays objectAtIndex:section];
    NSString *title = [self.sectionDateFormatter stringFromDate:dateRepresentingThisDay];
    header.label.text = title;
    
    return header;
}

#pragma mark - Utils

- (void)setupWithUser:(ZFUser *)aUser {
    levelLabel.text =[NSString stringWithFormat:NSLocalizedString(@"Lv %@",nil), aUser.venueDetails.level.stringValue];
}

@end
