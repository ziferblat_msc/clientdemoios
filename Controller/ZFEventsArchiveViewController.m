//
//  ZFEventsArchiveViewController.m
//  Ziferblat
//
//  Created by Terry Michel on 14/04/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "ZFEventsArchiveViewController.h"
#import "ZFEventsCell.h"

@interface ZFEventsArchiveViewController()
@property (nonatomic, strong) NSArray *archivedEvents;
@end

@implementation ZFEventsArchiveViewController

#pragma mark -
#pragma mark Lifecycle

- (void)loadView {
    contentView = [[ZFEventsArchiveView alloc] init];
    self.view = contentView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    TARGET_TOUCH_UP(contentView.backButton, @selector(backButtonTapped))
    [contentView.tableView setController:self];
}

#pragma mark - 
#pragma makr Buttons actions

- (void)backButtonTapped {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)interestingButtonTapped:(UIButton *)aButton {
#warning TO IMPLEMENT
}

#pragma mark - 
#pragma mark UITableViewDataSource

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ZFEventsCell *cell = [tableView dequeueReusableCellWithIdentifier:[ZFEventsCell reuseIdentifier]];
    [cell setupCellWithEvent:self.archivedEvents[indexPath.row] indexPath:indexPath];
    
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
#warning UPDATE WHEN WEBSERVICE READY
    return 0;
}

#pragma mark - 
#pragma mark UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
#warning UPDATE WHEN WEBSERVICE READY
    return 137.0;
}

@end

