//
//  ZFDiscussionCommentsViewController.m
//  Ziferblat
//
//  Created by Terry Michel on 08/04/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "ZFConversationDetailViewController.h"
#import "ZFBaseView.h"
#import "ZFBackButton.h"
#import "ZFGetConversationDetailsRequest.h"
#import "ZFSendMessageRequest.h"
#import "ZFOtherProfileViewController.h"
#import "ZFGetMemberFriendsListRequest.h"
#import "ZFGetMemberVenueDetailsRequest.h"
#import "ZFVenue.h"

#define kNameLabelFrame             (notIPad ? CGRectMake(45, 11, 232, 26):CGRectMake(108, 26, 557, 62))
#define kMessageButtonFrame         (notIPad ? CGRectMake(264, 7, 50, 35):CGRectMake(634, 17, 120, 84))
#define kMessageTextViewFrame       CGRectMake(17, 7, 240, 29)
#define kPlaceholderTypeMessage     NSLocalizedString(@"Start typing...", nil)
#define kTopPadding                 (notIPad ? 16:38)
#define kBackButtonHeight           (notIPad ? 26:48)
#define kDiscussionTableViewHeight  (kHeight - nameLabel.bottom - kBackButtonHeight - messagePanelImageView.height - (notIPad ?kPadding:44))
#warning SPECIFY TEXT LIMIT
#define kTextLimit                  4000

@interface ZFConversationDetailViewController()<UIGestureRecognizerDelegate, UITextViewDelegate, UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, copy) NSString *backButtonMessage;

@end

@implementation ZFConversationDetailViewController {
    ZFUser *toUser;
    UILabel *nameLabel;
    UITextView *messageTextView;
    UILabel *messagePlaceholderLabel;
    UIImageView *messagePanelImageView;
    UIButton *sendButton;
    NSMutableArray *messagesSortedByDate;
    UITableView *discussionTableView;
    NSUInteger currentPage;
    BOOL isFetchingNextPage;
    CGFloat lastTableViewYContentOffset;
}

#pragma mark -
#pragma mark Lifecycle

- (instancetype)initWithUser:(ZFUser *)aUser {
    self = [super init];
    if(self) {
        toUser = aUser;
        self.backButtonMessage = NSLocalizedString(@"BACK TO CONVERSATIONS", nil);
        messagesSortedByDate = [NSMutableArray new];
        currentPage = 0;
        isFetchingNextPage = YES;
        lastTableViewYContentOffset = 0;
    }
    return self;
}

- (instancetype)initWithUser:(ZFUser *)aUser backMessage:(NSString *)backMessage {
    if (self = [self initWithUser:aUser]) {
        self.backButtonMessage = backMessage;
    }
    return self;
}

- (void)loadView {
    self.view = [[ZFBaseView alloc] init];
    kScrollView.contentSize = CGSizeMake(self.view.width, self.view.height);
    
    ZFBackButton *backButton = [ZFBackButton backButtonWithTitle:self.backButtonMessage];
    [self.view addSubview:backButton];
    TARGET_TOUCH_UP(backButton, @selector(backButtonPressed));
    
    UIImageView *backgroundImageView = [UIImageView imageViewWithImageNamed:@"ConversationDetailBackground"];
    [kScrollView addSubview:backgroundImageView];
    backgroundImageView.top = backButton.bottom - 1;
    backgroundImageView.userInteractionEnabled = YES;
    
    nameLabel = [[UILabel alloc]initWithFrame:kNameLabelFrame];
    nameLabel.text = toUser.fullName;
    nameLabel.textAlignment = NSTextAlignmentCenter;
    nameLabel.font = FontRockWell(notIPad ? 17:41);
    nameLabel.adjustsFontSizeToFitWidth = YES;
    nameLabel.minimumScaleFactor = 0.5;
    [backgroundImageView addSubview:nameLabel];
    
    messagePanelImageView = [UIImageView imageViewWithImageNamed:@"ConversationMessagePanel"];
    [self.view addSubview:messagePanelImageView];
    messagePanelImageView.userInteractionEnabled = YES;
    messagePanelImageView.bottom = self.view.height;
    
    sendButton = [UIButton buttonWithType:UIButtonTypeCustom];
    sendButton.frame = kMessageButtonFrame;
    [messagePanelImageView addSubview:sendButton];
    TARGET_TOUCH_UP(sendButton, @selector(sendButtonPressed))
    
    messageTextView = [UITextView textViewWithText:@""
                                              font:FontRockWell(notIPad ? 12.5:30)
                                         textColor:Colour_Black
                                          editable:YES
                                      contentInset:0
                                             frame:(notIPad ? CGRectMake(17, 7, 240, 35):CGRectMake(41, 17, 576, 84))
                                     textAlignment:NSTextAlignmentLeft
                                     returnKeyType:UIReturnKeyDefault
                                      keyboardType:UIKeyboardTypeDefault
                                   autoCorrectMode:UITextAutocorrectionTypeDefault
                                               tag:0
                                        parentView:messagePanelImageView];
    messageTextView.delegate = self;
    messageTextView.autocapitalizationType = UITextAutocapitalizationTypeSentences;
    messageTextView.textContainerInset = (notIPad ? UIEdgeInsetsMake(kPadding, kPadding, kPadding, kPadding):UIEdgeInsetsMake(24, 19, 24, 24));
    messageTextView.backgroundColor = Colour_White;
    [messageTextView setCenterY:sendButton.center.y];
    
    CGFloat cursorPadding = (notIPad ? 8:19);
    messagePlaceholderLabel = [UILabel labelWithText:kPlaceholderTypeMessage
                                                font:FontRockWell(notIPad ? 12.5:30)
                                              colour:Colour_AppVeryLightGray
                                       textAlignment:NSTextAlignmentLeft
                                       numberOfLines:1
                                               frame:(notIPad ? CGRectMake(17+cursorPadding, 7, 240, 35):CGRectMake(41+cursorPadding, 17, 576, 84))
                                          resizeType:UILabelResizeType_none
                                           addToView:messagePanelImageView];
    
    discussionTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, nameLabel.bottom + kMediumPadding, kScrollView.width, kDiscussionTableViewHeight) style:UITableViewStylePlain];
    discussionTableView.contentInset = UIEdgeInsetsMake(kTopPadding, 0, 0, 0);
    discussionTableView.allowsSelection = NO;
    discussionTableView.delegate = self;
    discussionTableView.dataSource = self;
    discussionTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    discussionTableView.backgroundColor = Colour_Clear;
    [backgroundImageView addSubview:discussionTableView];
    
    [UITapGestureRecognizer gestureWithTarget:self selector:@selector(tapGestureDetected:) addToView:kScrollView];
    [NetworkService pollForType:ZFPollingRequestType_ConversationDetails memberId:toUser.uuid venueId:nil summary:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    Notification_Observe(UIKeyboardWillChangeFrameNotification, updateLayoutForKeyboard:);
    Notification_Observe(kLOConnectivityManager_connectivityOnline, enableChat);
    Notification_Observe(kLOConnectivityManager_connectivityOffline, disableChat);
    Notification_Observe(kNotification_ConversationDetailsRefreshed, conversationRefreshed);
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    currentPage = 0;
    [self getConversationsListAnimated:YES];
}

- (void)dealloc {
    Notification_RemoveObserver;
}

#pragma mark -
#pragma mark Notifications

- (void)conversationRefreshed {
    NSArray *updatedMessages = [NetworkService.conversationMessages sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        return [self compararisonResultForMessage1:obj1 message2:obj2];
    }];
    if(![[updatedMessages lastObject] isEqual:[messagesSortedByDate lastObject]]) {
        [discussionTableView beginUpdates];
        NSMutableArray *newIndexPath = [NSMutableArray new];
        NSUInteger i=messagesSortedByDate.count;
        for(ZFMessage *message in updatedMessages) {
            if(![messagesSortedByDate containsObject:message]) {
                [messagesSortedByDate insertObject:message atIndex:i];
                [newIndexPath addObject:[NSIndexPath indexPathForRow:i inSection:0]];
                i++;
            }
        }
        [discussionTableView insertRowsAtIndexPaths:newIndexPath withRowAnimation:UITableViewRowAnimationAutomatic];
        [discussionTableView endUpdates];
        if(messagesSortedByDate.count > 0) {
            [discussionTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForItem:(messagesSortedByDate.count-1) inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:YES];
        }
    }
}

#pragma mark -
#pragma mark Buttons actions

- (void)refreshData {
    [self getConversationsListAnimated:NO];
}

- (void)backButtonPressed {
    if(self.navigationController) {
        [self.navigationController popViewControllerAnimated:YES];
    } else {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

- (void)sendButtonPressed {
    [self sendMessage];
}

#pragma mark -
#pragma mark TableView

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ZFMessage *aMessage = [messagesSortedByDate objectAtIndex:indexPath.row];
    
    //    [user isEqual:UserService.loggedInUser]
    ZFConversationDetailCell *cell;
    if ([aMessage.fromMember isEqual:UserService.loggedInUser]) {
        cell = [tableView dequeueReusableCellWithIdentifier:[ZFConversationDetailRightCell reuseIdentifier]];
        
        if(!cell) {
            cell = [ZFConversationDetailRightCell cell];
            [cell setDelegate:self];
        }
    } else {
        cell = [tableView dequeueReusableCellWithIdentifier:[ZFConversationDetailCell reuseIdentifier]];
        
        if(!cell) {
            cell = [ZFConversationDetailCell cell];
            [cell setDelegate:self];
        }
    }
    [cell setupWithMessage:aMessage];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [ZFConversationDetailCell heightForMessage:[messagesSortedByDate objectAtIndex:indexPath.row]];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return messagesSortedByDate.count;
}

#pragma mark -
#pragma mark ScrollView

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if([scrollView isEqual:discussionTableView]) {
        if(scrollView.contentOffset.y < 0 && !isFetchingNextPage && scrollView.contentOffset.y < lastTableViewYContentOffset && currentPage != 0) {
            isFetchingNextPage = YES;
            [self getConversationsListAnimated:NO];
        }
        lastTableViewYContentOffset = scrollView.contentOffset.y;
    }
}

#pragma mark -
#pragma mark UITextViewDelegate

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    BOOL aux = YES;
    NSString *output = [textView.text stringByReplacingCharactersInRange:range withString:text];
    aux = output.length < kTextLimit;
//    aux &= ![text isEqualToString:@"\n"];
    
    sendButton.enabled = output.isValidString;
    if(output.length > 0) {
        [messagePlaceholderLabel hide];
    } else {
        [messagePlaceholderLabel show];
    }

    return aux;
}

- (void)textViewDidEndEditing:(UITextView *)textView {
    if(messageTextView.text == nil || [messageTextView.text isEqualToString:@""]) {
        [messagePlaceholderLabel show];
        messagePlaceholderLabel.text = kPlaceholderTypeMessage;
    }
}

#pragma mark -
#pragma mark UIGestureRecognizerDelegate

- (void)tapGestureDetected:(UITapGestureRecognizer *)gesture {
    switch (gesture.state) {
        case UIGestureRecognizerStateEnded:
            [self.view endEditing:YES];
            break;
        default:
            break;
    }
}

#pragma mark -
#pragma mark Request Methods

- (void)sendMessage {
    NSString *message = messageTextView.text;
    
    if (![message isEqualToString:@""]) {
        [self disableChat];
        [ZFSendMessageRequest requestWithMemberId:toUser.uuid content:message completion:^(LORESTRequest *aRequest) {
            if(aRequest.resultStatus == LORESTRequestResultStatusSucceeded) {
                ZFMessage *messageSent = aRequest.result;
                [discussionTableView beginUpdates];
                [messagesSortedByDate addObject:messageSent];
                [discussionTableView insertRowsAtIndexPaths:@[[NSIndexPath indexPathForItem:(messagesSortedByDate.count-1) inSection:0]] withRowAnimation:UITableViewRowAnimationAutomatic];
                [discussionTableView endUpdates];
                [discussionTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForItem:(messagesSortedByDate.count-1) inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:YES];
            }
            [self enableChat];
        }];
    }
}

- (void)getConversationsListAnimated:(BOOL)animated {
    [ZFGetConversationDetailsRequest requestWithMemberId:toUser.uuid page:@(currentPage) completion:^(LORESTRequest *aRequest) {
        if(aRequest.resultStatus == LORESTRequestResultStatusSucceeded) {
            ZFPagedResults *pagedResults = aRequest.result;
            NSArray *sortedResults = [pagedResults.result sortedArrayUsingComparator:^NSComparisonResult(ZFMessage *obj1, ZFMessage *obj2) {
                return [self compararisonResultForMessage1:obj1 message2:obj2];
            }];
            if(currentPage == 0) {
                [messagesSortedByDate removeAllObjects];
                [messagesSortedByDate addObjectsFromArray:sortedResults];
                [UIView transitionWithView:discussionTableView
                                  duration:(animated ? 0.35f : 0.0f)
                                   options:UIViewAnimationOptionTransitionCrossDissolve
                                animations:^{
                                    [discussionTableView reloadData];
                                    if(messagesSortedByDate.count > 0) {
                                        [discussionTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForItem:(messagesSortedByDate.count-1) inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:!animated];
                                    }
                                } completion:nil];
            } else {
                NSMutableIndexSet *indexes = [NSMutableIndexSet new];
                NSMutableArray *indexPaths = [NSMutableArray new];
                for(int i=0; i<sortedResults.count; i++) {
                    [indexes addIndex:i];
                    [indexPaths addObject:[NSIndexPath indexPathForItem:i inSection:0]];
                }
                if(![[messagesSortedByDate firstObject] isEqual:[sortedResults firstObject]]) {
                    [messagesSortedByDate insertObjects:sortedResults atIndexes:indexes];
                    [discussionTableView reloadData];
                    [discussionTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:[sortedResults count] inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:NO];
                }
            }
            if(pagedResults.hasNext) {
                currentPage++;
            }
            isFetchingNextPage = NO;
        }
    }];
}

#pragma mark -
#pragma mark Notifications

- (void)updateLayoutForKeyboard:(NSNotification *)aNotification {
    NSInteger heightKeyboard = [aNotification keyboardHeight];
    [UIView animateWithDuration:[aNotification keyboardAnimationDuration] animations:^{
        if(heightKeyboard) {
            discussionTableView.height = kDiscussionTableViewHeight - heightKeyboard;
            messagePanelImageView.bottom = self.view.height - heightKeyboard;
            if(messagesSortedByDate.count > 0) {
                [discussionTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForItem:(messagesSortedByDate.count-1) inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:NO];
            }
        } else {
            discussionTableView.height = kDiscussionTableViewHeight;
            messagePanelImageView.bottom = self.view.height;
        }
    }];
}

#pragma mark -
#pragma mark Utils

- (NSComparisonResult)compararisonResultForMessage1:(ZFMessage *)aMessage1 message2:(ZFMessage *)aMessage2 {
    return [aMessage1.dateSent compare:aMessage2.dateSent];
}

- (void)enableChat {
    sendButton.userInteractionEnabled = YES;
    messageTextView.userInteractionEnabled = YES;
    messagePlaceholderLabel.text = kPlaceholderTypeMessage;
    [messagePlaceholderLabel show];
}

- (void)disableChat {
    messageTextView.text = @"";
    [messagePlaceholderLabel show];
    messagePlaceholderLabel.text = kPlaceholderTypeMessage;
    sendButton.userInteractionEnabled = NO;
}

#pragma mark -
#pragma mark Cell Delegate Methods

- (void)showProfile:(ZFUser *)user {
    
    if ([messageTextView isFirstResponder]) {
        [messageTextView resignFirstResponder];
    }
    
    [SVProgressHUD showWithStatus:NSLocalizedString(@"Loading member...",nil) maskType:SVProgressHUDMaskTypeGradient];
    [ZFGetMemberFriendsListRequest requestWithMemberId:user.uuid andVenueId:CurrentUser.selectedVenue.uuid completion:^(LORESTRequest *aRequest) {
        if (aRequest.resultStatus == LORESTRequestResultStatusSucceeded) {
            user.friendsList = aRequest.result;
            user.friendsLoaded = TRUE;
            [self checkLoaded:user];
        } else {
            Show_ConnectionError
        }
    }];
    
    [ZFGetMemberVenueDetailsRequest requestWithMemberId:user.uuid venueId:CurrentUser.selectedVenue.uuid completion:^(LORESTRequest *aRequest) {
        if (aRequest.resultStatus == LORESTRequestResultStatusSucceeded) {
            user.venueDetails = aRequest.result;
            [self checkLoaded:user];
        } else {
            Show_ConnectionError
        }
    }];
}

- (void)checkLoaded:(ZFUser *)aUser {
    if ((aUser.venueDetails == nil) || (!aUser.friendsLoaded)) {
        return;
    }
    
    Hide_HudAndPerformBlock(^{
        if ([aUser.uuid isEqualToNumber:CurrentUser.uuid]) {
            ZFProfileViewController *profileViewController = [[ZFProfileViewController alloc] initWithUser:CurrentUser
                                                           backMessage:NSLocalizedString(@"BACK TO CONVERSATION", @"Back button title for conversations")
                                                             andFilter:NO
                                                            forVenueId:nil];
            profileViewController.canBeDismissed = YES;
            [self presentViewController:profileViewController animated:YES completion:nil];
            
        } else {
            ZFOtherProfileViewController *otherProfileViewController = [[ZFOtherProfileViewController alloc ] initWithUser:aUser
                                                                 backMessage:NSLocalizedString(@"BACK TO CONVERSATION", @"Back button title for conversations")
                                                                   andFilter:NO
                                                                  forVenueId:nil];
            otherProfileViewController.canBeDismissed = YES;
            [self presentViewController:otherProfileViewController animated:YES completion:nil];
        }
    });
}

@end
