//
//  ZFZiferblatDetailsViewController.h
//  Ziferblat
//
//  Created by Terry Michel on 08/04/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "ZFBaseView.h"
#import "LOPagingScrollView.h"
#import "ZFHeaderWithCaptionView.h"
#import "ZFViewController.h"

@class ZFVenue, LOPageControl;

@interface ZFZiferblatDetailsViewController : ZFViewController<MKMapViewDelegate, LOPagingScrollViewDatasource> {
    UILabel *headerLabel;
    UILabel *descriptionLabel;
    UILabel *membersPartOneLabel;
    UILabel *membersLabel;
    UILabel *membersPartTwoLabel;
    UILabel *peopleInsideLabel;
    UILabel *addressLabel;
    UIView *teamContainerView;
    UIButton *leftArrowButton;
    UIButton *rightArrowButton;
    ZFHeaderWithCaptionView *teamHeaderCaptionView;
    UIImageView *footerImageView;
    UILabel *stopCheckLabel;
    UIImageView *stopCheckImageView;
    UILabel *piastresLabel;
    UIImageView *piastresIcon;
    MKMapView *mapView;
    UIButton *mapButton;
    UIImageView *addressBackgroundImageView;
    UIImageView *addressSeparatorImageView;
    UIView *shareContainerView;
    UIImageView *shareBackgroundImageView;
    ZFVenue *ziferblat;
    NSUInteger imageIndex;
    NSMutableArray *ziferblatImagesViews;
    NSMutableArray *profileImages;
    LOPagingScrollView *pagingScrollView;
    LOPageControl *pageControl;
    UIButton *instagramButton;
    UIButton *facebookButton;
    UIButton *russianFacebookButton;
    UIButton *twitterButton;
    UIButton *ziferblatButton;
}

#pragma mark - Instance
- (instancetype)initWithVenue:(ZFVenue *)venue;

@end
