//
//  ZFNewPasswordViewController.h
//  Ziferblat
//
//  Created by Jose Fernandez on 29/07/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZFNewPasswordViewController : UIViewController
@property (nonatomic, strong) NSString *email;

//Constraints
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topNavigationControlHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *resetPasswordLabelYConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *resetPasswordLabelWidthConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *resetPasswordLabelHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *instructionsLabelYConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *instructionsLabelWidthConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *subInstructionsLabelWidhtConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *secretCodeTextFieldYConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *secretCodeTextFieldWidthConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *secretCodeTextFieldHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *passwordTextFieldYConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *confirmTextFieldYConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *buttonYConstraint;

@end
