//
//  ZFZiferblatDetailsViewController.m
//  Ziferblat
//
//  Created by Terry Michel on 08/04/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "ZFZiferblatDetailsViewController.h"
#import "ZFHeaderWithCaptionView.h"
#import "ZFOtherProfileViewController.h"
#import "ZFGetMemberFriendsListRequest.h"
#import "ZFGetMemberVenueDetailsRequest.h"
#import "ZFVenue.h"
#import "ZFBackButton.h"
#import "LOPageControl.h"
#import "ZFProfileImage.h"
#import "ZFGetOccupantsForVenueRequest.h"
#import "ZFWhoIsAtZiferblatViewController.h"
#import "ZFBackButton.h"
#import "ZFProfileImageCollectionViewCell.h"
#import "LOImageCache.h"
#import "LOImageDownloader.h"

@import SafariServices;

@interface ZFZiferblatDetailsViewController () <UICollectionViewDataSource, UICollectionViewDelegate>

@property (nonatomic, strong) UICollectionView *teamCollectionView;
@property (nonatomic, strong) NSArray *teamArray;

@end

@implementation ZFZiferblatDetailsViewController

@synthesize teamArray;

#define kBannerTopMargin        (notIPad ? 14:34)
#define kHeaderLabelFrame       (notIPad ? CGRectMake(27, 18, 255, 28):CGRectMake(65, 43, 612, 67))
#define kNameLabelFrame         CGRectMake(40, 40, 160, 26)
#define kPageControlSize        (notIPad ? CGSizeMake(257, 172):CGSizeMake(617, 413))
#define kMemberPartOneLabelFrame       (notIPad ? CGRectMake(114, 30, 92, 30):CGRectMake(274, 72, 221, 72))
#define kMemberLabelFrame              (notIPad ? CGRectMake(114, 52, 92, 30):CGRectMake(274, 125, 221, 72))
#define kMemberPartTwoLabelFrame       (notIPad ? CGRectMake(114, 65, 92, 60):CGRectMake(274, 156, 221, 144))
#define kMapViewSize            (notIPad ? 257:617)
#define kMapViewTopMargin       (notIPad ? 195:468)
#define kPeopleInsideLabelFrame (notIPad ? CGRectMake(49, 119, 222, 32):CGRectMake(118, 290, 533, 77))
#define kShareContainerFrame    CGRectMake(0, 0, kScrollView.width, 80)
#define kShareButtonSize        CGSizeMake(52, 52)
#define kStopCheckLabelFrame    (notIPad ? CGRectMake(92, 22, 165, 15):CGRectMake(221, 53, 396, 36))
#define kPiastresLabelFrame     CGRectMake(stopCheckLabel.left, kStopCheckLabelFrame.origin.y+kStopCheckLabelFrame.size.height, (notIPad ? 85:204), kStopCheckLabelFrame.size.height)
#define kMapHeaderPoint         (notIPad ? CGPointMake(0, 464):CGPointMake(0, 1114))

#pragma mark - 
#pragma mark Lifecycle

- (instancetype)initWithVenue:(ZFVenue *)venue {
    self = [super init];
    if(self) {
        ziferblat = venue;
        imageIndex = 0;
        ziferblatImagesViews = [[NSMutableArray alloc] init];
        teamArray = @[];
    }
    return self;
}

- (void)loadView {
    ZFBaseView *aView = [[ZFBaseView alloc] init];
    self.view = aView;
    
    ZFBackButton *backButton = [ZFBackButton backButtonWithTitle:NSLocalizedString(@"BACK TO ROOM", nil)];
    [self.view addSubview:backButton];
    TARGET_TOUCH_UP(backButton, @selector(backButtonTapped))

    UIImageView *headerImageView = [UIImageView imageViewWithImageNamed:@"ZiferblatDetailsBanner"];
    [kScrollView addSubview:headerImageView];
    headerImageView.top = backButton.bottom + kBannerTopMargin;
    [headerImageView centerHorizontallyInSuperView];
    
    headerLabel = [[UILabel alloc]initWithFrame:kHeaderLabelFrame];
    headerLabel.text = ziferblat.name;
    headerLabel.textAlignment = NSTextAlignmentCenter;
    headerLabel.font = FontRockWell((notIPad ? 24:58));
    headerLabel.textColor = Colour_White;
    headerLabel.adjustsFontSizeToFitWidth = YES;
    headerLabel.minimumScaleFactor = 0.5;
    [headerImageView addSubview:headerLabel];
    
    pagingScrollView = [[LOPagingScrollView alloc] initWithFrame:CGRectMake(0,
                                                                            headerImageView.bottom+kMediumPadding,
                                                                            kPageControlSize.width,
                                                                            kPageControlSize.height)];
    [kScrollView addSubview:pagingScrollView];
    [pagingScrollView setShowsHorizontalScrollIndicator:NO];
    [pagingScrollView centerHorizontallyInSuperView];
    pagingScrollView.datasource = self;
    
    leftArrowButton = [UIButton buttonWithFrame:CGRectMake(0, 0, kBackButtonWidth, kBackButtonHeight)
                                    isExclusive:YES
                          showsTouchOnHighlight:NO
                                           font:nil
                                    normalTitle:nil
                              normalTitleColour:nil
                                  selectedTitle:nil
                            selectedTitleColour:nil
                                normalImageName:RESOURCE_LEFT_ARROW
                              selectedImageName:nil
                      normalBackgroundImageName:nil
                    selectedBackgroundImageName:nil
                                      addToView:kScrollView];
    [leftArrowButton setCenterY:pagingScrollView.center.y];
    TARGET_TOUCH_UP(leftArrowButton, @selector(leftArrowButtonTapped:))

    rightArrowButton = [UIButton buttonWithFrame:CGRectMake(self.view.width-kBackButtonWidth, 0, kBackButtonWidth, kBackButtonHeight)
                                     isExclusive:YES
                           showsTouchOnHighlight:NO
                                            font:nil
                                     normalTitle:nil
                               normalTitleColour:nil
                                   selectedTitle:nil
                             selectedTitleColour:nil
                                 normalImageName:RESOURCE_RIGHT_ARROW
                               selectedImageName:nil
                       normalBackgroundImageName:nil
                     selectedBackgroundImageName:nil
                                       addToView:kScrollView];
    [rightArrowButton setCenterY:pagingScrollView.center.y];
    TARGET_TOUCH_UP(rightArrowButton, @selector(rightArrowButtonTapped:))
    
    pageControl = [LOPageControl pageControlWithActiveImageName:RESOURCE_PAGE_CONTROL_ON
                                           andInactiveImageName:RESOURCE_PAGE_CONTROL_OFF];
    [kScrollView addSubview:pageControl];
    pageControl.top = pagingScrollView.bottom+kMediumPadding;
    [pageControl centerHorizontallyInSuperView];
    
    ZFHeaderWithCaptionView *descritionHeaderCaptionView = [ZFHeaderWithCaptionView headerWithCaption:NSLocalizedString(@"Description", nil)
                                                                                               origin:CGPointZero
                                                                                            addToView:kScrollView];
    descritionHeaderCaptionView.top = pageControl.bottom+kMediumPadding;
    [descritionHeaderCaptionView centerHorizontallyInSuperView];
    
    descriptionLabel = [UILabel labelWithText:@""
                                         font:FontRockWell((notIPad ? 13:31))
                                       colour:Colour_Black
                                textAlignment:NSTextAlignmentLeft
                                numberOfLines:0
                                        frame:CGRectMake(pagingScrollView.left,
                                                         descritionHeaderCaptionView.bottom+ (notIPad ? kPadding : 24),
                                                         pagingScrollView.width,
                                                         0)
                                   resizeType:UILabelResizeType_none
                                    addToView:kScrollView];
    
    teamHeaderCaptionView = [ZFHeaderWithCaptionView headerWithCaption:NSLocalizedString(@"Team", nil)
                                                                                               origin:CGPointZero
                                                                                            addToView:kScrollView];
    [teamHeaderCaptionView centerHorizontallyInSuperView];
    
    teamContainerView = [UIView viewWithColor:Colour_Clear
                                        frame:CGRectMake(pagingScrollView.left,
                                                         teamHeaderCaptionView.bottom + (notIPad ? kPadding : 24),
                                                         pagingScrollView.width,
                                                         (notIPad ? 100 : 240))
                                    addToView:kScrollView];
    [self setupCollectionView];
    
    mapView = [[MKMapView alloc] initWithFrame:CGRectMake(0, 0, kMapViewSize, kMapViewSize)];
    [kScrollView addSubview:mapView];
    mapView.userInteractionEnabled = NO;
    [mapView centerHorizontallyInSuperView];
    mapView.delegate = self;

    mapButton = [UIButton buttonWithFrame:mapView.frame
                              isExclusive:YES
                    showsTouchOnHighlight:NO
                                     font:nil
                              normalTitle:nil
                        normalTitleColour:nil
                            selectedTitle:nil
                      selectedTitleColour:nil
                          normalImageName:nil
                        selectedImageName:nil
                normalBackgroundImageName:nil
              selectedBackgroundImageName:nil
                                addToView:kScrollView];
    TARGET_TOUCH_UP(mapButton, @selector(mapButtonTapped))

    footerImageView = [UIImageView imageViewWithImageNamed:RESOURCE_ZIFERBLAT_DETAILS_FOOTER
                                                    origin:CGPointZero
                                                 addToView:kScrollView];
    
    membersPartOneLabel = [UILabel labelWithText:NSLocalizedString(@"There are", nil)
                                            font:(notIPad ? FontRockWell(14):FontRockWell(34))
                                   colour:Colour_Black
                            textAlignment:NSTextAlignmentCenter
                            numberOfLines:1
                                    frame:kMemberPartOneLabelFrame
                               resizeType:UILabelResizeType_none
                                addToView:footerImageView];
    
    membersLabel = [UILabel labelWithText:nil
                                     font:(notIPad ? FontRockWell(24):FontRockWell(58))
                                   colour:Colour_Black
                            textAlignment:NSTextAlignmentCenter
                            numberOfLines:1
                                    frame:kMemberLabelFrame
                               resizeType:UILabelResizeType_none
                                addToView:footerImageView];
    
    membersPartTwoLabel = [UILabel labelWithText:NSLocalizedString(@"members.\nSee all", nil)
                                     font:(notIPad ? FontRockWell(14):FontRockWell(34))
                                   colour:Colour_Black
                            textAlignment:NSTextAlignmentCenter
                            numberOfLines:2
                                    frame:kMemberPartTwoLabelFrame
                               resizeType:UILabelResizeType_none
                                addToView:footerImageView];
    
    peopleInsideLabel = [UILabel labelWithText:nil
                                          font:(notIPad ? FontRockWell(14):FontRockWell(34))
                                        colour:Colour_Black
                                 textAlignment:NSTextAlignmentCenter
                                 numberOfLines:1
                                         frame:kPeopleInsideLabelFrame
                                    resizeType:UILabelResizeType_none
                                     addToView:footerImageView];
    
    UIButton *membersLabelButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIButton *membersPartOneButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIButton *membersPartTwoButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIButton *peopleInsideLabelButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [membersLabelButton setFrame:membersLabel.frame];
    [membersPartOneButton setFrame:membersPartOneLabel.frame];
    [membersPartTwoButton setFrame:membersPartTwoLabel.frame];
    [peopleInsideLabelButton setFrame:peopleInsideLabel.frame];
    
    TARGET_TOUCH_UP(membersLabelButton, @selector(teamButtonTapped:))
    TARGET_TOUCH_UP(membersPartOneButton, @selector(teamButtonTapped:))
    TARGET_TOUCH_UP(membersPartTwoButton, @selector(teamButtonTapped:))
    TARGET_TOUCH_UP(peopleInsideLabelButton, @selector(teamButtonTapped:))
    
    [footerImageView addSubview:membersLabelButton];
    [footerImageView addSubview:membersPartOneButton];
    [footerImageView addSubview:membersPartTwoButton];
    [footerImageView addSubview:peopleInsideLabelButton];
    
    footerImageView.userInteractionEnabled = YES;
    
    ZFHeaderWithCaptionView *mapHeaderCaptionView = [ZFHeaderWithCaptionView headerWithCaption:NSLocalizedString(@"Map", nil)
                                                                origin:kMapHeaderPoint
                                                             addToView:footerImageView];
    [mapHeaderCaptionView centerHorizontallyInSuperView];
    
    addressBackgroundImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.view.width, 0)];
    addressBackgroundImageView.image = [UIImage imageNamed:RESOURCE_BOTTOM_BACKGROUND];
    [kScrollView addSubview:addressBackgroundImageView];
    
    addressLabel = [UILabel labelWithText:@""
                                     font:FontRockWell((notIPad ? 12:29))
                                   colour:Colour_White
                            textAlignment:NSTextAlignmentLeft
                            numberOfLines:0
                                    frame:CGRectMake(teamContainerView.left, (notIPad ? kSmallPadding:12), teamContainerView.width, 0)
                               resizeType:UILabelResizeType_none
                                addToView:addressBackgroundImageView];
    
    [descriptionLabel sizeToWidthIsAttributed:NO];
    [addressLabel sizeToWidthIsAttributed:NO];
    teamHeaderCaptionView.top = descriptionLabel.bottom + (notIPad ? kPadding : 24);
    teamContainerView.top = teamHeaderCaptionView.bottom + (notIPad ? kPadding : 24);
    
    footerImageView.top = teamContainerView.bottom+(notIPad ? kPadding : 24);
    mapView.top = footerImageView.top+kMapViewTopMargin;
    mapButton.top = mapView.top;
    addressBackgroundImageView.top = footerImageView.bottom;
    addressBackgroundImageView.height = addressLabel.height+kSmallPadding*2;
    kScrollView.contentSize = CGSizeMake(kScrollView.width, addressBackgroundImageView.bottom);
    
    addressSeparatorImageView = [UIImageView imageViewWithImageNamed:@"HeaderDash"];
    [kScrollView addSubview:addressSeparatorImageView];
    
    stopCheckImageView = [UIImageView imageViewWithImageNamed:@"StopCheckBanner"];
    [addressBackgroundImageView addSubview:stopCheckImageView];
    [stopCheckImageView centerHorizontallyInSuperView];
    
    stopCheckLabel = [[UILabel alloc]initWithFrame:kStopCheckLabelFrame];
    stopCheckLabel.text = NSLocalizedString(@"Stop-check:",nil);
    stopCheckLabel.textAlignment = NSTextAlignmentCenter;
    stopCheckLabel.font = FontRockWell((notIPad ? 13:31));
    stopCheckLabel.textAlignment = NSTextAlignmentLeft;
    stopCheckLabel.adjustsFontSizeToFitWidth = YES;
    stopCheckLabel.minimumScaleFactor = 0.7;
    [stopCheckImageView addSubview:stopCheckLabel];
    
    piastresLabel = [[UILabel alloc]initWithFrame:kPiastresLabelFrame];
    piastresLabel.font = FontRockWell((notIPad ? 13:31));
    piastresLabel.textAlignment = NSTextAlignmentLeft;
    piastresLabel.adjustsFontSizeToFitWidth = YES;
    piastresLabel.minimumScaleFactor = 0.7;
    [stopCheckImageView addSubview:piastresLabel];
    
    piastresIcon = [UIImageView imageViewWithImageNamed:@"ZiferblatDetailsPiastresIcon"];
    [stopCheckImageView addSubview:piastresIcon];
    [piastresIcon setCenterY:piastresLabel.center.y];
    piastresIcon.left = piastresLabel.right + kSmallPadding;
    
    shareBackgroundImageView = [UIImageView imageViewWithImageNamed:@"StripesPattern"];
    shareBackgroundImageView.userInteractionEnabled = YES;
    [kScrollView addSubview:shareBackgroundImageView];
    
    shareContainerView = [UIView viewWithColor:Colour_Clear frame:CGRectZero addToView:shareBackgroundImageView];
   
    instagramButton = [UIButton buttonWithType:UIButtonTypeCustom];
    instagramButton.size = kShareButtonSize;
    [instagramButton setImage:[UIImage imageNamed:@"InstagramIcon"] forState:UIControlStateNormal];
    [shareContainerView addSubview:instagramButton];
    TARGET_TOUCH_UP(instagramButton, @selector(shareButtonPressed:))
    
    facebookButton = [UIButton buttonWithType:UIButtonTypeCustom];
    facebookButton.left = instagramButton.right;
    facebookButton.size = kShareButtonSize;
    [facebookButton setImage:[UIImage imageNamed:@"FacebookIcon"] forState:UIControlStateNormal];
    [shareContainerView addSubview:facebookButton];
    TARGET_TOUCH_UP(facebookButton, @selector(shareButtonPressed:))
    
    russianFacebookButton = [UIButton buttonWithType:UIButtonTypeCustom];
    russianFacebookButton.left = facebookButton.right;
    russianFacebookButton.size = kShareButtonSize;
    [russianFacebookButton setImage:[UIImage imageNamed:@"RussianFacebookIcon"] forState:UIControlStateNormal];
    [shareContainerView addSubview:russianFacebookButton];
    TARGET_TOUCH_UP(russianFacebookButton, @selector(shareButtonPressed:))
    
    twitterButton = [UIButton buttonWithType:UIButtonTypeCustom];
    twitterButton.left = russianFacebookButton.right;
    twitterButton.size = kShareButtonSize;
    [twitterButton setImage:[UIImage imageNamed:@"TwitterIcon"] forState:UIControlStateNormal];
    [shareContainerView addSubview:twitterButton];
    TARGET_TOUCH_UP(twitterButton, @selector(shareButtonPressed:))
    
    ziferblatButton = [UIButton buttonWithType:UIButtonTypeCustom];
    ziferblatButton.left = twitterButton.right;
    ziferblatButton.size = kShareButtonSize;
    [ziferblatButton setImage:[UIImage imageNamed:@"ZiferblatSocialIcon"] forState:UIControlStateNormal];
    [shareContainerView addSubview:ziferblatButton];
    TARGET_TOUCH_UP(ziferblatButton, @selector(shareButtonPressed:))
    
    shareContainerView.width = ziferblatButton.right;
    shareContainerView.height = ziferblatButton.height;
    [shareContainerView centerInSuperView];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupWithVenue:ziferblat];
    [self updateHeightForCollectionView];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    Notification_Observe(kNotification_SingleVenueRefreshed, venueRefreshed)
    
    [NetworkService pollForType:ZFPollingRequestType_SingleVenueDetails memberId:nil venueId:ziferblat.uuid summary:NO];}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
    Notification_Remove(kNotification_SingleVenueRefreshed)
}

- (void)dealloc {
    Notification_RemoveObserver
}

#pragma mark - 
#pragma mark Buttons actions

- (void)shareButtonPressed:(UIButton *)aButton {
    if ([aButton isEqual:instagramButton]) {
        ZFVenue *venue = ziferblat;
        [self openLink:venue.instagram];

    } else if([aButton isEqual:facebookButton]) {
        ZFVenue *venue = ziferblat;
        [self openLink:venue.facebook];
        
    } else if([aButton isEqual:russianFacebookButton]) {
        ZFVenue *venue = ziferblat;
        [self openLink:venue.vkontakte];
        
    } else if([aButton isEqual:twitterButton]) {
        ZFVenue *venue = ziferblat;
        [self openLink:venue.twitter];
        
    } else if([aButton isEqual:ziferblatButton]) {
        ZFVenue *venue = ziferblat;
        [self openLink:venue.website];
    }
}

- (void)openLink:(NSString *)pageString {
    
    if (pageString && pageString.length > 0 && [self validateUrl:pageString]) {
        
        NSURL *page = [NSURL URLWithString:pageString];
        
        if ([SFSafariViewController class]) {
            SFSafariViewController *vc = [[SFSafariViewController alloc] initWithURL:page];
            [self presentViewController:vc animated:YES completion:nil];
        } else {
            [[UIApplication sharedApplication] openURL:page];
        }
    } else {
        [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"Not avalible", @"Social button")];
    }
}

- (BOOL) validateUrl: (NSString *) candidate {
    NSString *urlRegEx =
    @"(http|https)://((\\w)*|([0-9]*)|([-|_])*)+([\\.|/]((\\w)*|([0-9]*)|([-|_])*))+";
    NSPredicate *urlTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", urlRegEx];
    return [urlTest evaluateWithObject:candidate];
}

- (void)profilePressed:(ZFProfileImage *)sender {
    ZFUser *user = [ziferblat.team objectAtIndex:[profileImages indexOfObject:sender]];
    [self userSelected:user];
}

- (void)leftArrowButtonTapped:(UIButton *)aButton {
    aButton.userInteractionEnabled = NO;
    if(imageIndex > 0 ) {
        imageIndex--;
        [pagingScrollView setCurrentIndex:imageIndex animated:YES];
    }
    aButton.userInteractionEnabled = YES;
}

- (void)rightArrowButtonTapped:(UIButton *)aButton {
    aButton.userInteractionEnabled = NO;
    if(imageIndex < ziferblat.imageUrls.count-1) {
        imageIndex++;
        [pagingScrollView setCurrentIndex:imageIndex animated:YES];
    }
    aButton.userInteractionEnabled = YES;
}

- (void)mapButtonTapped {
    CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(ziferblat.latitude, ziferblat.longitude);
    MKPlacemark *placemark = [[MKPlacemark alloc] initWithCoordinate:coordinate addressDictionary:nil];
    MKMapItem *mapItem = [[MKMapItem alloc] initWithPlacemark:placemark];
    [mapItem setName:[NSString stringWithFormat:NSLocalizedString(@"Ziferblat %@",nil), ziferblat.name]];
    [mapItem openInMapsWithLaunchOptions:nil];
}

- (void)teamButtonTapped:(UIButton *)aButton {
    
    [SVProgressHUD showWithStatus:NSLocalizedString(@"Getting occupants", nil)  maskType:SVProgressHUDMaskTypeGradient];
    [ZFGetOccupantsForVenueRequest requestWithVenueId:ziferblat.uuid
                                           completion:^(LORESTRequest *aRequest) {
                                               if(aRequest.resultStatus == LORESTRequestResultStatusSucceeded) {
                                                   Hide_HudAndPerformBlock(^{
                                    
                                                       NSArray *occupants = aRequest.result;
                                                       ziferblat.occupants = occupants;
                                                       ZFWhoIsAtZiferblatViewController *whoIsAtZFVC = [[ZFWhoIsAtZiferblatViewController alloc] initWithVenue:ziferblat
                                                                                                                                                   backMessage:NSLocalizedString(@"BACK", nil)];
                                                       [self.navigationController pushViewController:whoIsAtZFVC
                                                                                            animated:YES];
                                                   })
                                                   
                                               } else {
                                                   Show_ConnectionError
                                               }
                                           }];
}

#pragma mark -
#pragma mark Notifications

- (void)venueRefreshed {
    ziferblat = NetworkService.singleVenue;
    [self setupWithVenue:ziferblat];
}

#pragma mark - 
#pragma mark MKMapViewDelegate

- (void)mapViewDidFinishLoadingMap:(MKMapView *)aMapView {
    CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(ziferblat.latitude, ziferblat.longitude);
    CLLocationCoordinate2D coords[3] = {coordinate};
    MKCoordinateRegion region = [MKMapView regionForCoordinates:coords coordinateCount:1];
    [aMapView setRegion:region];
    [aMapView addAnnotation:ziferblat];
}

#pragma mark - 
#pragma mark LOPagingScrollViewDatasource

- (void)pagingScrollDidChangePage:(LOPagingScrollView *)aPagingScrollView {
    imageIndex = aPagingScrollView.currentIndex;
    [pageControl setCurrentPage:aPagingScrollView.currentIndex];
}

- (UIView *)pagingScrollView:(LOPagingScrollView *)aPagingScrollView viewAtIndex:(NSInteger)index withMaxSize:(CGSize)size {
    UIImageView *imageView;
    if(ziferblatImagesViews.count <= index) {
        imageView = [[UIImageView alloc] initWithFrame:CGRectMake(aPagingScrollView.width*index,
                                                                  0,
                                                                  aPagingScrollView.width,
                                                                  aPagingScrollView.height)];
        if (ziferblat.imageUrls.count > 0) {
            NSString *imageUrl = [ziferblat.imageUrls objectAtIndex:index];
            
            UIImage *image = [LOImageCache imageFromCache:imageUrl];
            if (!image) {
                [LOImageDownloader downloadImageFromPath:imageUrl
                                    withCompletionHander:^(UIImage *downloadedImage) {
                                        [UIView transitionWithView:imageView
                                                          duration:0.5
                                                           options:UIViewAnimationOptionTransitionCrossDissolve
                                                        animations:^{
                                                            imageView.image = downloadedImage;
                                                        } completion:nil];
                                    }];
            } else {
                imageView.image = image;
            }
            
            [ziferblatImagesViews addObject:imageView];
        }
    } else {
        imageView = [ziferblatImagesViews objectAtIndex:index];
    }
    
    return imageView;
}

- (NSInteger)pagingScrollViewRequestedNumberOfPages:(LOPagingScrollView *)aPagingScrollView {
    return ziferblat.imageUrls.count;
}

#pragma mark -
#pragma mark Utils

- (void)setupCollectionView {
    UICollectionViewFlowLayout *aFlowLayout = [[UICollectionViewFlowLayout alloc] init];
    [aFlowLayout setScrollDirection:UICollectionViewScrollDirectionVertical];
    aFlowLayout.itemSize = CGSizeMake([ZFProfileImage smallProfile].width, [ZFProfileImage smallProfile].height + kLargePadding + kMediumPadding);
    
    self.teamCollectionView = [[UICollectionView alloc] initWithFrame:teamContainerView.bounds collectionViewLayout:aFlowLayout];
    self.teamCollectionView.backgroundColor = [UIColor clearColor];
    [self.teamCollectionView registerClass:[ZFProfileImageCollectionViewCell class]
                forCellWithReuseIdentifier:[ZFProfileImageCollectionViewCell reuseIdentifier]];

    self.teamCollectionView.delegate = self;
    self.teamCollectionView.dataSource = self;
    [teamContainerView addSubview:self.teamCollectionView];
}

- (void)setupWithVenue:(ZFVenue *)aVenue {
    profileImages = [NSMutableArray array];
    teamArray = aVenue.team;
    headerLabel.text = aVenue.name;
    descriptionLabel.text = aVenue.venueDetails;
    addressLabel.text = aVenue.address;
    membersLabel.text = aVenue.memberCount.stringValue;
    peopleInsideLabel.text = [NSString stringWithFormat:NSLocalizedString(@"%@ people are inside right now",nil), aVenue.occupantCount];
    [self updateLayoutWithTeam:aVenue.team];
    pageControl.numberOfPages = aVenue.imageUrls.count;
    
    NSNumber *piastresHourStopCheck = [NSNumber numberWithInteger:[aVenue.stopCheck integerValue] / 60.0f];
    NSNumber *piastresStopCheck = [NSNumber numberWithInteger:([aVenue.piastresFactor integerValue] * [aVenue.stopCheck integerValue])];
    if (![piastresHourStopCheck isEqual:@(0)]) {
        piastresLabel.text = [NSString stringWithFormat:NSLocalizedString(@"%@ hours = %@",nil), piastresHourStopCheck, piastresStopCheck];
        piastresIcon.hidden = NO;
    } else {
        stopCheckLabel.text = @"";
        piastresIcon.hidden = YES;
    }
    [piastresLabel sizeToFit];
    piastresIcon.left = piastresLabel.right + kSmallPadding;
    
    if(pageControl.numberOfPages == 1) {
        [leftArrowButton hide];
        [rightArrowButton hide];
        [pageControl hide];
    }
    
    [self.teamCollectionView reloadData];
}

- (void)updateLayoutWithTeam:(NSArray *)team {
    [descriptionLabel sizeToWidthIsAttributed:NO];
    [addressLabel sizeToWidthIsAttributed:NO];
    teamHeaderCaptionView.top = descriptionLabel.bottom + (notIPad ? kPadding : 24);
    teamContainerView.top = teamHeaderCaptionView.bottom + (notIPad ? kPadding : 24);
    [self updateHeightForCollectionView];
}

- (void)userSelected:(ZFUser *)aUser {
    [SVProgressHUD showWithStatus:NSLocalizedString(@"Loading member...",nil) maskType:SVProgressHUDMaskTypeGradient];
    [ZFGetMemberFriendsListRequest requestWithMemberId:aUser.uuid andVenueId:CurrentUser.selectedVenue.uuid completion:^(LORESTRequest *aRequest) {
        if(aRequest.resultStatus == LORESTRequestResultStatusSucceeded) {
            aUser.friendsList = aRequest.result;
            aUser.friendsLoaded = TRUE;
            [self checkLoaded:aUser];
        } else {
            Show_ConnectionError
        }
    }];
    
    [ZFGetMemberVenueDetailsRequest requestWithMemberId:aUser.uuid venueId:CurrentUser.selectedVenue.uuid completion:^(LORESTRequest *aRequest) {
        if(aRequest.resultStatus == LORESTRequestResultStatusSucceeded) {
            aUser.venueDetails = aRequest.result;
            [self checkLoaded:aUser];
        } else {
            Show_ConnectionError
        }
    }];
}

- (void)checkLoaded:(ZFUser *)aUser {
    if ((aUser.venueDetails == nil) || (!aUser.friendsLoaded)) {
        return;
    }
    Hide_HudAndPerformBlock(^{
        if ([aUser.uuid isEqualToNumber:CurrentUser.uuid]) {
            ZFProfileViewController *profileViewController = [[ZFProfileViewController alloc] initWithUser:CurrentUser
                                                                                               backMessage:NSLocalizedString(@"BACK TO DETAIL", @"Back button title for details")
                                                                                                 andFilter:NO
                                                                                                forVenueId:nil];
            profileViewController.canBeDismissed = YES;
            [self presentViewController:profileViewController animated:YES completion:nil];
        } else {
            ZFOtherProfileViewController *otherProfileViewController = [[ZFOtherProfileViewController alloc] initWithUser:aUser
                                                                                                              backMessage:NSLocalizedString(@"BACK TO DETAIL", @"Back button title     for details")
                                                                                                                andFilter:NO
                                                                                                              forVenueId:nil];
            otherProfileViewController.canBeDismissed = YES;
            [self presentViewController:otherProfileViewController animated:YES completion:nil];
        }
    });
}

- (void)updateHeightForCollectionView {
    CGFloat minHeight = (notIPad ? kPadding : 24);
    self.teamCollectionView.height = (minHeight > self.teamCollectionView.contentSize.height) ? minHeight : self.teamCollectionView.contentSize.height;
    teamContainerView.height = self.teamCollectionView.height;
    [UIView animateWithDuration:0.3 animations:^{
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {
        footerImageView.top = teamContainerView.bottom+(notIPad ? kPadding : 24);
        mapView.top = footerImageView.top+kMapViewTopMargin;
        mapButton.top = mapView.top;
        [mapButton bringToFront];
        addressBackgroundImageView.top = footerImageView.bottom;
        stopCheckImageView.top = addressLabel.height + (notIPad ? kPadding : 24);
        addressBackgroundImageView.height = stopCheckImageView.bottom+(notIPad ? kPadding : 24);
        addressSeparatorImageView.top = addressBackgroundImageView.bottom - 1.5;
        shareBackgroundImageView.top = addressBackgroundImageView.bottom;
        kScrollView.contentSize = CGSizeMake(kScrollView.width, shareBackgroundImageView.bottom);
    }];
}

# pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return teamArray.count;
}

# pragma mark - UICollectionViewDelegate

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    ZFProfileImageCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:[ZFProfileImageCollectionViewCell reuseIdentifier]
                                                                                       forIndexPath:indexPath];
    [cell setupWithOccupant:[teamArray objectAtIndex:indexPath.row] isSelectable:NO];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    [self userSelected:[teamArray objectAtIndex:indexPath.row]];
}

@end
