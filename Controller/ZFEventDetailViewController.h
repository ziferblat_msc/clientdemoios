//
//  ZFEventDetailViewController.h
//  Ziferblat
//
//  Created by Jose Fernandez on 26/08/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZFEventViewController.h"

@class ZFEvents;

@interface ZFEventDetailViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIImageView *eventImageView;
@property (weak, nonatomic) IBOutlet UILabel *eventTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *eventDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *eventDescriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *eventLikeLabel;
@property (weak, nonatomic) IBOutlet UILabel *eventLikeCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *eventAttendeesLabel;
@property (weak, nonatomic) IBOutlet UIImageView *eventLikeHeartImageView;
@property (weak, nonatomic) IBOutlet UICollectionView *eventAttendeesCollectionView;
@property (weak, nonatomic) IBOutlet UIButton *eventAttendButton;
@property (weak, nonatomic) IBOutlet UIButton *eventLikeButton;
@property (weak, nonatomic) IBOutlet UIButton *eventShareButton;
@property (weak, nonatomic) IBOutlet UILabel *eventIWillComeLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *collectionViewHeightConstraint;
@property (strong, nonatomic) ZFEvents *event;
@property (strong, nonatomic) NSNumber *venueId;

@property (weak, nonatomic) ZFEventViewController *parent; // Need for the 'skip it'
@end
