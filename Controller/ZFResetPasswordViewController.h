//
//  ZFResetPasswordViewController.h
//  Ziferblat
//
//  Created by Terry Michel on 23/07/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "ZFBaseView.h"

@class ZFBackButton, ZFButton;

@interface ZFResetPasswordViewController : UIViewController<UIGestureRecognizerDelegate, UITextFieldDelegate> {
    UIImageView *backgroundImageView;
    ZFBackButton *backButton;
    UILabel *resetPasswordLabel;
    UILabel *emailLabel;
    UITextField *emailTextField;
    ZFButton *resetButton;
}

@end
