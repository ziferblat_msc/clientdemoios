//
//  ZFSharePiastresViewController.h
//  Ziferblat
//
//  Created by Terry Michel on 08/04/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZFKeyboard.h"
#import "ZFShareType.h"

@class ZFButton;
@class ZFVenue;
@class ZFKeyboard;

@interface ZFShareViewController : UIViewController<ZFKeyboardDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UIScrollViewDelegate> {
    ZFShareType type;
    ZFButton *doneButton;
    ZFUser *selectedUser;
    ZFVenue *selectedVenue;
    ZFKeyboard *keyboard;
    
    UIView *bottomContainerView;
    UIScrollView *venuesScrollView;
    UIImageView *sendToProfileImageView;
    UIImageView *sendToSeparatorImageView;
    UILabel *timeLabel;
    UILabel *sendToLabel;
    UILabel *sendToLocationLabel;
    UILabel *sendToNameLabel;
    UILabel *piastresLabel;
    UILabel *chooseTheLuckyOneLabel;
    UILabel *shareLabel;
    UIButton *piastresButton;
    UIButton *timeButton;    

    NSNumber *amount;
    CGFloat bottomContainerTop;
    
    NSMutableArray *friendsList;
    NSArray *venues;
}

- (instancetype)initWitShareType:(ZFShareType)aType venuesList:(NSArray *)venues;

@end
