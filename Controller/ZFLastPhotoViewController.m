//
//  ZFLastPhotoViewController.m
//  Ziferblat
//
//  Created by Terry Michel on 08/04/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "ZFLastPhotoViewController.h"

@implementation ZFLastPhotoViewController

#pragma mark - 
#pragma mark Lifecycle

- (void)loadView {
    contentView = [[ZFLastPhotoView alloc] init];
    self.view = contentView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    TARGET_TOUCH_UP(contentView.backButton, @selector(backButtonTapped))
    TARGET_TOUCH_UP(contentView.locationButton, @selector(locationButtonTapped))
    TARGET_TOUCH_UP(contentView.instagramButton, @selector(instagramButtonTapped))
}

#pragma mark - 
#pragma mark Buttons actions

- (void)backButtonTapped {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)locationButtonTapped {
    #warning TO IMPLEMENT
}

- (void)instagramButtonTapped {
    #warning TO IMPLEMENT
}

@end
