//
//  ZFEventCommentsViewController.m
//  Ziferblat
//
//  Created by Jose Fernandez on 26/08/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "ZFEventCommentsViewController.h"
#import "ZFCommentsCell.h"
#import "ZFCommentsRequest.h"
#import "ZFComment.h"
#import "ZFCommentLikeRequest.h"
#import "ZFAddCommentsRequest.h"
#import "ZFReplyCommentsRequest.h"
#import "ZFEvents.h"
#import "ZFButton.h"
#import "UIView+autolayout.h"
#import "ZFGuestService.h"

#define kTextLimit 4000

@interface ZFEventCommentsViewController () <UITableViewDataSource, UITableViewDelegate, ZFCommentsCellDelegate, UITextViewDelegate>
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UILabel *commentsTopLabel;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UITextView *textView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableViewHeightConstraint;
@property (weak, nonatomic) IBOutlet UILabel *commentPlaceholderLabel;
@property (weak, nonatomic) IBOutlet UILabel *replyToLabel;
@property (weak, nonatomic) IBOutlet UILabel *replyToPersonLabel;
@property (weak, nonatomic) IBOutlet UIButton *crossButton;
@property (strong, nonatomic) NSArray *comments;
@property (strong, nonatomic) NSMutableArray *rowHeights;
@property (nonatomic) CGFloat totalHeight;
@property (nonatomic, strong) NSMutableArray *totalComments;
@end

@implementation ZFEventCommentsViewController {
    ZFButton *postItButton;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        _comments = @[];
        _rowHeights = @[].mutableCopy;
        _totalHeight = 0;
        _totalComments = @[].mutableCopy;
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupViewSizes];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"ZFCommentsCell" bundle:nil] forCellReuseIdentifier:[ZFCommentsCell reuseIdentifier]];
    
    // Use Auto Layout to set each table view row height
    self.tableView.estimatedRowHeight = 104.0; // From xib. Assuming one line comment.
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    
    self.textView.layer.borderWidth = 2.0;
    self.textView.layer.borderColor = Colour_Black.CGColor;
    self.textView.textContainerInset = UIEdgeInsetsMake(8, 4, 8, 4);
    
    self.commentPlaceholderLabel.text = NSLocalizedString(@"Comment...", nil);
    
    self.commentsTopLabel.text = [[NSString stringWithFormat:NSLocalizedString(@"%d comments", nil), self.event.commentCount.integerValue] uppercaseString];
    
    [self setupPostItButton];
    
    [self fetchComments];
    
    Notification_Observe(kNotification_SingleEventRefreshed, receivedSingleEventRefreshed:);
    Notification_Observe(kNotification_SingleEventCommentsRefreshed, receivedSingleEventCommentsRefreshed:);
}

# pragma mark -
# pragma mark Setup UI

-(void) setupViewSizes
{
    if(isIPad)
    {
        self.commentsTopLabel.font = [UIFont fontWithName:self.commentsTopLabel.font.fontName size:38];
        self.replyToLabel.font = [UIFont fontWithName:self.replyToLabel.font.fontName size:34];
        self.replyToPersonLabel.font = [UIFont fontWithName:self.replyToPersonLabel.font.fontName size:34];
        self.commentPlaceholderLabel.font = [UIFont fontWithName:self.commentPlaceholderLabel.font.fontName size:36];
        self.textView.font = [UIFont fontWithName:self.textView.font.fontName size:36];
        
        for(NSLayoutConstraint *constraint in self.view.constraints)
        {
            constraint.constant *= 2;
        }
        
        for (UIView *subview in self.view.subviews )
        {
            for(NSLayoutConstraint *constraint in subview.constraints)
            {
                constraint.constant *= 2;
            }
        }
        
        for (UIView *subview in self.contentView.subviews )
        {
            for(NSLayoutConstraint *constraint in subview.constraints)
            {
                constraint.constant *= 2;
            }
        }
    }
    
    self.contentView.constraints[14].constant = -8;
    self.contentView.constraints[19].constant = -8;
}

- (void)setupPostItButton
{
    postItButton = [ZFButton buttonWithTitle:NSLocalizedString(@"POST IT!", nil) showMarks:NO];
    postItButton.translatesAutoresizingMaskIntoConstraints = NO;
    postItButton.enabled = NO;
    [self.view addSubview:postItButton];
    
    CGFloat width = postItButton.width;
    
    
    NSLayoutConstraint *postItButtonTopConstraint = [NSLayoutConstraint constraintWithItem:postItButton
                                                                                attribute:NSLayoutAttributeTop
                                                                                relatedBy:NSLayoutRelationEqual
                                                                                   toItem:self.textView
                                                                                attribute:NSLayoutAttributeBottom
                                                                               multiplier:1.0
                                                                                  constant:(notIPad ?15:36)];
    NSLayoutConstraint *postItButtonCenterXConstraint = [NSLayoutConstraint constraintWithItem:postItButton
                                                                                    attribute:NSLayoutAttributeCenterX
                                                                                    relatedBy:NSLayoutRelationEqual
                                                                                       toItem:self.view
                                                                                    attribute:NSLayoutAttributeCenterX
                                                                                   multiplier:1.0
                                                                                     constant:0];
    NSLayoutConstraint *postItButtonWidthConstraint = [NSLayoutConstraint constraintWithItem:postItButton
                                                                                  attribute:NSLayoutAttributeWidth
                                                                                  relatedBy:NSLayoutRelationEqual
                                                                                     toItem:nil
                                                                                  attribute:NSLayoutAttributeWidth
                                                                                 multiplier:1.0
                                                                                   constant:width];
    
    NSLayoutConstraint *postItButtonHeightConstraint = [NSLayoutConstraint constraintWithItem:postItButton
                                                                                   attribute:NSLayoutAttributeHeight
                                                                                   relatedBy:NSLayoutRelationEqual
                                                                                      toItem:nil
                                                                                   attribute:NSLayoutAttributeHeight
                                                                                  multiplier:1.0
                                                                                    constant:44];
    
    [self.view addConstraints:@[postItButtonTopConstraint, postItButtonCenterXConstraint, postItButtonWidthConstraint, postItButtonHeightConstraint]];
    
    TARGET_TOUCH_UP(postItButton, @selector(postItButtonWasTapped))
}

# pragma mark -
# pragma mark Flows

- (void)fetchComments
{
    [ZFCommentsRequest requestWithVenueId:self.venueId
                                  eventId:self.event.uuid
                               completion:^(LORESTRequest *aRequest) {
                                   if(aRequest.resultStatus == LORESTRequestResultStatusSucceeded) {
                                       [self updateCommentsArrayWithComments:aRequest.result];
                                   }
                               }];
}
- (void)iterateComment:(ZFComment *)comment {
    for (ZFComment *reply in comment.replies) {
        [self.totalComments addObject:reply];
        [self iterateComment:reply];
    }
}
- (void)updateCommentsArrayWithComments:(NSArray *)comments {
    [self clearComments];
    
    self.comments = comments;
    
    for (ZFComment *comment in self.comments) {
        [self.totalComments addObject:comment];
        [self iterateComment:comment];
    }
    
    // Now we have total comemnts,
    // need to udate table view height and scroll view height
    
    ZFCommentsCell *workingCommentCell = [_tableView dequeueReusableCellWithIdentifier:[ZFCommentsCell reuseIdentifier]];
    
    for (ZFComment *comment in self.totalComments) {
        [workingCommentCell setupCellForComment:comment];
        
        CGSize size = [workingCommentCell systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
        [self.rowHeights addObject:@(ceilf(size.height))];
        
        self.totalHeight += ceilf(size.height);
        self.tableViewHeightConstraint.constant = self.totalHeight;
    }
    self.tableViewHeightConstraint.constant = self.totalHeight;
    self.containerHeightConstraint.constant = self.tableViewHeightConstraint.constant + (notIPad ?324:627);
    [self.scrollView setContentSize:CGSizeMake(kWidth, self.containerHeightConstraint.constant)];
    
    [self.tableView reloadData];
}

- (void)receivedSingleEventRefreshed:(NSNotification *)notification
{
    if ([self.event.uuid isEqualToNumber:[ZFNetworkService sharedInstance].event.uuid])
    {
        self.commentsTopLabel.text = [[NSString stringWithFormat:NSLocalizedString(@"%d comments", nil), [ZFNetworkService sharedInstance].event.commentCount.integerValue] uppercaseString];
    }
}

- (void)clearComments {
    [self.totalComments removeAllObjects];
    self.comments = nil;
    [self.rowHeights removeAllObjects];
    self.totalHeight = 0;
}

# pragma mark -
# pragma mark Actions and Selectors

- (void)receivedSingleEventCommentsRefreshed:(NSNotification *)notification{
    [self updateCommentsArrayWithComments:[ZFNetworkService sharedInstance].eventComments];
}

- (void)postItButtonWasTapped {
    Show_Hud(@"Posting...");
    
    if (self.replyCommentId) {
        [ZFReplyCommentsRequest requestWithVenueId:self.venueId eventId:self.event.uuid commentText:self.textView.text parentComment:self.replyCommentId completion:^(LORESTRequest *aRequest) {
            Hide_HudAndPerformBlock(^{
                if(aRequest.resultStatus == LORESTRequestResultStatusSucceeded) {
                    self.textView.text = @"";
                    [self clearComments];
                    [self fetchComments];
                } else {
                    Show_Error(NSLocalizedString(@"There was an error sending your reply. Please try again.", @"ERROR_COMMENT_REPLY"));
                }
            });
        }];
    } else {
        [ZFAddCommentsRequest requestWithVenueId:self.venueId eventId:self.event.uuid commentText:self.textView.text completion:^(LORESTRequest *aRequest) {
            Hide_HudAndPerformBlock(^{
                if(aRequest.resultStatus == LORESTRequestResultStatusSucceeded) {
                    self.textView.text = @"";
                    [self clearComments];
                    [self fetchComments];
                } else {
                    Show_Error(NSLocalizedString(@"There was an error sending your comment. Please try again.", @"ERROR_COMMENT"));
                }
            });
        }];
    }
}

- (IBAction)cancelReplyWasTapped:(UIButton *)sender
{
    self.replyCommentId = nil;
    
    self.replyToLabel.hidden = YES;
    self.replyToPersonLabel.hidden = YES;
    self.crossButton.hidden = YES;
    self.replyToPersonLabel.text = @"";
    
    [self.textView resignFirstResponder];
}

# pragma mark -
# pragma mark UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return self.totalComments.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ZFCommentsCell *commentsCell = [tableView dequeueReusableCellWithIdentifier:[ZFCommentsCell reuseIdentifier] forIndexPath:indexPath];
    commentsCell.commentDelegate = self;
    ZFComment *comment = self.totalComments[indexPath.row];
    [commentsCell setupCellForComment:comment];
    
    return commentsCell;
}

- (void)togglePostButton {
    NSString *output = self.textView.text;
    if (output.length == 0 || [output stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length == 0) {
        postItButton.enabled = NO;
    } else {
        postItButton.enabled = YES;
    }
}

# pragma mark - ZFCommentsCellDelegate

- (void)commentWithId:(NSString *)commentId like:(BOOL)like {
    [ZFCommentLikeRequest requestWithVenueId:self.venueId eventId:self.event.uuid commentId:commentId like:like completion:nil];
}

- (void)replyCommentWithCommentId:(NSString *)uuid username:(NSString *)username {
    self.replyCommentId = uuid;
    
    self.replyToLabel.hidden = NO;
    
    self.replyToPersonLabel.hidden = NO;
    self.crossButton.hidden = NO;
    self.replyToPersonLabel.text = username;
    
    [self.textView becomeFirstResponder];
}

# pragma mark - UITextViewDelegate

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView {
    BOOL canEdit = YES;
    
    [self togglePostButton];
    
    if ([[ZFGuestService sharedInstance] makeSureUserIsNotAGuestFromViewController:self.parent]) {
        canEdit = NO;
    }
    
    return canEdit;
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        
        return NO;
    }
    
    BOOL aux = YES;
    NSString *output = [textView.text stringByReplacingCharactersInRange:range withString:text];
    aux = output.length < kTextLimit;
    
    self.commentPlaceholderLabel.hidden = output.length > 0;
    
    [self togglePostButton];

    return aux;
}

@end
