//
//  ZFEventViewController.m
//  Ziferblat
//
//  Created by Jose Fernandez on 26/08/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "ZFEventViewController.h"
#import "ZFEventDetailViewController.h"
#import "ZFEventCommentsViewController.h"
#import "UIView+autolayout.h"

@interface ZFEventViewController () <UIScrollViewDelegate>
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomConstraint;
@end

@implementation ZFEventViewController

- (void)dealloc {
    Notification_RemoveObserver
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.backLabel.text = NSLocalizedString(@"BACK TO EVENTS", nil);
    
    [self addEventDetailsViewController];
    [self addEventCommentsViewController];
    
    [self setupViewSizes];
}

-(void)setupViewSizes
{
    if(isIPad)
    {
        self.backLabel.font = [UIFont fontWithName:self.backLabel.font.fontName size:26];
        
        self.topNavigationControlHeightConstraint.constant = 48;
        self.parentContainerView.constraints[0].constant = 48;
        self.parentContainerView.constraints[1].constant = 48;
        self.parentContainerView.constraints[2].constant = 48;
    }

}

# pragma mark -
# pragma mark Flows

- (void)addEventDetailsViewController {
    ZFEventDetailViewController *eventDetailViewController = [[ZFEventDetailViewController alloc] initWithNibName:@"ZFEventDetailViewController" bundle:nil];
    eventDetailViewController.event = self.event;
    eventDetailViewController.venueId = self.venueId;
    [self addChildViewController:eventDetailViewController];
    eventDetailViewController.view.translatesAutoresizingMaskIntoConstraints = NO;
    [self.eventDetailContainerView  addSubview:eventDetailViewController.view];
    [eventDetailViewController didMoveToParentViewController:self];
    [self.view layoutIfNeeded];
    eventDetailViewController.parent = self; // Need for the "skip it"
    
    [self.eventDetailContainerView pinView:eventDetailViewController.view];
}

- (void)addEventCommentsViewController {
    ZFEventCommentsViewController *eventCommentsViewController = [[ZFEventCommentsViewController alloc] initWithNibName:@"ZFEventCommentsViewController" bundle:nil];
    eventCommentsViewController.venueId = self.venueId;
    eventCommentsViewController.event = self.event;
    eventCommentsViewController.containerHeightConstraint = self.eventCommentsContainerHeightConstraint;
    [self addChildViewController:eventCommentsViewController];
    eventCommentsViewController.view.translatesAutoresizingMaskIntoConstraints = NO;
    [self.eventCommentsContainerView addSubview:eventCommentsViewController.view];
    [eventCommentsViewController didMoveToParentViewController:self];
    self.eventCommentsContainerHeightConstraint.constant = eventCommentsViewController.view.height;
    [self.view layoutIfNeeded];
    eventCommentsViewController.parent = self; // Need for 'skip it' logic
    
    [self.eventCommentsContainerView pinView:eventCommentsViewController.view];
}

# pragma mark -
# pragma mark Actions and selectors

- (IBAction)backButtonTapped:(UIControl *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)viewWasTapped:(UITapGestureRecognizer *)sender {
    [self.view endEditing:YES];
}

- (void)keyboardWillShow:(NSNotification *)notification
{
    UIView *view = self.view;
    
    CGRect keyboardFrame = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue];
    keyboardFrame = [view convertRect:keyboardFrame fromView:self.view.window];
    
    CGFloat duration = [[[notification userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    NSInteger animationCurve = [[[notification userInfo] objectForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue];
    
    CGRect frame = [self.view convertRect:self.view.frame toView:view];
    CGFloat maxY = CGRectGetMaxY(frame);
    CGFloat bottomSpace = MAX(0, view.bounds.size.height - maxY);
    CGFloat amountToInset = keyboardFrame.size.height - bottomSpace;
    
    [UIView animateWithDuration:duration delay:0.0f options:animationCurve animations:^{
        UIEdgeInsets insets = self.scrollView.contentInset;
        insets.bottom = amountToInset;
        self.scrollView.contentInset = insets;
        self.scrollView.scrollIndicatorInsets = self.scrollView.contentInset;
        [self.scrollView scrollRectToVisible:CGRectMake(self.scrollView.contentSize.width - 1, self.scrollView.contentSize.height - 1, 1, 1) animated:YES];
    } completion:nil];
}

- (void)keyboardWillHide:(NSNotification *)notification
{
    CGFloat duration = [[[notification userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    NSInteger animationCurve = [[[notification userInfo] objectForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue];
    
    [UIView animateWithDuration:duration delay:0.0f options:animationCurve animations:^{
        UIEdgeInsets insets = self.scrollView.contentInset;
        insets.bottom = 0;
        self.scrollView.contentInset = insets;
        self.scrollView.scrollIndicatorInsets = self.scrollView.contentInset;
    } completion:nil];
}

@end