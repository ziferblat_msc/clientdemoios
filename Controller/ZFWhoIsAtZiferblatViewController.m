//
//  ZFWhoIsAtZiferblatViewController.m
//  Ziferblat
//
//  Created by Terry Michel on 08/04/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "ZFWhoIsAtZiferblatViewController.h"
#import "ZFOtherProfileViewController.h"
#import "ZFGetMemberVenueDetailsRequest.h"
#import "ZFGetMemberFriendsListRequest.h"
#import "ZFVenue.h"
#import "ZFProfileImageCollectionViewCell.h"
#import "ZFTestCollectionReusableView.h"

@interface ZFWhoIsAtZiferblatViewController ()

@property (nonatomic, copy) NSString *backMessage;

@end

@implementation ZFWhoIsAtZiferblatViewController

#pragma mark - 
#pragma mark Instance

- (instancetype)initWithVenue:(ZFVenue *)aVenue backMessage:(NSString *)backMessage{
    self = [super init];
    if(self) {
        venue = aVenue;
        _backMessage = backMessage;
    }
    return self;
}

#pragma mark - 
#pragma mark Lifecycle

- (void)loadView {
    contentView = [[ZFWhoIsAtZiferblatView alloc] initWithBackMessage:self.backMessage];
    self.view = contentView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    TARGET_TOUCH_UP(contentView.backButton, @selector(backButtonTapped))
    TARGET_TOUCH_UP(contentView.sortAZButton, @selector(sortAZButtonTapped:))
    TARGET_TOUCH_UP(contentView.sortRatingButton, @selector(sortRatingButtonTapped:))
    venue.occupants = [self sortedAZOccupantsList:venue.occupants];
    contentView.collectionView.dataSource = self;
    contentView.collectionView.delegate = self;
    [contentView.collectionView registerClass:[ZFProfileImageCollectionViewCell class] forCellWithReuseIdentifier:[ZFProfileImageCollectionViewCell reuseIdentifier]];
    UINib *nib = [UINib nibWithNibName:@"ZFTestCollectionReusableView" bundle:nil];
    [contentView.collectionView registerNib:nib forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"Test"];
    Notification_Observe(kNotification_VenueOccupantsRefreshed, occupantsUpdated);
    [contentView setupWithVenue:venue];
    [NetworkService pollForType:ZFPollingRequestType_SingleVenueDetails|ZFPollingRequestType_VenueOccupants memberId:CurrentUser.uuid venueId:venue.uuid summary:YES];
}

- (void)dealloc {
    Notification_RemoveObserver;
}

- (void)checkLoaded:(ZFUser *)aUser
{
    if((aUser.venueDetails == nil) || (!aUser.friendsLoaded))
    {
        return;
    }
    
    Hide_HudAndPerformBlock(^{
        
        if([aUser.uuid isEqualToNumber:CurrentUser.uuid]) {
            ZFProfileViewController *profileViewController = [[ZFProfileViewController alloc] initWithUser:CurrentUser
                                                           backMessage:NSLocalizedString(@"BACK TO WHO'S HERE", @"Back button title for whos here")
                                                             andFilter:NO
                                                            forVenueId:nil];
            profileViewController.canBeDismissed = YES;
            [self presentViewController:profileViewController animated:YES completion:nil];
        } else {
            ZFOtherProfileViewController *otherProfileViewController = [[ZFOtherProfileViewController alloc ] initWithUser:aUser
                                                                 backMessage:NSLocalizedString(@"BACK TO WHO'S HERE", @"Back button title for whos here")
                                                                   andFilter:NO
                                                                  forVenueId:nil];
            otherProfileViewController.canBeDismissed = YES;
            [self presentViewController:otherProfileViewController animated:YES completion:nil];
        }
    });
}

#pragma mark -
#pragma mark Notifications

- (void)occupantsUpdated {

    [contentView refreshContent];
    
    NSArray *occupantsUpdated = NetworkService.occupants;
    if ([((ZFUser *)[occupantsUpdated firstObject]).currentVenueId isEqualToNumber:venue.uuid]) {
        
        if (occupantsUpdated.count == venue.occupants.count) {
           
            venue.occupants = (contentView.sortAZButton.isSelected ? [self sortedAZOccupantsList:occupantsUpdated] : [self sortedRatingOccupantsList:occupantsUpdated]);
            NSArray *existingIndexPaths = [contentView.collectionView indexPathsForVisibleItems];
            NSMutableArray *newIndexPathsToAdd = [NSMutableArray arrayWithCapacity:existingIndexPaths.count];

            for (ZFUser *occupant in venue.occupants) {
            
                NSIndexPath *newIndexPath = [NSIndexPath indexPathForItem:[venue.occupants indexOfObject:occupant] inSection:0];
                
                if (![existingIndexPaths containsObject:newIndexPath]) {

                    /// Only need to add indexPaths for items that do not currently have an indexPath.
                    [newIndexPathsToAdd addObject:newIndexPath];
                }
            }
            
            [contentView.collectionView insertItemsAtIndexPaths:newIndexPathsToAdd];
        }
        else {
            
            venue.occupants = (contentView.sortAZButton.isSelected ? [self sortedAZOccupantsList:occupantsUpdated] : [self sortedRatingOccupantsList:occupantsUpdated]);
            [contentView.collectionView reloadSections:[NSIndexSet indexSetWithIndex:0]];
        }
    }
}

#pragma mark -
#pragma mark Buttons actions

- (void)backButtonTapped {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)sortAZButtonTapped:(UIControl *)aControl {
    if(!aControl.isSelected) {
        [aControl setSelected:YES];
        [contentView.sortRatingButton setSelected:NO];
        venue.occupants = [self sortedAZOccupantsList:venue.occupants];
        [contentView.collectionView reloadSections:[NSIndexSet indexSetWithIndex:0]];
    }
}

- (void)sortRatingButtonTapped:(UIControl *)aControl {
    if(!aControl.isSelected) {
        [aControl setSelected:YES];
        [contentView.sortAZButton setSelected:NO];
        
        venue.occupants = [self sortedRatingOccupantsList:venue.occupants];
        [contentView.collectionView reloadSections:[NSIndexSet indexSetWithIndex:0]];
    }
}

#pragma mark - 
#pragma mark UICollectionView datasource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return venue.occupants.count;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    ZFProfileImageCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:[ZFProfileImageCollectionViewCell reuseIdentifier]
                                                                         forIndexPath:indexPath];
    [cell setupWithOccupant:[venue.occupants objectAtIndex:indexPath.row] isSelectable:NO];
    
    return cell;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView
           viewForSupplementaryElementOfKind:(NSString *)kind
                                 atIndexPath:(NSIndexPath *)indexPath {
    ZFTestCollectionReusableView *view = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader
                                                                            withReuseIdentifier:@"ZFTestCollectionReusableView"
                                                                                   forIndexPath:indexPath];
    return view;
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
referenceSizeForHeaderInSection:(NSInteger)section {
    return CGSizeZero;
}

#pragma mark - 
#pragma mark UICollectionView delegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    ZFUser *user = [venue.occupants objectAtIndex:indexPath.row];
    [SVProgressHUD showWithStatus:NSLocalizedString(@"Loading member...",nil) maskType:SVProgressHUDMaskTypeGradient];
    [ZFGetMemberFriendsListRequest requestWithMemberId:user.uuid andVenueId:CurrentUser.selectedVenue.uuid completion:^(LORESTRequest *aRequest) {
        if(aRequest.resultStatus == LORESTRequestResultStatusSucceeded) {
            user.friendsList = aRequest.result;
            user.friendsLoaded = TRUE;
            [self checkLoaded:user];
        } else {
            Show_ConnectionError
        }
    }];
    
    [ZFGetMemberVenueDetailsRequest requestWithMemberId:user.uuid venueId:CurrentUser.selectedVenue.uuid completion:^(LORESTRequest *aRequest) {
        if(aRequest.resultStatus == LORESTRequestResultStatusSucceeded) {
            user.venueDetails = aRequest.result;
            [self checkLoaded:user];
        } else {
            Show_ConnectionError
        }
    }];
}

#pragma mark - 
#pragma mark UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return [ZFProfileImage largeSize];
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView
                        layout:(UICollectionViewLayout *)collectionViewLayout
        insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsZero;
}

#pragma mark -
#pragma mark Utils

- (NSArray *)sortedAZOccupantsList:(NSArray *)occupants {
    return [occupants sortedArrayUsingComparator:^NSComparisonResult(ZFUser *user1, ZFUser *user2) {
        return [user1.fullName compare:user2.fullName];
    }];
}

- (NSArray *)sortedRatingOccupantsList:(NSArray *)occupants
{
    
    return [occupants sortedArrayUsingComparator:^NSComparisonResult(ZFUser *user1, ZFUser *user2)
    {
        NSNumber *user1Rating = [NSNumber numberWithInt:0];
        NSNumber *user2Rating = [NSNumber numberWithInt:0];
        
        if (user1.venueDetails.experiencePoints.integerValue > 0)
        {
            user1Rating = user1.venueDetails.experiencePoints;
        }
        
        if (user2.venueDetails.experiencePoints.integerValue > 0)
        {
            user2Rating = user2.venueDetails.experiencePoints;
        }
        
        return ([user1Rating compare:user2Rating] * (-1));
    }];
}

@end
