//
//  ZFHomeViewController.h
//  Ziferblat
//
//  Created by Terry Michel on 08/04/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "ZFHomeView.h"

@interface ZFHomeViewController : UIViewController<UITableViewDelegate, UITableViewDataSource, UICollectionViewDataSource,
UICollectionViewDelegateFlowLayout> {
    BOOL isLaunchFromLogin;
    ZFHomeView *contentView;
    NSArray *venuesList;
    ZFVenue *selectedVenue;
    NSUInteger userVenueIndex;
    NSUInteger selectedVenueIndex;
}

#pragma mark - Instance
- (instancetype)initIsLaunchFromLogin:(BOOL)isLaunchFromLogin;

@end
