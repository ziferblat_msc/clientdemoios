//
//  ZFAchievementsViewController.h
//  Ziferblat
//
//  Created by Terry Michel on 08/04/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "ZFMemberVenueDetails.h"
#import "ZFAchievementsCell.h"
#import "ZFViewController.h"

@interface ZFAchievementsViewController : ZFViewController<ZFAchievementCellDelegate, UITableViewDelegate, UITableViewDataSource>
{
    ZFUser *user;
    
    NSString *backButtonMessage;
    
    NSArray *achievementArray;
    UILabel *achievementsLabel;
    UILabel *achievementsNumberLabel;
    UILabel *experiencePointsLabel;
    UILabel *numberOfTasksLabel;
    UITableView *achievementsTableView;
}

- (instancetype)initWithUser:(ZFUser *)aUser;
- (instancetype)initWithUser:(ZFUser *)aUser backMessage:(NSString *)backMessage;

@end
