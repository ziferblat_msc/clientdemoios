//
//  ZFTimeHistoryViewController.h
//  Ziferblat
//
//  Created by Terry Michel on 08/04/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "ZFTimeHistoryView.h"

@interface ZFTimeHistoryViewController : UIViewController <UITableViewDelegate, UITableViewDataSource> {
    
    ZFTimeHistoryView *contentView;
}

@end
