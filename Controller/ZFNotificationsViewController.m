  //
//  ZFNotificationsViewController.m
//  Ziferblat
//
//  Created by Terry Michel on 08/04/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "ZFNotificationsViewController.h"
#import "ZFTreasuryViewController.h"
#import "ZFExperiencePointsViewController.h"
#import "ZFConversationDetailViewController.h"
#import "ZFZiferblatDetailsViewController.h"
#import "ZFGetSingleMemberRequest.h"
#import "ZFGetSingleVenueRequest.h"
#import "ZFNotificationRequest.h"
#import "ZFNotificationContent.h"
#import "ZFNotificationsCell.h"

static CGFloat const ZFScrollViewLoadModeThreshold = 100;

@interface ZFNotificationsViewController() < UITableViewDataSource, UITableViewDelegate, UIScrollViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) ZFNotificationsCell *cell;
@property (nonatomic, strong) NSMutableArray *notifications;
@property (nonatomic) NSTimeInterval timestampForNewer;
@property (nonatomic) NSTimeInterval timestampForOlder;
@property (nonatomic) BOOL shouldGetMoreNotifications;
@property (nonatomic) BOOL isTableViewOffset;
@property (weak, nonatomic) IBOutlet UILabel *notificationsTopLabel;
@property (weak, nonatomic) IBOutlet UILabel *backLabel;
@property (nonatomic, strong) NSArray *venues;
@property (nonatomic, strong) ZFVenue *selectedVenue;

@end

@implementation ZFNotificationsViewController

- (ZFNotificationsViewController *)initWithSelectedVenue:(ZFVenue *)venue
                                              venuesList:(NSArray<ZFVenue *> *)venuesList {
    if (self = [super init]) {
        _venues = venuesList;
        _selectedVenue = venue;
    }
    return self;
}

- (void)dealloc {
    Notification_RemoveObserver
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.backLabel.text = NSLocalizedString(@"BACK TO ROOM", nil);
    
    self.notificationsTopLabel.text = NSLocalizedString(@"NOTIFICATIONS", nil);
    
    self.notifications = @[].mutableCopy;
    self.isTableViewOffset = NO;
    self.shouldGetMoreNotifications = NO;
    
    [self.tableView registerNib:[UINib nibWithNibName:@"ZFNotificationsCell" bundle:nil] forCellReuseIdentifier:[ZFNotificationsCell reuseIdentifier]];
    
    if ([UserDefaults valueForKey:kLastNotificationReadTimestamp] == nil) {
        self.timestampForNewer = [[NSDate date] timeIntervalSince1970];
    } else {
        self.timestampForNewer = [((NSNumber *)[UserDefaults valueForKey:kLastNotificationReadTimestamp]) doubleValue];
    }
    [ZFNetworkService sharedInstance].notificationsTimestamp = self.timestampForNewer;
    
    self.timestampForOlder = [[NSDate date] timeIntervalSince1970];
    
    Notification_Observe(kNotification_NotificationsRefreshed, receivedNewerNotifications:);
    
    [self loadOlderNotifications];
    
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    // Assume most of the rows will have 2 lines of text
    self.tableView.estimatedRowHeight = (notIPad ? 90:150);
    
    [self setupViewSizes];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [self updateLastUpdatedTimestamp];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.tableView reloadData];
}

-(void)setupViewSizes
{
    if(isIPad)
    {
        self.backLabel.font = [UIFont fontWithName:self.backLabel.font.fontName size:26];
        self.notificationsTopLabel.font = [UIFont fontWithName:self.notificationsTopLabel.font.fontName size:48];
        
        self.topNavigationControlHeightConstraint.constant = 48;
        self.headerViewHeightConstraint.constant = 219;
        self.headerLabelYConstraint.constant = -113;
        
    }
    
}

#pragma mark -
#pragma mark Flows

- (void)loadOlderNotifications {
    [ZFNotificationRequest requestWithTimestamp:self.timestampForOlder older:YES completion:^(LORESTRequest *aRequest) {
        if(aRequest.resultStatus == LORESTRequestResultStatusSucceeded) {
            
            if ([aRequest.result count] == 0) {
                self.shouldGetMoreNotifications = NO;
            } else {
                self.shouldGetMoreNotifications = YES;
                self.timestampForOlder = (long long)[((ZFNotificationContent *)[aRequest.result lastObject]).lastUpdated timeIntervalSince1970];
            }
            
            for(ZFNotificationContent *notification in aRequest.result)
            {
                NSLog(@"%@", notification.uuid);
                NSLog(@"%@", notification.dateCreated.timeString);
                NSLog(@"%@", notification.content);
                
            }
            
            
            [self addNewNotificationsAndSortFromArray:aRequest.result];
        }
    }];
}

- (void)addNewNotificationsAndSortFromArray:(NSArray *)array
{
    [self.notifications addObjectsFromArray:array];
        
    self.notifications = [self.notifications sortedArrayUsingComparator:^NSComparisonResult(ZFNotificationContent *obj1, ZFNotificationContent *obj2) {
        return [obj1.lastUpdated compare:obj2.lastUpdated] == NSOrderedAscending;
    }].mutableCopy;
    
    [self updateLastUpdatedTimestamp];
    
    [self.tableView reloadData];
}

- (void)updateLastUpdatedTimestamp {
    ZFNotificationContent *newestContent = self.notifications.firstObject;
    [UserDefaults setValue:@([newestContent.lastUpdated timeIntervalSince1970]) forKey:kLastNotificationReadTimestamp];
}

#pragma mark -
#pragma mark Actions and Selectors

- (IBAction)backButtonTapped:(UIControl *)sender {
    [super backButtonTapped];
}

- (void)receivedNewerNotifications:(NSNotification *)notification {
    NSDictionary *userInfo = notification.userInfo;
    if ([((NSNumber *)[userInfo valueForKey:@"older"]) isEqualToNumber:@0]) {
        [self addNewNotificationsAndSortFromArray:[ZFNetworkService sharedInstance].newerNotifications];
        [self updateLastUpdatedTimestamp];
    }
}

#pragma mark -
#pragma mark UITableViewDataSource

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ZFNotificationsCell *cell = [tableView dequeueReusableCellWithIdentifier:[ZFNotificationsCell reuseIdentifier]
                                                                forIndexPath:indexPath];
    ZFNotificationContent *notification = self.notifications[indexPath.row];
    [cell setupWithNotificationContent:notification
                             timestamp:self.timestampForNewer];
    
    ZFNotificationType notificationType = notification.notificationTypeInt.integerValue;
    if ([[ZFPresenter sharedInstanceWithNavigationController:nil] isNotificaionActionable:notificationType]) {
        cell.userInteractionEnabled = YES;
        cell.alpha = 1.0;
        cell.backgroundView.alpha = 1.0;
    } else {
        cell.userInteractionEnabled = NO;
        cell.alpha = 0.5;
        cell.backgroundView.alpha = 0.5;
    }
    
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.notifications.count;
}

#pragma mark -
#pragma mark UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    Show_Hud(nil)
    ZFNotificationContent *notification = [self.notifications objectAtIndex:indexPath.row];
    ZFNotificationType notificationType = notification.notificationTypeInt.integerValue;
    [[ZFPresenter sharedInstanceWithNavigationController:nil] presentAppropriateViewControllerForNotification:notificationType
                                                                                        fromNotificationsView:YES];
    Hide_HudAndPerformBlock(nil)
}

#pragma mark - Navigation

- (void)goToTreasury {
    ZFTreasuryViewController *treasuryViewController = [[ZFTreasuryViewController alloc] initWithSelectedVenue:self.selectedVenue
                                                                                                 andVenuesList:self.venues
                                                                                                   backMessage:NSLocalizedString(@"BACK TO NOTIFICATIONS", nil)];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self presentViewController:treasuryViewController animated:YES completion:nil];
    });
}

- (void)goToExperiencePoints {
    ZFExperiencePointsViewController *experienceViewController = [[ZFExperiencePointsViewController alloc] initWithBackMessage:NSLocalizedString(@"BACK TO NOTIFICATIONS", nil)];
    dispatch_async(dispatch_get_main_queue(), ^{
        experienceViewController.canBeDismissed = YES;
        [self presentViewController:experienceViewController animated:YES completion:nil];
    });
}

- (void)goToConversationDetail:(ZFNotificationContent *)notification {
    NSNumber *userId = notification.generalId;
    if (userId) {
        Show_Hud(NSLocalizedString(@"Fetching data", nil));
        [ZFGetSingleMemberRequest requestWithUser:userId completion:^(LORESTRequest *aRequest) {
            Hide_HudAndPerformBlock(^{
                if (aRequest.resultStatus == LORESTRequestResultStatusSucceeded) {
                    ZFUser *toUser = aRequest.result;
                    ZFConversationDetailViewController *convoDetailViewController = [[ZFConversationDetailViewController alloc] initWithUser:toUser
                                                                                                                                 backMessage:NSLocalizedString(@"BACK TO NOTIFICATIONS", nil)];
                    [self presentViewController:convoDetailViewController
                                       animated:YES
                                     completion:nil];
                } else {
                    Show_Error(NSLocalizedString(@"Error fetching conversation", nil));
                }
            });
        }];
    } else {
        Show_Error(NSLocalizedString(@"Error fetching conversation", nil));
    }
}

- (void)goToVenueInfo:(NSNumber *)venueId {
    if (venueId && ![venueId isEqualToNumber:@0]) {
        [SVProgressHUD showWithStatus:NSLocalizedString(@"Getting venue details", nil) maskType:SVProgressHUDMaskTypeGradient]; 
        [ZFGetSingleVenueRequest requestWithVenueId:venueId
                                            summary:NO
                                         completion:^(LORESTRequest *aRequest) {
                                             if(aRequest.resultStatus == LORESTRequestResultStatusSucceeded) {
                                                 Hide_HudAndPerformBlock(^{
                                                     ZFVenue *selectedVenue = aRequest.result;;
                                                     ZFZiferblatDetailsViewController *ziferblatDetailsViewController = [[ZFZiferblatDetailsViewController alloc] initWithVenue:selectedVenue];
                                                     [self presentViewController:ziferblatDetailsViewController
                                                                        animated:YES
                                                                      completion:nil];
                                                 })
                                             } else {
                                                 Show_ConnectionError
                                             }
                                         }];
    }
}


# pragma mark -
# pragma mark UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    // if scrolled to within 100 pixels of the bottom of the content
    // set flag to cause next page of notifications to be loaded.
    CGFloat contentOffsetBottom = scrollView.contentOffset.y + scrollView.contentInset.top + scrollView.frame.size.height;
    CGFloat contentHeight = scrollView.contentSize.height + scrollView.contentInset.top + scrollView.contentInset.bottom;
    
    
    if ((contentOffsetBottom > contentHeight - ZFScrollViewLoadModeThreshold)) {
        self.isTableViewOffset = YES;
    } else {
        self.isTableViewOffset = NO;
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    // if scrolled to bottom of scrollview then load more rows
    if (self.shouldGetMoreNotifications && self.isTableViewOffset) {
        self.shouldGetMoreNotifications = NO;
        [self loadOlderNotifications];
    }
}

@end
