//
//  ZFOurMenuViewController.m
//  Ziferblat
//
//  Created by Terry Michel on 08/04/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "ZFActivitiesViewController.h"
#import "ZFActivitiesArchivesViewController.h"
#import "ZFActivityDetailViewController.h"
#import "ZFActivitiesCell.h"
#import "ZFActivitiesRequest.h"
#import "ZFActivityRequest.h"
#import "ZFActivity.h"
#import "ZFVenue.h"

static CGFloat const ZFScrollViewLoadModeThreshold = 100;
static CGFloat const ZFFetchCount = 10;
#define kRowHeight (notIPad ? 160:384)

@interface ZFActivitiesViewController ()

@property (nonatomic, strong) ZFVenue *selectedVenue;
@property (nonatomic, strong) NSMutableArray *openActivies;
@property (nonatomic, strong) NSMutableArray *iDoActivities;
@property (nonatomic, strong) NSMutableArray *iDidActivities;
@property (nonatomic, strong) NSMutableArray *ongoingActivities;
@property (nonatomic) NSInteger currentOpenPage;
@property (nonatomic) NSInteger currentDoPage;
@property (nonatomic) NSInteger currentDidPage;
@property (nonatomic) NSInteger currentOngoingPage;
@property (nonatomic) BOOL shouldGetMoreOpenActivities;
@property (nonatomic) BOOL shouldGetMoreDoActivities;
@property (nonatomic) BOOL shouldGetMoreDidActivities;
@property (nonatomic) BOOL shouldGetMoreOngoingActivities;
@property (nonatomic) BOOL isTableViewOffset;
@property (nonatomic, strong) UITableView *tableView;

@end

@implementation ZFActivitiesViewController

- (instancetype)initWithSelectedVenue:(ZFVenue *)aVenue {
    if (self = [super init]) {
        self.selectedVenue = aVenue;
        self.openActivies = [NSMutableArray new];
        self.iDoActivities = [NSMutableArray new];
        self.iDidActivities = [NSMutableArray new];
        self.ongoingActivities = [NSMutableArray new];
        self.currentOpenPage = 0;
        self.currentDoPage = 0;
        self.currentDidPage = 0;
        self.currentOngoingPage = 0;
        self.shouldGetMoreOpenActivities = YES;
        self.shouldGetMoreDoActivities = YES;
        self.shouldGetMoreDidActivities = YES;
        self.shouldGetMoreOngoingActivities = YES;
        self.isTableViewOffset = NO;
    }
    return self;
}

#pragma mark - 
#pragma mark Lifecycle

- (void)loadView {
    contentView = [[ZFActivitiesView alloc] init];
    self.view = contentView;
    self.tableView = contentView.tableView.tableView;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"ZFActivitiesCell" bundle:nil]
         forCellReuseIdentifier:[ZFActivitiesCell reuseIdentifier]];
    contentView.scrollView.delegate = self;
    
    TARGET_TOUCH_UP(contentView.backButton, @selector(backButtonTapped))
    TARGET_TOUCH_UP(contentView.tab1.tabButton, @selector(tabButtonTapped:))
    TARGET_TOUCH_UP(contentView.tab2.tabButton, @selector(tabButtonTapped:))
    TARGET_TOUCH_UP(contentView.tab3.tabButton, @selector(tabButtonTapped:))
    TARGET_TOUCH_UP(contentView.tab4.tabButton, @selector(tabButtonTapped:))
    TARGET_TOUCH_UP(contentView.archiveButton, @selector(archiveButtonTapped))
    [contentView.tableView setController:self];
    self.tableView.tag = ZFActivityTypeOpen;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self clearData];
    [self fetchActivities];
}

- (void)clearData {
    [self.openActivies removeAllObjects];
    [self.iDoActivities removeAllObjects];
    [self.iDidActivities removeAllObjects];
    [self.ongoingActivities removeAllObjects];
    self.currentOpenPage = 0;
    self.currentDoPage = 0;
    self.currentDidPage = 0;
    self.currentOngoingPage = 0;
    self.shouldGetMoreOpenActivities = YES;
    self.shouldGetMoreDoActivities = YES;
    self.shouldGetMoreDidActivities = YES;
    self.shouldGetMoreOngoingActivities = YES;
}

#pragma mark - Requests

- (void)fetchActivities {
    Show_Hud(NSLocalizedString(@"Fetching data...",nil))
    [ZFActivitiesRequest requestWithActivityType:self.tableView.tag
                                         venueId:self.selectedVenue.uuid
                                            page:[self getCurrentPageNumberForType]
                                      fetchCount:ZFFetchCount
                                      completion:^(LORESTRequest *aRequest) {
                                          Hide_HudAndPerformBlock(^{
                                              if (aRequest.resultStatus == LORESTRequestResultStatusSucceeded) {
                                                  ZFPagedResults *pagedResult = (ZFPagedResults *)aRequest.result;
                                                  [self handleResultArray:pagedResult.result];
                                              } else {
                                                  // Handle error
                                                  [self handleResultArray:@[]];
                                              }
                                              
                                              [self.tableView reloadData];
                                          });
                                      }];
}

- (void)tabButtonTapped:(UIButton *)sender {
    if (self.tableView.tag != sender.tag) {
        self.tableView.tag = sender.tag;
        [[self currentActivitiesArrayForType:self.tableView.tag] removeAllObjects];
        [self resetPageNumbers];
        
        [contentView.tab1 setSelected:sender.tag == ZFActivityTypeOpen];
        [contentView.tab2 setSelected:sender.tag == ZFActivityTypeDo];
        [contentView.tab3 setSelected:sender.tag == ZFActivityTypeDid];
        [contentView.tab4 setSelected:sender.tag == ZFActivityTypeOngoing];
        
        [self fetchActivities];
    }
}

- (void)archiveButtonTapped {
    ZFActivitiesArchivesViewController *activitiesArchivesViewController = [[ZFActivitiesArchivesViewController alloc] init];
    [self presentViewController:activitiesArchivesViewController animated:YES completion:nil];
}

#pragma mark - 
#pragma mark UITableViewDataSource

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    ZFActivitiesCell *cell = [tableView dequeueReusableCellWithIdentifier:[ZFActivitiesCell reuseIdentifier]];
    ZFActivity *activity = [[self currentActivitiesArrayForType:self.tableView.tag]
                                                  objectAtIndex:indexPath.row];
    [cell updateContentForActivity:activity];
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self currentActivitiesArrayForType:self.tableView.tag].count;
}

#pragma mark - 
#pragma mark UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return kRowHeight;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    dispatch_async(dispatch_get_main_queue(), ^{
        
        __block ZFActivity *selectedActivity = [[self currentActivitiesArrayForType:self.tableView.tag] objectAtIndex:indexPath.row];
        Show_Hud(nil);
        [ZFActivityRequest requestWithVenueId:self.selectedVenue.uuid activityId:selectedActivity.uuid completion:^(LORESTRequest *aRequest) {
            Hide_HudAndPerformBlock(^{
                if (aRequest.resultStatus == LORESTRequestResultStatusSucceeded) {
                    ZFActivityDetailViewController *detailVC = [[ZFActivityDetailViewController alloc] initWithNibName:@"ZFActivityDetailViewController" bundle:nil];
                    detailVC.venueId = self.selectedVenue.uuid;
                    detailVC.selectedActivity = aRequest.result;
                    [self.navigationController pushViewController:detailVC animated:YES];
                }
            })
        }];
    });
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    // if scrolled to within 100 pixels of the bottom of the content
    // set flag to cause next page of notifications to be loaded.
    CGFloat contentOffsetBottom = scrollView.contentOffset.y + scrollView.contentInset.top + scrollView.frame.size.height;
    CGFloat contentHeight = scrollView.contentSize.height + scrollView.contentInset.top + scrollView.contentInset.bottom;
    
    self.isTableViewOffset = (contentOffsetBottom > contentHeight - ZFScrollViewLoadModeThreshold);
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    if (self.isTableViewOffset && [self getShouldGetMoreEventsOfType:self.tableView.tag]) {
        [self increaseCurrentPageNumberOfType:self.tableView.tag];
        [self fetchActivities];
    }
}

#pragma mark - Helpers

- (NSInteger)getCurrentPageNumberForType {
    switch ((ZFActivityType)self.tableView.tag) {
        case ZFActivityTypeOpen: {
            return self.currentOpenPage;
        }
        case ZFActivityTypeDo: {
            return self.currentDoPage;
        }
        case ZFActivityTypeDid: {
            return self.currentDidPage;
        }
        case ZFActivityTypeOngoing: {
            return self.currentOngoingPage;
        }
        default:
            return 0;
    }
}

- (BOOL)getShouldGetMoreEventsOfType:(ZFActivityType)type {
    switch ((ZFActivityType)self.tableView.tag) {
        case ZFActivityTypeOpen: {
            return self.shouldGetMoreOpenActivities;
        }
        case ZFActivityTypeDo: {
            return self.shouldGetMoreDoActivities;
        }
        case ZFActivityTypeDid: {
            return self.shouldGetMoreDidActivities;
        }
        case ZFActivityTypeOngoing: {
            return self.shouldGetMoreOngoingActivities;
        }
        default:
            return NO;
    }
}

- (void)increaseCurrentPageNumberOfType:(ZFActivityType)type {
    switch (type) {
        case ZFActivityTypeOpen: {
            self.currentOpenPage++;
            break;
        }
        case ZFActivityTypeDo: {
            self.currentDoPage++;
            break;
        }
        case ZFActivityTypeDid: {
            self.currentDidPage++;
            break;
        }
        case ZFActivityTypeOngoing: {
            self.currentOngoingPage++;
            break;
        }
        default:
            break;
    }
}

- (void)resetPageNumbers {
    self.currentOpenPage = 0;
    self.currentDoPage = 0;
    self.currentDidPage = 0;
    self.currentOngoingPage = 0;
}

- (NSMutableArray *)currentActivitiesArrayForType:(ZFActivityType)type {
    switch (type) {
        case ZFActivityTypeOpen:
            return self.openActivies;
        case ZFActivityTypeDo:
            return self.iDoActivities;
        case ZFActivityTypeDid:
            return self.iDidActivities;
        case ZFActivityTypeOngoing:
            return self.ongoingActivities;
    }
    return [NSMutableArray new];
}

- (void)handleResultArray:(NSArray *)resultArray {
    switch ((ZFActivityType)self.tableView.tag) {
        case ZFActivityTypeOpen: {
            [self.openActivies addObjectsFromArray:resultArray];
            self.shouldGetMoreOpenActivities = (resultArray.count == ZFFetchCount);
            break;
        }
        case ZFActivityTypeDo: {
            [self.iDoActivities addObjectsFromArray:resultArray];
            self.shouldGetMoreDoActivities = (resultArray.count == ZFFetchCount);
            break;
        }
        case ZFActivityTypeDid: {
            [self.iDidActivities addObjectsFromArray:resultArray];
            self.shouldGetMoreDidActivities = (resultArray.count == ZFFetchCount);
            break;
        }
        case ZFActivityTypeOngoing: {
            [self.ongoingActivities addObjectsFromArray:resultArray];
            self.shouldGetMoreOngoingActivities = (resultArray.count == ZFFetchCount);
            break;
        }
        default:
            break;
    }
    
    [contentView updateLayoutForNumberOfRows:[self currentActivitiesArrayForType:self.tableView.tag].count
                               withRowHeight:kRowHeight];
}

@end
