//
//  Created by Danny Bravo
//  Copyright (c) 2014 Locassa Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LOOverlayViewType.h"
#import "LOOverlayChildView.h"

@class LOOverlayChildViewController;
@protocol LOOverlayChildViewControllerDelegate <NSObject>
@optional
- (void)overlayChildViewControllerDidDismiss:(LOOverlayChildViewController *)aController;
@end

@interface LOOverlayChildViewController : UIViewController {
    LOOverlayChildView *contentView;
    LOOverlayViewType type;
    __weak id<LOOverlayChildViewControllerDelegate> delegate;
    BOOL isPersisted;
}

#pragma mark - initializers
- (instancetype)initWithType:(LOOverlayViewType)aType
                 andDelegate:(id <LOOverlayChildViewControllerDelegate>)aDelegate
                 isPersisted:(BOOL)persisted;
- (void)setupOverlayView;

#pragma mark - animations
- (void)dismissAndPerformBlock:(void(^)())aBlock;

@end
