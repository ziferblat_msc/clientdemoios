//
//  ZFPasswordSuccessfullyViewController.m
//  Ziferblat
//
//  Created by Jose Fernandez on 30/07/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "ZFPasswordSuccessfullyViewController.h"
#import "ZFButton.h"

@interface ZFPasswordSuccessfullyViewController ()
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UILabel *topNavigationLabel;
@property (weak, nonatomic) IBOutlet UILabel *subInstructionsLabel;
@property (weak, nonatomic) IBOutlet UILabel *instructionsLabel;
@property (weak, nonatomic) IBOutlet UILabel *resetPasswordLabel;

@end

@implementation ZFPasswordSuccessfullyViewController {
    ZFButton *continueButton;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupViewSizes];
    
    [self setupLocalizedStrings];
    [self setupContinueButton];
}

#pragma mark -
#pragma mark Setup UI

- (void)setupViewSizes
{
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        self.topNavigationLabel.font = FontRockWell(26);
        self.resetPasswordLabel.font = FontRockWell(30);
        self.instructionsLabel.font = FontRockWell(26);
        self.subInstructionsLabel.font = FontRockWell(26);
        
        self.topNavigationControllerHeightConstraint.constant = 48;
        self.resetPasswordLabelWidthConstraint.constant = 300;
        self.resetPasswordLabelYConstraint.constant = 22;
        self.resetPasswordLabelHeightConstraint.constant = 60;
        self.insructionsLabelYConstraint.constant = 140;
        self.insructionsLabelWidthConstraint.constant = 500;
        self.subinstructionsLabelYConstraint.constant = 30;
        self.subinstructionsLabelWidthConstraint.constant = 500;
    }
}

- (void)setupLocalizedStrings {
    self.topNavigationLabel.text = NSLocalizedString(@"BACK TO LOG IN PAGE", nil);
    self.instructionsLabel.text = NSLocalizedString(@"Congratulations!", nil);
    self.subInstructionsLabel.text = NSLocalizedString(@"Your password has been successfully reset. You can now access the app with your new password.", nil);
}

- (void)setupContinueButton {
    continueButton = [ZFButton buttonWithTitle:NSLocalizedString(@"CONTINUE", nil) showMarks:NO];
    continueButton.translatesAutoresizingMaskIntoConstraints = NO;
    [self.scrollView addSubview:continueButton];
    
    NSLayoutConstraint *continueButtonTopConstraint = [NSLayoutConstraint constraintWithItem:continueButton
                                                                                attribute:NSLayoutAttributeTop
                                                                                relatedBy:NSLayoutRelationEqual
                                                                                   toItem:self.subInstructionsLabel
                                                                                attribute:NSLayoutAttributeBottom
                                                                               multiplier:1.0
                                                                                 constant:(UI_USER_INTERFACE_IDIOM() !=UIUserInterfaceIdiomPad ? 34:60)];
    NSLayoutConstraint *continueButtonCenterXConstraint = [NSLayoutConstraint constraintWithItem:continueButton
                                                                                    attribute:NSLayoutAttributeCenterX
                                                                                    relatedBy:NSLayoutRelationEqual
                                                                                       toItem:self.subInstructionsLabel
                                                                                    attribute:NSLayoutAttributeCenterX
                                                                                   multiplier:1.0
                                                                                     constant:0];
    NSLayoutConstraint *continueButtonWidthConstraint = [NSLayoutConstraint constraintWithItem:continueButton
                                                                                  attribute:NSLayoutAttributeWidth
                                                                                  relatedBy:NSLayoutRelationEqual
                                                                                     toItem:nil
                                                                                  attribute:NSLayoutAttributeWidth
                                                                                 multiplier:1.0
                                                                                   constant:135];
    NSLayoutConstraint *continueButtonHeightConstraint = [NSLayoutConstraint constraintWithItem:continueButton
                                                                                   attribute:NSLayoutAttributeHeight
                                                                                   relatedBy:NSLayoutRelationEqual
                                                                                      toItem:nil
                                                                                   attribute:NSLayoutAttributeHeight
                                                                                  multiplier:1.0
                                                                                    constant:44];
    
    [self.scrollView addConstraints:@[continueButtonTopConstraint, continueButtonCenterXConstraint, continueButtonWidthConstraint, continueButtonHeightConstraint]];
    
    TARGET_TOUCH_UP(continueButton, @selector(continueButtonWasPressed:));
}

#pragma mark -
#pragma mark Actions and Selectors

- (IBAction)backButtonWasPressed:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)continueButtonWasPressed:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

@end
