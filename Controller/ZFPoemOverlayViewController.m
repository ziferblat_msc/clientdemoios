//
//  ZFPoemViewController.m
//  Ziferblat
//
//  Created by Terry Michel on 09/04/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "ZFPoemOverlayViewController.h"

@implementation ZFPoemOverlayViewController

#pragma mark - 
#pragma mark Lifecycle

- (instancetype)init {
    self = [super initWithType:LOOverlayViewType_Bottom andDelegate:self isPersisted:NO];
    if(self) {
        
    }
    return self;
}

- (void)setupOverlayView {
    castedViewToDisplay = [[ZFPoemView alloc] init];
    TARGET_TOUCH_UP(castedViewToDisplay.backButton, @selector(backButtonTapped))
    [contentView setupWithCastedViewToDisplay:castedViewToDisplay backgroundAlpha:0];
}

#pragma mark - 
#pragma mark Buttons actions

- (void)backButtonTapped {
    [self dismissAndPerformBlock:nil];
}

@end
