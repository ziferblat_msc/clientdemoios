//
//  ZFAddANewCollaborationViewController.h
//  Ziferblat
//
//  Created by Terry Michel on 08/04/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "ZFAddANewCollaborationView.h"

@interface ZFAddANewCollaborationViewController : UIViewController {
    ZFAddANewCollaborationView *contentView;
}

@end
