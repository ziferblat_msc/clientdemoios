//
//  ZFLeaderBoardsViewController.m
//  Ziferblat
//
//  Created by Terry Michel on 15/04/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "ZFLeaderBoardsViewController.h"
#import "ZFLeaderboardsIndividualViewController.h"
#import "ZFProfileViewController.h"
#import "ZFOtherProfileViewController.h"
#import "ZFVenue.h"
#import "ZFGetSingleMemberRequest.h"
#import "ZFGetMemberFriendsListRequest.h"
#import "ZFGetMemberVenueDetailsRequest.h"
#import "ZFGetExperiencePointsLeaderBoardByVenue.h"
#import "ZFGetExperiencePointsPositionByVenue.h"
#import "ZFGetTimeSpentLeaderBoardByVenue.h"
#import "ZFGetTimeSpentPositionByVenue.h"
#import "ZFGetMostTasksCompletedByVenue.h"
#import "ZFGetTasksCompletedPositionByVenue.h"

@interface ZFLeaderBoardsViewController()

@property (strong, nonatomic) ZFLeaderBoardCount *personalExpScore;
@property (strong, nonatomic) ZFLeaderBoardCount *personalTimeScore;
@property (strong, nonatomic) ZFLeaderBoardCount *personalTaskScore;
@property (strong, nonatomic) NSString *naviagationTitle;
@property (strong, nonatomic) NSNumber* initialVenueId;

@end

@implementation ZFLeaderBoardsViewController

@synthesize leaderBoardIndividualViewController;

#pragma mark - 
#pragma mark Lifecycle

- (instancetype)initWithTitle:(NSString *)title
                      venueId:(NSNumber *)venueId {
    self = [super init];
    
    if (self) {
        self.naviagationTitle = title;
        self.initialVenueId = venueId;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (!self.naviagationTitle) {
        self.naviagationTitle = NSLocalizedString(@"BACK TO ROOM", @"Default title for the Leaderboards");
    }
    
    contentView = [[ZFLeaderBoardsView alloc] initWithVenueId:self.initialVenueId
                                                        title:self.naviagationTitle];
    contentView.delegate = self;
    self.view = contentView;
    
    TARGET_TOUCH_UP(contentView.backButton, @selector(backButtonTapped))
    TARGET_TOUCH_UP(contentView.profileUserButton1, @selector(profileButtonTapped:))
    TARGET_TOUCH_UP(contentView.profileUserButton2, @selector(profileButtonTapped:))
    TARGET_TOUCH_UP(contentView.profileUserButton3, @selector(profileButtonTapped:))
    TARGET_TOUCH_UP(contentView.profileUserButton4, @selector(profileButtonTapped:))
    TARGET_TOUCH_UP(contentView.profileUserButton5, @selector(profileButtonTapped:))
    TARGET_TOUCH_UP(contentView.profileUserButton6, @selector(profileButtonTapped:))
    TARGET_TOUCH_UP(contentView.profileUserButton7, @selector(profileButtonTapped:))
    TARGET_TOUCH_UP(contentView.profileUserButton8, @selector(profileButtonTapped:))
    TARGET_TOUCH_UP(contentView.profileUserButton9, @selector(profileButtonTapped:))
    TARGET_TOUCH_UP(contentView.ziferblatSelector, @selector(switchZiferblatButtonTapped:));
    TARGET_TOUCH_UP(contentView.mostExperiencePointsSeeAllButton, @selector(experienceButtonTapped:))
    TARGET_TOUCH_UP(contentView.mostTimeSpentOnZiferblatSeeAllButton, @selector(timeButtonTapped:))
    TARGET_TOUCH_UP(contentView.mostTasksSpentOnZiferblatSeeAllButton, @selector(taskButtonTapped:))
    
    // Network
    [self makeRequestsWith:self.initialVenueId];
}

#pragma mark - Buttons actions

- (void)backButtonTapped {
    if([self.parentViewController isKindOfClass:[UINavigationController class]]) {
        [self.navigationController popViewControllerAnimated:YES];
    } else {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

- (void)profileButtonTapped:(UIButton *)sender {
    // Get reference to the user via associated identifier (configured in ZFLeaderBoard view)
    NSNumber *userId = [sender readLocassaIdentifier];
    
    if (userId) {
        [self openProfileForUserId:userId
                              from:self];
    } else {
        [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"This place can be yours!", @"Leaderboards error HUD: tap on profile placeholder")];
    }
}

- (void)switchZiferblatButtonTapped:(UIControl *)aControl {
    [contentView ziferblatSelectionButtonTapped];
}

- (void)experienceButtonTapped:(UIButton *)sender {
    
    NSNumber *venueId = [sender readLocassaIdentifier];
    [self makeIndividualLeaderBoardRequestForVenueId:venueId
                                      initialRequest:YES
                                     leaderBoardType:Experince
                                              search:@""];
}

- (void)timeButtonTapped:(UIButton *)sender {
    
    NSNumber *venueId = [sender readLocassaIdentifier];
    [self makeIndividualLeaderBoardRequestForVenueId:venueId
                                      initialRequest:YES
                                     leaderBoardType:Time
                                              search:@""];
}

- (void)taskButtonTapped:(UIButton *)sender {
    
    NSNumber *venueId = [sender readLocassaIdentifier];
    [self makeIndividualLeaderBoardRequestForVenueId:venueId
                                      initialRequest:YES
                                     leaderBoardType:Task
                                              search:@""];
}

- (ZFLeaderBoardCount *)getPersonalScoreForType:(ZFLeaderBoardType)aType {
    ZFLeaderBoardCount *personalScore;
    switch (aType) {
        case Experince:
            personalScore = self.personalExpScore;
            break;
        case Time:
            personalScore = self.personalTimeScore;
            break;
        case Task:
            personalScore = self.personalTaskScore;
            break;
        default:
            break;
    }
    return personalScore;
}

- (void)presentLeaderBoard:(NSArray<ZFLeaderBoard *>*)aBoard
                  withType:(ZFLeaderBoardType)aType
                   venueId:(NSNumber *)venueId
                      from:(UIViewController *)presenter {
    
    ZFLeaderBoardCount *personalScore = [self getPersonalScoreForType:aType];

    leaderBoardIndividualViewController = [[ZFLeaderboardsIndividualViewController alloc] initWithContent:aBoard
                                                                                                  andType:aType
                                                                                            personalScore:personalScore
                                                                                                    venue:venueId
                                                                                                      tab:ZFLeaderBoardsSortType_LastDays];
    leaderBoardIndividualViewController.delegate = self;

    [presenter presentViewController:leaderBoardIndividualViewController
                       animated:YES
                     completion:nil];
}

#pragma mark - ZFLeaderboardsViewDelegate

- (void)makeIndividualRequestWithVenue:(NSNumber *)venueId
                           resultItems:(int)total
                               andType:(ZFLeaderBoardType)aType {
    
    [self makeIndividualLeaderBoardRequestForVenueId:venueId
                                      initialRequest:NO
                                     leaderBoardType:aType
                                               search:@""];
}

- (void)makeIndividualRequestWithVenue:(NSNumber *)venueId
                           resultItems:(int)total
                                search:(NSString *)query
                               andType:(ZFLeaderBoardType)aType {
    
    [self makeIndividualLeaderBoardRequestForVenueId:venueId initialRequest:NO leaderBoardType:aType search:query];
    
}

- (void)makeRequestsWith:(NSNumber *)aVenueId {
    
    BOOL filter = leaderBoardIndividualViewController.contentView.isAllTimeTabSelected;
    
    [SVProgressHUD showWithStatus:NSLocalizedString(@"Fetching data...",nil) maskType:SVProgressHUDMaskTypeGradient];
    [ZFGetExperiencePointsLeaderBoardByVenue requestWithCompletion:^(LORESTRequest *experiencePointsLeaderBoardByVenueRequest) {
        if(experiencePointsLeaderBoardByVenueRequest.resultStatus == LORESTRequestResultStatusSucceeded) {
            
            [ZFGetTimeSpentLeaderBoardByVenue requestWithCompletion:^(LORESTRequest *getTimeSpentLeaderBoardByVenueRequest) {
                if(getTimeSpentLeaderBoardByVenueRequest.resultStatus == LORESTRequestResultStatusSucceeded) {
                    
                    [ZFGetTimeSpentPositionByVenue requestWithCompletion:^(LORESTRequest *getTimeSpentPositionRequest) {
                        if(getTimeSpentPositionRequest.resultStatus == LORESTRequestResultStatusSucceeded) {
                            
                            [ZFGetExperiencePointsPositionByVenue requestWithCompletion:^(LORESTRequest *getExperiencePositionRequest) {
                                
                                if(getExperiencePositionRequest.resultStatus == LORESTRequestResultStatusSucceeded) {
                                    
                                    // Get all tasks charts
                                    [ZFGetMostTasksCompletedByVenue requestWithCompletion:^(LORESTRequest *getMostTasksRequest) {
                                        if (getMostTasksRequest.resultStatus == LORESTRequestResultStatusSucceeded) {
                                            
                                            [ZFGetTasksCompletedPositionByVenue requestWithCompletion:^(LORESTRequest *getTasksRankRequest) {
                                                
                                                if (getTasksRankRequest.resultStatus == LORESTRequestResultStatusSucceeded) {
                                                    
                                                    // Refresy UI
                                                    [self updateUIWithExperienceBoard:experiencePointsLeaderBoardByVenueRequest
                                                                            timeBoard:getTimeSpentLeaderBoardByVenueRequest
                                                                         timePosition:getTimeSpentPositionRequest
                                                                    experiencePistion:getExperiencePositionRequest
                                                                                tasks:getMostTasksRequest
                                                                        personalTasks:getTasksRankRequest];
                                                }
                                                
                                            } forVenue:aVenueId disableFilter:filter];
                                            
                                        } else {
                                            [self showError];
                                        }
                                    } forVenue:aVenueId resultItems:ZFLeaderBoardsTopPlaces search:@"" disableFilter:filter];
                                    
                                } else {
                                    [self showError];
                                }
                            } disableFilter:filter venueId:aVenueId forMemberId:nil];
                        } else {
                            [self showError];
                        }
                    } forVenue:aVenueId disableFilter:filter];
                    
                } else {
                    [self showError];
                }
            } forVenue:aVenueId resultItems:ZFLeaderBoardsTopPlaces search:@"" disableFilter:filter];
            
        } else {
            [self showError];
        }
    } forVenue:aVenueId resultItems:ZFLeaderBoardsTopPlaces search:@"" disableFilter:filter];
}

- (void)openProfileForUserId:(NSNumber *)userId from:(UIViewController *)vc {
    
    BOOL conditionalFilter = YES; // Be default shows 30 days
    NSNumber *conditionalVenue = contentView.ziferblatSelector.selectedVenue.uuid;
    if ([vc isMemberOfClass:[ZFLeaderboardsIndividualViewController class]]) {
        ZFLeaderboardsIndividualViewController *sender = (ZFLeaderboardsIndividualViewController *)vc;
        if (sender.contentView.isAllTimeTabSelected) {
            conditionalFilter = NO;
        }
        conditionalVenue = sender.contentView.ziferblatSelector.selectedVenue.uuid;
    }
    // Please use this 2 conditional values if you want data showed in Profile View depending on from it was opened
    // To enable conditions, comment out 2 following lines:
    conditionalFilter = NO;
    conditionalVenue = nil;
    
    if ([userId isEqualToNumber:CurrentUser.uuid]) {
        // Related to ZFI-303. Done by Peter
        [self openOwnProfileWithViewController:vc];
    } else {
        // Download profile
        [ZFGetSingleMemberRequest requestWithUser:userId completion:^(LORESTRequest *getUserRequest) {
            if (getUserRequest.resultStatus == LORESTRequestResultStatusSucceeded) {
                
                ZFUser *downloadedUser = getUserRequest.result;
                
                // Download additional info...
                [SVProgressHUD showWithStatus:NSLocalizedString(@"Loading member...",nil) maskType:SVProgressHUDMaskTypeGradient];
                [ZFGetMemberFriendsListRequest requestWithMemberId:downloadedUser.uuid andVenueId:CurrentUser.selectedVenue.uuid completion:^(LORESTRequest *aRequest) {
                    if(aRequest.resultStatus == LORESTRequestResultStatusSucceeded) {
                        downloadedUser.friendsList = aRequest.result;
                        downloadedUser.friendsLoaded = TRUE;
                        
                        [ZFGetMemberVenueDetailsRequest requestWithMemberId:downloadedUser.uuid
                                                                    venueId:CurrentUser.selectedVenue.uuid // This is probably not good but profile view controller is not ready to work with Worldwide venue yet
                                                                 completion:^(LORESTRequest *aRequest) {
                            if(aRequest.resultStatus == LORESTRequestResultStatusSucceeded) {
                                downloadedUser.venueDetails = aRequest.result;
                                
                                // Open profile view controller
                                ZFOtherProfileViewController *otherProfileVC = [[ZFOtherProfileViewController alloc] initWithUser:downloadedUser backMessage:NSLocalizedString(@"BACK TO LEADERBOARDS", nil) andFilter:conditionalFilter forVenueId:conditionalVenue];
                                
                                Hide_HudAndPerformBlock((^{
                                    otherProfileVC.canBeDismissed = YES;
                                    [vc presentViewController:otherProfileVC animated:YES completion:nil];
                                }))
                                
                            } else {
                                Show_ConnectionError
                            }
                        }];
                    } else {
                        Show_ConnectionError
                    }
                }];
            } else {
                [self showError];
            }
        }];
    }
}

- (void)openOwnProfileWithViewController:(UIViewController *)vc {
    ZFProfileViewController *profileViewController = [[ZFProfileViewController alloc] initWithUser:CurrentUser
                                                                                       backMessage:NSLocalizedString(@"BACK TO LEADERBOARDS", @"Back button title for leaderboards")
                                                                                         andFilter:YES
                                                                                        forVenueId:contentView.ziferblatSelector.selectedVenue.uuid];
    profileViewController.canBeDismissed = YES;
    [vc presentViewController:profileViewController animated:YES completion:nil];
}

#pragma mark - Network

- (void)showError {
    [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"Could not read in data",nil)];
}

- (void)personalRequestWithVenueId:(NSNumber *)venueId
                           content:(NSArray<ZFLeaderBoard *> *)board
                              type:(ZFLeaderBoardType)leaderboardType {
    
    BOOL filter = leaderBoardIndividualViewController.contentView.isAllTimeTabSelected;
    
    if (leaderboardType == Experince) {
     
        [ZFGetExperiencePointsPositionByVenue requestWithCompletion:^(LORESTRequest *request) {
            
            ZFLeaderBoardCount *result = request.result;
            
            [leaderBoardIndividualViewController refreshWithContent:board
                                                            andType:leaderboardType
                                                      personalScore:result];
            
        } disableFilter:filter venueId:venueId forMemberId:nil];
        
    } else if (leaderboardType == Time) {
        
        [ZFGetTimeSpentPositionByVenue requestWithCompletion:^(LORESTRequest *request) {
            
            ZFLeaderBoardCount *result = request.result;
            
            [leaderBoardIndividualViewController refreshWithContent:board
                                                            andType:leaderboardType
                                                      personalScore:result];
            
        } forVenue:venueId disableFilter:filter];
        
    } else if (leaderboardType == Task) {
        
        [ZFGetTasksCompletedPositionByVenue requestWithCompletion:^(LORESTRequest *request) {
            
            ZFLeaderBoardCount *result = request.result;
            
            [leaderBoardIndividualViewController refreshWithContent:board
                                                            andType:leaderboardType
                                                      personalScore:result];
            
        } forVenue:venueId disableFilter:filter];
    }
}

- (void)personalTimeRequest {
    
}


#pragma mark - Other

- (void)makeIndividualLeaderBoardRequestForVenueId:(NSNumber *)aVenueId
                                    initialRequest:(BOOL)isInitial
                                   leaderBoardType:(ZFLeaderBoardType)aType
                                            search:(NSString *)query {
    [SVProgressHUD showWithStatus:NSLocalizedString(@"Fetching data...",nil) maskType:SVProgressHUDMaskTypeGradient];
    
    BOOL filter = leaderBoardIndividualViewController.contentView.isAllTimeTabSelected;
    
    if (aType == Experince) {
        
        [ZFGetExperiencePointsLeaderBoardByVenue requestWithCompletion:^(LORESTRequest *experiencePointsLeaderBoardByVenueRequest) {
            if(experiencePointsLeaderBoardByVenueRequest.resultStatus == LORESTRequestResultStatusSucceeded) {
                
                Hide_HudAndPerformBlock((^{
                    ZFPagedResults *pageResults = experiencePointsLeaderBoardByVenueRequest.result;
                    NSArray<ZFLeaderBoard *> *leaderboardExpArray = [NSArray arrayWithArray:pageResults.result];
                    if (isInitial) {
                        [self presentLeaderBoard:leaderboardExpArray
                                        withType:aType
                                         venueId:aVenueId
                                            from:self];
                    } else {
                        // Just update, controller already visible
                        [self personalRequestWithVenueId:aVenueId content:leaderboardExpArray type:aType];
                    }
                }))
            } else {
                [self showError];
            }
        } forVenue:aVenueId resultItems:LOMaxItemCount search:query disableFilter:filter];
        
    } else if (aType == Time) {
        
        [ZFGetTimeSpentLeaderBoardByVenue requestWithCompletion:^(LORESTRequest *experiencePointsLeaderBoardByVenueRequest) {
            if(experiencePointsLeaderBoardByVenueRequest.resultStatus == LORESTRequestResultStatusSucceeded) {
                
                Hide_HudAndPerformBlock((^{
                    ZFPagedResults *secondPageResults = experiencePointsLeaderBoardByVenueRequest.result;
                    NSArray<ZFLeaderBoard *> *leaderboardTimeSpentArray = [NSArray arrayWithArray:secondPageResults.result];
                    if (isInitial) {
                        [self presentLeaderBoard:leaderboardTimeSpentArray
                                        withType:aType
                                         venueId:aVenueId
                                            from:self];
                    } else {
                        // Refresh
                        [self personalRequestWithVenueId:aVenueId content:leaderboardTimeSpentArray type:aType];
                    }
                }))
            } else {
                [self showError];
            }
        } forVenue:aVenueId resultItems:LOMaxItemCount search:query disableFilter:filter];
        
    } else if (aType == Task) {
        
        [ZFGetMostTasksCompletedByVenue requestWithCompletion:^(LORESTRequest *mostTasksCompletedByVenueRequest) {
            if(mostTasksCompletedByVenueRequest.resultStatus == LORESTRequestResultStatusSucceeded) {
                
                Hide_HudAndPerformBlock((^{
                    ZFPagedResults *secondPageResults = mostTasksCompletedByVenueRequest.result;
                    NSArray<ZFLeaderBoard *> *leaderboardTasksArray = [NSArray arrayWithArray:secondPageResults.result];
                    if (isInitial) {
                        [self presentLeaderBoard:leaderboardTasksArray
                                        withType:aType
                                         venueId:aVenueId
                                            from:self];
                    } else {
                        // Refresh
                        [self personalRequestWithVenueId:aVenueId content:leaderboardTasksArray type:aType];
                    }
                }))
            } else {
                [self showError];
            }
        } forVenue:aVenueId resultItems:LOMaxItemCount search:query disableFilter:filter];
    }
}

- (void)updateUIWithExperienceBoard:(LORESTRequest *)experiencePointsLeaderBoardRequest
                          timeBoard:(LORESTRequest *)timeSpentLeaderBoardRequest
                       timePosition:(LORESTRequest *)getTimeSpentPositionRequest
                  experiencePistion:(LORESTRequest *)getExperiencePositionRequest
                              tasks:(LORESTRequest *)tasksRequest
                      personalTasks:(LORESTRequest *)personalTasksRequest {
    
    Hide_HudAndPerformBlock((^{
        ZFPagedResults *pageResults = experiencePointsLeaderBoardRequest.result;
        ZFPagedResults *secondPageResults = timeSpentLeaderBoardRequest.result;
        ZFPagedResults *thirdPageResults = tasksRequest.result;
        
        NSArray<ZFLeaderBoard *> *leaderboardExpArray = [NSArray arrayWithArray:pageResults.result];
        ZFLeaderBoardCount *expiriencePosition = getExperiencePositionRequest.result;
        
        NSArray<ZFLeaderBoard *> *leaderboardTimeSpentArray = [NSArray arrayWithArray:secondPageResults.result];
        ZFLeaderBoardCount *timeSpentPosition = getTimeSpentPositionRequest.result;
        
        NSArray<ZFLeaderBoard *> *tasksArray = [NSArray arrayWithArray:thirdPageResults.result];
        ZFLeaderBoardCount *personalTasksPosition = personalTasksRequest.result;
        
        [contentView clean];
        
        [contentView reloadDataWithTimeLeaderBoards:leaderboardExpArray
                                      pesonalResult:expiriencePosition
                                           position:Top
                                        placeholder:NO];
        
        [contentView reloadDataWithTimeLeaderBoards:leaderboardTimeSpentArray
                                      pesonalResult:timeSpentPosition
                                           position:Middle
                                        placeholder:NO];
        
        [contentView reloadDataWithTimeLeaderBoards:tasksArray
                                      pesonalResult:personalTasksPosition
                                           position:Bottom
                                        placeholder:NO];
        
        self.personalExpScore = expiriencePosition;
        self.personalTimeScore = timeSpentPosition;
        self.personalTaskScore = personalTasksPosition;
    }))
}

@end
