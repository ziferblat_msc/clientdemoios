//
//  ZFEventDetailViewController.m
//  Ziferblat
//
//  Created by Jose Fernandez on 26/08/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "ZFEventDetailViewController.h"
#import "ZFEvents.h"
#import "ZFAttendeesCell.h"
#import "ZFRoundImageView.h"
#import "ZFAttendees.h"
#import "ZFEventAttendRequest.h"
#import "ZFEventLikeRequest.h"
#import "LOFavouriteActivityItemProvider.h"
#import "ZFGuestService.h"
#import "LOImageCache.h"
#import "LOImageDownloader.h"

@interface ZFEventDetailViewController() <UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>

@end

@implementation ZFEventDetailViewController

- (void)dealloc {
    Notification_RemoveObserver
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupViewSizes];
    
    [self registerCells];
    [self setupEventDetailsUI];
    
    Notification_Observe(kNotification_SingleEventRefreshed, receivedSingleEventRefreshed:);
    
    [[ZFNetworkService sharedInstance] pollForType:ZFPollingRequestType_SingleEvent|ZFPollingRequestType_SingleEventComments eventId:self.event.uuid venueId:self.venueId summary:YES];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self updateUI];
}

# pragma mark - Setup UI

-(void) setupViewSizes
{
    if(isIPad)
    {
        self.eventTitleLabel.font = [UIFont fontWithName:self.eventTitleLabel.font.fontName size:41];
        self.eventDateLabel.font = [UIFont fontWithName:self.eventDateLabel.font.fontName size:36];
        self.eventDescriptionLabel.font = [UIFont fontWithName:self.eventDescriptionLabel.font.fontName size:34];
        self.eventLikeLabel.font = [UIFont fontWithName:self.eventLikeLabel.font.fontName size:31];
        self.eventLikeCountLabel.font = [UIFont fontWithName:self.eventLikeCountLabel.font.fontName size:31];
        self.eventIWillComeLabel.font = [UIFont fontWithName:self.eventIWillComeLabel.font.fontName size:34];
        self.eventAttendeesLabel.font = [UIFont fontWithName:self.eventAttendeesLabel.font.fontName size:34];
    
        for(NSLayoutConstraint *constraint in self.view.constraints)
        {
            constraint.constant *= 2;
        }
    
        for (UIView *subview in self.view.subviews)
        {
            for(NSLayoutConstraint *constraint in subview.constraints)
            {
                constraint.constant *= 2;
            }
        }
    }
}

- (void)registerCells {
    [self.eventAttendeesCollectionView registerNib:[UINib nibWithNibName:@"ZFAttendeesCell" bundle:nil] forCellWithReuseIdentifier:[ZFAttendeesCell reuseIdentifier]];
}

- (void)setupEventDetailsUI
{
    UIImage *image = [LOImageCache imageFromCache:self.event.imageUrl];
    if (!image) {
        self.eventImageView.image = [UIImage imageNamed:@"ZFPlaceholderOfflineEvent 320pt width"];
        [LOImageDownloader downloadImageFromPath:self.event.imageUrl
                            withCompletionHander:^(UIImage *downloadedImage) {
                                [UIView transitionWithView:self.eventImageView
                                                  duration:0.5
                                                   options:UIViewAnimationOptionTransitionCrossDissolve
                                                animations:^{
                                                    self.eventImageView.image = downloadedImage;
                                                } completion:nil];
                            }];
    } else {
        self.eventImageView.image = image;
    }
    
    self.eventImageView.layer.borderColor = Colour_Black.CGColor;
    self.eventImageView.layer.borderWidth = 2.0;
    
    self.eventTitleLabel.text = self.event.eventTitle;
    
    self.eventDateLabel.text = [self.event.eventDate shortDatePointString];
    self.eventDescriptionLabel.text = self.event.eventDescription;
    
    [self.eventAttendButton setSelected:[self.event.attended boolValue]];
    self.eventIWillComeLabel.textColor = self.eventAttendButton.selected ? Colour_Black : Colour_Hex(@"A3A4A5");
	
	// Check is event date in the past
	NSDate *eventDate = _event.eventDate;
	NSDate *now = [NSDate new];
	BOOL isEventDateInPast = NO;
	if ([now compare:eventDate] == NSOrderedDescending)
    {
		isEventDateInPast = YES;
	}
    
	if (isEventDateInPast) {
		self.eventAttendButton.hidden = YES;
		self.eventIWillComeLabel.hidden = YES;
	} else {
		self.eventAttendButton.hidden = NO;
		self.eventIWillComeLabel.hidden = NO;
	}
    
    self.eventIWillComeLabel.text = NSLocalizedString(@"I will come", nil);
    self.eventAttendeesLabel.text = NSLocalizedString(@"Attendees:", nil);
    
    self.eventLikeLabel.text = NSLocalizedString(@"Like", nil);
    [self.eventLikeButton setSelected:[self.event.liked boolValue]];
    self.eventLikeHeartImageView.highlighted = self.eventLikeButton.selected;
    
    self.eventDescriptionLabel.preferredMaxLayoutWidth = kWidth - 80; // leading + trailing of the container view
}

- (void)updateUI {
    self.eventLikeCountLabel.text = [NSString stringWithFormat:@"%ld", (long)[self.event.likeCount integerValue]];
    self.collectionViewHeightConstraint.constant = (notIPad ? 31:74) * ceilf(self.event.attendees.count / 7.0);
    
    [self.eventAttendeesCollectionView reloadData];
}

# pragma mark - Actions and Selectors

- (void)receivedSingleEventRefreshed:(NSNotification *)notification {
    self.event = [ZFNetworkService sharedInstance].event;
    
    [self updateUI];
}

- (IBAction)shareButtonWasTapped:(UIButton *)sender {
    NSString *sharingText = [NSString stringWithFormat:@"WOW! I really love '%@' event on #Ziferblat!", self.event.eventTitle];
    
    LOFavouriteActivityItemProvider *itemProvider = [[LOFavouriteActivityItemProvider alloc] initWithTwitterShareText:sharingText
                                                                                                    facebookShareText:sharingText
                                                                                                       otherShareText:sharingText];
    
    UIActivityViewController *controller = [[UIActivityViewController alloc] initWithActivityItems:@[itemProvider] applicationActivities:nil];
    if(notIPad)
    {
        [self presentViewController:controller animated:YES completion:nil];
    }
    else
    {
        controller.popoverPresentationController.sourceView = self.eventShareButton;
        [self presentViewController:controller animated:YES completion:nil];
    }
}

- (IBAction)attendEventButtonWasTapped:(UIButton *)sender {
    if ([[ZFGuestService sharedInstance] makeSureUserIsNotAGuestFromViewController:self.parent]) {
        return;
    }
    [self.eventAttendButton setSelected:!sender.selected];
    self.eventIWillComeLabel.textColor = self.eventAttendButton.selected ? Colour_Black : Colour_Hex(@"A3A4A5");
    [ZFEventAttendRequest requestWithVenueId:self.venueId eventId:self.event.uuid attend:self.eventAttendButton.selected completion:nil];
}

- (IBAction)likeEventButtonWasTapped:(UIButton *)sender {
    if ([[ZFGuestService sharedInstance] makeSureUserIsNotAGuestFromViewController:self]) {
        return;
    }
    [self.eventLikeButton setSelected:!sender.selected];
    self.eventLikeHeartImageView.highlighted = self.eventLikeButton.selected;
    NSInteger countsLike = [self.eventLikeCountLabel.text integerValue];
    self.eventLikeCountLabel.text = [NSString stringWithFormat:@"%ld", (long)(self.eventLikeButton.selected ? ++countsLike : --countsLike)];
    [ZFEventLikeRequest requestWithVenueId:self.venueId eventId:self.event.uuid like:self.eventLikeButton.selected completion:nil];
}

# pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.event.attendees.count;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (notIPad)
    {
        return CGSizeMake(27, 27);
    }
    else
    {
        return CGSizeMake(65, 65);
    }
}

# pragma mark -
# pragma mark UICollectionViewDelegate

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    ZFAttendeesCell *attendeesCell = [collectionView dequeueReusableCellWithReuseIdentifier:[ZFAttendeesCell reuseIdentifier] forIndexPath:indexPath];
    ZFAttendees *attendees = self.event.attendees[indexPath.row];
    [attendeesCell layoutForAttendee:attendees];
    
    return attendeesCell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    ZFAttendees *member = self.event.attendees[indexPath.row];
    NSNumber *memberIdentifier = member.uuid;
    [[ZFPresenter sharedInstanceWithNavigationController:nil] presentProfileForMemberId:memberIdentifier andOptionalMember:member];
}

@end
