//
//  ZFLastPhotoViewController.h
//  Ziferblat
//
//  Created by Terry Michel on 08/04/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "ZFLastPhotoView.h"

@interface ZFLastPhotoViewController : UIViewController {
    ZFLastPhotoView *contentView;
}

@end
