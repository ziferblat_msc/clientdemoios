//
//  ZFTaskViewController.m
//  Ziferblat
//
//  Created by Terry Michel on 09/04/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "ZFTaskViewController.h"

@implementation ZFTaskViewController

#pragma mark - 
#pragma mark Lifecycle

- (void)loadView {
    contentView = [[ZFTaskView alloc] init];
    self.view = contentView;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    TARGET_TOUCH_UP(contentView.backButton, @selector(backButtonTapped))
    TARGET_TOUCH_UP(contentView.doTaskButton, @selector(doTaskButtonTapped))
}

#pragma mark - 
#pragma mark Buttons actions

- (void)backButtonTapped {
#warning TO IMPLEMENT
}

- (void)doTaskButtonTapped {
#warning TO IMPLEMENT
}

@end
