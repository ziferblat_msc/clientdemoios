//
//  ZFPageContentViewController.m
//  Ziferblat
//
//  Created by Boris Yurkevich on 22/10/2015.
//  Copyright © 2015 Locassa. All rights reserved.
//

#import "ZFPageContentViewController.h"

@interface ZFPageContentViewController ()

@end

@implementation ZFPageContentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.imageView.image = [UIImage imageNamed:self.imageFile];
    
    [self.openZiferblatButton setTitle:NSLocalizedString(@"Come in", @"Open tutorial button")
                              forState:UIControlStateNormal];
    self.openZiferblatButton.titleLabel.adjustsFontSizeToFitWidth = YES;
    self.openZiferblatButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    self.view.tintColor = [UIColor blackColor];
    
    if (self.pageIndex == kIndexLastTutorialPage) {
        self.openZiferblatButton.hidden = NO;
    } else {
        self.openZiferblatButton.hidden = YES;
    }
    
    switch (self.pageIndex) {
        case 0:
            self.pageTitle.text = NSLocalizedString(@"Welcome\nto Ziferblat!", @"Tutorial title 1");
            self.pageDescription.text = NSLocalizedString(@"Now your Ziferblat experience is even more fun and interesting, stretching beyond your visit.\n • • • \nLet's take a look at\n what you can do here:", @"Tutorial page body 1");
            self.pageDescriptionLarge.text = nil;
            break;
        case 1:
            self.pageTitle.text = nil;
            self.pageDescription.text = NSLocalizedString(@"See who's sitting in Ziferblat right now, learn more about them, make friends, and start a conversation.", @"Tutorial page body 2");
            self.pageDescriptionLarge.text = nil;
            break;
        case 2:
            self.pageTitle.text = nil;
            self.pageDescription.text = NSLocalizedString(@"Find information about each Ziferblat, read the latest news, browse through new photos and tasks.", @"Tutorial page body 3");
            self.pageDescriptionLarge.text = nil;
            break;
        case 3:
            self.pageTitle.text = NSLocalizedString(@"Turn your Ziferblat experience into a game!", @"Tutorial title 4");
            self.pageDescription.text = nil;
            self.pageDescriptionLarge.text = NSLocalizedString(@"Earn experience points! 1 minute in Z = 1⭐️\n\n\nCollect achievements\n\n\n\nGet a level-up for each achievement", @"Tutorial page body 4");
            break;
        case 4:
            self.pageTitle.text = NSLocalizedString(@"Your accomplishments will be remembered and rewarded:", @"Tutorial title 5");
            self.pageDescription.text = nil;
            self.pageDescriptionLarge.text = NSLocalizedString(@"\nEarn happy Ziferblat minutes\n\n\n\nSave Piastres\n\n\nActivate the time decelerator for achieving ranks in the leaderboard", @"Tutorial page body 5");
            break;
        case 5:
            self.pageTitle.text = nil;
            self.pageDescription.text = NSLocalizedString(@"In the Menu of Good Deeds you will find out how you can help Ziferblat in exchange for minutes and other perks.", @"Tutorial page body 6");
            self.pageDescriptionLarge.text = nil;
            break;
        case 6:
            self.pageTitle.text = nil;
            self.pageDescription.text = NSLocalizedString(@"You can earn happy minutes as a bonus, as well as purchase them from your Ziferblat or this app.", @"Tutorial page body 7");
            self.pageDescriptionLarge.text = nil;
            break;
        case 7:
            self.pageTitle.text = nil;
            self.pageDescription.text = NSLocalizedString(@"And don't forget about the walrus!", @"Tutorial page body 8");
            self.pageDescriptionLarge.text = nil;
            break;
        default:
            break;
    }
    
    [self setupConstraints];
}

- (void)setupConstraints
{
    NSInteger screenHeight = [UIScreen mainScreen].bounds.size.height;
    
    if(screenHeight == 480)
    {
        self.pageTitle.font = [UIFont fontWithName:self.pageTitle.font.fontName size:15];
        self.pageDescriptionLarge.font = [UIFont fontWithName:self.pageDescriptionLarge.font.fontName size:12.5];
        self.pageDescription.font = [UIFont fontWithName:self.pageDescription.font.fontName size:12.5];
        
        self.pageTitleYConstraint.constant = self.pageTitleYConstraint.constant * 960 / 1136;
        self.pageTitleWidthConstraint.constant = self.pageTitleWidthConstraint.constant * 960 / 1136;
        self.pageTitleHeightConstraint.constant = self.pageTitleHeightConstraint.constant * 960 / 1136;
        
        self.pageDescriptionLargeXConstrait.constant = self.pageDescriptionLargeXConstrait.constant + 10;
        self.pageDescriptionLargeYConstraint.constant = self.pageDescriptionLargeYConstraint.constant * 960 / 1136 - 5;
        self.pageDescriptionLargeWidthConstrait.constant = self.pageDescriptionLargeWidthConstrait.constant * 960 / 1136;
        self.pageDescriptionLargeHeightConstraint.constant = self.pageDescriptionLargeHeightConstraint.constant * 960 / 1136;
        
        self.pageDescriptionYConstrait.constant = self.pageDescriptionYConstrait.constant * 960 / 1136;
        self.pageDescriptionWidthConstrait.constant = self.pageDescriptionWidthConstrait.constant * 960 / 1136;
        self.pageDescriptionHeightConstrait.constant = self.pageDescriptionHeightConstrait.constant * 960 / 1136;
        
        self.openZiferblatButtonYConstrait.constant = self.openZiferblatButtonYConstrait.constant * 960 / 1136 - 3;
        self.openZiferblatButtonWidthConstrait.constant = self.openZiferblatButtonWidthConstrait.constant * 960 / 1136;
    }
    else if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        self.pageTitle.font = [UIFont fontWithName:self.pageTitle.font.fontName size:30];
        self.pageDescriptionLarge.font = [UIFont fontWithName:self.pageDescriptionLarge.font.fontName size:25];
        self.pageDescription.font = [UIFont fontWithName:self.pageDescription.font.fontName size:25];
        self.openZiferblatButton.titleLabel.font = [UIFont fontWithName:self.openZiferblatButton.titleLabel.font.fontName size:30];
        
        self.pageTitleYConstraint.constant = self.pageTitleYConstraint.constant * 1024 / 568;
        self.pageTitleWidthConstraint.constant = self.pageTitleWidthConstraint.constant * 1024 / 568;
        self.pageTitleHeightConstraint.constant = self.pageTitleHeightConstraint.constant * 1024 / 568;
        
        self.pageDescriptionLargeXConstrait.constant = self.pageDescriptionLargeXConstrait.constant + 180;
        self.pageDescriptionLargeYConstraint.constant = self.pageDescriptionLargeYConstraint.constant * 1024 / 568 - 5;
        self.pageDescriptionLargeWidthConstrait.constant = self.pageDescriptionLargeWidthConstrait.constant * 1024 / 568;
        self.pageDescriptionLargeHeightConstraint.constant = self.pageDescriptionLargeHeightConstraint.constant * 1024 / 568;
        
        self.pageDescriptionYConstrait.constant = self.pageDescriptionYConstrait.constant * 1024 / 568;
        self.pageDescriptionWidthConstrait.constant = self.pageDescriptionWidthConstrait.constant * 1024 / 568;
        self.pageDescriptionHeightConstrait.constant = self.pageDescriptionHeightConstrait.constant * 1024 / 568;
        
        self.openZiferblatButtonYConstrait.constant = self.openZiferblatButtonYConstrait.constant * 1024 / 568 + 22;
        self.openZiferblatButtonWidthConstrait.constant = self.openZiferblatButtonWidthConstrait.constant * 1024 / 568;
        self.openZiferbatButtonHeightConstraint.constant = self.openZiferbatButtonHeightConstraint.constant * 1024 / 568;
    }
}

- (IBAction)openTapped:(UIButton *)sender {
    [self.delegate turorialFinished];
}

@end
