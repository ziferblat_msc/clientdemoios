//
//  ZFPageContentViewController.h
//  Ziferblat
//
//  Created by Boris Yurkevich on 22/10/2015.
//  Copyright © 2015 Locassa. All rights reserved.
//

#import <UIKit/UIKit.h>

static int kIndexLastTutorialPage = 7;

@protocol ZFPageContentViewControllerDelegate <NSObject>

@required
- (void)turorialFinished;

@end

@interface ZFPageContentViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *pageTitle;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *pageDescription;
@property (weak, nonatomic) IBOutlet UILabel *pageDescriptionLarge;


@property NSUInteger pageIndex;
@property NSString *imageFile;
@property (strong, nonatomic) IBOutlet UIButton *openZiferblatButton;

//Constraits
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *pageTitleYConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *pageTitleWidthConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *pageTitleHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *pageDescriptionLargeXConstrait;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *pageDescriptionLargeYConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *pageDescriptionLargeWidthConstrait;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *pageDescriptionLargeHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *pageDescriptionYConstrait;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *pageDescriptionWidthConstrait;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *pageDescriptionHeightConstrait;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *openZiferblatButtonYConstrait;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *openZiferblatButtonWidthConstrait;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *openZiferbatButtonHeightConstraint;



@property (weak, nonatomic) id<ZFPageContentViewControllerDelegate> delegate;

@end
