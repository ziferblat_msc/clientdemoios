//
//  ZFTutorialViewController.m
//  Ziferblat
//
//  Created by Boris Yurkevich on 22/10/2015.
//  Copyright © 2015 Locassa. All rights reserved.
//

#import "ZFTutorialViewController.h"
#import "ZFPageContentViewController.h"
#import "ZFSplashScreenViewController.h"

@implementation ZFTutorialViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.pageImages = @[@"tutorial1", @"tutorial2", @"tutorial3", @"tutorial4", @"tutorial5", @"tutorial6", @"tutorial7", @"tutorial8"];
    
    // Create page view controller
    self.pageViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PageViewController"];
    self.pageViewController.dataSource = self;
    self.pageViewController.delegate = self;
    
    // Start tutorial
    ZFPageContentViewController *startingViewController = [self viewControllerAtIndex:0];
    NSArray *viewControllers = @[startingViewController];
    [self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionReverse animated:NO completion:nil];
    
    // Change the size of page view controller to compensate navigation bar height
    self.pageViewController.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height + 37);
    
    [self addChildViewController:_pageViewController];
    [self.view addSubview:_pageViewController.view];
    [self.pageViewController didMoveToParentViewController:self];
        
    [self.stackOfBooks bringToFront];
    [self.plant bringToFront];
}

- (void)viewWillAppear:(BOOL)animated
{
    NSInteger screenHeight = [UIScreen mainScreen].bounds.size.height;
    
    //Background
    if(screenHeight == 568 || screenHeight == 667)
    {
        self.backgroundImageView.image = [UIImage imageNamed:@"ZFTutorialBackground-568h&667h"];
    }
    
    [self setupConstraints];
}

- (void) setupConstraints
{
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        self.lightBulbLeadingConstraint.constant = 128;
        self.lightbulbYConstraint.constant = 53;
    }
}

- (ZFPageContentViewController *)viewControllerAtIndex:(NSUInteger)index {
    
    if (([self.pageImages count] == 0) || (index >= [self.pageImages count])) {
        return nil;
    }
    
    // Create a new view controller and pass suitable data.
    ZFPageContentViewController *pageContentViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PageContentViewController"];
    pageContentViewController.imageFile = self.pageImages[index];
//    pageContentViewController.titleText = self.pageTitles[index];
    pageContentViewController.pageIndex = index;
    pageContentViewController.delegate = self;
    
    return pageContentViewController;
}

#pragma mark UIPageViewControllerDataSource

- (nullable UIViewController *)pageViewController:(UIPageViewController *)pageViewController
               viewControllerBeforeViewController:(UIViewController *)viewController {
    
    NSUInteger index = ((ZFPageContentViewController*) viewController).pageIndex;
    
    if ((index == 0) || (index == NSNotFound)) {
        return nil;
    }
    
    index--;
    
    ZFPageContentViewController *vc = [self viewControllerAtIndex:index];
    return vc;
}

- (nullable UIViewController *)pageViewController:(UIPageViewController *)pageViewController
                viewControllerAfterViewController:(UIViewController *)viewController {
    
    NSUInteger index = ((ZFPageContentViewController*) viewController).pageIndex;
    
    if (index == NSNotFound) {
        return nil;
    }
    
    index++;

    if (index == [self.pageImages count]) {
        return nil;
    }
    
    ZFPageContentViewController *vc = [self viewControllerAtIndex:index];
    return vc;
}

- (NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController
{
    return [self.pageImages count];
}

- (NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController
{
    return 0;
}

#pragma mark - UIPageViewControllerDelegate

- (void)pageViewController:(UIPageViewController *)pageViewController
        didFinishAnimating:(BOOL)finished
   previousViewControllers:(NSArray<UIViewController *> *)previousViewControllers
       transitionCompleted:(BOOL)completed {
   
    if (finished && completed) {
        
        ZFPageContentViewController *viewController = [self.pageViewController.viewControllers lastObject];
        NSUInteger pageIndex = viewController.pageIndex;
        // From 0 to 7
        switch (pageIndex) {
            case 0:
                [self moveLightbulbPosition:12 forIpad:128];
                break;
            case 1:
                [self moveLightbulbPosition:50 forIpad:195.5];
                break;
            case 2:
                [self moveLightbulbPosition:87 forIpad:263];
                break;
            case 3:
                [self moveLightbulbPosition:124 forIpad:331];
                break;
            case 4:
                [self moveLightbulbPosition:162 forIpad:398];
                break;
            case 5:
                [self moveLightbulbPosition:199 forIpad:466];
                break;
            case 6:
                [self moveLightbulbPosition:236 forIpad:533.5];
                break;
            case 7:
                [self moveLightbulbPosition:274 forIpad:601];
                break;
            default:
                break;
        }
    }
}
- (void)moveLightbulbPosition:(CGFloat)position forIpad:(CGFloat)positionIpad
{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    
    self.lightbulb.alpha = 0;
    [self.view layoutIfNeeded];
    self.lightbulb.alpha = 1;
    
    [UIView commitAnimations];
    
    if(UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPad)
    {
        self.lightBulbLeadingConstraint.constant = position;
    }
    else
    {
        self.lightBulbLeadingConstraint.constant = positionIpad;
    }
    
}

#pragma mark - ZFPageContentViewControllerDelegate

- (void)turorialFinished {
    [self dismissViewControllerAnimated:YES completion:^(void) {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kTutorialPresented];
        [self.delegate startLogin];
    }];
}


@end
