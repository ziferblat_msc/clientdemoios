//
//  ZFTutorialViewController.h
//  Ziferblat
//
//  Created by Boris Yurkevich on 22/10/2015.
//  Copyright © 2015 Locassa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZFPageContentViewController.h"

static NSString *kTutorialPresented = @"kTutorialPresented";

@protocol ZFTutorialViewControllerDelegate <NSObject>

@required

- (void)startLogin;

@end

@interface ZFTutorialViewController : UIViewController <UIPageViewControllerDataSource, UIPageViewControllerDelegate, ZFPageContentViewControllerDelegate>

@property (weak, nonatomic) id<ZFTutorialViewControllerDelegate> delegate;

@property (strong, nonatomic) UIPageViewController *pageViewController;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImageView;
@property (strong, nonatomic) NSArray<NSString *> *pageImages;
@property (weak, nonatomic) IBOutlet UIImageView *stackOfBooks;
@property (weak, nonatomic) IBOutlet UIImageView *plant;
//Light
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lightBulbLeadingConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lightbulbYConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lightbulbWidthConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lightbulbHeightConstraint;
@property (weak, nonatomic) IBOutlet UIImageView *lightbulb;

@end
