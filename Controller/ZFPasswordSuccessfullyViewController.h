//
//  ZFPasswordSuccessfullyViewController.h
//  Ziferblat
//
//  Created by Jose Fernandez on 30/07/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZFPasswordSuccessfullyViewController : UIViewController

//Constraints
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topNavigationControllerHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *resetPasswordLabelYConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *resetPasswordLabelWidthConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *resetPasswordLabelHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *insructionsLabelYConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *insructionsLabelWidthConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *subinstructionsLabelYConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *subinstructionsLabelWidthConstraint;


@end
