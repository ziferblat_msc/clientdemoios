//
//  ZFOtherProfileViewController.m
//  Ziferblat
//
//  Created by Simon Lee on 10/06/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "ZFOtherProfileViewController.h"
#import "ZFAddFriendRequest.h"
#import "ZFUnfriendRequest.h"
#import "ZFTimeView.h"
#import "ZFConversationDetailViewController.h"
#import "ZFGetExperiencePointsPositionByVenue.h"
#import "ZFLeaderBoardCount.h"
#import "ZFUser.h"
#import "ZFVenue.h"
#import "ZFGetExperiencePointsPositionByVenue.h"
#import "ZFProfileImage.h"
#import "ZFGuestService.h"

@interface ZFProfileViewController ()

- (UIButton *)buttonWithFrame:(CGRect)aFrame selector:(SEL)aSelector;
- (void)setupWithUser:(ZFUser *)aUser;
@property BOOL isFilterEnabled;
@property (nonatomic, strong) NSNumber *venueIdPickedByParent;

@end

@interface ZFOtherProfileViewController ()

@property (nonatomic, strong) NSString *username;
@property (nonatomic, strong) UIImageView *greenDotImageView;

@end

@implementation ZFOtherProfileViewController

#define kAddFriendButtonFrame                       (notIPad ? CGRectMake(60, 228, 38, 46):CGRectMake(144, 547, 91, 110))
#define kConversationButtonFrame                    (notIPad ? CGRectMake(219, 228, 38, 46):CGRectMake(526, 547, 91, 110))
#define kFriendImageViewOrigin                      (notIPad ? CGPointMake(59.5, 228.7):CGPointMake(143, 538))

#define kFriendsLabelOtherUserTopOrigin             319
#define kFriendsCollectionViewOtherUserTopOrigin    354

- (void)loadView
{
    [super loadView];
    
    [timeHistoryButton hide];
    [settingsButton hide];
    [timeView hide];
    
    // These 2 lines bellow are a fix because of the background asset
    rankLabel.top -= 1;
    levelLabel.top -= 1;
    
    conversationButton = [self buttonWithFrame:kConversationButtonFrame selector:@selector(conversationButtonTapped)];
    
    UIImage *friendImage = [UIImage imageNamed:RESOURCE_FRIEND_IMAGE_VIEW];
    addFriendButton  = [self buttonWithFrame:CGRectZero selector:@selector(addFriendButtonTapped)];
    addFriendButton.size = friendImage.size;
    addFriendButton.origin = kFriendImageViewOrigin;
    [addFriendButton setBackgroundImage:friendImage forState:UIControlStateNormal];
    [addFriendButton setBackgroundImage:[UIImage imageNamed:RESOURCE_UNFRIEND_IMAGE_VIEW] forState:UIControlStateSelected];
    [addFriendButton setAdjustsImageWhenHighlighted:FALSE];
    
    ZFUser *currentUser = CurrentUser;
    BOOL isFriend = [currentUser.friendsList containsObject:user];
    addFriendButton.selected = isFriend;

    scoreButton.left = addFriendButton.right;
    scoreButton.width = conversationButton.left - addFriendButton.right;
    userDetailsView.userInteractionEnabled = NO;
    
    // Add green dot on the topImageView
    self.greenDotImageView = [[UIImageView alloc] initWithFrame:(notIPad ? CGRectMake(81, 91, 17, 17):CGRectMake(194, 218, 41, 41))];
    self.greenDotImageView.image = [UIImage imageNamed:RESOURCE_AUTO_PAYMENT_ON];
    [topImageView addSubview:self.greenDotImageView];
    
    BOOL showGreenDot = (CurrentUser.currentVenueId.integerValue > 0) &&
    (user.currentVenueId.integerValue == CurrentUser.currentVenueId.integerValue);
    
    if (showGreenDot) {
        self.greenDotImageView.hidden = NO;
    } else {
        self.greenDotImageView.hidden = YES;
    }
    
}

- (void)fetchExperience { // Override
    [SVProgressHUD showWithStatus:NSLocalizedString(@"Fetching data...",nil) maskType:SVProgressHUDMaskTypeGradient];
    [ZFGetExperiencePointsPositionByVenue requestWithCompletion:^(LORESTRequest *request) {
        
        Hide_HudAndPerformBlock((^{
            if (request.resultStatus != LORESTRequestResultStatusSucceeded) {
                Show_ConnectionError
                return;
            }
            ZFLeaderBoardCount *result = request.result;
            NSNumber *value = result.valueCount;
            scoreLabel.text = [NSString stringWithFormat:@"%d", value.intValue];
            rankLabel.text = [NSString stringWithFormat:NSLocalizedString(@"%@ is #%d",nil), self.username, result.position.intValue];
        }));
        
    } disableFilter:!self.isFilterEnabled venueId:self.venueIdPickedByParent forMemberId:user.uuid];
}

#pragma mark -
#pragma mark Elements for layout

- (NSString *)topImage {
    return RESOURCE_PROFILE_VIEW_TOHER_USER_TOP;
}

#pragma mark -
#pragma mark Button Methods

- (void)addFriendButtonTapped {
    if ([[ZFGuestService sharedInstance] makeSureUserIsNotAGuestFromViewController:self]) {
        return;
    }
    
    if (user.friendStatus == ZFFriendStatusFriendRequested) {
        
        [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"You have already sent this user a friend request",nil)];
        
    } else if(!addFriendButton.selected) {
        Show_Hud(NSLocalizedString(@"Adding friend...",nil));
        [ZFAddFriendRequest requestWithId:user.uuid completion:^(LORESTRequest *aRequest) {
            if(aRequest.resultStatus == LORESTRequestResultStatusSucceeded) {
                NSString *status = aRequest.result;
                
                if([status isEqualToString:@"FRIEND_REQUESTED"]) {
                    
                    [SVProgressHUD showSuccessWithStatus:NSLocalizedString(@"Friend request sent!",nil)];
                    user.friendStatus = ZFFriendStatusFriendRequested;
                    
                    } else if([status isEqualToString:@"FRIENDS"]) {
                    [SVProgressHUD showSuccessWithStatus:NSLocalizedString(@"Friend added",nil)];
                    [CurrentUser addFriend:user];
                    [addFriendButton setSelected:TRUE];
                     user.friendStatus = ZFFriendStatusFriends;
                    }
                
            } else {
                Show_ConnectionError;
            }
        }];
        
    } else {
        NSString *title = [NSString stringWithFormat:NSLocalizedString(@"Are you sure you want to unfriend %@?", @"Unfriend alert"), user.firstName];
        UIAlertController *alertC = [UIAlertController alertControllerWithTitle:title
                                                                        message:nil
                                                                 preferredStyle:UIAlertControllerStyleAlert];

        UIAlertAction *okAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Yes", @"Unfriend alert")
                                                           style:UIAlertActionStyleDestructive
                                                         handler:^(UIAlertAction *action){
                                                             Show_Hud(NSLocalizedString(@"Removing friend...",nil));
                                                             [ZFUnfriendRequest requestWithId:user.uuid completion:^(LORESTRequest *aRequest) {
                                                                 if(aRequest.resultStatus == LORESTRequestResultStatusSucceeded) {
                                                                     [SVProgressHUD showSuccessWithStatus:NSLocalizedString(@"Friend removed",nil)];
                                                                     [CurrentUser removeFriend:user];
                                                                     [addFriendButton setSelected:FALSE];
                                                                     user.friendStatus = ZFFriendStatusNotFriends;
                                                                     
                                                                 } else {
                                                                     Show_ConnectionError;
                                                                 }
                                                             }];
                                                         }];
        UIAlertAction *cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"No", @"Unfriend alert")
                                                         style:UIAlertActionStyleCancel
                                                       handler:nil];
        [alertC addAction:okAction];
        [alertC addAction:cancel];
        
        [self presentViewController:alertC animated:YES completion:nil];
    }
}

- (void)conversationButtonTapped {
    if ([[ZFGuestService sharedInstance] makeSureUserIsNotAGuestFromViewController:self]) {
        return;
    }
    
    ZFConversationDetailViewController *conversationDetailViewController = [[ZFConversationDetailViewController alloc] initWithUser:user];
    [self presentViewController:conversationDetailViewController animated:YES completion:nil];
}

- (void)setupWithUser:(ZFUser *)aUser
{
    [super setupWithUser:aUser];
    self.username = aUser.firstName;
}

@end
