//
//  ZFPoemViewController.h
//  Ziferblat
//
//  Created by Terry Michel on 09/04/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "ZFPoemView.h"
#import "LOOverlayChildViewController.h"

@interface ZFPoemOverlayViewController : LOOverlayChildViewController<LOOverlayChildViewControllerDelegate> {
    ZFPoemView *castedViewToDisplay;
}

@end
