//
//  ZFOtherProfileViewController.h
//  Ziferblat
//
//  Created by Simon Lee on 10/06/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "ZFProfileViewController.h"

@interface ZFOtherProfileViewController : ZFProfileViewController {
    UIButton *addFriendButton;
    UIButton *conversationButton;
    
}

@end
