//
//  ZFCollaborationViewController.h
//  Ziferblat
//
//  Created by Terry Michel on 08/04/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "ZFCollaborationView.h"

@interface ZFCollaborationViewController : UIViewController {
    ZFCollaborationView *contentView;
}

@end
