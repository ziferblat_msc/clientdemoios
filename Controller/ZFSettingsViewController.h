//
//  ZFSettingsViewController.h
//  Ziferblat
//
//  Created by Terry Michel on 08/04/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "ZFBaseView.h"

@interface ZFSettingsViewController : UIViewController {
    ZFUser *userUpdated;
    BOOL settingsHaveChanged;
}

- (instancetype)initWithUser:(ZFUser *)user;

@end
