//
//  ZFLoginViewController.m
//  Ziferblat
//
//  Created by Terry Michel on 08/04/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "ZFSignUpViewController.h"
#import "ZFHomeViewController.h"
#import "ZFCompletePendingSignupRequest.h"
#import "ZFGetAccountBalanceRequest.h"
#import "ZFButton.h"

typedef NS_ENUM(NSInteger, ZFSignUpView_FieldsTag) {
    ZFSignUpView_FieldsTag_Email,
    ZFSignUpView_FieldsTag_Password,
    ZFSignUpView_FieldsTag_ConfirmPassword,
    ZFSignUpView_FieldsTag_RegistrationCode
};

#define kBackgroundKeyboardVisibleShift 184
#define kHeaderLabelFrame               (UI_USER_INTERFACE_IDIOM() !=UIUserInterfaceIdiomPad ? CGRectMake(56, 44, 213, 29) : CGRectMake(227, 65, 300, 60))
#define kSecretMessageLabelFrame        (UI_USER_INTERFACE_IDIOM() !=UIUserInterfaceIdiomPad ? CGRectMake(77, 126, 175, 65) : CGRectMake(234, 180, 280, 130))
#define kEmailTextFieldFrame            (UI_USER_INTERFACE_IDIOM() !=UIUserInterfaceIdiomPad ? CGRectMake(59, 198, 208, 38) : CGRectMake(218, 317, 343, 68))
#define kPasswordTextFieldFrame         (UI_USER_INTERFACE_IDIOM() !=UIUserInterfaceIdiomPad ? CGRectMake(59, 252, 208, 38) : CGRectMake(218, 405, 343, 68))
#define kConfirmPasswordTextFieldFrame  (UI_USER_INTERFACE_IDIOM() !=UIUserInterfaceIdiomPad ? CGRectMake(59, 305, 208, 38) : CGRectMake(218, 491, 343, 68))
#define kSecretWordTextFieldFrame       (UI_USER_INTERFACE_IDIOM() !=UIUserInterfaceIdiomPad ? CGRectMake(59, 359, 208, 38) : CGRectMake(218, 578, 343, 68))
#define kSignInButtonTopOrigin          (UI_USER_INTERFACE_IDIOM() !=UIUserInterfaceIdiomPad ? 424 : 700)
#define kSkipItLabelFrame               (UI_USER_INTERFACE_IDIOM() !=UIUserInterfaceIdiomPad ? CGRectMake(122, 494, 75, 20) : CGRectMake(309, 798, 150, 40))
#define kSkipItButtonFrame              (UI_USER_INTERFACE_IDIOM() !=UIUserInterfaceIdiomPad ? CGRectMake(82, 494, 149, 29) : CGRectMake(234, 808, 300, 58))
#define kJustWannaSeeLabelFrame         (UI_USER_INTERFACE_IDIOM() !=UIUserInterfaceIdiomPad ? CGRectMake(60, 516, 200, 15) :  CGRectMake(184, 838, 400, 45))
#define kBackButtonFrame                (UI_USER_INTERFACE_IDIOM() !=UIUserInterfaceIdiomPad ? CGRectMake(63, 552, 196, 159) : CGRectMake(204, 915, 360, 50))
#define kBackLabelFrame                 (UI_USER_INTERFACE_IDIOM() !=UIUserInterfaceIdiomPad ? CGRectMake(83, 565, 154, 28.5)  : CGRectMake(240 ,910, 280, 56))

static NSString *const crossImage = @"ZFLoginBadgeX";
static NSString *const checkImage = @"ZFLoginBadgeV";

@interface ZFSignUpViewController()

@property (strong, nonatomic) NSString *canonicalMessage;
@property (strong, nonatomic) UIImageView *cross;
@property (strong, nonatomic) UIImageView *done;

@end

@implementation ZFSignUpViewController

@synthesize done;
@synthesize cross;

#pragma mark - 
#pragma mark Lifecycle

- (void)loadView {
    ZFBaseView *aView = [[ZFBaseView alloc] init];
    self.view = aView;
    
    backgroundImageView = [UIImageView imageViewWithImageNamed:@"ZFSignUpView"];
    [kScrollView addSubview:backgroundImageView];
    kScrollView.contentSize = backgroundImageView.size;
    
    headerLabel = [[UILabel alloc]initWithFrame:kHeaderLabelFrame];
    headerLabel.text = NSLocalizedString(@"SIGN UP TO ZIFERBLAT",nil);
    headerLabel.textColor = Colour_White;
    headerLabel.textAlignment = NSTextAlignmentCenter;
    headerLabel.font = (UI_USER_INTERFACE_IDIOM() !=UIUserInterfaceIdiomPad ? FontRockWell(17) : FontRockWell(27));
    headerLabel.adjustsFontSizeToFitWidth = YES;
    headerLabel.minimumScaleFactor = 0.5;
    [kScrollView addSubview:headerLabel];
    
    self.canonicalMessage = NSLocalizedString(@"In Ziferblat we will tell you secret word to become our member",nil);
    
    secretMessageLabel = [[UILabel alloc]initWithFrame:kSecretMessageLabelFrame];
    secretMessageLabel.numberOfLines = 3;
    secretMessageLabel.text = self.canonicalMessage;
    secretMessageLabel.textColor = Colour_White;
    secretMessageLabel.textAlignment = NSTextAlignmentCenter;
    secretMessageLabel.font = (UI_USER_INTERFACE_IDIOM() !=UIUserInterfaceIdiomPad ? FontRockWell(16) : FontRockWell(25));
    secretMessageLabel.adjustsFontSizeToFitWidth = YES;
    secretMessageLabel.minimumScaleFactor = 0.5;
    [kScrollView addSubview:secretMessageLabel];
    
    emailTextField = [UITextField textfieldWithFrame:kEmailTextFieldFrame
                                     placeholderText:NSLocalizedString(@"Email", nil)
                                           textColor:Colour_Black
                                            textFont:(UI_USER_INTERFACE_IDIOM() !=UIUserInterfaceIdiomPad ? FontRockWell(13) : FontRockWell(24))
                                        keyboardMode:UIKeyboardTypeEmailAddress
                                           clearMode:UITextFieldViewModeAlways
                                       returnKeyMode:UIReturnKeyNext
                                  capitalizationMode:UITextAutocapitalizationTypeNone
                                     autoCorrectMode:UITextAutocorrectionTypeNo
                                                 tag:ZFSignUpView_FieldsTag_Email
                                            isSecure:NO
                                           addToView:kScrollView];
    
    passwordTextField = [UITextField textfieldWithFrame:kPasswordTextFieldFrame
                                        placeholderText:NSLocalizedString(@"Password", nil)
                                              textColor:Colour_Black
                                               textFont:(UI_USER_INTERFACE_IDIOM() !=UIUserInterfaceIdiomPad ? FontRockWell(13) : FontRockWell(24))
                                           keyboardMode:UIKeyboardTypeDefault
                                              clearMode:UITextFieldViewModeAlways
                                          returnKeyMode:UIReturnKeyNext
                                     capitalizationMode:UITextAutocapitalizationTypeNone
                                        autoCorrectMode:UITextAutocorrectionTypeNo
                                                    tag:ZFSignUpView_FieldsTag_Password
                                               isSecure:YES
                                              addToView:kScrollView];
    
    confirmPasswordTextField = [UITextField textfieldWithFrame:kConfirmPasswordTextFieldFrame
                                               placeholderText:NSLocalizedString(@"Confirm password", nil)
                                                     textColor:Colour_Black
                                                      textFont:(UI_USER_INTERFACE_IDIOM() !=UIUserInterfaceIdiomPad ? FontRockWell(13) : FontRockWell(24))
                                                  keyboardMode:UIKeyboardTypeDefault
                                                     clearMode:UITextFieldViewModeAlways
                                                 returnKeyMode:UIReturnKeyNext
                                            capitalizationMode:UITextAutocapitalizationTypeNone
                                               autoCorrectMode:UITextAutocorrectionTypeNo
                                                           tag:ZFSignUpView_FieldsTag_ConfirmPassword
                                                      isSecure:YES
                                                     addToView:kScrollView];

    secretWordTextField = [UITextField textfieldWithFrame:kSecretWordTextFieldFrame
                                          placeholderText:NSLocalizedString(@"Code",nil)
                                                textColor:Colour_Black
                                                 textFont:(UI_USER_INTERFACE_IDIOM() !=UIUserInterfaceIdiomPad ? FontRockWell(13) : FontRockWell(24))
                                             keyboardMode:UIKeyboardTypeDefault
                                                clearMode:UITextFieldViewModeAlways
                                            returnKeyMode:UIReturnKeyDone
                                       capitalizationMode:UITextAutocapitalizationTypeNone
                                          autoCorrectMode:UITextAutocorrectionTypeNo
                                                      tag:ZFSignUpView_FieldsTag_RegistrationCode
                                                 isSecure:YES
                                                addToView:kScrollView];
    [secretWordTextField addToolbar];
    
    signInButton = [ZFButton buttonWithTitle:NSLocalizedString(@"ENTER", nil) showBorder:YES];
    signInButton.enabled = NO;
    signInButton.top = kSignInButtonTopOrigin;
    [kScrollView addSubview:signInButton];
    [signInButton centerHorizontallyInSuperView];
    
    skipItLabel = [[UILabel alloc]initWithFrame:kSkipItLabelFrame];
    skipItLabel.text = NSLocalizedString(@"Skip it",nil);
    skipItLabel.textAlignment = NSTextAlignmentCenter;
    skipItLabel.textColor = Colour_Black;
    skipItLabel.font = (UI_USER_INTERFACE_IDIOM() !=UIUserInterfaceIdiomPad ? FontRockWell(15) : FontRockWell(30));
    skipItLabel.adjustsFontSizeToFitWidth = YES;
    skipItLabel.minimumScaleFactor = 0.5;
    [kScrollView addSubview:skipItLabel];
    
    justWannaSeeLabel = [[UILabel alloc]initWithFrame:kJustWannaSeeLabelFrame];
    justWannaSeeLabel.text = NSLocalizedString(@"(just wanna see what's there)",nil);
    justWannaSeeLabel.numberOfLines = 0;
    justWannaSeeLabel.textAlignment = NSTextAlignmentCenter;
    justWannaSeeLabel.textColor = Colour_Black;
    justWannaSeeLabel.font = (UI_USER_INTERFACE_IDIOM() !=UIUserInterfaceIdiomPad ? FontRockWell(11) : FontRockWell(22));
    justWannaSeeLabel.adjustsFontSizeToFitWidth = YES;
    justWannaSeeLabel.minimumScaleFactor = 0.5;
    [kScrollView addSubview:justWannaSeeLabel];
    
    skipItButton = [UIButton buttonWithFrame:kSkipItButtonFrame
                                 isExclusive:YES
                       showsTouchOnHighlight:NO
                                        font:nil
                                 normalTitle:nil
                           normalTitleColour:nil
                               selectedTitle:nil
                         selectedTitleColour:nil
                             normalImageName:nil
                           selectedImageName:nil
                   normalBackgroundImageName:nil
                 selectedBackgroundImageName:nil
                                   addToView:kScrollView];
    
    backLabel = [[UILabel alloc]initWithFrame:kBackLabelFrame];
    backLabel.text = NSLocalizedString(@"Entrance for citizens",nil);
    backLabel.textAlignment = NSTextAlignmentCenter;
    backLabel.font = (UI_USER_INTERFACE_IDIOM() !=UIUserInterfaceIdiomPad ? FontRockWell(15) : FontRockWell(28));
    backLabel.adjustsFontSizeToFitWidth = YES;
    backLabel.minimumScaleFactor = 0.5;
    [kScrollView addSubview:backLabel];
    
    backButton = [UIButton buttonWithFrame:kBackButtonFrame
                                isExclusive:YES
                      showsTouchOnHighlight:NO
                                       font:nil
                                normalTitle:nil
                          normalTitleColour:nil
                              selectedTitle:nil
                        selectedTitleColour:nil
                            normalImageName:nil
                          selectedImageName:nil
                  normalBackgroundImageName:nil
                selectedBackgroundImageName:nil
                                  addToView:kScrollView];
    
    UITapGestureRecognizer *tapGesture = [UITapGestureRecognizer gestureWithTarget:self
                                     selector:@selector(tapGestureDetected:)
                                    addToView:self.view];
    tapGesture.delegate = self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    TARGET_TOUCH_UP(signInButton, @selector(signInButtonPressed));
    TARGET_TOUCH_UP(skipItButton, @selector(skipItButtonPressed));
    TARGET_TOUCH_UP(backButton, @selector(backButtonPressed));
    emailTextField.delegate = self;
    passwordTextField.delegate = self;
    confirmPasswordTextField.delegate = self;
    secretWordTextField.delegate = self;
    [emailTextField addTarget:self action:@selector(checkStatus) forControlEvents:UIControlEventEditingChanged];
    [passwordTextField addTarget:self action:@selector(checkStatus) forControlEvents:UIControlEventEditingChanged];
    [confirmPasswordTextField addTarget:self action:@selector(checkStatus) forControlEvents:UIControlEventEditingChanged];
    [secretWordTextField addTarget:self action:@selector(checkStatus) forControlEvents:UIControlEventEditingChanged];
    
    self.cross = [UIImageView imageViewWithImageNamed:crossImage];
    self.cross.hidden = YES;
    self.done = [UIImageView imageViewWithImageNamed:checkImage];
    self.done.hidden = YES;
    [kScrollView addSubview:cross];
    [kScrollView addSubview:done];
    
    Notification_Observe(UIKeyboardWillChangeFrameNotification, updateLayoutForKeyboard:)
}

- (void)dealloc {
    Notification_RemoveObserver
}

#pragma mark -
#pragma mark Buttons actions

- (void)signInButtonPressed {
    NSString *password = passwordTextField.text;
    if(password.length < TEXT_FIELD_PASSWORD_MIN_LENGTH || ![password containsCharacter] || ![password containsNumber]) {
        [SVProgressHUD dismiss];
        [UIAlertView showAlertWithTitle:NSLocalizedString(@"Passwords must be a minimum of 6 characters and contain at least one number and character.",nil) andMessage:nil];
    } else {
    [self.view endEditing:YES];
    NSString *password =  passwordTextField.text;
    [SVProgressHUD showWithStatus:NSLocalizedString(@"Signing in...",nil) maskType:SVProgressHUDMaskTypeGradient];
    [ZFCompletePendingSignupRequest requestWithEmail:emailTextField.text
                                            password:password
                                          secretWord:secretWordTextField.text
                                          completion:^(LORESTRequest *aRequest) {
                                              ZFUser *user = aRequest.result;
                                              user.password = password;
                                              [UserService setLoggedInUser:user];
                                              if(aRequest.resultStatus == LORESTRequestResultStatusSucceeded) {
                                                  [ZFGetAccountBalanceRequest requestWithId:CurrentUser.uuid
                                                                                 completion:^(LORESTRequest *aRequest) {
                                                                                     if(aRequest.resultStatus == LORESTRequestResultStatusSucceeded) {
                                                                                         CurrentUser.accountBalance = aRequest.result;
                                                                                         Notification_Post(kNotification_AccountBalanceRefreshed);
                                                                                         [self pushHomeViewController];
                                                                                     } else {
                                                                                         [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"Authentication failed",nil)];
                                                                                     }
                                                                                 }];
                                              } else {
                                                  [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"Sign in failed",nil)];
                                              }
                                          }];
    }
}

- (void)skipItButtonPressed {
    #warning UPDATE WHEN WEBSERVICE READY
    [self pushHomeViewController];
}

- (void)backButtonPressed {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - 
#pragma mark Gestures

- (void)tapGestureDetected:(UITapGestureRecognizer *)aTapGesture {
    [self.view endEditing:YES];
}

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer {
    CGPoint location = [gestureRecognizer locationInView:kScrollView];
    return !(CGRectContainsPoint(signInButton.frame, location));
}

#pragma mark - 
#pragma mark UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    switch (textField.tag) {
        case ZFSignUpView_FieldsTag_Email:
            [passwordTextField becomeFirstResponder];
            break;
            
        case ZFSignUpView_FieldsTag_Password:
            [confirmPasswordTextField becomeFirstResponder];
            break;
            
        case ZFSignUpView_FieldsTag_ConfirmPassword:
            [secretWordTextField becomeFirstResponder];
            break;
            
        case ZFSignUpView_FieldsTag_RegistrationCode:
            [self.view endEditing:YES];
            break;
            
        default:
            break;
    }
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSString *output = [textField.text stringByReplacingCharactersInRange:range withString:string];
    NSCharacterSet *blockedCharacters = [[NSCharacterSet alphanumericCharacterSet] invertedSet];
    BOOL isValidPassword = [output rangeOfCharacterFromSet:blockedCharacters].location == NSNotFound;
                                         
    switch (textField.tag) {
        case ZFSignUpView_FieldsTag_Password:
        case ZFSignUpView_FieldsTag_ConfirmPassword:
            return isValidPassword;
            break;
            
        default:
            break;
    }
    
    [self checkStatus];
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    [self checkStatus];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    if(textField.tag == ZFSignUpView_FieldsTag_RegistrationCode) {
        
    }
}

#pragma mark -
#pragma mark Notifications

- (void)updateLayoutForKeyboard:(NSNotification *)notification {
    BOOL showingKeyboard = [notification keyboardHeight] > 0;
    [kScrollView setScrollEnabled:!showingKeyboard];
    [kScrollView setContentOffset:CGPointMake(0, showingKeyboard ? kBackgroundKeyboardVisibleShift : 0)];
}

#pragma mark - 
#pragma mark Utils

- (void)pushHomeViewController {
    ZFHomeViewController *homeViewController = [[ZFHomeViewController alloc] init];
    [self.navigationController pushViewController:homeViewController animated:YES];
}

- (void)checkStatus {
    BOOL signInEnabled = NO;
    BOOL validEmail = NO;
    BOOL validPassword = NO;
    BOOL validConfirmPassword = NO;
    BOOL validSecret = NO;
    UIColor *errorColor = [UIColor lightGrayColor];
    UIColor *normalColor = [UIColor whiteColor];
    
    if (secretWordTextField.text.length > 0) {
        validSecret = YES;
        if ([secretWordTextField isFirstResponder]) {
            [self annotateTextField:secretWordTextField asValidated:validSecret];
        }
    } else {
        validSecret = NO;
        secretMessageLabel.text = NSLocalizedString(@"Secret word can't be empty.", @"Registration title");
        secretMessageLabel.textColor = errorColor;
        [self annotateTextField:secretWordTextField asValidated:validSecret];
    }
    
    if (confirmPasswordTextField.text.isValidString &&
        [passwordTextField.text isEqualToString:confirmPasswordTextField.text]) {
        validConfirmPassword = YES;
        if ([confirmPasswordTextField isFirstResponder]) {
            [self annotateTextField:confirmPasswordTextField asValidated:validConfirmPassword];
        }
    }
    else
    {
        validConfirmPassword = NO;
        secretMessageLabel.text = NSLocalizedString(@"Passwords do not match.", @"Registration title");
        secretMessageLabel.textColor = errorColor;
        [self annotateTextField:confirmPasswordTextField asValidated:validConfirmPassword];
    }
    
    if (passwordTextField.text.isValidString
        && passwordTextField.text.length >= TEXT_FIELD_PASSWORD_MIN_LENGTH
        && [passwordTextField.text containsNumber]
        && [passwordTextField.text containsCharacter]) {
        validPassword = YES;
        if ([passwordTextField isFirstResponder]) {
            [self annotateTextField:passwordTextField asValidated:validPassword];
        }
    } else {
        validPassword = NO;
        if (!passwordTextField.text.isValidString
            || !(passwordTextField.text.length >= TEXT_FIELD_PASSWORD_MIN_LENGTH)) {
            
            secretMessageLabel.text = [NSString stringWithFormat:NSLocalizedString(@"Please enter a password with more than %d characters.", @"Registration title"), TEXT_FIELD_PASSWORD_MIN_LENGTH];
            secretMessageLabel.textColor = errorColor;
        } else if (![passwordTextField.text containsCharacter]) {
            secretMessageLabel.text = NSLocalizedString(@"Password doesn't have any characters.", @"Registration title");
            secretMessageLabel.textColor = errorColor;
        } else if (![passwordTextField.text containsNumber]) {
            secretMessageLabel.text = NSLocalizedString(@"Password doesn't have any numbers.", @"Registration title");
            secretMessageLabel.textColor = errorColor;
        }
        [self annotateTextField:passwordTextField asValidated:validPassword];
    }
    
    if (emailTextField.text.isValidEmailAddress) {
        validEmail = YES;
        if ([emailTextField isFirstResponder]) {
            [self annotateTextField:emailTextField asValidated:validEmail];
        }
    } else {
        validEmail = NO;
        secretMessageLabel.text = NSLocalizedString(@"Email is not correct.", @"Registration title");
        secretMessageLabel.textColor = errorColor;
        [self annotateTextField:emailTextField asValidated:validEmail];
    }
    
    // Button
    if (validEmail && validPassword && validConfirmPassword && validSecret) {
        secretMessageLabel.text = self.canonicalMessage;
        secretMessageLabel.textColor = normalColor;
        signInEnabled = YES;
    }
    signInButton.enabled = signInEnabled;
}

- (void)annotateTextField:(UITextField *)field asValidated:(BOOL)valid {
    
    static int leftPadding = 26;
    
    cross.center = CGPointMake(leftPadding, field.center.y);
    done.center = cross.center;

    if (valid) {
        cross.hidden = YES;
        done.hidden = NO;
    } else {
        cross.hidden = NO;
        done.hidden = YES;
    }
}

@end
