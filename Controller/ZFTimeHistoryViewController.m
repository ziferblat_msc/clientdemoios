//
//  ZFTimeHistoryViewController.m
//  Ziferblat
//
//  Created by Terry Michel on 08/04/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "ZFTimeHistoryViewController.h"
#import "ZFBalanceHistoryViewController.h"
#import "ZFExperiencePointsViewController.h"
#import "ZFLeaderBoardsViewController.h"
#import "ZFLeaderboardsIndividualViewController.h"
#import "ZFLeaderBoardCount.h"
#import "ZFHistoryTableViewCell.h"
#import "ZFGetListTimeHistory.h"
#import "ZFGetListTopTimeSpent.h"
#import "ZFHistoryItem.h"
#import "ZFGetTimeSpentLeaderBoardByVenue.h"
#import "ZFGetTimeSpentPositionByVenue.h"
#import "ZFLeaderBoard.h"
#import "ZFGetExperiencePointsPositionByVenue.h"
#import "ZFGetTimeDecelerator.h"
#import "ZFGetSingleMemberRequest.h"

static NSString *const cellIdentifier = @"ZFHistoryTableViewCell";
static NSString *const cellNibName = @"ZFHistoryTableViewCell";
#define tableRowHeight (notIPad ?48:115)

@interface ZFTimeHistoryViewController()

@property (strong, nonatomic) NSArray<ZFHistoryItem *>*historyList;
@property (strong, nonatomic) ZFLeaderBoardsViewController *leaderboardsController;
@property (strong, nonatomic) NSArray <NSNumber *>*topVenues;

@end

@implementation ZFTimeHistoryViewController

#pragma mark - Lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    contentView = [[ZFTimeHistoryView alloc] init];
    self.view = contentView;
    
    TARGET_TOUCH_UP(contentView.backButton, @selector(backButtonTapped))
    TARGET_TOUCH_UP(contentView.scoreButton, @selector(scoreButtonTapped))
    TARGET_TOUCH_UP(contentView.leaderboardsButton, @selector(leaderboardsButtonTapped))
    TARGET_TOUCH_UP(contentView.localLeaderboardsButtonOne, @selector(localLeaderboardsButtonFirstTapped))
    TARGET_TOUCH_UP(contentView.localLeaderboardsButtonTwo, @selector(localLeaderboardsButtonSecondTapped))
    TARGET_TOUCH_UP(contentView.localLeaderboardsButtonThree, @selector(localLeaderboardsButtonThirdTapped))
    TARGET_TOUCH_UP(contentView.balanceHistory, @selector(balanceHistoryButtonTapped))
    
    Notification_Observe(kNotification_TimeHistory_TimeSpentRefreshed, handleNotificationTimeSpentRefreshed)
    Notification_Observe(kNotification_TimeHistory_TopTimeSpentRefreshed, handleNotificationTopTimeSpentRefreshed)
    Notification_Observe(kNotification_TimeHistory_ExpereinceRefreshed, handleNotificationTimeExpereinceRefreshed)
    Notification_Observe(kNotification_TimeHistory_HistoryTable, handleNotificationTimeHistoryTable)

    [contentView updateMinutes:CurrentUser.accountBalance.baseMinutes];
    
    [self loadNetworkData];
    
    self.leaderboardsController = [[ZFLeaderBoardsViewController alloc] initWithTitle:NSLocalizedString(@"BACK TO TIME HISTORY", @"View opened from Time History")
                                                                              venueId:nil];
    
    [NetworkService pollForType:ZFPollingRequestType_TimeHistoryTimeSpentRefreshed
                       memberId:nil
                        venueId:nil
                        summary:YES];
}

#pragma mark - Buttons actions

- (void)backButtonTapped {
    if([self.parentViewController isKindOfClass:[UINavigationController class]]) {
        [self.navigationController popViewControllerAnimated:YES];
    } else {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}
- (void)leaderboardsButtonTapped {
    [self.navigationController pushViewController:self.leaderboardsController animated:YES];
}

- (void)balanceHistoryButtonTapped {
    ZFBalanceHistoryViewController *balanceHistoryViewController = [[ZFBalanceHistoryViewController alloc] init];
    [self.navigationController pushViewController:balanceHistoryViewController animated:YES];
}
- (void)scoreButtonTapped {
    ZFExperiencePointsViewController *scoreViewController = [[ZFExperiencePointsViewController alloc] initWithBackMessage:NSLocalizedString(@"BACK TO TIME HISTORY", @"View opened from Time History")];
    [self.navigationController pushViewController:scoreViewController animated:YES];
}

- (void)localLeaderboardsButtonFirstTapped {
    [self localLeaderboardsButtonTappedForButtonRank:First];
}
- (void)localLeaderboardsButtonSecondTapped {
    [self localLeaderboardsButtonTappedForButtonRank:Second];
}
- (void)localLeaderboardsButtonThirdTapped {
    [self localLeaderboardsButtonTappedForButtonRank:Third];
}

- (void)localLeaderboardsButtonTappedForButtonRank:(ZFHistoryTopRank)rank {
    
    if (rank >= self.topVenues.count) {
        // Sometines we have just 2 or 1 ranks out of 3.
        return;
    }
    
    [SVProgressHUD showWithStatus:NSLocalizedString(@"Fetching data...", nil) maskType:SVProgressHUDMaskTypeGradient];
    
    [ZFGetTimeSpentLeaderBoardByVenue requestWithCompletion:^(LORESTRequest *experiencePointsLeaderBoardByVenueRequest) {
        if(experiencePointsLeaderBoardByVenueRequest.resultStatus == LORESTRequestResultStatusSucceeded) {
            
            [ZFGetTimeSpentPositionByVenue requestWithCompletion:^(LORESTRequest *personalPositionRequestest) {
                if (personalPositionRequestest.resultStatus == LORESTRequestResultStatusSucceeded) {
                    
                    Hide_HudAndPerformBlock((^{
                        ZFPagedResults *personalPositionRequests = experiencePointsLeaderBoardByVenueRequest.result;
                        NSArray<ZFLeaderBoard *> *leaderboardTimeSpentArray = [NSArray arrayWithArray:personalPositionRequests.result];
                        
                        ZFLeaderBoardCount *personalTasksPosition = personalPositionRequestest.result;
                        
                        self.leaderboardsController.leaderBoardIndividualViewController = [[ZFLeaderboardsIndividualViewController alloc] initWithContent:leaderboardTimeSpentArray
                                                                                                                                                  andType:Time
                                                                                                                                            personalScore:personalTasksPosition
                                                                                                                                                    venue:self.topVenues[rank]
                                                                                                                                                      tab:ZFLeaderBoardsSortType_AllTime];
                        
                        self.leaderboardsController.leaderBoardIndividualViewController.delegate = self.leaderboardsController;
                        
                        [self presentViewController:self.leaderboardsController.leaderBoardIndividualViewController animated:YES completion:nil];
                        
                    }))
                } else {
                    [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"Could not read in data",nil)];
                }
                
            } forVenue:self.topVenues[rank] disableFilter:NO];
        } else {
            [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"Could not read in data",nil)];
        }
    } forVenue:self.topVenues[rank] resultItems:LOMaxItemCount search:nil disableFilter:YES];
}

#pragma mark - Network

- (void)loadNetworkData {
    [self loadPersonalWorldWideTimePosition];
    [self loadPersonalExpereincePosition];
    [self loadTopThreePositions];
    [self loadTable];
}

- (void)loadPersonalWorldWideTimePosition {
    [ZFGetTimeSpentPositionByVenue requestWithCompletion:^(LORESTRequest *request) {
        if (request.resultStatus != LORESTRequestResultStatusSucceeded) {
            return;
        }
        
        ZFLeaderBoardCount *timePosition = request.result;
        [contentView updatePosition:timePosition.position];
        [contentView updateLevel:CurrentUser.venueDetails.level];
        
    } forVenue:nil disableFilter:YES];
}

- (void)loadPersonalExpereincePosition {
    [ZFGetExperiencePointsPositionByVenue requestWithCompletion:^(LORESTRequest *request) {
        if (request.resultStatus != LORESTRequestResultStatusSucceeded) {
            return;
        }
        ZFLeaderBoardCount *expiriencePosition = request.result;
        [contentView updateExperiencePoints:expiriencePosition.valueCount];
        
    } disableFilter:YES venueId:nil forMemberId:nil];
}

- (void)loadTopThreePositions {    
    [ZFGetListTopTimeSpent requestWithCompletion:^(LORESTRequest *request) {
        
        if (request.resultStatus != LORESTRequestResultStatusSucceeded) {
            NSLog(@"⚠️ ZFGetListTopTimeSpent failed");
            return;
        }
        
        NSArray<ZFHistoryItem *>* historyTop = request.result;
        
        // Get places for red ribbons
        // for each venue
        NSMutableArray *topVenuesCollector = [NSMutableArray new];
        for (int i = 0; i < historyTop.count; i++) {
            ZFHistoryItem *item = historyTop[i];
            if (item) {
                [self loadPositionForVenueId:item.venueId rank:i];
                topVenuesCollector[i] = item.venueId;
            }
        }
        self.topVenues = topVenuesCollector;
        [contentView updateHistoryTop:historyTop];
    }];
}

- (void)loadPositionForVenueId:(NSNumber *)venueId rank:(ZFHistoryTopRank)rank{
    [ZFGetTimeSpentPositionByVenue requestWithCompletion:^(LORESTRequest *request) {
        if (request.resultStatus != LORESTRequestResultStatusSucceeded) {
            NSLog(@"⚠️ ZFGetTimeSpentPositionByVenue failed");
        }
        
        ZFLeaderBoard* userPlace = request.result;
        [contentView updateHistoryPosition:userPlace.position rank:rank];
        
    } forVenue:venueId disableFilter:YES];
}

- (void)loadTable {
    [ZFGetListTimeHistory requestWithCompletion:^(LORESTRequest *request) {
        if (request.resultStatus != LORESTRequestResultStatusSucceeded) {
            NSLog(@"⚠️ ZFGetListTimeHistory fail");
            return;
        }
        
        self.historyList = request.result;
        CGFloat tableViewHeight = tableRowHeight * self.historyList.count;
        [contentView setupTableViewForHeight:tableViewHeight];
        [contentView updateLayout];
        contentView.tableView.delegate = self;
        contentView.tableView.dataSource = self;
        UINib *nib = [UINib nibWithNibName:cellNibName bundle:nil];
        [contentView.tableView registerNib:nib forCellReuseIdentifier:cellIdentifier];
        contentView.tableView.scrollEnabled = NO;
        contentView.tableView.userInteractionEnabled = NO;
        
        int totalMintues = 0;
        for (ZFHistoryItem *item in self.historyList) {
            totalMintues += item.totalTime.intValue;
        }
        [contentView updateClockWithMinutesSpentTotal:@(totalMintues)];
        [self updateTimeDecelerator];
    }];
}

- (void)updateTimeDecelerator {
    
    [ZFGetSingleMemberRequest requestWithUser:CurrentUser.uuid completion:^(LORESTRequest *memberRequest) {
        
        if (memberRequest.resultStatus == LORESTRequestResultStatusSucceeded) {
            ZFUser *updatedUser = memberRequest.result;
            
            NSNumber *neededVenueIdentifier;
            // Is user checked in?
            if (updatedUser.currentVenueId.intValue != 0) {
                neededVenueIdentifier = updatedUser.currentVenueId;
            } else {
                // User not checked in, time decelerator will show the discounr based on favourite venue
                // Favourite venue is the venue with most minutes spent
                NSMutableDictionary<NSNumber *, NSNumber *> *venuesMap = [NSMutableDictionary new];
                for (ZFHistoryItem *historyItem in self.historyList) {
                    if (![venuesMap objectForKey:historyItem.venueId]) {
                        venuesMap[historyItem.venueId] = historyItem.totalTime;
                    } else {
                        venuesMap[historyItem.venueId] = @(venuesMap[historyItem.venueId].intValue + historyItem.totalTime.intValue);
                    }
                }
                // Fnd the venue with most time spent
                NSDictionary<NSNumber *, NSNumber *> *venuesMapFinal = [NSDictionary dictionaryWithDictionary:venuesMap];
                NSNumber *largestKey;
                int largestValue = 0;
                int currentValue = 0;
                for (NSNumber *key in venuesMapFinal.allKeys) {
                    currentValue = venuesMapFinal[key].intValue;
                    if (currentValue > largestValue) {
                        largestValue = currentValue;
                        largestKey = key;
                    }
                }
                neededVenueIdentifier = largestKey;
            }
            
            [ZFGetTimeDecelerator requestWithCompletion:^(LORESTRequest *timeDeceleratorRequest) {
                if (timeDeceleratorRequest.resultStatus != LORESTRequestResultStatusSucceeded) {
                    NSLog(@"⚠️ ZFGetTimeDecelerator fail");
                    return;
                }
                NSNumber *dicount = timeDeceleratorRequest.result;
                NSNumber *discountPercent = @(dicount.floatValue * 100);
                [contentView updateDiscount:discountPercent];
                
            } venueIdentifer:neededVenueIdentifier];
        }
    }];
}

#pragma mark - UITableViewDataSource

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return tableRowHeight;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    NSInteger rowsTotal = 0;
    
    if (self.historyList.count) {
        rowsTotal = self.historyList.count;
    }
    
    return rowsTotal;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    ZFHistoryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    if ((indexPath.row % 2) != 0) {
        // Non Even cell has grey background
        UIColor *mercuryColor = [UIColor colorWithRed:234/255.0 green:234/255.0 blue:234/255.0 alpha:1];
        
        cell.contentView.backgroundColor = mercuryColor;
        cell.date.backgroundColor = mercuryColor;
        cell.minutes.backgroundColor = mercuryColor;
        cell.cityName.backgroundColor = mercuryColor;
        cell.backgroundFillView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"ZFTimeHistoryTableBackgroundFilled"]];
    } else {
        cell.contentView.backgroundColor = [UIColor whiteColor];
        cell.date.backgroundColor = [UIColor whiteColor];
        cell.minutes.backgroundColor = [UIColor whiteColor];
        cell.cityName.backgroundColor = [UIColor whiteColor];
        cell.backgroundFillView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"ZFTimeHistoryTableBackground"]];
    }
    
    // Setup data
    ZFHistoryItem *item = self.historyList[indexPath.row];
    
    // Location
    cell.cityName.text = item.location;
    
    // Date
    NSTimeInterval secondsIntervalSinceCheckOut = item.checkOutDate.doubleValue / 1000;
    NSDate *checkOutDate = [NSDate dateWithTimeIntervalSince1970:secondsIntervalSinceCheckOut];
    if (secondsIntervalSinceCheckOut == 0) {
        // User haven't check out yet.
        checkOutDate = [NSDate new];
    }
    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
    [dateFormatter setTimeStyle:NSDateFormatterNoStyle];
    dateFormatter.doesRelativeDateFormatting = YES;
    cell.date.text = [dateFormatter stringFromDate:checkOutDate];
    
    // Date component
    NSTimeInterval minutesIntervalFromCheckInToCheckOut = item.totalTime.doubleValue;
    NSDateComponentsFormatter *formatter = [NSDateComponentsFormatter new];
    formatter.unitsStyle = NSDateComponentsFormatterUnitsStyleShort;
    NSDateComponents *components = [NSDateComponents new];
    components.minute = minutesIntervalFromCheckInToCheckOut;
    cell.minutes.text = [formatter stringFromDateComponents:components];
    
    // Badge
    if (indexPath.row == 0 && [CurrentUser.currentVenueId isEqualToNumber:item.venueId]) {
        // Show green badge
        cell.badge.hidden = NO;
        // Move minutes label out of the way
        
        cell.minutesRightConstraint.constant = (notIPad ? 80:192);
        cell.badgeLabel.text = NSLocalizedString(@"still here", @"Greed badge in History of Time");
    } else {
        cell.badge.hidden = YES;
        
        cell.minutesRightConstraint.constant = (notIPad ? 20:48);
    }
    [cell setNeedsLayout];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

#pragma mark - Notifications

- (void)handleNotificationTimeHistoryTable {
    // Updates clock and time decelerator but not the table
    // Table View Controller needs to be rewrited
    self.historyList = [ZFNetworkService sharedInstance].timeHistoryList;
    
    int totalMintues = 0;
    for (ZFHistoryItem *item in self.historyList) {
        totalMintues += item.totalTime.intValue;
    }
    [contentView updateClockWithMinutesSpentTotal:@(totalMintues)];
    [self updateTimeDecelerator];
}

- (void)handleNotificationTimeSpentRefreshed {
    ZFLeaderBoardCount *timePosition = [ZFNetworkService sharedInstance].timeHistoryTimePosition;
    [contentView updatePosition:timePosition.position];
    [contentView updateLevel:CurrentUser.venueDetails.level];
}
- (void)handleNotificationTimeExpereinceRefreshed {
    ZFLeaderBoardCount *expiriencePosition = [ZFNetworkService sharedInstance].timeHistoryExperiencePosition;
    [contentView updateExperiencePoints:expiriencePosition.valueCount];
}
- (void)handleNotificationTopTimeSpentRefreshed {
    NSArray<ZFHistoryItem *>* historyTop = [ZFNetworkService sharedInstance].timeHistoryTop;
    
    // Get places for red ribbons
    // for each venue
    NSMutableArray *topVenuesCollector = [NSMutableArray new];
    for (int i = 0; i < historyTop.count; i++) {
        ZFHistoryItem *item = historyTop[i];
        if (item) {
            [self loadPositionForVenueId:item.venueId rank:i];
            topVenuesCollector[i] = item.venueId;
        }
    }
    self.topVenues = topVenuesCollector;
    [contentView updateHistoryTop:historyTop];
}

- (void)dealloc {
    Notification_RemoveObserver;
}

@end
