//
//  ZFAchievementsInfoViewController.m
//  Ziferblat
//
//  Created by Terry Michel on 08/04/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "ZFAchievementsInfoViewController.h"
#import "ZFMemberVenueDetails.h"
#import "ZFGetAchievementsRequest.h"
#import "ZFGetAchievementLevelsRequest.h"
#import "ZFGetSingleAchievementRequest.h"
#import "ZFAchievement.h"
#import "ZFAchievementLevel.h"
#import "ZFBackButton.h"
#import "LOCarouselView.h"
#import "ZFAchievementsCarouselCell.h"
#import "ZFBaseView.h"

@implementation ZFAchievementsInfoViewController

#define kHeaderLabelFrame           (notIPad ? CGRectMake(48, 40, 223, 27):CGRectMake(115, 76, 535, 65))
#define kCarouselFrame              (notIPad ? CGRectMake(0.0, 30.0, self.view.width, 150.0):CGRectMake(0.0, 72, self.view.width, 360))
#define kTitleLabelFrame            (notIPad ? CGRectMake(100.0, 222.0, 150.0, 20.0):CGRectMake(240, 513, 360, 48))
#define kYouHaveItImageViewFrame    (notIPad ? CGRectMake(235.0, 212.0,70.0,39.0):CGRectMake(564, 489, 168, 94))
#define kYouHaveItLabelFrame        (notIPad ? CGRectMake(250.0, 232.0, 40.0, 20.0):CGRectMake(600, 557, 96, 48))

#pragma mark -
#pragma mark Lifecycle

- (void)loadView {
    ZFBaseView *aView = [[ZFBaseView alloc] init];
    self.view = aView;
    
    UIImageView *topImageView = [UIImageView imageViewWithImageNamed:@"ZFAchievementsInfoView"
                                                              origin:CGPointMake(0, 0)
                                                           addToView:kScrollView];
    
    ZFBackButton *backButton = [ZFBackButton backButtonWithTitle:NSLocalizedString(@"BACK TO ACHIEVEMENTS",nil)];
    [self.view addSubview:backButton];
    topImageView.top = backButton.bottom - 1;
    TARGET_TOUCH_UP(backButton, @selector(backButtonTapped))
    
    UILabel *headerLabel = [[UILabel alloc]initWithFrame:kHeaderLabelFrame];
    headerLabel.textAlignment = NSTextAlignmentCenter;
    headerLabel.text = NSLocalizedString(@"ACHIEVEMENT LIST",nil);
    headerLabel.font = FontRockWell((notIPad ? 20:48));
    headerLabel.textColor = Colour_White;
    headerLabel.adjustsFontSizeToFitWidth = YES;
    headerLabel.minimumScaleFactor = 0.5;
    [kScrollView addSubview:headerLabel];
    
    carousel = [[LOCarouselView alloc] initWithFrame:kCarouselFrame];
    carousel.horizontalSpread = kWidth * 0.75;
    carousel.verticalSpread = 0.0;
    carousel.alphaAdjustment = 0;
    [kScrollView addSubview:carousel];
    
    titleLabel = [[UILabel alloc]initWithFrame:kTitleLabelFrame];
    titleLabel.numberOfLines = 0;
    [titleLabel setFont:FontRockWell((notIPad ? 16:30))];
    [titleLabel setTextColor:Colour_Black];
    [titleLabel setTextAlignment:NSTextAlignmentCenter];
    [kScrollView addSubview:titleLabel];
    [titleLabel centerHorizontallyInSuperView];
    
    youHaveContainer = [[UIView alloc] initWithFrame:kYouHaveItImageViewFrame];
    [kScrollView addSubview:youHaveContainer];
    
    youHaveItImageView = [[UIImageView alloc] initWithFrame:kYouHaveItImageViewFrame];
    youHaveItImageView.image = [UIImage imageNamed:RESOURCE_ACHIEVEMENT_LEVEL_REWARD];
    youHaveItImageView.origin = CGPointZero;
    [youHaveContainer addSubview:youHaveItImageView];
    
    youHaveItLabel = [[UILabel alloc]initWithFrame:kYouHaveItLabelFrame];
    [youHaveItLabel setFont:FontRockWell((notIPad ? 8:19))];
    [youHaveItLabel setTextColor:Colour_Black];
    [youHaveItLabel setTextAlignment:NSTextAlignmentCenter];
    youHaveItLabel.origin = (notIPad ? CGPointMake(15, 20):CGPointMake(36, 48));
    [youHaveContainer addSubview:youHaveItLabel];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self makeRequests];
    carousel.delegate =self;
    carousel.dataSource = self;
}

#pragma mark -
#pragma mark Buttons actions

- (void)backButtonTapped {
    if([self.parentViewController isKindOfClass:[UINavigationController class]]) {
        [self.navigationController popViewControllerAnimated:YES];
    } else {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

#pragma mark -
#pragma mark Carousel Datasource Methods

- (NSInteger)elementCount {
    return achievements.count;
}

- (UIControl *)elementForIndex:(NSUInteger)index {
    ZFAchievement *achievement = [achievements objectAtIndex:index];
    BOOL achieved = FALSE;
    
    for (ZFAchievementLevel *achievementlevel in achievementLevels) {
        if ([achievementlevel.achievement.uuid isEqualToNumber:achievement.uuid]) {
            achieved = TRUE;
        }
    }
    
    ZFAchievementsCarouselCell *cell = [[ZFAchievementsCarouselCell alloc] initWithAchievement:achievement status:achieved ? ZFAchievementStatusAchieved : ZFAchievementStatusNotAchieved];
    [cell addTarget:self action:@selector(onElementSelected:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
}

- (void)onElementSelected:(UIControl *)sender {
    // Called only when user taps on already visible achivemnt icon
    // There's no need to scroll here.
    // [carousel scrollToElement:sender];
}

#pragma mark -
#pragma mark Carousel Delegate Methods

- (void)carousel:(LOCarouselView *)aCarousel isAtIndex:(NSUInteger)anIndex {
    BOOL showAchievementImage = FALSE;
    
    ZFAchievement *achievement = [achievements objectAtIndex:anIndex];
    ZFAchievementLevel *achievementlevel;
    
    for (achievementlevel in achievementLevels) {
        if ([achievementlevel.achievement.uuid isEqualToNumber:achievement.uuid]) {
            showAchievementImage = TRUE;
            break;
        }
    }
    
    if (showAchievementImage) {
        [youHaveItImageView setHidden:FALSE];
        [youHaveItLabel setHidden:FALSE];
        NSString *string =[NSString stringWithFormat:NSLocalizedString(@"Lv.%@",nil),achievementlevel.sequenceIndex];
        youHaveItLabel.text =string;
    } else {
        [youHaveItImageView setHidden:TRUE];
        [youHaveItLabel setHidden:TRUE];
    }
    
    titleLabel.text = achievement.name;
    CGSize size = [titleLabel sizeThatFits:CGSizeMake(CGRectGetWidth(kTitleLabelFrame), 1000)];
    titleLabel.frame = CGRectMake(0.f, CGRectGetMinY(kTitleLabelFrame), size.width, size.height);
    [titleLabel centerHorizontallyInSuperView];
    [youHaveContainer verticalAlignToView:titleLabel];
    
    [self setDescriptionTextViewText:achievement.achievementDetails];
}

#pragma mark -
#pragma mark Request Methods

- (void)makeRequests {
    [SVProgressHUD showWithStatus:NSLocalizedString(@"Fetching data...", nil) maskType:SVProgressHUDMaskTypeGradient];
    [ZFGetAchievementsRequest requestWithCompletion:^(LORESTRequest *aRequest) {
        if(aRequest.resultStatus == LORESTRequestResultStatusSucceeded) {
            achievements = aRequest.result;
            
            [ZFGetAchievementLevelsRequest requestWithCompletion:^(LORESTRequest *aRequest) {
                Hide_HudAndPerformBlock(^{
                
                    if(aRequest.resultStatus == LORESTRequestResultStatusSucceeded) {
                        if ([aRequest.result isKindOfClass:[NSArray class]]) {
                            achievementLevels = aRequest.result;
                            
                            [carousel reload]; // This will show images
                            
                            ZFAchievement *neededAchivement;
                            for (ZFAchievement *ach in achievements) {
                                if ([ach.uuid isEqual:@(_selectedAchievementIdentifier)]) {
                                    neededAchivement = ach;
                                    break;
                                }
                            }
                            NSUInteger neededIndex = [achievements indexOfObject:neededAchivement];
                            if (neededIndex > 0) {
                                // No need to scroll in this case
                                // When achivement list opened, first
                                // achivement already enlarged
                                [carousel scrollToElementAtIndex:neededIndex];
                                _selectedAchievementIdentifier = 0;
                            }
                        }
                    }
                })
            }];
        } else {
            [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"Could not read in data", nil)];
        }
    }];
}

#pragma mark -
#pragma mark Update layout

- (void)setDescriptionTextViewText:(NSString *)text {
    [descriptionTextView removeFromSuperview];
    
    CGFloat maxY = (titleLabel.bottom > youHaveContainer.bottom) ? titleLabel.bottom : youHaveContainer.bottom;
    
    descriptionTextView = [[UITextView alloc]initWithFrame:CGRectMake(kPadding, maxY + kPadding, self.view.width - kPadding*2, 100)];
    descriptionTextView.text = text;
    descriptionTextView.font = FontRockWell(notIPad ? 13:26);
    descriptionTextView.editable = NO;
    descriptionTextView.scrollEnabled = NO;
    [kScrollView addSubview:descriptionTextView];
    [descriptionTextView sizeToFit];
    [descriptionTextView layoutIfNeeded];
    
    kScrollView.contentSize = CGSizeMake(self.view.width, descriptionTextView.bottom + kMediumPadding);
}

@end
