//
//  ZFBalanceHistoryViewController.m
//  Ziferblat
//
//  Created by Terry Michel on 08/04/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "ZFBalanceHistoryViewController.h"
#import "ZFGetBalanceHistoryRequest.h"
#import "ZFBalanceHistoryCell.h"
#import "ZFBalanceHistory.h"
#import "ZFBackButton.h"
#import "LOTableView.h"
#import "ZFLocalisedImageDefinitions.h"

@implementation ZFBalanceHistoryViewController

#define ktopImageHeight (notIPad ? 133:298)

#pragma mark - 
#pragma mark Lifecycle

- (void)loadView {
    ZFBaseView *aView = [[ZFBaseView alloc] init];
    self.view = aView;
    
    ZFBackButton *backButton = [ZFBackButton backButtonWithTitle:NSLocalizedString(@"BACK TO MY TREASURY", nil)];
    [self.view addSubview:backButton];
    TARGET_TOUCH_UP(backButton, @selector(backButtonTapped))
    
    UIImageView *topImageView = [UIImageView imageViewWithImageNamed:@"BalanceHistoryHeader"];
    [kScrollView addSubview:topImageView];
    topImageView.top = backButton.bottom - 1 ;
    
    UILabel *headerLabel = [[UILabel alloc]initWithFrame:(notIPad ? CGRectMake(44, 46, 232, 33):CGRectMake(106, 97, 557, 79))];
    headerLabel.text = NSLocalizedString(@"BALANCE HISTORY",nil);
    headerLabel.textAlignment = NSTextAlignmentCenter;
    headerLabel.font = FontRockWell(notIPad ? 21:50);
    headerLabel.adjustsFontSizeToFitWidth = YES;
    headerLabel.minimumScaleFactor = 0.7;
    [kScrollView addSubview:headerLabel];
    
    tableView = [LOTableView tableViewWithFrame:CGRectMake(0,
                                                           ktopImageHeight,
                                                           kScrollView.width,
                                                           kScrollView.height - ktopImageHeight)
                                  noContentView:nil
                                  noContentText:NSLocalizedString(@"No history",nil)
                                     canRefresh:NO
                                      addToView:kScrollView];
    tableView.tableView.allowsSelection = NO;
    tableView.backgroundColor = Colour_Clear;
    tableView.tableView.backgroundColor = Colour_Clear;
    tableView.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    tableView.tableView.rowHeight = (notIPad ? 60.0:144);
    [tableView.tableView setShowsVerticalScrollIndicator:NO];
    [tableView setController:self];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self getBalanceHistory];
}

#pragma mark -
#pragma mark UITableViewDataSource

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [ZFBalanceHistoryCell heightForBalanceHistory:[balanceHistory objectAtIndex:indexPath.row] isLast:(indexPath.row == balanceHistory.count-1)];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return balanceHistory.count;
}

- (UITableViewCell *)tableView:(UITableView *)aTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *const ident = [ZFBalanceHistoryCell reuseIdentifier];
    
    ZFBalanceHistoryCell *cell = (ZFBalanceHistoryCell *)[aTableView dequeueReusableCellWithIdentifier:ident];
    
    if(cell == nil) {
        cell = [[ZFBalanceHistoryCell alloc] initWithReuseIdentifier:ident];
    }
    
    ZFBalanceHistory *item = [balanceHistory objectAtIndex:indexPath.row];
    [cell setBalanceHistory:item isLast:(indexPath.row == balanceHistory.count-1)];
    return cell;
}

#pragma mark -
#pragma mark Request Methods

- (void)getBalanceHistory {
    Show_Hud(NSLocalizedString(@"Fetching data...",nil))
    [ZFGetBalanceHistoryRequest requestWithCompletion:^(LORESTRequest *aRequest) {
        if(aRequest.resultStatus == LORESTRequestResultStatusSucceeded) {
            Hide_HudAndPerformBlock(^{
                balanceHistory = ((NSMutableArray *)aRequest.result).mutableCopy;
                [tableView reloadData];
            })
        } else {
            Show_ConnectionError
        }
    }];
}

@end
