//
//  ZFNotificationsViewController.h
//  Ziferblat
//
//  Created by Terry Michel on 08/04/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "ZFNotificationsView.h"
#import "ZFViewController.h"

@interface ZFNotificationsViewController : ZFViewController<UITableViewDelegate, UITableViewDataSource> {
    ZFNotificationsView *contentView;
}

//Constraints
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topNavigationControlHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *headerViewHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *headerLabelYConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *noNotificationsLabelYConstraint;



- (ZFNotificationsViewController *)init __attribute__((unavailable("init not available")));
- (ZFNotificationsViewController *)initWithSelectedVenue:(ZFVenue *)venue
                                              venuesList:(NSArray<ZFVenue *> *)venuesList;

@end
