//
//  ZFNewsArticleViewController.h
//  Ziferblat
//
//  Created by Terry Michel on 08/04/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZFNews.h"
#import "ZFViewController.h"

@interface ZFNewsArticleViewController : ZFViewController

@property (nonatomic, strong) ZFNews *selectedNews;
@property (nonatomic, strong) NSNumber *venueId;

@end
