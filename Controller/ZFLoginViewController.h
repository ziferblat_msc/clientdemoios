//
//  ZFLoginViewController.h
//  Ziferblat
//
//  Created by Terry Michel on 08/04/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "ZFBaseView.h"

@class ZFButton;

@interface ZFLoginViewController : UIViewController<UITextFieldDelegate, UIGestureRecognizerDelegate> {
    UIImageView *backgroundImageView;
    UITextField *emailTextField;
    UITextField *passwordTextField;
    UIButton *resetPasswordButton;
    ZFButton *loginButton;
    UIButton *skipItButton;
    UIButton *signupButton;
    UILabel *headerLabel;
    UILabel *comeBackLabel;
    UILabel *resetPasswordLabel;
    UILabel *skipItLabel;
    UILabel *justWannaSeeLabel;
    UILabel *signupLabel;
}

@end
