//
//  ZFOurMenuViewController.h
//  Ziferblat
//
//  Created by Terry Michel on 08/04/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "ZFActivitiesView.h"
#import "ZFViewController.h"

@interface ZFActivitiesViewController : ZFViewController<UITableViewDataSource, UITableViewDelegate> {
    ZFActivitiesView *contentView;
}

- (instancetype)initWithSelectedVenue:(ZFVenue *)aVenue;

@end
