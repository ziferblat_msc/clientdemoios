//
//  ZFEventViewController.h
//  Ziferblat
//
//  Created by Jose Fernandez on 26/08/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZFEventsRequest.h"

@class ZFEvents;

@interface ZFEventViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *backLabel;
@property (weak, nonatomic) IBOutlet UIView *parentContainerView;

@property (weak, nonatomic) IBOutlet UIView *eventDetailContainerView;
@property (weak, nonatomic) IBOutlet UIView *eventCommentsContainerView;
@property (nonatomic, strong) ZFEvents *event;
@property (nonatomic, strong) NSNumber *venueId;

//Constraint
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topNavigationControlHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *eventCommentsContainerHeightConstraint;

@end
