//
//  ZFActivitiesArchivesViewController.m
//  Ziferblat
//
//  Created by Terry Michel on 16/04/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "ZFActivitiesArchivesViewController.h"

@implementation ZFActivitiesArchivesViewController

#pragma mark - 
#pragma mark Lifecycle

- (void)loadView {
    contentView = [[ZFActivitiesArchivesView alloc] init];
    self.view = contentView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [contentView.backButton addTarget:self action:@selector(backButtonTapped) forControlEvents:UIControlEventTouchUpInside];
}

#pragma mark - 
#pragma mark Buttons actions

- (void)backButtonTapped {
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
