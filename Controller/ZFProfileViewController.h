//
//  ZFProfileViewController.h
//  Ziferblat
//
//  Created by Terry Michel on 08/04/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZFFriendsHorizontalScrollView.h"
#import "ZFAchievementsHorizontalScrollView.h"
#import "ZFUserDetailsView.h"
#import "ZFViewController.h"

@class ZFBackButton;
@class ZFTimeView;
@class ZFProfileImage;
@class ZFUser;
@class LOHorizontalCollectionViewLayout;
@class ZFUserDetailsView;
@class ZFDashedHeaderView;

@interface ZFProfileViewController : ZFViewController<ZFFriendsHorizontalScrollViewDelegate, ZFAchievementsHorizontalScrollViewDelegate,
ZFUserDetailsViewProtocol, UIScrollViewDelegate>
{
    ZFBackButton *backButton;
    UILabel *nameLabel;
    UILabel *rankLabel;
    UILabel *levelLabel;
    UILabel *scoreLabel;
    UILabel *activityLabel;
    UILabel *backLabel;
    UIButton *timeHistoryButton;
    UIButton *settingsButton;
    UIButton *scoreButton;
    
    UIImageView *topImageView;
    UIImageView *largeImageView;
    
    ZFDashedHeaderView *friendHeaderView;
    ZFFriendsHorizontalScrollView *friendsView;
    ZFAchievementsHorizontalScrollView *achievementsView;
    ZFUserDetailsView *userDetailsView;
    ZFProfileImage *smallImageView;
    ZFTimeView *timeView;
    ZFUser *user;
}

#pragma mark - Instance

- (instancetype _Nonnull)initWithUser:(ZFUser * _Nonnull)aUser;

- (instancetype _Nonnull)initWithUser:(ZFUser * _Nonnull)aUser
                         backMessage:(NSString * _Nonnull)aBackMessage
                           andFilter:(BOOL)isFilterEnabled
                          forVenueId:(NSNumber * _Nullable)venuedId; // Pass nil to see worldwide

@end
