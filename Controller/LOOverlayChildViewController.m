//
//  Created by Danny Bravo
//  Copyright (c) 2014 Locassa Ltd. All rights reserved.
//

#import "LOOverlayChildViewController.h"

@implementation LOOverlayChildViewController

#pragma mark - initializers
- (instancetype)initWithType:(LOOverlayViewType)aType andDelegate:(id <LOOverlayChildViewControllerDelegate>)aDelegate isPersisted:(BOOL)persisted
{
    self = [super init];
    if (self) {
        type = aType;
        delegate = aDelegate;
        isPersisted = persisted;
    }
    return self;
}

- (void)loadView {
    contentView = [[LOOverlayChildView alloc] init];
    [self setupOverlayView];
    self.view = contentView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    TARGET_TOUCH_UP(contentView.dismissButton, @selector(dismissButtonTapped));
}

- (void)setupOverlayView {
    printTodo(@"subclasses should implement this method");
    printFailPointAndAbort;
}

#pragma mark - interaction
- (void)dismissButtonTapped {
    [self dismissAndPerformBlock:nil];
}

#pragma mark - animations
- (void)dismissAndPerformBlock:(void (^)())aBlock
{
    [contentView dismissWithType:type andPerformBlock:^{
        if ([delegate respondsToSelector:@selector(overlayChildViewControllerDidDismiss:)]) {
            [delegate overlayChildViewControllerDidDismiss:self];
        }
        if(!isPersisted) {
            [contentView removeFromSuperview];
            [self.parentViewController dismissChildController:self];
        }
        [self runBlock:aBlock];
    }];
}

-(void)didMoveToParentViewController:(UIViewController *)parent {
    if(!isPersisted)
    {
        if (parent) {
            [contentView presentWithType:type animated:YES andPerformBlock:^{
                self.parentViewController.view.userInteractionEnabled = YES;
            }];
        }
    }
}

@end
