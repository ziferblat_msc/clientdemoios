//
//  ZFSubscriptionTopUpViewController.h
//  Ziferblat
//
//  Created by Terry Michel on 08/04/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

@class ZFBaseView;

@interface ZFSubscriptionTopUpViewController : UIViewController {
    ZFBaseView *contentView;
    UIButton *piastresButton;
    UILabel *piastresLabel;
    UIButton *timeButton;
    UILabel *timeLabel;
}

@end
