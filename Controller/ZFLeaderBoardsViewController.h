//
//  ZFLeaderBoardsViewController.h
//  Ziferblat
//
//  Created by Terry Michel on 15/04/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "ZFLeaderBoardsView.h"
#import "ZFleaderBoard.h"
#import "ZFLeaderboardsIndividualViewController.h"

@interface ZFLeaderBoardsViewController : UIViewController <ZFLeaderBoardsViewDelegate> {
    ZFLeaderBoardsView *contentView;
}

@property (strong, nonatomic) ZFLeaderboardsIndividualViewController *leaderBoardIndividualViewController;

- (void)presentLeaderBoard:(NSArray<ZFLeaderBoard *>*)aBoard
                  withType:(ZFLeaderBoardType)aType
                   venueId:(NSNumber *)venueId
                      from:(UIViewController *)presenter;

- (void)makeRequestsWith:(NSNumber *)aVenueId;

- (instancetype)initWithTitle:(NSString *)title
                      venueId:(NSNumber *)venueId;
// If you don't know which venue to pass use CurrentUser.selectedVenue.uuid
// Use nil to select worldwide venue.

@end
