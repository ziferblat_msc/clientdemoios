//
//  ZFSharePiastresViewController.m
//  Ziferblat
//
//  Created by Terry Michel on 08/04/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "ZFShareViewController.h"
#import "ZFSharePiastresOrTimeRequest.h"
#import "LOHorizontalCollectionViewLayout.h"
#import "ZFProfileImageCollectionViewCell.h"
#import "ZFBaseView.h"
#import "ZFVenue.h"
#import "ZFButton.h"
#import "ZFBackButton.h"
#import "ZFLocalisedImageDefinitions.h"
#import "LOImageCache.h"
#import "LOImageDownloader.h"

@implementation ZFShareViewController

#define kSendToProfileImageViewWidth    (notIPad ? 94:226)
#define kSendToProfileImageViewHeight   (notIPad ? 76:182)
#define kSendToProfileImageViewInset    (notIPad ? 68:163)

#define kPiastresTop                    (notIPad ? 190.0:456)
#define kSendToNameLabelTop             (notIPad ? 147.0:353)
#define kDoneButtonTop                  (notIPad ? 194.0:466)
#define kHeaderLabelFrame               (notIPad ? CGRectMake(59, 37, 201, 38):CGRectMake(142, 79, 482, 91))
#define kWhatKindOfLoveLabelFrame       (notIPad ? CGRectMake(35, 94, 250, 55):CGRectMake(84, 216, 600, 132))
#define kYouHaveLabelFrame              (notIPad ? CGRectMake(102, 149, 120, 25):CGRectMake(245, 342, 288, 60))
#define kChooseTheLuckyOneLabelFrame    (notIPad ? CGRectMake(70, 268, 180, 25):CGRectMake(168, 643, 432, 60))
#define kAmountShareFrame               (notIPad ? CGRectMake(30, 290, 258, 42):CGRectMake(72, 696, 619, 101))
#define kSendToLabelFrame               (notIPad ? CGRectMake(200, 290, 285, 24):CGRectMake(480, 696, 684, 58))
#define kSendToLocationLabelFrame       (notIPad ? CGRectMake(200, 320, 285, 24):CGRectMake(480, 768, 684, 58))

- (instancetype)initWitShareType:(ZFShareType)aType venuesList:(NSArray *)venuesList {
    self = [super init];
    if(self) {
        type = aType;
        selectedUser = [CurrentUser.friendsList firstObject];
        venues = venuesList;
        friendsList = [NSMutableArray arrayWithArray:CurrentUser.friendsList];
    }
    return self;
}

- (void)loadView {
    ZFBaseView *aView = [[ZFBaseView alloc] init];
    [aView.scrollView setDelegate:self];
    self.view = aView;
    
    UIImageView *topImageView = [UIImageView imageViewWithImageNamed:RESOURCE_SHARE_VIEW_TOP];
    [aView addElement:topImageView addPadding:FALSE];
    topImageView.userInteractionEnabled = YES;
    
    ZFBackButton *backButton = [ZFBackButton backButtonWithTitle:NSLocalizedString(@"BACK TO MY TREASURY", nil)];
    [self.view addSubview:backButton];
    TARGET_TOUCH_UP(backButton, @selector(backButtonTapped));
    
    UILabel *headerLabel = [[UILabel alloc]initWithFrame:kHeaderLabelFrame];
    headerLabel.text = NSLocalizedString(@"SEND SOME LOVE",nil);
    headerLabel.textAlignment = NSTextAlignmentCenter;
    headerLabel.font = FontRockWell(notIPad ? 19:46);
    headerLabel.textColor = Colour_White;
    headerLabel.adjustsFontSizeToFitWidth = YES;
    headerLabel.minimumScaleFactor = 0.5;
    [kScrollView addSubview:headerLabel];
    
    UILabel *whatKindOfLoveLabel = [[UILabel alloc]initWithFrame:kWhatKindOfLoveLabelFrame];
    whatKindOfLoveLabel.text = NSLocalizedString(@"What kind of\nlove would you like to share?",nil);
    whatKindOfLoveLabel.textAlignment = NSTextAlignmentCenter;
    whatKindOfLoveLabel.font = FontRockWell(notIPad ? 19:46);
    whatKindOfLoveLabel.numberOfLines = 2;
    whatKindOfLoveLabel.adjustsFontSizeToFitWidth = YES;
    whatKindOfLoveLabel.minimumScaleFactor = 0.5;
    [kScrollView addSubview:whatKindOfLoveLabel];
    
    UILabel *youHaveLabel = [[UILabel alloc]initWithFrame:kYouHaveLabelFrame];
    youHaveLabel.text = NSLocalizedString(@"YOU HAVE",nil);
    youHaveLabel.textAlignment = NSTextAlignmentCenter;
    youHaveLabel.font = FontRockWell(notIPad ? 19:46);
    youHaveLabel.adjustsFontSizeToFitWidth = YES;
    youHaveLabel.minimumScaleFactor = 0.5;
    [kScrollView addSubview:youHaveLabel];
    
    keyboard = [[ZFKeyboard alloc] init];
    [keyboard setDelegate:self];
    [topImageView addSubview:keyboard];
    keyboard.bottom = topImageView.height - (notIPad ? 84:202);
    
    ZFLocalisedImageDefinitions *imageDefinitions = [ZFLocalisedImageDefinitions sharedInstance];
    
    piastresButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [piastresButton setImage:imageDefinitions.resourceSharePiastres selectedImage:imageDefinitions.resourceSharePiastresSelected];
    piastresButton.left = kLargePadding;
    piastresButton.top = kPiastresTop;
    [piastresButton.titleLabel setFont:FontHelvetica(notIPad ? 19:46)];
    [piastresButton setTitleColor:Colour_Black forState:UIControlStateNormal];
    [piastresButton setTitle:NSLocalizedString(@"0",nil) forState:UIControlStateNormal];
    [piastresButton setTitleEdgeInsets:UIEdgeInsetsMake((notIPad ? -61:-146), 0, 0, 0)];
    [kScrollView addSubview:piastresButton];
    
    TARGET_TOUCH_UP(piastresButton, @selector(piastresButtonPressed:));
    
    timeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [timeButton setImage:imageDefinitions.resourceShareTime selectedImage:imageDefinitions.resourceShareTimeSelected];
    timeButton.right = self.view.width - kLargePadding;
    timeButton.top = piastresButton.top - 3.0;
    [kScrollView addSubview:timeButton];
    
    CGFloat timeLabelSize = (notIPad ? 48:115);
    CGFloat x = (notIPad ? 217:521);
    CGFloat y = (notIPad ? 215:516);
    timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(x, y, timeLabelSize, timeLabelSize)];
    timeLabel.font = FontLobster(notIPad ? 22:53);
    timeLabel.textColor = Colour_Black;
    timeLabel.adjustsFontSizeToFitWidth = YES;
    timeLabel.minimumScaleFactor = 0.25;
    timeLabel.clipsToBounds = YES;
    timeLabel.layer.cornerRadius = timeLabelSize / 2;
    timeLabel.text = @"0";
    timeLabel.textAlignment = NSTextAlignmentCenter;
    [kScrollView addSubview:timeLabel];
    
    TARGET_TOUCH_UP(timeButton, @selector(timeButtonPressed:));
    
    venuesScrollView = [[UIScrollView alloc] initWithFrame:(notIPad ? CGRectMake(0, 0, 255.0, 44):CGRectMake(0, 0, 612, 106))];
    [topImageView addSubview:venuesScrollView];
    [venuesScrollView centerHorizontallyInSuperView];
    venuesScrollView.left += 1.0;
    venuesScrollView.bottom = topImageView.height - (notIPad ? 31:74);
    [venuesScrollView setPagingEnabled:TRUE];
    [venuesScrollView setDelegate:self];
    
    piastresLabel = [[UILabel alloc] init];
    [piastresLabel setFont:FontRockWell(notIPad ? 14:34)];
    [piastresLabel setTextAlignment:NSTextAlignmentCenter];
    [piastresLabel setTextColor:Colour_White];
    [topImageView addSubview:piastresLabel];
    piastresLabel.adjustsFontSizeToFitWidth = YES;
    [piastresLabel centerHorizontallyInSuperView];
    piastresLabel.bottom = topImageView.height - (notIPad ? 30:72);
    
    shareLabel = [[UILabel alloc] init];
    shareLabel.numberOfLines = 0;
    [shareLabel setFrame:kAmountShareFrame];
    shareLabel.textAlignment = NSTextAlignmentCenter;
    shareLabel.adjustsFontSizeToFitWidth = YES;
    shareLabel.font = FontRockWell(notIPad ? 18:43);
    shareLabel.textColor = Colour_White;
    
    [topImageView addSubview:shareLabel];
    
    bottomContainerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScrollView.width, 0)];
    
    UIImageView *bottomImageView = [UIImageView imageViewWithImageNamed:RESOURCE_SHARE_VIEW_BOTTOM];
    bottomImageView.userInteractionEnabled = YES;
    [bottomContainerView addSubview:bottomImageView];
    
    sendToProfileImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, kSendToProfileImageViewWidth, kSendToProfileImageViewHeight)];
    sendToProfileImageView.contentMode = UIViewContentModeScaleAspectFill;
    [bottomContainerView insertSubview:sendToProfileImageView belowSubview:bottomImageView];
    [sendToProfileImageView centerHorizontallyInSuperView];
    sendToProfileImageView.top = kSendToProfileImageViewInset;
    
    sendToLabel = [[UILabel alloc] init];
    [sendToLabel setFrame:kSendToLabelFrame];
    sendToLabel.textAlignment = NSTextAlignmentCenter;
    sendToLabel.adjustsFontSizeToFitWidth = YES;
    sendToLabel.font = FontHelvetica(notIPad ? 24:58);
    
    [bottomContainerView addSubview:sendToLabel];
    
    sendToLocationLabel = [[UILabel alloc] init];
    [sendToLocationLabel setFrame:kSendToLocationLabelFrame];
    sendToLocationLabel.adjustsFontSizeToFitWidth = YES;
    sendToLocationLabel.textAlignment = NSTextAlignmentCenter;
    [sendToLocationLabel setFont:FontHelvetica(notIPad ? 14:34)];
    [bottomContainerView addSubview:sendToLocationLabel];
    
    sendToNameLabel = [[UILabel alloc] init];
    [sendToNameLabel setFont:FontHelvetica(notIPad ? 18:43)];
    [sendToNameLabel setTextAlignment:NSTextAlignmentCenter];
    [bottomImageView addSubview:sendToNameLabel];
    sendToNameLabel.adjustsFontSizeToFitWidth = YES;
    [sendToNameLabel centerHorizontallyInSuperView];
    
    [bottomContainerView addSubview:sendToSeparatorImageView];
    
    chooseTheLuckyOneLabel = [[UILabel alloc]initWithFrame:kChooseTheLuckyOneLabelFrame];
    chooseTheLuckyOneLabel.text = NSLocalizedString(@"CHOOSE THE LUCKY ONE",nil);
    chooseTheLuckyOneLabel.textAlignment = NSTextAlignmentCenter;
    chooseTheLuckyOneLabel.font = FontHelvetica(notIPad ? 14:34);
    chooseTheLuckyOneLabel.adjustsFontSizeToFitWidth = YES;
    chooseTheLuckyOneLabel.minimumScaleFactor = 0.5;
    [bottomImageView addSubview:chooseTheLuckyOneLabel];
    
    LOHorizontalCollectionViewLayout *layout = [[LOHorizontalCollectionViewLayout alloc] init];
    layout.itemSize = CGSizeMake((kScrollView.width - kPadding * 2) / 3.0, [ZFProfileImage largeSize].height);
    
    CGFloat maxColumnNumber = 3;
    NSInteger maxLineNumber = 4;
    CGFloat collectionViewHeight = ceil(friendsList.count / maxColumnNumber) * [ZFProfileImage largeSize].height;
    collectionViewHeight = MIN(collectionViewHeight, [ZFProfileImage largeSize].height * maxLineNumber);
    
    UICollectionView *collectionView = [[UICollectionView alloc]
                                        initWithFrame:CGRectMake(kPadding, 0, kScrollView.width - kPadding * 2, collectionViewHeight)
                                        collectionViewLayout:layout];
    [bottomContainerView addSubview:collectionView];
    collectionView.top = bottomImageView.height;
    
    collectionView.backgroundColor = Colour_Clear;
    collectionView.allowsMultipleSelection = NO;
    collectionView.scrollsToTop = NO;
    collectionView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    collectionView.pagingEnabled = YES;
    collectionView.delegate = self;
    collectionView.dataSource = self;
    
    [collectionView registerClass:[ZFProfileImageCollectionViewCell class]  forCellWithReuseIdentifier:[ZFProfileImageCollectionViewCell reuseIdentifier]];
    [collectionView selectItemAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0] animated:YES scrollPosition:UICollectionViewScrollPositionNone];
    
    doneButton = [ZFButton buttonWithTitle:NSLocalizedString(@"DONE !",nil) showMarks:YES];
    [bottomImageView addSubview:doneButton];
    [doneButton centerHorizontallyInSuperView];
    doneButton.top = kDoneButtonTop;
    
    TARGET_TOUCH_UP(doneButton, @selector(doneButtonTapped:));
    
    bottomContainerView.height = collectionView.bottom;
    [aView addElement:bottomContainerView];
    [aView layoutElements];
    bottomContainerTop = bottomContainerView.top;
    
    piastresButton.selected = (type == ZFShareType_Piastres);
    timeButton.selected = (type == ZFShareType_Time);
    selectedUser = friendsList.firstObject;
    selectedVenue = venues.firstObject;
    
    [self updateLayout:FALSE];
    [self updateAmount];
    [self updateSelectedUser];
    [self updateBalance];
    [self checkStatus];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    Notification_Observe(kNotification_AccountBalanceRefreshed, updateBalance)
    Notification_Observe(kNotification_VenuesListRefreshed, venuesListRefreshed)
    [NetworkService pollForType:ZFPollingRequestType_AccountBalance|ZFPollingRequestType_VenuesList memberId:CurrentUser.uuid venueId:nil summary:YES];
}

- (void)dealloc {
    Notification_RemoveObserver
}

#pragma mark -
#pragma mark Notifications

- (void)venuesListRefreshed {
    venues = NetworkService.venuesList;
    [self updateAmount];
}

#pragma mark -
#pragma mark Update Methods

- (void)updateLayout:(BOOL)animated {
    NSString *piastresToShareAtVenue = [NSString stringWithFormat:NSLocalizedString(@"Piastres to share (%@):",nil), CurrentUser.selectedVenue.name];
    NSString *minutesToShareAtVenue = [NSString stringWithFormat:NSLocalizedString(@"Minutes to share (%@):",nil), CurrentUser.selectedVenue.name];
    [shareLabel setText:piastresButton.selected ? piastresToShareAtVenue : minutesToShareAtVenue];
    
    [UIView animateWithDuration:animated ? 0.3 : 0.0 animations:^{
        bottomContainerView.top = bottomContainerTop - (piastresButton.selected ? (notIPad ? 75:180) : (notIPad ? 5:12));
        [kScrollView setContentSize:CGSizeMake(kScrollView.width, bottomContainerView.bottom + kLargePadding)];
    }];
}

- (void)updateBalance {
    [piastresButton setTitle:[NSString stringWithFormat:@"%d", CurrentUser.accountBalance.piastres.intValue]
                    forState:UIControlStateNormal];
    
    double minutes = round(CurrentUser.accountBalance.baseMinutes.doubleValue * CurrentUser.selectedVenue.baseMinutesFactor.doubleValue);
    timeLabel.text = [NSString stringWithFormat:@"%.0f", minutes];
}
- (void)updateSelectedUser {
    
    UIImage *image = [LOImageCache imageFromCache:selectedUser.imageUrl];
    if (!image) {
        image = [UIImage imageNamed:@"ZFPlaceHolderZorro"];
        [LOImageDownloader downloadImageFromPath:selectedUser.imageUrl
                            withCompletionHander:^(UIImage *downloadedImage) {
                                [UIView transitionWithView:sendToProfileImageView
                                                  duration:0.5
                                                   options:UIViewAnimationOptionTransitionCrossDissolve
                                                animations:^{
                                                    sendToProfileImageView.image = downloadedImage;
                                                } completion:nil];
                            }];
    } else {
        sendToProfileImageView.image = image;
    }

    sendToNameLabel.text = selectedUser.fullName;
    [sendToNameLabel sizeToFit];
    [sendToNameLabel centerHorizontallyInSuperView];
    sendToNameLabel.top = kSendToNameLabelTop;
}

- (void)updateAmount {
    [venuesScrollView removeSubviews];
    
    if (!amount || amount.doubleValue == 0) {
        // Keyboard is inactive
        piastresLabel.hidden = YES;
        return;
    }
    piastresLabel.hidden = NO;
    
    double base = CurrentUser.selectedVenue.baseMinutesFactor.doubleValue;
    double baseMinutes;
    
    if (base != 0) {
        baseMinutes = amount.doubleValue / base;
    } else {
        baseMinutes = amount.doubleValue;
    }
    
    for(ZFVenue *venue in venues) {
        UILabel *venueLabel = [UILabel new];
        [venueLabel setFont:FontRockWell((notIPad ? 14:34))];
        [venueLabel setFrame:(notIPad ? CGRectMake(0, 0, 210, 20):CGRectMake(0, 0, 504, 48))];
        venueLabel.textAlignment = NSTextAlignmentCenter;
        venueLabel.adjustsFontSizeToFitWidth = YES;
        venueLabel.contentMode = UIViewContentModeTop;
        [venuesScrollView addSubview:venueLabel];
        
        double minutes = venue.baseMinutesFactor.doubleValue * baseMinutes;
        int roundMinutes = round(minutes);
        if (minutes < 1 && minutes > 0) {
            // We need to guarantee that user will not see zero in the UI.
            // ZFI-374
            roundMinutes = 1;
        }
        
        venueLabel.text =[NSString stringWithFormat:NSLocalizedString(@"%d min at %@",nil), roundMinutes, venue.name];
        [venueLabel centerInSuperView];
        venueLabel.left += venuesScrollView.width * [venues indexOfObject:venue];
    }
    
    [venuesScrollView setContentSize:CGSizeMake(venues.count * venuesScrollView.width, venuesScrollView.height)];
    [self updateConvertedValues];
    [self checkStatus];
}

- (void)updateConvertedValues {
    if (!amount || amount.doubleValue == 0) {
        // Keyboard is inactive
        return;
    }
    sendToLocationLabel.hidden = piastresButton.selected;
    
    NSNumber *base = CurrentUser.selectedVenue.baseMinutesFactor;
    double baseDouble = base.doubleValue;
    double baseMinutesDouble;
    if (baseDouble != 0) {
        baseMinutesDouble = amount.doubleValue / baseDouble;
    } else {
        baseMinutesDouble = amount.doubleValue;
    }
    
    int piastresValue = MAX(1, floor(baseMinutesDouble / selectedVenue.piastresFactor.doubleValue));
    piastresLabel.text = [NSString localizedStringWithFormat:NSLocalizedString(@"(equal to %d piastres)",@"Do not translate"), piastresValue];
    [piastresLabel sizeToFit];
    [piastresLabel centerHorizontallyInSuperView];
    piastresLabel.hidden = amount == 0;
    
    if(piastresButton.selected) {
        sendToLabel.text = [NSString localizedStringWithFormat:NSLocalizedString(@"send %d piastres to",@"Do not translate"), amount.intValue];
    } else {
        double minutes = round(baseMinutesDouble * selectedVenue.baseMinutesFactor.doubleValue);
        int minutesInt = (int)minutes;
        sendToLabel.text = [NSString localizedStringWithFormat:NSLocalizedString(@"send %d minutes",@"Do not translate"), minutesInt];
    }
    
    [sendToLabel centerHorizontallyInSuperView];
    sendToLabel.top = piastresButton.selected ? kMediumPadding : kSmallPadding;
    
    [sendToLocationLabel setText:[NSString stringWithFormat:NSLocalizedString(@"AT ZIFERBLAT %@ TO",nil), selectedVenue.name.uppercaseString]];

    [sendToLocationLabel centerHorizontallyInSuperView];
    sendToLocationLabel.top = sendToLabel.bottom;
}

- (void)checkStatus {
    doneButton.enabled = amount > 0 && selectedUser != nil;
}

#pragma mark -
#pragma mark ZFKeyboard Delegate Methods

- (BOOL)keyboardCanInsertAmount:(NSNumber *)anAmount {
    switch (type) {
        case ZFShareType_Piastres:
            return anAmount.doubleValue <= CurrentUser.accountBalance.piastres.doubleValue;
            
        case ZFShareType_Time:
            return anAmount.doubleValue <= round(CurrentUser.accountBalance.baseMinutes.doubleValue / CurrentUser.selectedVenue.baseMinutesFactor.doubleValue);
            
        default:
            return NO;
    }
}

- (void)keyboardAmountUpdated:(NSNumber *)aAmount {
    amount = aAmount;
    [self updateAmount];
}

#pragma mark -
#pragma mark Buttons actions

- (void)backButtonTapped {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)doneButtonTapped:(UIButton *)aButton {
    Show_Hud(NSLocalizedString(@"Sharing...",nil));
    
    double shareAmount = amount.doubleValue;
    if(timeButton.selected) {
        shareAmount /= CurrentUser.selectedVenue.baseMinutesFactor.doubleValue;
    }
    
    [ZFSharePiastresOrTimeRequest requestWitAmount:[NSNumber numberWithDouble:shareAmount]
                                         shareType:type
                                     otherMemberId:selectedUser.uuid
                                           venueId:selectedVenue.uuid
                                        completion:^(LORESTRequest *aRequest) {
                                            if(aRequest.resultStatus == LORESTRequestResultStatusSucceeded) {
                                                [SVProgressHUD showSuccessWithStatus:[NSString stringWithFormat:NSLocalizedString(@"%@ shared",nil), (type == ZFShareType_Piastres ? @"Piastres" : @"Time")]];
                                                CurrentUser.accountBalance = aRequest.result;
                                                Notification_Post(kNotification_AccountBalanceRefreshed);
                                                amount = 0;
                                                [keyboard reset];
                                                [self updateAmount];
                                                [self updateBalance];
                                            } else {
                                                Show_ConnectionError
                                            }
                                        }];
}

- (void)piastresButtonPressed:(UIButton *)sender {
    if(sender.selected) {
        return;
    }
    
    type = ZFShareType_Piastres;
    timeButton.selected = FALSE;
    sender.selected = TRUE;
    amount = 0;
    [keyboard reset];
    [self updateAmount];
    [self updateLayout:TRUE];
}

- (void)timeButtonPressed:(UIButton *)sender {
    if(sender.selected) {
        return;
    }
    
    type = ZFShareType_Time;
    piastresButton.selected = FALSE;
    sender.selected = TRUE;
    amount = 0;
    [keyboard reset];
    [self updateAmount];
    [self updateLayout:TRUE];
}


#pragma mark -
#pragma mark UICollectionView datasource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return friendsList.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    ZFProfileImageCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:[ZFProfileImageCollectionViewCell reuseIdentifier]
                                                                                       forIndexPath:indexPath];
    [cell setupWithOccupant:[friendsList objectAtIndex:indexPath.row] isSelectable:YES];
    
    return cell;
}

#pragma mark -
#pragma mark UICollectionView delegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    selectedUser = [friendsList objectAtIndex:indexPath.row];
    [self updateSelectedUser];
}

#pragma mark -
#pragma mark Scrollview Delegate Methods

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    if(!decelerate) {
        [self scrollViewDidEndDecelerating:scrollView];
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    if([scrollView isEqual:kScrollView]) {
        return;
    }
    
    NSUInteger index = scrollView.contentOffset.x / scrollView.width;
    selectedVenue = [venues objectAtIndex:index];
    [self updateConvertedValues];
}

@end
