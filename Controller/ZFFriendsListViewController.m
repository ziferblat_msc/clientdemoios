//
//  ZFFriendsListViewController.m
//  Ziferblat
//
//  Created by Terry Michel on 08/04/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//


#import "ZFFriendsListViewController.h"
#import "ZFFriendListCollectionViewCell.h"
#import "ZFGetMemberFriendsListRequest.h"
#import "ZFConversationDetailViewController.h"
#import "ZFOtherProfileViewController.h"
#import "ZFGetMemberVenueDetailsRequest.h"
#import "ZFFriendsSearchTableViewCell.h"
#import "LOSeparatorImageView.h"
#import "ZFAddFriendRequest.h"
#import "ZFGetFriendRequestList.h"
#import "ZFUnfriendRequest.h"
#import "ZFGetMemberFriendsListRequest.h"
#import "ZFGetMemberVenueDetailsRequest.h"
#import "ZFVenue.h"
#import "ZFFilterTextButton.h"
#import "ZFFriendRequestView.h"
#import "ZFBaseView.h"
#import "ZFBackButton.h"

#define kHeaderLabelFrame   (notIPad ? CGRectMake(50, 40, 222, 28):CGRectMake(120, 80, 533, 67))

typedef NS_ENUM(NSUInteger, ZFFriendsListViewController_TableViewType) {
    ZFFriendViewController_TableViewType_Request,
    ZFFriendViewController_TableViewType_Search,
};

@interface ZFFriendsListViewController()<UICollectionViewDataSource,
UICollectionViewDelegateFlowLayout, UITableViewDataSource, UITableViewDelegate, ZFFriendRequestTableViewCellDelegate, ZFFriendSearchTableViewCellDelegate, ZFSearchViewDelegate>

@end

@implementation ZFFriendsListViewController {
    NSArray *friendsList;
    NSArray *searchFriendsList;
    NSIndexPath *selectedIndexPath;
    NSMutableArray *friendRequestArray;
    ZFFilterTextButton *sortAZButton;
    ZFFilterTextButton * sortRatingButton;
    ZFFriendRequestView *friendRequestView;
    ZFSearchView *searchView;
    UICollectionView *friendsCollectionView;
    UILabel *numberOfFriendRequests;
    UIView *overlayView;
    NSString *searchText;
}

#pragma mark -
#pragma mark Lifecycle

- (instancetype)initWithFriendsList:(NSArray *)friends {
    self = [super init];
    if(self) {
        friendsList = [self sortedAZFriends:friends];
        searchFriendsList = friendsList;
    }
    return self;
}

- (void)loadView {
    ZFBaseView *aView = [[ZFBaseView alloc] init];
    self.view = aView;
    [kScrollView setScrollEnabled:NO];
    
    UIImageView *topImageView = [UIImageView imageViewWithImageNamed:RESOURCE_FRIENDS_LIST_TOP
                                                              origin:CGPointMake(0, 0)
                                                           addToView:kScrollView];
    
    ZFBackButton *backButton = [ZFBackButton backButtonWithTitle:NSLocalizedString(@"BACK TO ROOM",nil)];
    [self.view addSubview:backButton];
    TARGET_TOUCH_UP(backButton, @selector(backButtonTapped));
    topImageView.top = backButton.bottom - 1;
    
    UIImageView *middleImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0,
                                                                                 topImageView.bottom,
                                                                                 kScrollView.width,
                                                                                 kScrollView.height-topImageView.bottom)];
    [kScrollView addSubview:middleImageView];
    middleImageView.image = [UIImage imageNamed:RESOURCE_FRIENDS_LIST_MIDDLE];
    middleImageView.contentMode = UIViewContentModeScaleToFill;
    
    UILabel *headerLabel = [[UILabel alloc]initWithFrame:kHeaderLabelFrame];
    headerLabel.text = NSLocalizedString(@"FRIENDS",nil);
    headerLabel.textAlignment = NSTextAlignmentCenter;
    headerLabel.font = FontRockWell(notIPad ? 24:58);
    headerLabel.adjustsFontSizeToFitWidth = YES;
    headerLabel.minimumScaleFactor = 0.5;
    [kScrollView addSubview:headerLabel];
    
    UIImageView *friendRequestImageView = [[UIImageView alloc]initWithFrame:(notIPad ? CGRectMake(0, 101, 61, 21):CGRectMake(0, 222, 146, 50))];
    friendRequestImageView.image  = [UIImage imageNamed:RESOURCE_FRIEND_REQUEST_TAG];
    [kScrollView addSubview:friendRequestImageView];
    
    UIView *sortButtonsContainer = [UIView viewWithColor:Colour_Clear frame:CGRectZero addToView:self.view];
    [kScrollView addSubview:sortButtonsContainer];
    sortButtonsContainer.top = (notIPad ? 66:158);
    
    sortAZButton = [ZFFilterTextButton buttonWithType:ZFFilterButtonType_White fontSize:(notIPad ? 14:34) text:NSLocalizedString(@"a-z, ",nil) andComa:NO];
    [sortButtonsContainer addSubview:sortAZButton];
    [sortAZButton setSelected:YES];
    TARGET_TOUCH_UP(sortAZButton, @selector(sortAZButtonTapped:));
    
    sortRatingButton = [ZFFilterTextButton buttonWithType:ZFFilterButtonType_White fontSize:(notIPad ? 14:34) text:NSLocalizedString(@"rating",nil) andComa:NO];
    [sortButtonsContainer addSubview:sortRatingButton];
    sortRatingButton.left = sortAZButton.right;
    [sortRatingButton setCenterY:sortAZButton.center.y];
    TARGET_TOUCH_UP(sortRatingButton, @selector(sortRatingButtonTapped:));
    
    sortButtonsContainer.width = sortRatingButton.right;
    sortButtonsContainer.height = sortRatingButton.height;
    [sortButtonsContainer centerHorizontallyInSuperView];
    
    UICollectionViewFlowLayout *collectionViewFlowLayout = [[UICollectionViewFlowLayout alloc] init];
    collectionViewFlowLayout.minimumLineSpacing = 0;
    collectionViewFlowLayout.minimumInteritemSpacing = 0;
    collectionViewFlowLayout.headerReferenceSize = CGSizeMake(self.view.width-kSmallPadding*2, ZFFriendsListCellSize.height);
    CGFloat rightOffset = 9;
    friendsCollectionView = [[UICollectionView alloc]
                             initWithFrame:CGRectMake(0,
                                                      middleImageView.top + (notIPad ? kPadding:18) * 4.2,
                                                      ZFFriendsListCellSize.width*3+rightOffset,
                                                      kScrollView.height-middleImageView.top-kPadding*2)
                             collectionViewLayout:collectionViewFlowLayout];
    friendsCollectionView.backgroundColor = Colour_Clear;
    friendsCollectionView.contentInset = UIEdgeInsetsMake(0, 0, 0, rightOffset);
    [kScrollView addSubview:friendsCollectionView];
    [friendsCollectionView centerHorizontallyInSuperView];
    friendsCollectionView.left = friendsCollectionView.left + rightOffset/2;
    [friendsCollectionView registerClass:[ZFFriendListCollectionViewCell class] forCellWithReuseIdentifier:[ZFFriendListCollectionViewCell reuseIdentifier]];
    friendsCollectionView.delegate = self;
    friendsCollectionView.dataSource = self;
    
    numberOfFriendRequests = [[UILabel alloc]initWithFrame:(notIPad ? CGRectMake(25, 2, 25, 16):CGRectMake(60, 5, 60, 38))];
    numberOfFriendRequests.textColor = Colour_White;
    numberOfFriendRequests.font = FontRockWell(notIPad ? 11:26);
    numberOfFriendRequests.textAlignment = NSTextAlignmentLeft;
    [friendRequestImageView addSubview:numberOfFriendRequests];
    
    UIButton *friendRequestButton = [[UIButton alloc]initWithFrame:(notIPad ? CGRectMake(0, 95, 63, 37):CGRectMake(0, 218, 151, 89))];
    [kScrollView addSubview:friendRequestButton];
    TARGET_TOUCH_UP(friendRequestButton, @selector(friendRequestButtonTapped:));
    
    searchView = [ZFSearchView searchViewOfType:ZFSearchViewType_Friends delegate:self tableViewTag:ZFFriendViewController_TableViewType_Search];
    searchView.top = (notIPad ? 101:222);
    searchView.left = (notIPad ? 75:180);
    [kScrollView addSubview:searchView];
    
    friendRequestView = [[ZFFriendRequestView alloc] init];
    [kScrollView addSubview:friendRequestView];
    friendRequestView.friendRequestTableView.tag = ZFFriendViewController_TableViewType_Request;
    friendRequestView.friendRequestTableView.delegate = self;
    friendRequestView.friendRequestTableView.dataSource = self;
    TARGET_TOUCH_UP(friendRequestView.closeButton, @selector(friendRequestCloseButtonTapped));
    
    // Easy way to dismiss search view
    UITapGestureRecognizer *tapGestureRecognized = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapGestureRecognizer:)];
    tapGestureRecognized.cancelsTouchesInView = NO;
    [kScrollView addGestureRecognizer:tapGestureRecognized];

}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    Notification_Observe(kNotification_FriendsListUpdated, friendsListUpdated:)
    Notification_Observe(kNotification_FriendsRequestListRefreshed, friendsRequestsListRefreshed)
    Notification_Observe(kNotification_FriendsLoggedInUserRefreshed, friendsListLoggedInUserRefreshed)
    
    for (ZFUser *friend in friendsList) {
        [self makeVenueDetailsRequestWithUser:friend];
    }
    [self makeFriendRequestListRequest];
}

- (void)dealloc {
    Notification_RemoveObserver;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [NetworkService pollForType:ZFPollingRequestType_PendingFriendsRequest|ZFPollingRequestType_FriendsListLoggedInUser memberId:nil venueId:nil summary:YES];
}


- (void)handleTapGestureRecognizer:(UITapGestureRecognizer *)gestureRecognizer {
    
    [self hideSearch];
}

- (void)hideSearch {
    [searchView compact];
}

#pragma mark -
#pragma mark Buttons actions

- (void)friendRequestCloseButtonTapped
{
    [UIView animateWithDuration:0.2 animations:^{
        [friendRequestView compact];
    } completion:^ (BOOL finished) {
        if (finished) {
            overlayView.alpha = 0;
            [overlayView removeFromSuperview];
        }
    }];
}

- (void)backButtonTapped {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)sortAZButtonTapped:(UIControl *)aControl {
    if(!aControl.isSelected) {
        [aControl setSelected:YES];
        [sortRatingButton setSelected:NO];
        friendsList = [self sortedAZFriends:friendsList];
        selectedIndexPath = nil;
        [friendsCollectionView reloadSections:[NSIndexSet indexSetWithIndex:0]];
    }
}

- (void)sortRatingButtonTapped:(UIControl *)aControl {
    if(!aControl.isSelected) {
        [aControl setSelected:YES];
        [sortAZButton setSelected:NO];
        friendsList = [self sortedRatingFriends:friendsList];
        selectedIndexPath = nil;
        [friendsCollectionView reloadSections:[NSIndexSet indexSetWithIndex:0]];
    }
}

- (void)friendRequestButtonTapped: (UIButton *)aButton {
    [self displayFriendRequests];
}

- (void)profileButtonTapped:(UIButton *)aButton {
    ZFUser *aUser = [friendsList objectAtIndex:aButton.tag];
    [SVProgressHUD showWithStatus:NSLocalizedString(@"Loading member...",nil) maskType:SVProgressHUDMaskTypeGradient];
    
    [ZFGetMemberFriendsListRequest requestWithMemberId:aUser.uuid andVenueId:CurrentUser.selectedVenue.uuid completion:^(LORESTRequest *aRequest) {
        if(aRequest.resultStatus == LORESTRequestResultStatusSucceeded) {
            aUser.friendsList = aRequest.result;
            aUser.friendsLoaded = TRUE;
            [self checkLoaded:aUser];
        } else {
            Show_ConnectionError
        }
    }];
    
    [ZFGetMemberVenueDetailsRequest requestWithMemberId:aUser.uuid venueId:CurrentUser.selectedVenue.uuid completion:^(LORESTRequest *aRequest) {
        if(aRequest.resultStatus == LORESTRequestResultStatusSucceeded) {
            aUser.venueDetails = aRequest.result;
            [self checkLoaded:aUser];
        } else {
            Show_ConnectionError
        }
    }];
}

- (void)conversationButtonTapped:(UIButton *)aButton {
    ZFUser *aUser = [friendsList objectAtIndex:aButton.tag];
    ZFConversationDetailViewController *conversationViewController = [[ZFConversationDetailViewController alloc] initWithUser:aUser];
    [self presentViewController:conversationViewController animated:YES completion:nil];
}

- (void)checkLoaded:(ZFUser *)aUser {
    if((aUser.venueDetails == nil) || (!aUser.friendsLoaded)) {
        return;
    }
    
    Hide_HudAndPerformBlock(^{
        [[ZFPresenter sharedInstanceWithNavigationController:nil] presentProfileForMemberId:aUser.uuid
                                                                          andOptionalMember:aUser];
    });
}

#pragma mark -
#pragma mark UICollectionView datasource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return friendsList.count;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    ZFFriendListCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:[ZFFriendListCollectionViewCell reuseIdentifier]
                                                                                     forIndexPath:indexPath];
    ZFUser *friend = [friendsList objectAtIndex:indexPath.row];
    
    [cell setupWithUser:friend atIndex:indexPath.row showGreenDot:((CurrentUser.currentVenueId.integerValue > 0) &&
                                                                    (friend.currentVenueId.integerValue == CurrentUser.currentVenueId.integerValue))];
    
    [cell.profileButton addTarget:self action:@selector(profileButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    [cell.conversationButton addTarget:self action:@selector(conversationButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView
           viewForSupplementaryElementOfKind:(NSString *)kind
                                 atIndexPath:(NSIndexPath *)indexPath {
    return nil;
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
referenceSizeForHeaderInSection:(NSInteger)section {
    return CGSizeZero;
}

#pragma mark -
#pragma mark UICollectionViewDelegateFlowLayout

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if([selectedIndexPath isEqualToIndexPath:indexPath]) {
        [collectionView deselectItemAtIndexPath:indexPath animated:NO];
        selectedIndexPath = nil;
    } else {
        selectedIndexPath = indexPath;
    }
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(ZFFriendsListCellSize.width, ZFFriendsListCellSize.height);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView
                        layout:(UICollectionViewLayout *)collectionViewLayout
        insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsZero;
}

#pragma mark -
#pragma mark UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    switch (tableView.tag)
    {
        case ZFFriendViewController_TableViewType_Request:
            
            
            return friendRequestArray.count;
            
        case ZFFriendViewController_TableViewType_Search:
            return searchFriendsList.count;
            
        default:return 0;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (tableView.tag) {
        case ZFFriendViewController_TableViewType_Request:
            return (notIPad ? 32:77);
            
        case ZFFriendViewController_TableViewType_Search:
            return [ZFFriendsSearchTableViewCell height];
            
        default:
            return 0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (tableView.tag) {
        case ZFFriendViewController_TableViewType_Search:
        {
            NSString *identifier = NSStringFromClass([ZFFriendsSearchTableViewCell class]);
            ZFFriendsSearchTableViewCell *cell = (ZFFriendsSearchTableViewCell *)[tableView dequeueReusableCellWithIdentifier:identifier];
            
            if (!cell) {
                cell = [[ZFFriendsSearchTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
                cell.delegate = self;
            }
            ZFUser *friend = [searchFriendsList objectAtIndex:indexPath.row];
#warning CLARIFY BEHAVIOUR DISPLAY GREEN OR YELLOW DOT
            [cell setupWithUser:friend isLast:(indexPath.row == searchFriendsList.count-1) isCheckIn:(friend.currentVenueId.integerValue == CurrentUser.currentVenueId.integerValue) isInSameZiferblat:NO];
            return cell;
        }
            
        case ZFFriendViewController_TableViewType_Request:
        {
            NSString *identifier = NSStringFromClass([ZFFriendRequestTableViewCell class]);
            ZFFriendRequestTableViewCell *cell = (ZFFriendRequestTableViewCell *)[tableView dequeueReusableCellWithIdentifier:identifier];
            
            if (!cell) {
                cell = [[ZFFriendRequestTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
                cell.delegate = self;
            }
            ZFUser *selectedUser = [friendRequestArray objectAtIndex:indexPath.row];
            [cell configureWithUser:selectedUser];
            
            return cell;
        }
            
        default:
            return nil;
    }
}

#pragma mark UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == searchView.friendsSearchTableView)
    {
        ZFUser *user = searchFriendsList[indexPath.row];
        
        ZFOtherProfileViewController *controller = [[ZFOtherProfileViewController alloc] initWithUser:user];
        controller.canBeDismissed = YES;
        [self presentViewController:controller animated:YES completion:nil];
    }
}


#pragma mark -
#pragma mark Notifications

- (void)friendsListUpdated:(NSNotification *)sender {
    ZFUser *sourceUser = [sender.userInfo objectForKey:kNotificationKey_FriendSource];
    ZFUser *destinationUser = [sender.userInfo objectForKey:kNotificationKey_FriendDestination];
    BOOL isRemoved = ((NSNumber *)[sender.userInfo objectForKey:kNotificationKey_FriendRemoved]).boolValue;
    
    if(([CurrentUser isEqual:sourceUser]) || ([CurrentUser isEqual:destinationUser])) {
        NSMutableArray *friendsUpdated = [NSMutableArray arrayWithArray:friendsList];
        if(isRemoved) {
            [friendsUpdated removeObject:destinationUser];
        } else {
            [friendsUpdated addObject:destinationUser];
        }
        friendsList = friendsUpdated;
        searchFriendsList = friendsList;
        
        selectedIndexPath = nil;
        [friendsCollectionView reloadData];
    }
}

- (void)friendsRequestsListRefreshed
{
    [self refreshFriendRequestList:NetworkService.friendsRequestList];
}

- (void)friendsListLoggedInUserRefreshed {
    if(friendsList.count != CurrentUser.friendsList.count) {
        friendsList = (sortAZButton.isSelected ? [self sortedAZFriends:CurrentUser.friendsList] : [self sortedRatingFriends:CurrentUser.friendsList]);
        selectedIndexPath = nil;
        [friendsCollectionView reloadSections:[NSIndexSet indexSetWithIndex:0]];
    }
}

#pragma mark -
#pragma mark ZFFriendsSearchTableViewCell delegate

- (void)friendSearchCellDidRequestToPresentProfile:(ZFUser *)aUser {
}

#pragma mark -
#pragma mark Friend Request Cell Delegate methods

- (void)userDidAcceptRequest:(ZFFriendRequestTableViewCell *)aCell {
    NSUInteger index = [friendRequestView.friendRequestTableView indexPathForCell:aCell].row;
    ZFUser *selectedUser = [friendRequestArray objectAtIndex:index];
    
    Show_Hud(NSLocalizedString(@"Accepting friend...",nil));
    [ZFAddFriendRequest requestWithId:selectedUser.uuid completion:^(LORESTRequest *aRequest)
    {
        if(aRequest.resultStatus == LORESTRequestResultStatusSucceeded)
        {
            NSString *status = aRequest.result;
            Hide_HudAndPerformBlock(^{
                if([status isEqualToString:@"FRIENDS"]) {
                    [SVProgressHUD showSuccessWithStatus:NSLocalizedString(@"Friend added",nil)];
                    [CurrentUser addFriend:selectedUser];
                    [self removeTableViewRowWithIndex:index];
                    
                    NetworkService.unreadFriendsCount -= 1;
                    Notification_Post(kNotification_FriendsRequestListItemRemoved);
                }
            })
        }
        else
        {
            Show_ConnectionError
        }
    }];
}

- (void)userDidDeclineRequest:(ZFFriendRequestTableViewCell *)aCell
{
    NSUInteger index = [friendRequestView.friendRequestTableView indexPathForCell:aCell].row;
    ZFUser *selectedUser = [friendRequestArray objectAtIndex:index];

    [ZFUnfriendRequest requestWithId:selectedUser.uuid completion:^(LORESTRequest *aRequest)
    {
        if(aRequest.resultStatus == LORESTRequestResultStatusSucceeded)
        {
            [CurrentUser removeFriend:selectedUser];
            aCell.acceptButton.enabled = NO;
            [self removeTableViewRowWithIndex:index];
            
            NetworkService.unreadFriendsCount -= 1;
            Notification_Post(kNotification_FriendsRequestListItemRemoved);
        }
    }];
}

- (void)removeTableViewRowWithIndex:(NSUInteger)index
{
    NSMutableArray *arrayOfDeletes = [NSMutableArray arrayWithCapacity:1];
    NSIndexPath *path = [NSIndexPath indexPathForRow:index inSection:0];
    [arrayOfDeletes addObject:path];
    [friendRequestArray removeObjectAtIndex:index];

    [friendRequestView.friendRequestTableView deleteRowsAtIndexPaths:arrayOfDeletes withRowAnimation:UITableViewRowAnimationNone];

    [self updateFriendRequestsNumberLabel];

    if(friendRequestArray.count == 0) //Compact friendRequestView if number of request is 0
    {
        [self friendRequestCloseButtonTapped];
    }
}

#pragma mark
#pragma mark Request Methods

- (void) makeFriendRequestListRequest {
    [ZFGetFriendRequestList requestWithCompletion:^(LORESTRequest *aRequest) {
        if (aRequest.resultStatus == LORESTRequestResultStatusSucceeded) {
            [self refreshFriendRequestList:aRequest.result];
            
        } else {
            [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"Could not fetch friend requests",nil)];
        }
    }];
}

- (void)makeVenueDetailsRequestWithUser:(ZFUser *)aUser {
#warning SPECIFY BEHAVIOUR WHEN NO CURRENT VENUE ID SET
    if(aUser.currentVenueId.integerValue != 0) {
        [ZFGetMemberVenueDetailsRequest requestWithMemberId:aUser.uuid
                                                    venueId:aUser.currentVenueId
                                                 completion:^(LORESTRequest *aRequest) {
                                                     if(aRequest.resultStatus == LORESTRequestResultStatusSucceeded) {
                                                         Hide_HudAndPerformBlock(^{
                                                             aUser.venueDetails = aRequest.result;
                                                         })
                                                     } else {
                                                         Show_ConnectionError
                                                     }
                                                 }];
    }
}

#pragma mark -
#pragma mark ZFSearchView delegate

- (void)searchViewWillExpand {
    overlayView = [UIView viewWithColor:Colour_Black frame:self.view.frame addToView:nil];
    overlayView.alpha = 0.0;
    [kScrollView addSubview:overlayView];
    [searchView bringToFront];
    [UIView animateWithDuration:0.3 animations:^{
        overlayView.alpha = 0.5;
    } completion:nil];
}

- (void)searchViewWillCompact
{
    [UIView animateWithDuration:0.3 animations:^{
        overlayView.alpha = 0.0;
    } completion:^(BOOL finished) {
        [overlayView removeFromSuperview];
    }];
}

- (void)searchViewDidSearchForText:(NSString *)aText {
    if(aText.isValidString) {
        searchFriendsList = [friendsList filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"fullName contains[c] %@", aText]];
    } else {
        searchFriendsList = friendsList;
    }
    [searchView reloadData];
}

#pragma mark -
#pragma mark Utils

- (NSArray *)sortedAZFriends:(NSArray *)aFriendsList {
    return [aFriendsList sortedArrayUsingComparator:^NSComparisonResult(ZFUser *user1, ZFUser *user2) {
        return [user1.fullName compare:user2.fullName];
    }];
}

- (NSArray *)sortedRatingFriends:(NSArray *)aFriendsList
{
    return [aFriendsList sortedArrayUsingComparator:^NSComparisonResult(ZFUser *user1, ZFUser *user2) {
        NSNumber *user1Rating = user1.venueDetails.experiencePoints;
        NSNumber *user2Rating = user2.venueDetails.experiencePoints;
        
        if (user1Rating == nil && user2Rating == nil) {
            return [user1.fullName compare:user2.fullName];
        } else if (user1Rating == nil) {
            return NSOrderedDescending;
        } else if (user2Rating == nil) {
            return NSOrderedAscending;
        } else {
            return ([user1.venueDetails.experiencePoints compare:user2.venueDetails.experiencePoints] * (-1));
        }
    }];
}

- (void)refreshFriendRequestList:(NSArray *)friendsRequest
{
    friendRequestArray = [NSMutableArray arrayWithArray:friendsRequest];

    [self updateFriendRequestsNumberLabel];
    
    [friendRequestView.friendRequestTableView reloadData];
}

- (void)updateFriendRequestsNumberLabel
{
    if (friendRequestArray.count == 0)
    {
        numberOfFriendRequests.text = @"0";
    }
    else
    {
        numberOfFriendRequests.text = [NSString stringWithFormat:@"+%lu", (unsigned long)friendRequestArray.count];
    }
}

- (void)displayFriendRequests {
    overlayView = [UIView viewWithColor:Colour_Black frame:self.view.frame addToView:nil];
    overlayView.alpha = 0.0;
    [self.view addSubview:overlayView];
    [self.view addSubview:friendRequestView];
    
    [UIView animateWithDuration:0.2 animations:^{
        overlayView.alpha = 0.5;
    }];
    
    [friendRequestView expand];
}

@end