//
//  ZFNewsViewController.h
//  Ziferblat
//
//  Created by Terry Michel on 08/04/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "ZFBaseView.h"
#import "ZFNewsView.h"
#import "ZFViewController.h"

@interface ZFNewsViewController : ZFViewController<UITableViewDataSource, UITableViewDelegate, UIScrollViewDelegate> {
    ZFNewsView *contentView;
}

- (ZFNewsViewController *)init __attribute__((unavailable("init not available")));
- (ZFNewsViewController *)initWithVenueId:(NSNumber *)aVenueId;

@end
