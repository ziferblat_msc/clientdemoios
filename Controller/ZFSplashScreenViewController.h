//
//  ZFSplashScreenViewController.h
//  Ziferblat
//
//  Created by Terry Michel on 08/04/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "ZFSplashScreenView.h"
#import "ZFTutorialViewController.h"

@interface ZFSplashScreenViewController : UIViewController <ZFTutorialViewControllerDelegate>{
    ZFSplashScreenView *contentView;
}

@end
