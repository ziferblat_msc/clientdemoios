//
//  ZFDiscussionCommentsViewController.h
//  Ziferblat
//
//  Created by Terry Michel on 08/04/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "ZFConversationDetailCell.h"
#import "ZFConversationDetailRightCell.h"
#import "ZFMessage.h"

@interface ZFConversationDetailViewController : UIViewController<ZFConversationDetailCellDelegate>

- (instancetype)initWithUser:(ZFUser *)aUser;
- (instancetype)initWithUser:(ZFUser *)aUser backMessage:(NSString *)backMessage;

@end
