//
//  ZFSettingsViewController.m
//  Ziferblat
//
//  Created by Terry Michel on 08/04/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "ZFSettingsViewController.h"
#import "ZFUpdateMemberRequest.h"
#import "LORESTRequestService.h"
#import "ZFBackButton.h"
#import "ZFNetworkService.h"
#import "ZFLocalSettingsManager.h"
#import "ZFGuestService.h"

#define kHeaderLabelFrame                   (notIPad ? CGRectMake(38, 2, 204, 36):CGRectMake(91, 5, 490, 86))
#define kShowThatImAtZiferblatTopMargin     (notIPad ? 143:293)
#define kSignoutLabelFrame                  (notIPad ? CGRectMake(200, 513, 75, 26):CGRectMake(480, 1216, 180, 62))
#define kSignoutButtonFrame                 (notIPad ? CGRectMake(183, 495, 109, 73):CGRectMake(439, 1178, 262, 175))

typedef NS_ENUM(NSUInteger, ZFSettingsView_ToogleType) {
    ZFSettingsView_ToogleType_ShowThatImAtZiferblat,
    ZFSettingsView_ToogleType_ShowOnlyMyName
};

@interface ZFSettingsViewController()

@property (strong, nonatomic) ZFLocalSettingsManager *localSettingsManager;
@end

@implementation ZFSettingsViewController

#pragma mark - 
#pragma mark Instance

- (instancetype)initWithUser:(ZFUser *)aUser {
    self = [super init];
    if(self) {
        userUpdated = aUser.copy;
    }
    return self;
}

#pragma mark - 
#pragma mark Lifecycle

- (void)loadView
{
	
    ZFBaseView *aView = [[ZFBaseView alloc] init];
    self.view = aView;
    
    ZFBackButton *backButton = [ZFBackButton backButtonWithTitle:NSLocalizedString(@"BACK TO PROFILE", nil)];
    [self.view addSubview:backButton];
    TARGET_TOUCH_UP(backButton, @selector(backButtonPressed))
    
    UIImageView *redBannerImageView = [UIImageView imageViewWithImageNamed:@"SettingsRedBanner"];
    [kScrollView addSubview:redBannerImageView];
    [redBannerImageView centerHorizontallyInSuperView];
    redBannerImageView.top = backButton.bottom + kMediumPadding;
    
    UILabel *headerLabel = [[UILabel alloc]initWithFrame:kHeaderLabelFrame];
    headerLabel.text = NSLocalizedString(@"Settings",nil);
    headerLabel.textAlignment = NSTextAlignmentCenter;
    headerLabel.textColor = Colour_White;
    headerLabel.adjustsFontSizeToFitWidth = YES;
    headerLabel.font = FontRockWell((notIPad ? 26:62));
    [redBannerImageView addSubview:headerLabel];
    
    UIButton *showThatImAtZiferblatToggleButton = [UIButton toggleButtonWithFrame:CGRectMake(kLargePadding + kPadding,
                                                                                   kShowThatImAtZiferblatTopMargin,
                                                                                   kToggleButtonSize.width,
                                                                                   kToggleButtonSize.height) addToView:kScrollView];
    showThatImAtZiferblatToggleButton.tag = ZFSettingsView_ToogleType_ShowThatImAtZiferblat;
    TARGET_TOUCH_UP(showThatImAtZiferblatToggleButton, @selector(toggleButtonPressed:))
    
    UILabel *showThatImAtZiferblatLabel = [UILabel labelWithText:NSLocalizedString(@"Show that I'm at Ziferblat to other people",nil)
                                                            font:FontHelvetica((notIPad ? 15:28))
                                                 colour:Colour_Black
                                          textAlignment:NSTextAlignmentLeft
                                          numberOfLines:2
                                                  frame:CGRectMake(showThatImAtZiferblatToggleButton.right+kLargePadding,
                                                                   showThatImAtZiferblatToggleButton.top - (notIPad ? 3:9),
                                                                   kScrollView.width-showThatImAtZiferblatToggleButton.right-kLargePadding*2, 0)
                                             resizeType:UILabelResizeType_sizeToWidth
                                              addToView:kScrollView];
    
    UIButton *showOnlyMyNameToggleButton = [UIButton toggleButtonWithFrame:CGRectMake(showThatImAtZiferblatToggleButton.left,
                                                                                      showThatImAtZiferblatLabel.bottom + (notIPad ? kLargePadding : 19),
                                                                            kToggleButtonSize.width,
                                                                            kToggleButtonSize.height) addToView:kScrollView];
    _localSettingsManager = [ZFLocalSettingsManager new];
    if ([_localSettingsManager userEnabledShowOnlyMyName]) {
        showOnlyMyNameToggleButton.selected = YES;
        // User prefer not to share profile image with other people
        // Save profile image on device so user can see it
        // ⚠️ Warning, this will not sync with other devices
        // You can remove this when server bug with show_name_only BOOL is fixed
    }
    
    showOnlyMyNameToggleButton.tag = ZFSettingsView_ToogleType_ShowOnlyMyName;
    TARGET_TOUCH_UP(showOnlyMyNameToggleButton, @selector(toggleButtonPressed:))

    UILabel *showOnlyMyNameLabel = [UILabel labelWithText:NSLocalizedString(@"Show only my name",nil)
                                            font:FontHelvetica((notIPad ? 15:28))
                                          colour:Colour_Black
                                   textAlignment:NSTextAlignmentLeft
                                   numberOfLines:2
                                           frame:CGRectMake(showOnlyMyNameToggleButton.right+kLargePadding,
                                                            showOnlyMyNameToggleButton.top,
                                                            kScrollView.width-showOnlyMyNameToggleButton.right-kLargePadding*2, 0)
                                      resizeType:UILabelResizeType_sizeToWidth
                                       addToView:kScrollView];
    [showOnlyMyNameLabel setCenterY:showOnlyMyNameToggleButton.center.y];
    
    UIImageView *bottomImageView = [UIImageView imageViewWithImageNamed:@"SettingsFooter"];
    [kScrollView addSubview:bottomImageView];
    bottomImageView.bottom = kScrollView.height;
    
    UIButton *signOutButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [signOutButton setFrame:kSignoutButtonFrame];
    [signOutButton setTitle:NSLocalizedString(@"Sign out",nil) forState:UIControlStateNormal];
    [signOutButton setTitleColor:Colour_White forState:UIControlStateNormal];
    [signOutButton.titleLabel setFont:FontRockWell((notIPad ? 16:38))];
    signOutButton.bottom = kScrollView.height - (notIPad  ? 5.0:12);
    [kScrollView addSubview:signOutButton];
    TARGET_TOUCH_UP(signOutButton, @selector(signOutButtonPressed))

    [showThatImAtZiferblatToggleButton setSelected:userUpdated.showInVenue.boolValue];
    [showOnlyMyNameToggleButton setSelected:userUpdated.showNameOnly.boolValue];
    
    if ([[ZFGuestService sharedInstance] isGuest]) {
        showOnlyMyNameLabel.hidden = YES;
        showThatImAtZiferblatLabel.hidden = YES;
        
        showThatImAtZiferblatToggleButton.hidden = YES;
        showOnlyMyNameToggleButton.userInteractionEnabled = NO;
        
        showOnlyMyNameToggleButton.hidden = YES;
        showOnlyMyNameToggleButton.userInteractionEnabled = NO;
    }
}

#pragma mark -
#pragma mark Buttons actions

- (void)backButtonPressed {
    if(!settingsHaveChanged)
    {
		[self dismissViewControllerAnimated:YES completion:^(){}];
    } else {
        [SVProgressHUD showWithStatus:NSLocalizedString(@"Updating user data...", nil)
                             maskType:SVProgressHUDMaskTypeGradient];
        [ZFUpdateMemberRequest requestWithUser:userUpdated completion:^(LORESTRequest *aRequest) {
            if(aRequest.resultStatus == LORESTRequestResultStatusSucceeded) {
                Hide_HudAndPerformBlock(^
                {
                    CurrentUser = userUpdated;
					[self dismissViewControllerAnimated:YES completion:^() {}];
                })
            } else {
               Show_ConnectionError
            }
        }];
    }
}

- (void)toggleButtonPressed:(UIButton *)aButton {
    settingsHaveChanged = YES;
    [aButton setSelected:!aButton.isSelected];
    switch (aButton.tag) {
        case ZFSettingsView_ToogleType_ShowThatImAtZiferblat: {
            userUpdated.showInVenue = @(aButton.isSelected);
            break;
        }
        case ZFSettingsView_ToogleType_ShowOnlyMyName: {
            BOOL hideAvatar = aButton.isSelected;
            if (hideAvatar) { // Order is important
                // Update local settings
                [self.localSettingsManager updateShowOnlyMyNameUserDefaultsTo:hideAvatar];
                self.localSettingsManager.userProfileImageURL = userUpdated.imageUrl;
                // Remove img
                userUpdated.imageUrl = nil;
            } else {
                // Bring back
                userUpdated.imageUrl = self.localSettingsManager.userProfileImageURL;
                // Update local settings
                [self.localSettingsManager updateShowOnlyMyNameUserDefaultsTo:hideAvatar];
            }
            
            // ⚠️ When server is fixed, just call this
            // userUpdated.showNameOnly = @(aButton.isSelected);
            break;
        }
        default: {
            break;
        }
    }
}

- (void)signOutButtonPressed
{
    [UserService logoutWithCompletion:^(BOOL success)
     {
        [LORESTRequestService logout];
     }];
	
//	[AppDelegate navigationController ]
	// TODO fix this
    [AppDelegate.navigationController dismissViewControllerAnimated:YES
                                                         completion:^{
        [AppDelegate.navigationController popToRootViewControllerAnimated:YES];
    }];
}

@end
