//
//  ZFTaskViewController.h
//  Ziferblat
//
//  Created by Terry Michel on 09/04/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "ZFTaskView.h"

@interface ZFTaskViewController : UIViewController {
    ZFTaskView *contentView;
}

@end
