//
//  ZFActivityDetailViewController.h
//  Ziferblat
//
//  Created by Peter Su on 28/10/2015.
//  Copyright © 2015 Locassa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZFViewController.h"

@class ZFActivity;

@interface ZFActivityDetailViewController : ZFViewController

@property (nonatomic, strong) ZFActivity *selectedActivity;
@property (nonatomic, strong) NSNumber *venueId;

@end
