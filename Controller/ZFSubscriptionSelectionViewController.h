//
//  ZFSubscriptionSelectionViewController.h
//  Ziferblat
//
//  Created by Terry Michel on 08/04/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

@class ZFBaseView;
@class ZFButton;

@interface ZFSubscriptionSelectionViewController : UIViewController<UITableViewDataSource, UITableViewDelegate> {
    ZFBaseView *contentView;
    ZFButton *workingButton;
    UILabel *descriptionLabel;
    UIImageView *descriptionSeparator;
    UILabel *selectZiferblatLabel;
    UILabel *piastresLabel;
    UIButton *chooseAllButton;
    UITableView *subscriptionTableView;
    ZFButton *getItButton;
    ZFSubscriptionType type;
    NSMutableArray *ziferblatsSelected;
    NSMutableArray *summaryArray;
    BOOL isSelected;
}

- (instancetype)initWithType:(ZFSubscriptionType)aType;

@end
