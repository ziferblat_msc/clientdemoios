//
//  ZFEventsViewController.h
//  Ziferblat
//
//  Created by Jose Fernandez on 25/08/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "ZFEventsView.h"
#import "ZFViewController.h"

@interface ZFEventsViewController : ZFViewController <UITableViewDataSource, UITableViewDelegate> {
    ZFEventsView *contentView;
}

- (instancetype)initWithSelectedVenue:(ZFVenue *)aVenue;

@end
