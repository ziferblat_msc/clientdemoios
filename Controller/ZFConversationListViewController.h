//
//  ZFDiscussionViewController.h
//  Ziferblat
//
//  Created by Terry Michel on 08/04/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "ZFConversationListCell.h"
#import "ZFViewController.h"

@interface ZFConversationListViewController : ZFViewController<ZFConversationListCellDelegate>

- (instancetype)initWithMessages:(NSArray *)messages
				  andFriendsList:(NSArray *)aFriendList;

@end
