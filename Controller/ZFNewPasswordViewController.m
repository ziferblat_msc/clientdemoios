//
//  ZFNewPasswordViewController.m
//  Ziferblat
//
//  Created by Jose Fernandez on 29/07/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "ZFNewPasswordViewController.h"
#import "ZFPasswordSuccessfullyViewController.h"
#import "ZFButton.h"
#import "ZFResetPasswordRequest.h"

#define kBackgroundKeyboardVisibleShift 200

typedef NS_ENUM(NSInteger, ZFSignUpView_FieldsTag) {
    ZFSignUpView_FieldsTag_SecretCode,
    ZFSignUpView_FieldsTag_Password,
    ZFSignUpView_FieldsTag_ConfirmPassword
};

typedef NS_ENUM(NSInteger, ZFSubinstructionsErrorMessages) {
    ZFSubinstructionsErrorMessages_Empty,
    ZFSubinstructionsErrorMessages_GoodPassword,
    ZFSubinstructionsErrorMessages_InvalidPassword,
    ZFSubinstructionsErrorMessages_PasswordDontMatch,
    ZFSubinstructionsErrorMessages_InvalidCode
};

@interface ZFNewPasswordViewController() <UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UILabel *topNavigationLabel;
@property (weak, nonatomic) IBOutlet UILabel *topFramedLabel;
@property (weak, nonatomic) IBOutlet UILabel *instructionsLabel;
@property (weak, nonatomic) IBOutlet UILabel *subInstructionsLabel;
@property (weak, nonatomic) IBOutlet UITextField *secretCodeTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UITextField *confirmPasswordTextField;
@property (weak, nonatomic) IBOutlet UIImageView *secretCodeCheckImageView;
@property (weak, nonatomic) IBOutlet UIImageView *passwordCheckImageView;
@property (weak, nonatomic) IBOutlet UIImageView *confirmPasswordCheckImageView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIButton *greyResetButton;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImageView;
@end

@implementation ZFNewPasswordViewController {
    ZFButton *redResetButton;
}

# pragma mark -
# pragma mark Life cycle

- (void)dealloc {
    Notification_RemoveObserver
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    Notification_Observe(UIKeyboardWillChangeFrameNotification, updateLayoutForKeyboard:)
    
    [self setupUI];
}

#pragma mark -
#pragma mark Setup UI

- (void)setupUI
{
    [self setupViewSizes];
    
    [self setupRedResetButton];
    [self setupGestureRecognizers];
    [self setupLocalizedStrings];
}

- (void)setupViewSizes
{
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        self.topNavigationLabel.font = FontRockWell(26);
        self.topFramedLabel.font = FontRockWell(30);
        self.instructionsLabel.font = FontRockWell(26);
        self.subInstructionsLabel.font = FontRockWell(26);
        
        self.topNavigationControlHeightConstraint.constant = 48;
        self.resetPasswordLabelWidthConstraint.constant = 300;
        self.resetPasswordLabelYConstraint.constant = 22;
        self.resetPasswordLabelHeightConstraint.constant = 60;
        
        self.instructionsLabelYConstraint.constant = 200;
        self.instructionsLabelWidthConstraint.constant = 400;
        self.subInstructionsLabelWidhtConstraint.constant = 400;
        
        self.secretCodeTextFieldYConstraint.constant = 75;
        self.secretCodeTextFieldWidthConstraint.constant = 365;
        self.secretCodeTextFieldHeightConstraint.constant = 68;
        self.secretCodeTextField.font = FontRockWell(24);
        
        self.passwordTextFieldYConstraint.constant = 33;
        self.passwordTextField.font = FontRockWell(24);
        
        self.confirmTextFieldYConstraint.constant = 33;
        self.confirmPasswordTextField.font = FontRockWell(24);
        
        self.buttonYConstraint.constant = 60;
    }
}

- (void)setupRedResetButton {
    redResetButton = [ZFButton buttonWithTitle:NSLocalizedString(@"RESET", nil) showMarks:NO];
    redResetButton.translatesAutoresizingMaskIntoConstraints = NO;
    redResetButton.alpha = 0.0;
    [self.scrollView addSubview:redResetButton];
    
    NSLayoutConstraint *resetButtonTopConstraint = [NSLayoutConstraint constraintWithItem:redResetButton
                                                                                attribute:NSLayoutAttributeTop
                                                                                relatedBy:NSLayoutRelationEqual
                                                                                   toItem:self.confirmPasswordTextField
                                                                                attribute:NSLayoutAttributeBottom
                                                                               multiplier:1.0
                                                                                 constant:(UI_USER_INTERFACE_IDIOM() !=UIUserInterfaceIdiomPad ? 34:60)];
    NSLayoutConstraint *resetButtonCenterXConstraint = [NSLayoutConstraint constraintWithItem:redResetButton
                                                                                    attribute:NSLayoutAttributeCenterX
                                                                                    relatedBy:NSLayoutRelationEqual
                                                                                       toItem:self.confirmPasswordTextField
                                                                                    attribute:NSLayoutAttributeCenterX
                                                                                   multiplier:1.0
                                                                                     constant:0];
    NSLayoutConstraint *resetButtonWidthConstraint = [NSLayoutConstraint constraintWithItem:redResetButton
                                                                                  attribute:NSLayoutAttributeWidth
                                                                                  relatedBy:NSLayoutRelationEqual
                                                                                     toItem:nil
                                                                                  attribute:NSLayoutAttributeWidth
                                                                                 multiplier:1.0
                                                                                   constant:95];
    NSLayoutConstraint *resetButtonHeightConstraint = [NSLayoutConstraint constraintWithItem:redResetButton
                                                                                   attribute:NSLayoutAttributeHeight
                                                                                   relatedBy:NSLayoutRelationEqual
                                                                                      toItem:nil
                                                                                   attribute:NSLayoutAttributeHeight
                                                                                  multiplier:1.0
                                                                                    constant:44];
    
    [self.scrollView addConstraints:@[resetButtonTopConstraint, resetButtonCenterXConstraint, resetButtonWidthConstraint, resetButtonHeightConstraint]];
    
    TARGET_TOUCH_UP(redResetButton, @selector(redResetButtonWasPressed:))
}

- (void)setupGestureRecognizers {
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [self.backgroundImageView addGestureRecognizer:tapGestureRecognizer];
    
    UIPanGestureRecognizer *panGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [self.backgroundImageView addGestureRecognizer:panGestureRecognizer];
}

- (void)setupLocalizedStrings {
    self.topNavigationLabel.text = NSLocalizedString(@"BACK TO LOG IN PAGE", nil);
    self.topFramedLabel.text = NSLocalizedString(@"RESET PASSWORD", nil);
    self.instructionsLabel.text = NSLocalizedString(@"Create new password.", nil);
    [self.greyResetButton setTitle:NSLocalizedString(@"RESET", nil) forState:UIControlStateNormal];
}

- (void)setupSecretCodeInvalid {
    [self hideErrorHintsForTextField:self.secretCodeTextField removeSubinstructions:YES];
    self.secretCodeCheckImageView.hidden = NO;
    self.secretCodeCheckImageView.highlighted = NO;
    [self setupSubinstructionsLabelWithError:ZFSubinstructionsErrorMessages_InvalidCode];
}

- (void)setupPasswordIsGood:(BOOL)goodPassword textfield:(UITextField *)textField {
    if (textField == self.passwordTextField) {
        self.passwordCheckImageView.hidden = NO;
        self.passwordCheckImageView.highlighted = goodPassword;
        
    } else if (textField == self.confirmPasswordTextField) {
        self.confirmPasswordCheckImageView.hidden = NO;
        self.confirmPasswordCheckImageView.highlighted = goodPassword;
    }
    
    if (textField == self.passwordTextField) {
        if (goodPassword) {
            [self setupSubinstructionsLabelWithError:ZFSubinstructionsErrorMessages_GoodPassword];
        } else {
            [self setupSubinstructionsLabelWithError:ZFSubinstructionsErrorMessages_InvalidPassword];
        }
    }
}

- (void)setupPasswordsDontMatch {
    [self hideAllErrorHints];
    self.confirmPasswordCheckImageView.hidden = NO;
    self.confirmPasswordCheckImageView.highlighted = NO;
    [self setupSubinstructionsLabelWithError:ZFSubinstructionsErrorMessages_PasswordDontMatch];
}

- (void)setupShowRedReset:(BOOL)showRedReset {
    [UIView animateWithDuration:0.2 animations:^{
        self.greyResetButton.alpha = showRedReset ? 0.0f : 1.0f;
        redResetButton.alpha = showRedReset ? 1.0f : 0.0f;
    }];
}

- (void)setupSubinstructionsLabelWithError:(ZFSubinstructionsErrorMessages)error {
    UIColor *errorColor = Colour_Hex(@"96928D");
    
    switch (error) {
        case ZFSubinstructionsErrorMessages_Empty:
            self.subInstructionsLabel.text = @"";
            break;
        case ZFSubinstructionsErrorMessages_GoodPassword:
            self.subInstructionsLabel.text = NSLocalizedString(@"Good password.", @"New Password View");
            errorColor = Colour_Hex(@"A5CE43");
            break;
        case ZFSubinstructionsErrorMessages_InvalidPassword:
            self.subInstructionsLabel.text = NSLocalizedString(@"Password should contain at least 6 characters and 1 number.", @"New Password View");
            break;
        case ZFSubinstructionsErrorMessages_PasswordDontMatch:
            self.subInstructionsLabel.text = NSLocalizedString(@"The passwords don't match.\nPlease re-enter.", @"New Password View");
            break;
        case ZFSubinstructionsErrorMessages_InvalidCode:
            self.subInstructionsLabel.text = NSLocalizedString(@"Code is invalid.", @"New Password View");
            break;
        default:
            break;
    }
    
    self.subInstructionsLabel.textColor = errorColor;
}

- (void)hideAllErrorHints {
    [self hideErrorHintsForTextField:self.secretCodeTextField removeSubinstructions:YES];
    [self hideErrorHintsForTextField:self.passwordTextField removeSubinstructions:YES];
    [self hideErrorHintsForTextField:self.confirmPasswordTextField removeSubinstructions:YES];
    
}

- (void)hideErrorHintsForTextField:(UITextField *)textField removeSubinstructions:(BOOL)removeSubinstructions {
    if (textField == self.secretCodeTextField) {
        self.secretCodeCheckImageView.hidden = YES;
    } else if (textField == self.passwordTextField) {
        self.passwordCheckImageView.hidden = YES;
    } else {
        self.confirmPasswordCheckImageView.hidden = YES;
    }
    
    if (removeSubinstructions) {
        [self setupSubinstructionsLabelWithError:ZFSubinstructionsErrorMessages_Empty];
    }
}

#pragma mark -
#pragma mark - Actions and selectors

- (IBAction)backButtonWasPressed:(id)sender {
    [self dismissKeyboard];
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (IBAction)checkTextFieldStatus:(id)sender {
    [self checkStatus];
}

- (void)redResetButtonWasPressed:(id)sender {
    [self dismissKeyboard];
    [self hideAllErrorHints];
    
    [ZFResetPasswordRequest requestWithEmail:self.email
                                  secretWord:self.secretCodeTextField.text
                                    password:self.passwordTextField.text
                                  completion:^(LORESTRequest *aRequest) {
                                      
                                      ZFPasswordSuccessfullyViewController *passwordSuccessfullyViewController = [ZFPasswordSuccessfullyViewController new];
                                      
                                      switch (aRequest.resultStatus) {
                                          case LORESTRequestResultStatusSucceeded:
                                              [self.navigationController pushViewController:passwordSuccessfullyViewController animated:YES];
                                              break;
                                          case LORESTRequestResultStatusPending:
                                              break;
                                          case LORESTRequestResultStatusFailed:
                                              break;
                                              
                                          case LORESTRequestResultStatusNotFound:
                                              [self setupSecretCodeInvalid];
                                              break;
                                          case LORESTRequestResultStatusConflict:
                                              break;
                                          default:
                                              break;
                                      }
                                      
                                      [SVProgressHUD dismiss];
                                  }];
}

- (void)dismissKeyboard {
    [self.view endEditing:YES];
}

#pragma mark -
#pragma mark Utils

- (void)checkStatus {
    [self setupShowRedReset:[self checkShowReset]];
}

- (BOOL)checkPasswordsDontMatch {
    return ![self.passwordTextField.text isEqualToString:self.confirmPasswordTextField.text];
}

- (BOOL)checkPasswordIsGood {
    BOOL passwordIsGood = self.passwordTextField.text.length >= TEXT_FIELD_PASSWORD_MIN_LENGTH;
    passwordIsGood &= [self.passwordTextField.text rangeOfCharacterFromSet:[NSCharacterSet decimalDigitCharacterSet]].location != NSNotFound;
    passwordIsGood &= ![self.passwordTextField.text hasOnlyNumbers];
    passwordIsGood &= self.passwordTextField.text.isValidString;
    
    return passwordIsGood;
}

- (BOOL)checkShowReset {
    BOOL showReset = self.passwordTextField.text.isValidString;
    showReset &= [self checkPasswordIsGood];
    showReset &= ![self checkPasswordsDontMatch];
    showReset &= self.secretCodeTextField.text.length > 0;
    
    return showReset;
}

# pragma mark -
# pragma mark UITextFieldDelegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSString *output = [textField.text stringByReplacingCharactersInRange:range withString:string];
    NSCharacterSet *blockedCharacters = [[NSCharacterSet alphanumericCharacterSet] invertedSet];
    
    switch (textField.tag) {
        case ZFSignUpView_FieldsTag_Password:
            self.confirmPasswordTextField.text = @"";
            self.confirmPasswordCheckImageView.hidden = YES;
            self.passwordCheckImageView.hidden = YES;
            [self setupSubinstructionsLabelWithError:ZFSubinstructionsErrorMessages_Empty];
        case ZFSignUpView_FieldsTag_ConfirmPassword:
            return [output rangeOfCharacterFromSet:blockedCharacters].location == NSNotFound;
            break;
            
        default:
            break;
    }
    
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == self.secretCodeTextField) {
        [self.passwordTextField becomeFirstResponder];
    }
    else if (textField == self.passwordTextField) {
        [self.confirmPasswordTextField becomeFirstResponder];
    }
    else {
        [self dismissKeyboard];
    }
    
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    if (textField == self.secretCodeTextField) {
        [self hideErrorHintsForTextField:textField removeSubinstructions:NO];
    } else {
        if (textField.text.length == 0) {
            [self hideErrorHintsForTextField:textField removeSubinstructions:YES];
        } else {
            BOOL goodPasswordForPasswordTextField = [self checkPasswordIsGood];
            if (!(textField == self.confirmPasswordTextField && self.passwordTextField.text.length == 0)) {
                [self setupPasswordIsGood:goodPasswordForPasswordTextField textfield:self.passwordTextField];
            }
            
            if (goodPasswordForPasswordTextField) {
                if (self.passwordTextField.text.length > 0 && self.confirmPasswordTextField.text.length > 0) {
                    if (![self checkPasswordsDontMatch]) {
                        [self setupPasswordIsGood:goodPasswordForPasswordTextField textfield:self.passwordTextField];
                        [self setupPasswordIsGood:goodPasswordForPasswordTextField textfield:self.confirmPasswordTextField];
                    } else {
                        [self setupPasswordsDontMatch];
                    }
                }
            }
            
            if (self.confirmPasswordTextField.text.length == 0) {
                [self hideErrorHintsForTextField:self.confirmPasswordTextField removeSubinstructions:NO];
            }
        }
    }
}

#pragma mark -
#pragma mark Notifications

- (void)updateLayoutForKeyboard:(NSNotification *)notification {
    BOOL showingKeyboard = [notification keyboardHeight] > 0;
    [self.scrollView setScrollEnabled:!showingKeyboard];
    [self.scrollView setContentOffset:CGPointMake(0, showingKeyboard ? kBackgroundKeyboardVisibleShift : 0)];
}

@end
