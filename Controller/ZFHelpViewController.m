//
//  ZFTimeBonusViewController.m
//  Ziferblat
//
//  Created by Terry Michel on 08/04/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "ZFHelpViewController.h"
#import "ZFLeaderBoardsViewController.h"
#import "ZFTopUpViewController.h"
#import "ZFAchievementsInfoViewController.h"
#import "ZFActivitiesViewController.h"

@implementation ZFHelpViewController

#pragma mark - 
#pragma mark Lifecycle

- (instancetype)initWithVenues:(NSArray *)venuesList {
    self = [super init];
    if(self) {
        venues = venuesList;
    }
    return self;
}

- (void)loadView {
    contentView = [[ZFHelpView alloc] init];
    self.view = contentView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    TARGET_TOUCH_UP(contentView.backButton, @selector(backButtonTapped))
    TARGET_TOUCH_UP(contentView.leaderBoardsToggleButton, @selector(leaderBoardsToggleButtonTapped:))
    TARGET_TOUCH_UP(contentView.buyingTimeToggleButton, @selector(buyingTimeToggleButtonTapped:))
    TARGET_TOUCH_UP(contentView.rewardsToggleButton, @selector(rewardsToggleButtonTapped:))
    TARGET_TOUCH_UP(contentView.achievementsButton, @selector(achievementsButtonTapped))
    TARGET_TOUCH_UP(contentView.menuButton, @selector(menuButtonTapped))
    TARGET_TOUCH_UP(contentView.leaderBoardsButton, @selector(leaderBoardsButtonTapped))
    TARGET_TOUCH_UP(contentView.timeTopUpButton, @selector(timeTopUpButtonTapped))
    [contentView updateForHelpType:ZFHelpType_Leaderboards];
}

#pragma mark - 
#pragma mark Buttons actions

- (void)backButtonTapped {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)leaderBoardsToggleButtonTapped:(UIButton *)aButton {
    if(!aButton.isSelected) {
        [contentView.buyingTimeToggleButton setSelected:NO];
        [contentView.rewardsToggleButton setSelected:NO];
        [aButton setSelected:YES];
        [contentView updateForHelpType:ZFHelpType_Leaderboards];
    }
}

- (void)buyingTimeToggleButtonTapped:(UIButton *)aButton {
    if(!aButton.isSelected) {
        [contentView.leaderBoardsToggleButton setSelected:NO];
        [contentView.rewardsToggleButton setSelected:NO];
        [aButton setSelected:YES];
        [contentView updateForHelpType:ZFHelpType_BuyingTime];
    }
}

- (void)rewardsToggleButtonTapped:(UIButton *)aButton {
    if(!aButton.isSelected) {
        [contentView.leaderBoardsToggleButton setSelected:NO];
        [contentView.buyingTimeToggleButton setSelected:NO];
        [aButton setSelected:YES];
        [contentView updateForHelpType:ZFHelpType_Rewards];
    }
}

- (void)leaderBoardsButtonTapped {
    ZFLeaderBoardsViewController *leaderBoardsViewController = [[ZFLeaderBoardsViewController alloc] init];
    [self.navigationController pushViewController:leaderBoardsViewController animated:YES];
}

- (void)achievementsButtonTapped {
    ZFAchievementsInfoViewController *achievementsInfoViewController = [[ZFAchievementsInfoViewController alloc] init];
    [self.navigationController pushViewController:achievementsInfoViewController animated:YES];
}

- (void)menuButtonTapped {
    ZFActivitiesViewController *menuViewController = [[ZFActivitiesViewController alloc] init];
    menuViewController.canBeDismissed = YES;
    [self.navigationController presentViewController:menuViewController animated:YES completion:nil];
}

- (void)timeTopUpButtonTapped {
    ZFTopUpViewController *timTopUpViewController = [[ZFTopUpViewController alloc] initWithTopUpType:ZFTopUpTypeMinutes venuesList:venues];
    [self.navigationController pushViewController:timTopUpViewController animated:YES];
}

@end
