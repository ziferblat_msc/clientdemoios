//
//  ZFAchievementsInfoViewController.h
//  Ziferblat
//
//  Created by Terry Michel on 08/04/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "LOCarouselView.h"
#import "LOLayoutView.h"
#import "ZFMemberVenueDetails.h"
#import "ZFAchievementLevel.h"

@interface ZFAchievementsInfoViewController : UIViewController <LOLayoutViewDataSource, LOCarouselViewDelegate> {
    ZFMemberVenueDetails *memberDetails;
    ZFAchievementLevel *achievementLevel;
    NSArray *imageNames;
    NSArray *achievements;
    NSArray *achievementLevels;
    LOCarouselView *carousel;
    UIView *youHaveContainer;
    UIImageView *youHaveItImageView;
    UILabel *youHaveItLabel;
    UILabel *titleLabel;
    UITextView *descriptionTextView;
}

@property (nonatomic) NSInteger selectedAchievementIdentifier;

@end
