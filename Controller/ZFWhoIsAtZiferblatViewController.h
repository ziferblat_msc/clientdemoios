//
//  ZFWhoIsAtZiferblatViewController.h
//  Ziferblat
//
//  Created by Terry Michel on 08/04/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "ZFWhoIsAtZiferblatView.h"

@interface ZFWhoIsAtZiferblatViewController : UIViewController<UICollectionViewDataSource,
UICollectionViewDelegateFlowLayout> {
    ZFWhoIsAtZiferblatView *contentView;
    ZFVenue *venue;
}

#pragma mark - Instance
- (instancetype)initWithVenue:(ZFVenue *)venue backMessage:(NSString *)backMessage;

@end
