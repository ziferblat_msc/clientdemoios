//
//  ZFLeaderboardsIndividualViewController.h
//  Ziferblat
//
//  Created by Terry Michel on 22/04/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "ZFLeaderboardsIndividualView.h"
#import "ZFLeaderBoard.h"

@interface ZFLeaderboardsIndividualViewController : UIViewController <ZFLeaderBoardsViewDelegate, UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate>

@property (strong, nonatomic) ZFLeaderboardsIndividualView *contentView;
@property (weak, nonatomic)UIViewController<ZFLeaderBoardsViewDelegate> *delegate;
@property ZFLeaderBoardType myType;

- (instancetype)initWithContent:(NSArray<ZFLeaderBoard *> *)aLeaderBoard
                        andType:(ZFLeaderBoardType)aType
                  personalScore:(ZFLeaderBoardCount *)aPersonalScore
                          venue:(NSNumber *)venueId
                            tab:(ZFLeaderBoardsSortType)selectedTab;

- (void)refreshWithContent:(NSArray<ZFLeaderBoard *> *)aLeaderBoard
                   andType:(ZFLeaderBoardType)aType
             personalScore:(ZFLeaderBoardCount *)aPersonalScore
;

@end
