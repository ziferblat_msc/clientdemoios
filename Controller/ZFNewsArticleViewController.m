//
//  ZFNewsArticleViewController.m
//  Ziferblat
//
//  Created by Terry Michel on 08/04/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "ZFNewsArticleViewController.h"
#import "ZFCommentsCell.h"
#import "ZFNews.h"
#import "ZFComment.h"
#import "ZFButton.h"
#import "ZFGetVenueNewsRequest.h"
#import "ZFGetVenueNewsCommentsRequest.h"
#import "ZFNewsLikeRequest.h"
#import "ZFNewsCommentLikeRequest.h"
#import "ZFAddCommentToNewsRequest.h"
#import "LOFavouriteActivityItemProvider.h"
#import "LOImageCache.h"
#import "LOImageDownloader.h"

#define kCommentPlaceholderString NSLocalizedString(@"Comment...", nil)
#define kTextLimit 255
#define kReplyToViewHeight (notIPad ? 30:62)

@interface ZFNewsArticleViewController () <UITableViewDataSource, UITableViewDelegate, UITextViewDelegate, ZFCommentsCellDelegate>

@property (weak, nonatomic) IBOutlet UIControl *topNavigationControlView;
@property (weak, nonatomic) IBOutlet UILabel *backButtonLabel;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;


// Top View
@property (weak, nonatomic) IBOutlet UIView *topView;
@property (weak, nonatomic) IBOutlet UIView *aboveDivider;
@property (weak, nonatomic) IBOutlet UILabel *newsTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *newsDateLabel;
@property (weak, nonatomic) IBOutlet UIImageView *newsImageView;
@property (weak, nonatomic) IBOutlet UILabel *newsContentLabel;

@property (weak, nonatomic) IBOutlet UIView *commentsAndLikesView;
@property (weak, nonatomic) IBOutlet UILabel *numberOfCommentsLabel;
@property (weak, nonatomic) IBOutlet UILabel *numberOfLikesLabel;
@property (weak, nonatomic) IBOutlet UIImageView *likeImageView;
@property (weak, nonatomic) IBOutlet UIButton *likeButton;
@property (weak, nonatomic) IBOutlet UILabel *likeLabel;
@property (weak, nonatomic) IBOutlet UIButton *newsShareButton;

// Comment Table View
@property (weak, nonatomic) IBOutlet UITableView *commentsTableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *commentsTableHeight;

// Submit Comment view
@property (weak, nonatomic) IBOutlet UIView *submitCommentView;
@property (weak, nonatomic) IBOutlet UITextView *commentTextView;
@property (weak, nonatomic) IBOutlet UIView *replyToView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *replyToViewHeightConstraint;
@property (weak, nonatomic) IBOutlet UILabel *replyToLabel;
@property (weak, nonatomic) IBOutlet UILabel *replyToPersonLabel;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *keyboardPaddingHeight;

@property (nonatomic, strong) UITapGestureRecognizer *tapGestureRecognizer;
@property (nonatomic, strong) NSMutableArray *comments;
@property (nonatomic, copy) NSString *replyComment;

@end

@implementation ZFNewsArticleViewController {
    ZFButton *postItButton;
}

@synthesize selectedNews;
@synthesize comments;

#pragma mark -
#pragma mark Lifecycle

- (void)dealloc {
    Notification_RemoveObserver
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setupViewSizes];
    
    comments = [NSMutableArray new];
    self.backButtonLabel.text = NSLocalizedString(@"BACK TO NEWS", nil);
    
    // register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    
    Notification_Observe(kNotification_SingleNewsRefreshed, receivedSingleNewsRefreshed:);
    Notification_Observe(kNotification_SingleNewsCommentsRefreshed, receivedSingleNewsCommentsRefreshed:);

    [[ZFNetworkService sharedInstance] pollForType:ZFPollingRequestType_SingleNews|ZFPollingRequestType_SingleNewsComments newsId:selectedNews.uuid venueId:self.venueId summary:YES];
    self.tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTapOnScrollView:)];
    self.tapGestureRecognizer.cancelsTouchesInView = NO;
    [self.scrollView addGestureRecognizer:self.tapGestureRecognizer];
    [self setupTopView];
    [self setupCommentsTable];
    [self setupSubmitCommentView];
    [self updateContentForSelectedNews];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self fetchComments];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.view endEditing:YES];
}

- (void)setupViewSizes
{
    if(isIPad)
    {
        self.backButtonLabel.font = [UIFont fontWithName:self.backButtonLabel.font.fontName size:26];
        self.newsTitleLabel.font = [UIFont fontWithName:self.newsTitleLabel.font.fontName size:41];
        self.newsDateLabel.font = [UIFont fontWithName:self.newsDateLabel.font.fontName size:34];
        self.newsContentLabel.font = [UIFont fontWithName:self.newsContentLabel.font.fontName size:34];
        self.numberOfCommentsLabel.font = [UIFont fontWithName:self.numberOfCommentsLabel.font.fontName size:31];
        self.numberOfLikesLabel.font = [UIFont fontWithName:self.numberOfLikesLabel.font.fontName size:34];
        self.likeLabel.font = [UIFont fontWithName:self.likeLabel.font.fontName size:36];
        self.replyToLabel.font = [UIFont fontWithName:self.replyToLabel.font.fontName size:36];
        self.replyToPersonLabel.font = [UIFont fontWithName:self.replyToPersonLabel.font.fontName size:36];
        self.commentTextView.font = [UIFont fontWithName:self.commentTextView.font.fontName size:34];
        
        self.topNavigationControlView.constraints[0].constant = 48;
        
        for(NSLayoutConstraint *constraint in self.topView.constraints)
        {
            constraint.constant *= 2.4;
        }
        
        for (UIView *subview in self.topView.subviews )
        {
            for(NSLayoutConstraint *constraint in subview.constraints)
            {
                constraint.constant *= 2.4;
            }
        }
        
        for (UIView *subview in self.commentsAndLikesView.subviews )
        {
            for(NSLayoutConstraint *constraint in subview.constraints)
            {
                constraint.constant *= 2.4;
            }
        }
        
        for(NSLayoutConstraint *constraint in self.submitCommentView.constraints)
        {
            constraint.constant *= 2.4;
        }
        
        for (UIView *subview in self.submitCommentView.subviews )
        {
            for(NSLayoutConstraint *constraint in subview.constraints)
            {
                constraint.constant *= 2.4;
            }
        }
        
        for (UIView *subview in self.replyToView.subviews )
        {
            for(NSLayoutConstraint *constraint in subview.constraints)
            {
                constraint.constant *= 2.4;
            }
        }
    }
}

#pragma mark - Setup Top

- (void)setupTopView {
    UIImage *tiledImage = [UIImage imageNamed:@"NewsItemPattern"];
    self.aboveDivider.backgroundColor = [UIColor colorWithPatternImage:tiledImage];
}

#pragma mark - Setup Comments Table

- (void)setupCommentsTable {
    [self.commentsTableView registerNib:[UINib nibWithNibName:[ZFCommentsCell className] bundle:nil] forCellReuseIdentifier:[ZFCommentsCell reuseIdentifier]];
    self.commentsTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.commentsTableView.delegate = self;
    self.commentsTableView.dataSource = self;
    [self updateCommentsTableViewHeightForNumberOfRows:0];
}

#pragma mark - Setup Submit Comment

- (void)setupSubmitCommentView {
    [self setReplyToVisible:NO];
    self.commentTextView.delegate = self;
    self.commentTextView.layer.borderColor = Colour_Black.CGColor;
    self.commentTextView.layer.borderWidth = 2.0f;
    [self resetCommentTextView];
    
    [self setupPostItButton];
}

- (void)resetCommentTextView {
    self.commentTextView.text = kCommentPlaceholderString;
    self.commentTextView.textColor = Colour_Hex(@"727374");
    
    self.replyComment = nil;
    [self setReplyToVisible:NO];
}

- (void)setupPostItButton {
    postItButton = [ZFButton buttonWithTitle:NSLocalizedString(@"POST IT!", nil) showMarks:NO];
    postItButton.translatesAutoresizingMaskIntoConstraints = NO;
    postItButton.enabled = NO;
    [self.submitCommentView addSubview:postItButton];
    
    CGFloat width = postItButton.width;
    
    NSLayoutConstraint *postItButtonTopConstraint = [NSLayoutConstraint constraintWithItem:postItButton
                                                                                 attribute:NSLayoutAttributeTop
                                                                                 relatedBy:NSLayoutRelationEqual
                                                                                    toItem:self.commentTextView
                                                                                 attribute:NSLayoutAttributeBottom
                                                                                multiplier:1.0
                                                                                  constant: (notIPad ?15:25)];
    NSLayoutConstraint *postItButtonCenterXConstraint = [NSLayoutConstraint constraintWithItem:postItButton
                                                                                     attribute:NSLayoutAttributeCenterX
                                                                                     relatedBy:NSLayoutRelationEqual
                                                                                        toItem:self.submitCommentView
                                                                                     attribute:NSLayoutAttributeCenterX
                                                                                    multiplier:1.0
                                                                                      constant:0];
    NSLayoutConstraint *postItButtonWidthConstraint = [NSLayoutConstraint constraintWithItem:postItButton
                                                                                   attribute:NSLayoutAttributeWidth
                                                                                   relatedBy:NSLayoutRelationEqual
                                                                                      toItem:nil
                                                                                   attribute:NSLayoutAttributeWidth
                                                                                  multiplier:1.0
                                                                                    constant:width];
    NSLayoutConstraint *postItButtonHeightConstraint = [NSLayoutConstraint constraintWithItem:postItButton
                                                                                    attribute:NSLayoutAttributeHeight
                                                                                    relatedBy:NSLayoutRelationEqual
                                                                                       toItem:nil
                                                                                    attribute:NSLayoutAttributeHeight
                                                                                   multiplier:1.0
                                                                                     constant:44];
    NSLayoutConstraint *postItButtonBottomConstraint = [NSLayoutConstraint constraintWithItem:self.submitCommentView
                                                                                    attribute:NSLayoutAttributeBottom
                                                                                    relatedBy:NSLayoutRelationEqual
                                                                                       toItem:postItButton
                                                                                    attribute:NSLayoutAttributeBottom
                                                                                   multiplier:1.0
                                                                                     constant:15];
    
    [self.submitCommentView addConstraints:@[postItButtonTopConstraint,
                                postItButtonCenterXConstraint,
                                postItButtonWidthConstraint,
                                postItButtonHeightConstraint,
                                postItButtonBottomConstraint]];
    
    TARGET_TOUCH_UP(postItButton, @selector(postItButtonWasTapped))
}

#pragma mark - Keyboard

- (void)keyboardWillShow:(NSNotification *)notification {
    UIView *view = self.view;
    
    CGRect keyboardFrame = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue];
    keyboardFrame = [view convertRect:keyboardFrame fromView:self.view.window];
    
    CGFloat duration = [[[notification userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    NSInteger animationCurve = [[[notification userInfo] objectForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue];
    
    self.keyboardPaddingHeight.constant = CGRectGetHeight(keyboardFrame);
    [self.view setNeedsUpdateConstraints];
    
    [UIView animateWithDuration:duration delay:0.0f options:animationCurve animations:^{
        [self.view layoutIfNeeded];
        CGPoint bottomOffset = CGPointMake(0, self.scrollView.contentSize.height - self.scrollView.bounds.size.height);
        [self.scrollView setContentOffset:bottomOffset animated:NO];
    } completion:^(BOOL finished) {
    }];
}

- (void)keyboardWillHide:(NSNotification *)notification
{
    CGFloat duration = [[[notification userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    NSInteger animationCurve = [[[notification userInfo] objectForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue];
    
    self.keyboardPaddingHeight.constant = 0.0f;
    [self.view setNeedsUpdateConstraints];
    
    [UIView animateWithDuration:duration delay:0.0f options:animationCurve animations:^{
        [self.view layoutIfNeeded];
    } completion:nil];
}

#pragma mark - Actions

- (IBAction)backButtonTapped:(id)sender {
    [super backButtonTapped];
}

- (IBAction)likeButtonWasTapped:(id)sender {
    [self likeNews];
}

- (IBAction)shareButtonWasTapped:(UIButton *)sender {
    NSString *sharingText = [NSString stringWithFormat:@"WOW! I really love '%@' news on #Ziferblat!", selectedNews.title];
    
    LOFavouriteActivityItemProvider *itemProvider = [[LOFavouriteActivityItemProvider alloc] initWithTwitterShareText:sharingText
                                                                                                    facebookShareText:sharingText
                                                                                                       otherShareText:sharingText];
    
    UIActivityViewController *controller = [[UIActivityViewController alloc] initWithActivityItems:@[itemProvider] applicationActivities:nil];
    
    if(notIPad)
    {
        [self presentViewController:controller animated:YES completion:nil];
    }
    else
    {
        controller.popoverPresentationController.sourceView = self.newsShareButton;
        [self presentViewController:controller animated:YES completion:nil];
    }
}

- (void)postItButtonWasTapped {
    [self.view endEditing:YES];
    NSString *comment = self.commentTextView.text;
    
    if (![comment isEqualToString:@""] && ![comment isEqualToString:kCommentPlaceholderString]) {
        [self submitComment:comment];
    } else {
        Show_Error(NSLocalizedString(@"Please enter a comment", nil));
    }
}

- (IBAction)cancelReplyWasTapped:(UIButton *)sender {
    self.replyComment = nil;
    self.replyToPersonLabel.text = @"";
    
    [self.commentTextView resignFirstResponder];
    [self setReplyToVisible:NO];
}

- (void)receivedSingleNewsRefreshed:(NSNotification *)notification {
    selectedNews = [ZFNetworkService sharedInstance].news;
    [self updateContentForSelectedNews];
}

- (void)receivedSingleNewsCommentsRefreshed:(NSNotification *)notification {
    // Only reload the data in table if user is not typing a message since it causes a lag for user's input
    if (![self.commentTextView isFirstResponder]) {
        [self.comments removeAllObjects];
        [self flattenCommentsArray:[ZFNetworkService sharedInstance].newsComments];
        [self.commentsTableView reloadData];
        [self updateCommentsTableViewHeightForNumberOfRows:self.comments.count];
    }
}

- (void)didTapOnScrollView:(UIGestureRecognizer *)recognizer {
    NSLog(@"Did tap ons croll voew");
    [self.view endEditing:YES];
}

#pragma mark - Requests

- (void)updateNews {
    [ZFGetVenueNewsRequest requestForNewsItemWithId:selectedNews.uuid
                                            venueId:self.venueId
                                         completion:^(LORESTRequest *aRequest) {
                                             if (aRequest.resultStatus == LORESTRequestResultStatusSucceeded) {
                                                 
                                                 Hide_HudAndPerformBlock(^{
                                                     if ([aRequest.result isKindOfClass:[ZFNews class]]) {
                                                         selectedNews = aRequest.result;
                                                         [self updateContentForSelectedNews];
                                                         [self fetchComments];
                                                     } else {
                                                         // Failed to update comment, return user to news list
                                                     }
                                                 });
                                             }
                                         }];
}

- (void)fetchComments {
    [self.comments removeAllObjects];
    Show_Hud(nil);
    [ZFGetVenueNewsCommentsRequest requestWithVenueId:self.venueId
                                           newsId:selectedNews.uuid
                                       completion:^(LORESTRequest *aRequest) {
                                           Hide_HudAndPerformBlock(^{
                                               if (aRequest.resultStatus == LORESTRequestResultStatusSucceeded) {
                                                   if ([aRequest.result isKindOfClass:[NSArray class]]) {
                                                       NSArray *parentComments = aRequest.result;
                                                       [self flattenCommentsArray:parentComments];
                                                   }
                                               } else {
                                                   Show_Error(NSLocalizedString(@"Error fetching comments", nil));
                                               }
                                               [self.commentsTableView reloadData];
                                               [self updateCommentsTableViewHeightForNumberOfRows:self.comments.count];
                                           });
                                       }];
}

- (void)likeNews {
    Show_Hud(nil);
    [ZFNewsLikeRequest requestWithVenueId:self.venueId
                                   newsId:selectedNews.uuid
                                     like:![selectedNews.liked boolValue]
                               completion:^(LORESTRequest *aRequest) {
                                   if (aRequest.resultStatus == LORESTRequestResultStatusSucceeded) {
                                       [self updateNews];
                                   } else {
                                       Hide_HudAndPerformBlock(^{
                                           Show_Error(NSLocalizedString(@"Error liking news",nil));
                                       });
                                   }
                               }];
}

- (void)submitComment:(NSString *)comment {
    Show_Hud(nil);
    if (self.replyComment) {
        [ZFAddCommentToNewsRequest requestWithVenueId:self.venueId
                                               newsId:selectedNews.uuid
                                             parentId:self.replyComment
                                          commentText:comment
                                           completion:^(LORESTRequest *aRequest) {
                                               if (aRequest.resultStatus == LORESTRequestResultStatusSucceeded) {
                                                   [self resetCommentTextView];
                                                   [self updateNews];
                                               } else {
                                                   Hide_HudAndPerformBlock(^{
                                                       Show_Error(NSLocalizedString(@"Error submitting comment", nil));
                                                   });
                                               }
                                           }];
    } else {
        [ZFAddCommentToNewsRequest requestWithVenueId:self.venueId
                                               newsId:selectedNews.uuid
                                          commentText:comment
                                           completion:^(LORESTRequest *aRequest) {
                                               if (aRequest.resultStatus == LORESTRequestResultStatusSucceeded) {
                                                   [self resetCommentTextView];
                                                   [self updateNews];
                                               } else {
                                                   Hide_HudAndPerformBlock(^{
                                                       Show_Error(NSLocalizedString(@"Error submitting comment", nil));
                                                   });
                                               }
                                           }];
    }
}

#pragma mark - Helpers

- (void)flattenCommentsArray:(NSArray *)array {
    for (ZFComment *comment in array) {
        // Polling causing issues with fetch since both poll result and actual fetch request delivering results to flattenCommentsArray
        // at the same time which results in the duplicate comments and replies. Predicate used to only add unique comments to display array
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"uuid == %@", comment.uuid];
        if ([comments filteredArrayUsingPredicate:predicate].count == 0) {
            [comments addObject:comment];
        }
        
        if ([comment.replies count] > 0) {
            [self flattenCommentsArray:comment.replies];
        }
    }
}

- (void)updateContentForSelectedNews {
    if (selectedNews) {
        self.newsTitleLabel.text = selectedNews.title;
        
        NSDateFormatter *dateFormatter = [NSDateFormatter new];
        [dateFormatter setLocale:[NSLocale currentLocale]];
        [dateFormatter setDateFormat:@"d MMMM"];
        NSString *dateString = [dateFormatter stringFromDate:selectedNews.date];
        [dateFormatter setDateFormat:@"H:mm"];
        NSString *timeString = [dateFormatter stringFromDate:selectedNews.date];
        self.newsDateLabel.text = [[NSString stringWithFormat:NSLocalizedString(@"%@ at %@", nil), dateString, timeString] lowercaseString];
        
        self.newsContentLabel.text = selectedNews.newsDescription;
        
        UIImage *image = [LOImageCache imageFromCache:selectedNews.imageUrl];
        if (!image) {
            [LOImageDownloader downloadImageFromPath:selectedNews.imageUrl
                                withCompletionHander:^(UIImage *downloadedImage) {
                                    [UIView transitionWithView:self.newsImageView
                                                      duration:0.5
                                                       options:UIViewAnimationOptionTransitionCrossDissolve
                                                    animations:^{
                                                        self.newsImageView.image = downloadedImage;
                                                    } completion:nil];
                                }];
        } else {
            self.newsImageView.image = image;
        }
        
        NSNumber *numberOfLikes = (selectedNews.likeCount) ? selectedNews.likeCount : @0;
        self.numberOfLikesLabel.text = [NSString stringWithFormat:@"%@", numberOfLikes];
        
        NSNumber *numberOfComments = (selectedNews.commentCount) ? selectedNews.commentCount : @0;
        self.numberOfCommentsLabel.text = [[NSString stringWithFormat:NSLocalizedString(@"%d comments", nil), numberOfComments.integerValue] uppercaseString];
        NSString *likeImagePath = ([selectedNews.liked boolValue]) ? @"LikeIconRed" : @"NewsLikeIconGrey";
        self.likeImageView.image = [UIImage imageNamed:likeImagePath];
    }
}

- (void)updateCommentsTableViewHeightForNumberOfRows:(NSInteger)numberOfRows {
    CGFloat totalRowHeight = 0;
    for (NSInteger index = 0; index < numberOfRows; index++) {
        totalRowHeight += [self heightForCommentCellAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0]];
    }
    self.commentsTableHeight.constant = totalRowHeight;
    
    [self.view setNeedsUpdateConstraints];
    
    [UIView animateWithDuration:0.3 animations:^{
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {

    }];
}

- (void)setReplyToVisible:(BOOL)visible {
    if (visible) {
        self.replyToView.alpha = 1.0;
        self.replyToViewHeightConstraint.constant = kReplyToViewHeight;
    } else {
        self.replyToView.alpha = 0.0;
        self.replyToViewHeightConstraint.constant = 0;
    }
    [self.view setNeedsUpdateConstraints];
    
    [UIView animateWithDuration:0.3 animations:^{
        [self.view layoutIfNeeded];
        if (visible) {
            CGPoint bottomOffset = CGPointMake(0, self.scrollView.contentSize.height - self.scrollView.bounds.size.height);
            [self.scrollView setContentOffset:bottomOffset animated:NO];
        }
    }];
}

#pragma mark - UITableViewDataSource / UITableViewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return comments.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [self heightForCommentCellAtIndexPath:indexPath];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ZFCommentsCell *commentsCell = [tableView dequeueReusableCellWithIdentifier:[ZFCommentsCell reuseIdentifier] forIndexPath:indexPath];
    commentsCell.commentDelegate = self;
    ZFComment *comment = [comments objectAtIndex:indexPath.row];
    [commentsCell setupCellForComment:comment];
    
    return commentsCell;
}

#pragma mark - UITableView Helper

- (CGFloat)heightForCommentCellAtIndexPath:(NSIndexPath *)indexPath {
    static ZFCommentsCell *sizingCell = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sizingCell = [self.commentsTableView dequeueReusableCellWithIdentifier:[ZFCommentsCell reuseIdentifier]];
    });
    
    ZFComment *comment = [comments objectAtIndex:indexPath.row];
    [sizingCell setupCellForComment:comment];
    return [self calculateHeightForConfiguredSizingCell:sizingCell];
}

- (CGFloat)calculateHeightForConfiguredSizingCell:(UITableViewCell *)sizingCell {
    sizingCell.bounds = CGRectMake(0.0f, 0.0f, CGRectGetWidth(self.commentsTableView.frame), CGRectGetHeight(sizingCell.bounds));
    [sizingCell setNeedsLayout];
    [sizingCell layoutIfNeeded];
    
    CGSize size = [sizingCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
    return size.height;
}


#pragma mark - ZFCommentsCellDelegate

- (void)replyCommentWithCommentId:(NSString *)uuid username:(NSString *)username {
    self.replyComment = uuid;
    self.replyToPersonLabel.text = username;
    [self setReplyToVisible:YES];
}

- (void)commentWithId:(NSString *)commentId like:(BOOL)like {
    [ZFNewsCommentLikeRequest requestWithVenueId:self.venueId
                                          newsId:selectedNews.uuid
                                       commentId:commentId
                                            like:like
                                      completion:nil];
}

#pragma mark - UITextViewDelegate

- (void)textViewDidBeginEditing:(UITextView *)textView {
    if ([textView.text isEqualToString:kCommentPlaceholderString]) {
        textView.text = @"";
        textView.textColor = Colour_Black;
    }
}

- (void)textViewDidEndEditing:(UITextView *)textView {
    if ([textView.text isEqualToString:@""]) {
        [self resetCommentTextView];
    }
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        
        return NO;
    }
    
    BOOL aux = YES;
    NSString *output = [textView.text stringByReplacingCharactersInRange:range withString:text];
    aux = output.length < kTextLimit;
    
    if (output.length == 0 || [output stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length == 0) {
        postItButton.enabled = NO;
    } else {
        postItButton.enabled = YES;
    }
    
    return aux;
}

@end
