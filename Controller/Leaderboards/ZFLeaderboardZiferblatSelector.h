//
//  ZFLeaderboardZiferblatSelector.h
//  Ziferblat
//
//  Created by Boris Yurkevich on 26/10/2015.
//  Copyright © 2015 Locassa. All rights reserved.
//

#import "ZFTreasuryZiferblatSelector.h"

@interface ZFLeaderboardZiferblatSelector : ZFTreasuryZiferblatSelector

- (instancetype)initWithVenue:(NSNumber *)venueId;

@end
