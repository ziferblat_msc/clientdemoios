//
//  ZFLeaderboardZiferblatSelector.m
//  Ziferblat
//
//  Created by Boris Yurkevich on 26/10/2015.
//  Copyright © 2015 Locassa. All rights reserved.
//

#import "ZFLeaderboardZiferblatSelector.h"
#import "ZFVenue.h"

#define kCompatactedHeight      (notIPad ? 21:50)
#define kExpandedHeight         (notIPad ? 168 + 20:451)
#define kZiferblatSelectedFrame (notIPad ? CGRectMake(1, 2, 161, 16):CGRectMake(2.4, 4.8, 386, 38))

@implementation ZFLeaderboardZiferblatSelector

- (instancetype)initWithVenue:(NSNumber *)venueId {
    
    UIImageView *switchLocationImageView = [UIImageView imageViewWithImageNamed:RESOURCE_TREASURY_SELECTOR];
    CGRect frame = CGRectMake(0, 0, switchLocationImageView.width, kCompatactedHeight);
    self = [super initWithFrame:frame];
    
    if(self) {
        self.clipsToBounds = YES;
        switchLocationImageView.height = self.height;
        switchLocationImageView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
        [self addSubview:switchLocationImageView];
        
        UIImageView *arrowSwitchLocationImageView = [UIImageView imageViewWithImageNamed:RESOURCE_TREASURY_SELECTOR_BUTTON];
        [switchLocationImageView addSubview:arrowSwitchLocationImageView];
        [arrowSwitchLocationImageView centerVerticallyInSuperView];
        arrowSwitchLocationImageView.right = switchLocationImageView.width - (notIPad ? 7:17);
        
        ziferblatSelectedLabel = [[UILabel alloc] init];
        ziferblatSelectedLabel.frame = kZiferblatSelectedFrame;
        [ziferblatSelectedLabel setFont:FontRockWell((notIPad ? 12:29))];
        [ziferblatSelectedLabel setAdjustsFontSizeToFitWidth:YES];
        [ziferblatSelectedLabel setTextAlignment:NSTextAlignmentCenter];
        [self addSubview:ziferblatSelectedLabel];
        ziferblatSelectedLabel.text = CurrentUser.selectedVenue.name;
        
        tableView = [[UITableView alloc] initWithFrame:CGRectMake(2, kCompatactedHeight, self.width-4, kExpandedHeight-kCompatactedHeight)];
        [self addSubview:tableView];
        tableView.delegate = self;
        tableView.dataSource = self;
        tableView.rowHeight = kCompatactedHeight;
        tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        tableView.backgroundColor = Colour_Clear;
        
        NSArray *allZiferblats = [ZFZiferblatService sharedInstance].allZiferblats;
        
        int i=0;
        for(ZFVenue *selectedZiferblat in allZiferblats) {
            if(venueId && [selectedZiferblat.uuid isEqualToNumber:venueId]) {
                ziferblatSelectedLabel.text = selectedZiferblat.name;
                self.selectedVenue = selectedZiferblat;
                break;
            }
            i++;
        }
        
        if (!venueId) {
            // None of ziferblats were selected
            // This means that Worldwide Ziferblat is selected
            ziferblatSelectedLabel.text = NSLocalizedString(@"Worldwide", @"Leaderboards venue picker");
            self.selectedVenue = nil;
        }
    }
    Notification_Observe(kNotification_VenuesListRefreshed, venuesListUpdated);
    return self;;
}

#pragma mark - Notifications

- (void)venuesListUpdated {
    ZiferblatService.allZiferblats = NetworkService.venuesList;
    [tableView reloadData];
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    ZFVenue *venueSelected;
    
    NSInteger index = indexPath.row;
    NSInteger count = ZiferblatService.allZiferblats.count;
    if (index > count - 1) {
        venueSelected = [[ZFVenue alloc] init];
        venueSelected.name = NSLocalizedString(@"Worldwide", @"Leaderboards venue picker");
        venueSelected.uuid = nil;
    } else {
        venueSelected = [ZiferblatService.allZiferblats objectAtIndex:indexPath.row];
    }
    
    ziferblatSelectedLabel.text = venueSelected.name;
    self.selectedVenue = venueSelected;
    
    if([self.delegate respondsToSelector:@selector(ziferblatSelectorDidSelectVenue:)]) {
        [self.delegate ziferblatSelectorDidSelectVenue:venueSelected];
    }
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return ZiferblatService.allZiferblats.count + 1;
}

- (UITableViewCell *)tableView:(UITableView *)aTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString * const reuseIdentifier = @"ZFTreasuryZiferblatSelector";
    UITableViewCell *cell = [aTableView dequeueReusableCellWithIdentifier:reuseIdentifier];
    
    ZFVenue *venue;
    if (indexPath.row > ZiferblatService.allZiferblats.count - 1) {
        venue = [[ZFVenue alloc] init];
        venue.name = NSLocalizedString(@"Worldwide", @"Leaderboards venue picker");
    } else {
        venue = (ZFVenue *)[ZiferblatService.allZiferblats objectAtIndex:indexPath.row];
    }
    
    if(!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
        cell.textLabel.font = ziferblatSelectedLabel.font;
        cell.backgroundColor = Colour_Clear;
        
        UIImageView *separatorImageView = [UIImageView imageViewWithImageNamed:RESOURCE_TREASURY_SELECTOR_DIVIDER];
        separatorImageView.bottom = cell.height;
        separatorImageView.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
        [cell addSubview:separatorImageView];
    }
    cell.textLabel.text = venue.name;
    
    return cell;
}

#pragma mark - Animations
- (void)expand {
    self.height = kExpandedHeight;
}

- (double)expandedHeight {
    return kExpandedHeight;
}

@end
