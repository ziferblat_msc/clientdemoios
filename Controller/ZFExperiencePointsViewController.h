//
//  ZFExperiencePointsViewController.h
//  Ziferblat
//
//  Created by Terry Michel on 08/04/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "ZFBaseView.h"
#import "ZFViewController.h"

@interface ZFExperiencePointsViewController : ZFViewController<UITableViewDataSource, UITableViewDelegate> {
    UIImageView *topImageView;
    UILabel *headerLabel;
    UILabel *pointsLabel;
    UILabel *levelLabel;
    UIButton *allButton;
    UIButton *historyButton;
}

- (instancetype)initWithBackMessage:(NSString *)backMessage;

@end
