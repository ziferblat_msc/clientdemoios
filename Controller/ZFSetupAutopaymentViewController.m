//
//  ZFSetupAutopaymentViewController.m
//  Ziferblat
//
//  Created by Terry Michel on 08/04/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "ZFSetupAutopaymentViewController.h"

@implementation ZFSetupAutopaymentViewController

#pragma mark - 
#pragma mark Lifecycle

- (void)loadView {
    contentView = [[ZFSetupAutopaymentView alloc] init];
    self.view = contentView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    TARGET_TOUCH_UP(contentView.backButton, @selector(backButtonTapped))
    TARGET_TOUCH_UP(contentView.confirmButton, @selector(confirmButtonTapped:))
    TARGET_TOUCH_UP(contentView.turnOnOffButton, @selector(turnOnOffButtonTapped:))
}

#pragma mark - 
#pragma mark Buttons actions

- (void)backButtonTapped {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)confirmButtonTapped:(UIButton *)aButton {
    [aButton setSelected:!aButton.isSelected];
}

- (void)turnOnOffButtonTapped:(UIButton *)aButton {
    [aButton setSelected:!aButton.isSelected];
    if(aButton.isSelected) {
        contentView.badgeImageView.image = [UIImage imageNamed:RESOURCE_AUTO_PAYMENT_ON];
    } else {
        contentView.badgeImageView.image = [UIImage imageNamed:RESOURCE_AUTO_PAYMENT_OFF];
    }
}

@end
