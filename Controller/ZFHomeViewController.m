//
//  ZFHomeViewController.m
//  Ziferblat
//
//  Created by Terry Michel on 08/04/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "ZFHomeViewController.h"
#import "ZFEventsViewController.h"
#import "ZFNewsViewController.h"
#import "ZFConversationListViewController.h"
#import "ZFDiscussionViewController.h"
#import "ZFNotificationsViewController.h"
#import "ZFFriendsListViewController.h"
#import "ZFPoemOverlayViewController.h"
#import "ZFTreasuryViewController.h"
#import "ZFExperiencePointsViewController.h"
#import "ZFLeaderBoardsViewController.h"
#import "ZFProfileViewController.h"
#import "ZFAchievementsViewController.h"
#import "ZFWhoIsAtZiferblatViewController.h"
#import "ZFActivitiesViewController.h"
#import "ZFLastPhotoViewController.h"
#import "ZFZiferblatDetailsViewController.h"
#import "ZFTimeHistoryViewController.h"
#import "ZFVenueCell.h"
#import "ZFListAllVenuesRequest.h"
#import "ZFGetSingleVenueRequest.h"
#import "ZFEventCell.h"
#import "ZFEvents.h"
#import "ZFNewsCell.h"
#import "ZFActivityCell.h"
#import "ZFGetMemberVenueDetailsRequest.h"
#import "ZFGetOccupantsForVenueRequest.h"
#import "ZFMemberVenueDetails.h"
#import "ZFGetMemberFriendsListRequest.h"
#import "ZFAchievementCollectionViewCell.h"
#import "ZFHomeAchievementCollectionViewCell.h"
#import "ZFNetworkService.h"
#import "ZFGetSingleMemberRequest.h"
#import "ZFUnreadMessageCountRequest.h"
#import "ZFNotificationRequest.h"
#import "ZFNotificationContent.h"
#import "ZFGetConversationsListRequest.h"
#import "LOLocationManager.h"
#import "ZFPresenter.h"

@interface ZFHomeViewController () <ZFHomeSortDelegate> {
    SortButtonType currentSortButtonType;
    CLLocationCoordinate2D currentLocation;
}

@end

@implementation ZFHomeViewController

#define          kInfoButtonXLimit (UI_USER_INTERFACE_IDIOM() !=UIUserInterfaceIdiomPad ? 60:100)
#define          kInfoButtonEmptyRect   CGRectMake(50, 35, 201, 15)

#pragma mark -
#pragma mark Instance

- (instancetype)initIsLaunchFromLogin:(BOOL)launchFromLogin {
    self = [super init];
    if(self) {
        isLaunchFromLogin = launchFromLogin;
        venuesList = @[];
    }
    return self;
}

- (void)loadView {
    contentView = [[ZFHomeView alloc] init];
    contentView.delegate = self;
    self.view = contentView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    TARGET_TOUCH_UP(contentView.eventsButton, @selector(eventsButtonPressed))
//    TARGET_TOUCH_UP(contentView.poemButton, @selector(poemButtonPressed)) // Please uncomment to enable the Warlus.
    TARGET_TOUCH_UP(contentView.discussionButton, @selector(discussionsButtonPressed))
    TARGET_TOUCH_UP(contentView.newsButton, @selector(newsButtonPressed))
    TARGET_TOUCH_UP(contentView.conversationButton, @selector(conversationButtonPressed))
    TARGET_TOUCH_UP(contentView.friendsButton, @selector(friendsButtonPressed))
    TARGET_TOUCH_UP(contentView.leaderboardsButton, @selector(leaderboardsButtonPressed))
    TARGET_TOUCH_UP(contentView.notificationsButton, @selector(notificationsButtonPressed))
    TARGET_TOUCH_UP(contentView.profileButton, @selector(profileButtonPressed))
    TARGET_TOUCH_UP(contentView.treasuryButton, @selector(treasuryButtonPressed))
    TARGET_TOUCH_UP(contentView.achievementsButton, @selector(achievementsButtonPressed))
    TARGET_TOUCH_UP(contentView.experiencePointsButton, @selector(experiencePointsButtonPressed))
    TARGET_TOUCH_UP(contentView.whoIsZiferblatButton, @selector(whoIsZiferblatButtonPressed))
    TARGET_TOUCH_UP(contentView.menuButton, @selector(menuButtonPressed:forEvent:))
    TARGET_TOUCH_UP(contentView.activitiesButton, @selector(activitiesButtonPressed))
//    TARGET_TOUCH_UP(contentView.lastPhotoButton, @selector(lastPhotoButtonPressed)) Plase uncomment this to enable frame.
    TARGET_TOUCH_UP(contentView.timeHistoryButton, @selector(timeHistoryButtonPressed))
    TARGET_TOUCH_UP(contentView.sortAZButton, @selector(sortAZButtonPressed:))
    TARGET_TOUCH_UP(contentView.sortFavouriteButton, @selector(sortFavouriteButtonPressed:))
    TARGET_TOUCH_UP(contentView.sortMembersButton, @selector(sortMembersButtonPressed:))
    TARGET_TOUCH_UP(contentView.sortDistanceButton, @selector(sortDistanceButtonPressed:))
    
    contentView.venuesTableView.delegate = self;
    contentView.venuesTableView.dataSource = self;
    contentView.eventsTableView.delegate = self;
    contentView.eventsTableView.dataSource = self;
    contentView.newsTableView.delegate = self;
    contentView.newsTableView.dataSource = self;
    contentView.activitiesTableView.delegate = self;
    contentView.activitiesTableView.dataSource = self;
    [contentView.achievementCollectionView registerClass:[ZFHomeAchievementCollectionViewCell class]
                              forCellWithReuseIdentifier:[ZFHomeAchievementCollectionViewCell reuseIdentifier]];
    contentView.achievementCollectionView.delegate = self;
    contentView.achievementCollectionView.dataSource = self;
    
    Show_Hud(NSLocalizedString(@"Fetching data...",nil))
    contentView.userInteractionEnabled = NO;
    [ZFListAllVenuesRequest requestWithCompletion:^(LORESTRequest *aRequest) {
        if(aRequest.resultStatus == LORESTRequestResultStatusSucceeded) {
            Hide_HudAndPerformBlock(^
            {
                userVenueIndex = 0;
                
                [self refreshVenuesList:aRequest.result];

                if(CurrentUser.currentVenueId && CurrentUser.currentVenueId.integerValue != 0)
                {
                    for (ZFVenue *venue in venuesList)
                    {
                        if ([venue.uuid isEqualToNumber:CurrentUser.currentVenueId])
                        {
                            userVenueIndex = [venuesList indexOfObject:venue];
                        }
                    }
                }
                else if(CurrentUser.favouriteVenueId)
                {
                    for (ZFVenue *venue in venuesList)
                    {
                        if ([venue.uuid isEqualToNumber:CurrentUser.favouriteVenueId])
                        {
                            userVenueIndex = [venuesList indexOfObject:venue];
                        }
                    }
                }

                selectedVenueIndex = userVenueIndex;
                
                ZFVenue *venue = [venuesList objectAtIndex:userVenueIndex];
                if(CurrentUser) {
                    ZFUser *user = CurrentUser;
                    [ZFGetMemberVenueDetailsRequest requestWithMemberId:user.uuid
                                                                venueId:venue.uuid
                                                             completion:^(LORESTRequest *aRequest) {
                                                                 if(aRequest.resultStatus == LORESTRequestResultStatusSucceeded) {
                                                                     [self refreshMemberVenueDetails:aRequest.result];
                                                                     [ZFGetMemberFriendsListRequest requestWithMemberId:user.uuid
                                                                                                             andVenueId:venue.uuid
                                                                                                             completion:^(LORESTRequest *aRequest) {
                                                                                                                 if(aRequest.resultStatus == LORESTRequestResultStatusSucceeded) {
                                                                                                                     CurrentUser.friendsList = aRequest.result;
                                                                                                                     Hide_HudAndPerformBlock(^{
                                                                                                                         [ZFGetSingleVenueRequest requestWithVenueId:venue.uuid
                                                                                                                                                             summary:YES
                                                                                                                                                          completion:^(LORESTRequest *aRequest) {
                                                                                                                                                              if(aRequest.resultStatus == LORESTRequestResultStatusSucceeded) {
                                                                                                                                                                  [self refreshSelectedVenue:aRequest.result];
                                                                                                                                                                  contentView.userInteractionEnabled = YES;
                                                                                                                                                                  [NetworkService pollForType:ZFPollingRequestType_SingleVenueDetails|ZFPollingRequestType_MemberVenueDetails|ZFPollingRequestType_VenuesList|ZFPollingRequestType_VenueOccupants|ZFPollingRequestType_AccountBalance|                                                                                                                                                                   ZFPollingRequestType_SingleMemberRequest|ZFPollingRequestType_NewerNotifications |                                                                                                                                                                                                                                                                                                                               ZFPollingRequestType_UnreadMessageCount |
                                                                                                                                                        ZFPollingRequestType_PendingFriendsRequest
                                                                                                                                                                                     memberId:CurrentUser.uuid
                                                                                                                                                                                      venueId:selectedVenue.uuid summary:YES];
                                                                                                                                                              } else {
                                                                                                                                                                  [self processConnectionError];
                                                                                                                                                              }
                                                                                                                                                          }];
                                                                                                                     })
                                                                                                                     
                                                                                                                 } else {
                                                                                                                     [self processConnectionError];
                                                                                                                 }
                                                                                                             }];
                                                                     
                                                                 } else {
                                                                     [self processConnectionError];
                                                                 }
                                                             }];
                } else {
                    [ZFGetSingleVenueRequest requestWithVenueId:venue.uuid
                                                        summary:YES
                                                     completion:^(LORESTRequest *aRequest) {
                                                         if(aRequest.resultStatus == LORESTRequestResultStatusSucceeded) {
                                                             Hide_HudAndPerformBlock(^{
                                                                 [self refreshSelectedVenue:aRequest.result];
                                                                 contentView.userInteractionEnabled = YES;
                                                             })
                                                         } else {
                                                             [self processConnectionError];
                                                         }
                                                     }];
                }
            })
        } else {
            if(isLaunchFromLogin) {
                Show_ConnectionError
            }
            [UserService logout];
            [AppDelegate.navigationController popToRootViewControllerAnimated:YES];
        }
    }];
    
    Notification_Observe(UIApplicationDidBecomeActiveNotification, appDidBecomeActive);
    Notification_Observe(kNotification_SingleVenueRefreshed, singleVenueUpdated);
    Notification_Observe(kNotification_MemberVenueDetailsRefreshed, memberVenueDetailsUpdated);
    Notification_Observe(kNotification_SingleMemberDetailsRefreshed, singleMemberUpdated);
    Notification_Observe(kNotification_VenuesListRefreshed, venuesListUpdated);
    Notification_Observe(kNotification_VenueOccupantsRefreshed, occupantsUpdated);
    Notification_Observe(kNotification_AccountBalanceRefreshed, accountBalanceUpdated);
    Notification_Observe(kCMConnectivityManager_ConnectivityChanged, connectivityChanged);
    Notification_Observe(kNotification_NotificationsRefreshed, receivedNewerNotifications:);
    Notification_Observe(kNotification_UnreadMessageCountRefreshed, receivedUnreadMessageCount:);
    Notification_Observe(kNotification_FriendsRequestListRefreshed, receivedFriendsRequestCount:);
    Notification_Observe(kNotification_FriendsRequestListItemRemoved, receivedFriendsRequestCount:);
    
    currentLocation = kCLLocationCoordinate2DInvalid;
    LOLocationManager *locationManager = [LOLocationManager sharedInstance];
    if (locationManager.locationServicesEnabled) {
        [locationManager getCurrentLocationInBlock:^(CLLocationCoordinate2D aLocation) {
            currentLocation = aLocation;
        }];
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if(CurrentUser) {
        [NetworkService pollForType:ZFPollingRequestType_MemberVenueDetails|ZFPollingRequestType_SingleVenueDetails|ZFPollingRequestType_VenuesList|ZFPollingRequestType_VenueOccupants|ZFPollingRequestType_AccountBalance|ZFPollingRequestType_SingleMemberRequest|ZFPollingRequestType_NewerNotifications|ZFPollingRequestType_UnreadMessageCount|ZFPollingRequestType_PendingFriendsRequest
                           memberId:CurrentUser.uuid
                            venueId:selectedVenue.uuid
                            summary:YES];
        
        [self fetchNotificationsBadge];
        [self fetchMessagesBadge];
    } else {
        [NetworkService pollForType:ZFPollingRequestType_SingleVenueDetails|ZFPollingRequestType_VenueOccupants
                           memberId:nil
                            venueId:selectedVenue.uuid
                            summary:YES];
    }
}

- (void)dealloc {
    Notification_RemoveObserver;
}

#pragma mark -
#pragma mark Notifications

- (void)appDidBecomeActive {
    LOLocationManager *locationManager = [LOLocationManager sharedInstance];
    
    if (locationManager.locationServicesEnabled) {
        [locationManager getCurrentLocationInBlock:^(CLLocationCoordinate2D aLocation) {
            if (CLLocationCoordinate2DIsValid(aLocation)) {
                currentLocation = aLocation;
                [self refreshVenuesList:venuesList];
            }
        }];
    }
}

- (void)connectivityChanged {
    if(ConnectivityManager.isConnected) {
        [contentView.offlineView dismiss];
    } else {
        [contentView.offlineView present];
    }
}

- (void)accountBalanceUpdated {
    [contentView setupWithSelectedVenue:selectedVenue];
}

- (void)singleVenueUpdated {
    [self refreshSelectedVenue:NetworkService.singleVenue];
}

- (void)memberVenueDetailsUpdated {
    [self refreshMemberVenueDetails:CurrentUser.venueDetails];
}

- (void)singleMemberUpdated {
    [contentView setupWithSelectedVenue:selectedVenue];
}

- (void)venuesListUpdated {
    [self refreshVenuesList:NetworkService.venuesList];
}

- (void)occupantsUpdated
{
    [contentView refreshOccupants:NetworkService.occupants inVenue:selectedVenue]; 
}

- (void)receivedUnreadMessageCount:(NSNotification *)notification {
    [contentView refreshMessagesBadge:NetworkService.unreadMessageCount];
}

- (void)receivedFriendsRequestCount:(NSNotification *)notification {
    [contentView refreshFriendsBadge:NetworkService.unreadFriendsCount];
}

- (void)receivedNewerNotifications:(NSNotification *)notification {
    NSInteger unreadNotificationsBadgeNumber = NetworkService.newerNotifications.count;
    [contentView refreshNotificationsBadge:unreadNotificationsBadgeNumber];
}

- (void)fetchMessagesBadge {
    [ZFUnreadMessageCountRequest requestWithCompletion:^(LORESTRequest *aRequest) {
        if(aRequest.resultStatus == LORESTRequestResultStatusSucceeded) {
            NetworkService.unreadMessageCount = ((NSString *)aRequest.result).integerValue;
            Notification_Post(kNotification_UnreadMessageCountRefreshed);
            [UserDefaults setValue:[NSDate date] forKey:kLastContentUpdateDateKey];
        }
    }];
}
- (void)fetchNotificationsBadge {
    // newer notifications
    NSNumber *lastTimestamp = [UserDefaults objectForKey:kLastNotificationReadTimestamp];
    BOOL firstTime = lastTimestamp == nil;
    if (firstTime) {
        lastTimestamp = @([[NSDate date] timeIntervalSince1970]);
    }
    
    [ZFNotificationRequest requestWithTimestamp:[lastTimestamp doubleValue] older:firstTime completion:^(LORESTRequest *aRequest) {
        ZFNotificationRequest *notificationRequest = (ZFNotificationRequest *)aRequest;
        
        if(notificationRequest.resultStatus == LORESTRequestResultStatusSucceeded) {
            
            if ([notificationRequest.result count] > 0) {
                [ZFNetworkService sharedInstance].notificationsTimestamp = (long long)[((ZFNotificationContent *)[notificationRequest.result firstObject]).lastUpdated timeIntervalSince1970];
            }
            [ZFNetworkService sharedInstance].newerNotifications = notificationRequest.result;
            Notification_PostInfo(kNotification_NotificationsRefreshed, @{@"older": [NSNumber numberWithBool:notificationRequest.older]});
            [[ZFNetworkService sharedInstance] updateBadgeCount];
        }
    }];
}

#pragma mark -
#pragma mark Button actions

- (void)sortAZButtonPressed:(UIControl *)aControl {
    if(!aControl.isSelected) {
        [aControl setSelected:YES];
        [contentView.sortFavouriteButton setSelected:NO];
        [contentView.sortMembersButton setSelected:NO];
        [contentView.sortDistanceButton setSelected:NO];
    }
}

- (void)sortFavouriteButtonPressed:(UIControl *)aControl {
    if(!aControl.isSelected) {
        [aControl setSelected:YES];
        [contentView.sortAZButton setSelected:NO];
        [contentView.sortMembersButton setSelected:NO];
        [contentView.sortDistanceButton setSelected:NO];
    }
}

- (void)sortMembersButtonPressed:(UIControl *)aControl {
    if(!aControl.isSelected) {
        [aControl setSelected:YES];
        [contentView.sortAZButton setSelected:NO];
        [contentView.sortFavouriteButton setSelected:NO];
        [contentView.sortDistanceButton setSelected:NO];
    }
}

- (void)sortDistanceButtonPressed:(UIControl *)aControl {
    if(!aControl.isSelected) {
        [aControl setSelected:YES];
        [contentView.sortAZButton setSelected:NO];
        [contentView.sortFavouriteButton setSelected:NO];
        [contentView.sortMembersButton setSelected:NO];
    }
}

- (void)eventsButtonPressed
{
    ZFEventsViewController *eventsViewController = [[ZFEventsViewController alloc] initWithSelectedVenue:selectedVenue];
    [self.navigationController pushViewController:eventsViewController animated:YES];
}

- (void)poemButtonPressed {
    ZFPoemOverlayViewController *poemViewController = [[ZFPoemOverlayViewController alloc] init];
    if(!self.childViewControllers.count > 0) {
        [self presentChildController:poemViewController inView:contentView];
    }
}

- (void)conversationButtonPressed {
    
    Show_Hud(NSLocalizedString(@"Fetching data...",nil))
    [ZFGetConversationsListRequest requestWithPage:@(0) completion:^(LORESTRequest *aRequest) {
        if(aRequest.resultStatus == LORESTRequestResultStatusSucceeded) {
            ZFPagedResults *pagedResults = aRequest.result;
            NSArray *conversations = pagedResults.result;
            [ZFGetMemberFriendsListRequest requestWithMemberId:CurrentUser.uuid andVenueId:selectedVenue.uuid completion:^(LORESTRequest *aRequest) {
                if(aRequest.resultStatus == LORESTRequestResultStatusSucceeded) {
                    Hide_HudAndPerformBlock(^{
                        ZFConversationListViewController *conversationViewController = [[ZFConversationListViewController alloc] initWithMessages:conversations andFriendsList:aRequest.result];
                        [self.navigationController pushViewController:conversationViewController
                                                             animated:YES];
                    })
                } else {
                    Show_ConnectionError
                }
            }];
        } else {
            Show_ConnectionError
        }
    }];
}

- (void)discussionsButtonPressed {
    // Invisible button with two persons talking under "Our Events"
    // Currently not used.
    //
    //    ZFDiscussionViewController *discussionViewController = [[ZFDiscussionViewController alloc] init];
    //    [self.navigationController presentViewController:discussionViewController animated:YES completion:nil];
    //    [self.navigationController pushViewController:discussionViewController animated:YES];
}

- (void)newsButtonPressed {
    ZFNewsViewController *newsViewController = [[ZFNewsViewController alloc] initWithVenueId:selectedVenue.uuid];
    [self.navigationController pushViewController:newsViewController
                                         animated:YES];
}

- (void)friendsButtonPressed {
    [SVProgressHUD showWithStatus:NSLocalizedString(@"Fetching friends list...", nil) maskType:SVProgressHUDMaskTypeGradient];
    [ZFGetMemberFriendsListRequest requestWithMemberId:CurrentUser.uuid
                                            andVenueId:selectedVenue.uuid
                                            completion:^(LORESTRequest *aRequest) {
                                                if(aRequest.resultStatus == LORESTRequestResultStatusSucceeded) {
                                                    Hide_HudAndPerformBlock(^{
                                                        NSArray *friends = aRequest.result;
                                                        ZFFriendsListViewController *friendsViewController = [[ZFFriendsListViewController alloc] initWithFriendsList:friends];
                                                        [self.navigationController pushViewController:friendsViewController animated:YES];
                                                    })
                                                } else {
                                                    Show_ConnectionError
                                                }
                                            }];
}

- (void)leaderboardsButtonPressed {
    ZFLeaderBoardsViewController *leadersBoardViewController = [[ZFLeaderBoardsViewController alloc] initWithTitle:nil
                                                                                                           venueId:CurrentUser.selectedVenue.uuid];
    [self.navigationController pushViewController:leadersBoardViewController
                                         animated:YES];
}

- (void)notificationsButtonPressed {
    ZFNotificationsViewController *notificationsViewController = [[ZFNotificationsViewController alloc] initWithSelectedVenue:selectedVenue venuesList:venuesList];
    [self.navigationController pushViewController:notificationsViewController
                                         animated:YES];
}

- (void)profileButtonPressed
{
    ZFProfileViewController *profileViewController = [[ZFProfileViewController alloc] initWithUser:CurrentUser];
    [self.navigationController pushViewController:profileViewController animated:YES];
}

- (void)treasuryButtonPressed {
    
    ZFTreasuryViewController *treasuryViewController = [[ZFTreasuryViewController alloc] initWithSelectedVenue:selectedVenue
                                                                                                 andVenuesList:venuesList
                                                                                                   backMessage:NSLocalizedString(@"BACK TO ROOM", nil)];
	[self.navigationController pushViewController:treasuryViewController animated:YES];
}

- (void)achievementsButtonPressed {
    ZFAchievementsViewController *achievementsViewController = [[ZFAchievementsViewController alloc] initWithUser:CurrentUser];
    
    [self.navigationController pushViewController:achievementsViewController animated:YES];
}

- (void)experiencePointsButtonPressed {
    ZFExperiencePointsViewController *experienceViewController = [ZFExperiencePointsViewController new];
    [self.navigationController pushViewController:experienceViewController
                                         animated:YES];
}

- (void)whoIsZiferblatButtonPressed {
    [SVProgressHUD showWithStatus:NSLocalizedString(@"Getting occupants", nil)  maskType:SVProgressHUDMaskTypeGradient];
    [ZFGetOccupantsForVenueRequest requestWithVenueId:selectedVenue.uuid
                                           completion:^(LORESTRequest *aRequest) {
                                               if(aRequest.resultStatus == LORESTRequestResultStatusSucceeded) {
                                                   Hide_HudAndPerformBlock(^{
                                                       NSArray *occupants = aRequest.result;
                                                       selectedVenue.occupants = [occupants sortedArrayUsingComparator:^NSComparisonResult(ZFUser *occupant1, ZFUser *occupant2) {
                                                           return [occupant1.firstName compare:occupant2.firstName];
                                                       }];
                                                       ZFWhoIsAtZiferblatViewController *whoIsAtZiferblatController = [[ZFWhoIsAtZiferblatViewController alloc] initWithVenue:selectedVenue backMessage:NSLocalizedString(@"BACK TO ROOM", nil)];
                                                       [self.navigationController pushViewController:whoIsAtZiferblatController animated:YES];
                                                   })
                                                   
                                               } else {
                                                   Show_ConnectionError
                                               }
                                           }];
}

- (void)lastPhotoButtonPressed {
    ZFLastPhotoViewController *lastPhotoViewController = [[ZFLastPhotoViewController alloc] init];
    [self.navigationController pushViewController:lastPhotoViewController
                                         animated:YES];
}

- (void)menuButtonPressed:(UIButton *)aButton forEvent:(UIEvent *)aEvent {
    NSSet *touches = [aEvent touchesForView:aButton];
    UITouch *touch = [touches anyObject];
    CGPoint touchPoint = [touch locationInView:aButton];
    
    if(touchPoint.x < kInfoButtonXLimit) {
        [self ziferblatInfoButtonPressed];
        
    } else if(!CGRectContainsPoint(kInfoButtonEmptyRect, touchPoint)) {
        CGFloat xContentOffset = roundf(contentView.scrollView.contentOffset.x);
        CGFloat defaultOffset = roundf(contentView.scrollView.contentSize.width/2-contentView.scrollView.width/2);
        CGFloat animationDuration = (xContentOffset > defaultOffset || xContentOffset < defaultOffset) ? 0.3 : 0.0;
        
        [UIView animateWithDuration:animationDuration
                         animations:^{
                             [contentView.scrollView scrollRectToVisible:CGRectMake(defaultOffset, 0,
                                                                                    contentView.scrollView.width,
                                                                                    contentView.scrollView.height)
                                                                animated:NO];
                         } completion:^(BOOL finished) {
                             if(aButton.isSelected) {
                                 [contentView dismissSwitchMenuAndPerformBlock:^{
                                     [aButton setSelected:NO];
                                 }];
                             } else {
                                 [contentView presentSwitchMenuAndPerformBlock:^{
                                     [aButton setSelected:YES];
                                 }];
                             }
                         }];
    }
}

- (void)ziferblatInfoButtonPressed {
    ZFVenue *currentVenue = [venuesList objectAtIndex:selectedVenueIndex];
    
    [SVProgressHUD showWithStatus:NSLocalizedString(@"Getting venue details", nil) maskType:SVProgressHUDMaskTypeGradient];
    [ZFGetSingleVenueRequest requestWithVenueId:currentVenue.uuid
                                        summary:NO
                                     completion:^(LORESTRequest *aRequest) {
                                         if(aRequest.resultStatus == LORESTRequestResultStatusSucceeded) {
                                             Hide_HudAndPerformBlock(^{
                                                 selectedVenue = aRequest.result;
                                                 [self pushZiferblatDetailsViewControllerWithVenue:selectedVenue];
                                             })
                                         } else {
                                             Show_ConnectionError
                                         }
                                     }];
}

- (void)activitiesButtonPressed {
    ZFActivitiesViewController *activitiesViewController = [[ZFActivitiesViewController alloc] initWithSelectedVenue:selectedVenue];
    [self.navigationController pushViewController:activitiesViewController
                                         animated:YES];
}

- (void)timeHistoryButtonPressed {
    ZFTimeHistoryViewController *timeHistoryViewController = [[ZFTimeHistoryViewController alloc] init];
    [self.navigationController pushViewController:timeHistoryViewController animated:YES];
}

- (void)goButtonPressed:(UIButton *)aButton {
    ZFVenue *venue;
    NSUInteger index = 0;
    for(ZFVenue *currentVenue in venuesList) {
        if(currentVenue.uuid.integerValue == aButton.tag) {
            venue = currentVenue;
            break;
        }
        index++;
    }
    
    if (![venue.uuid isEqualToNumber:selectedVenue.uuid]) {
        [SVProgressHUD showWithStatus:NSLocalizedString(@"Switching Ziferblat", nil)
                             maskType:SVProgressHUDMaskTypeGradient];
        [ZFGetSingleVenueRequest requestWithVenueId:venue.uuid
                                            summary:YES
                                         completion:^(LORESTRequest *aRequest) {
                                             if(aRequest.resultStatus == LORESTRequestResultStatusSucceeded) {
                                                 selectedVenue = aRequest.result;
                                                 [[ZFUserService sharedInstance] updateSelectedVenueIdentifier:selectedVenue.uuid];
                                                 NetworkService.venueId = selectedVenue.uuid;
                                                 [ZFGetMemberVenueDetailsRequest requestWithMemberId:CurrentUser.uuid
                                                                                             venueId:venue.uuid
                                                                                          completion:^(LORESTRequest *aRequest) {
                                                                                              if(aRequest.resultStatus == LORESTRequestResultStatusSucceeded) {
                                                                                                  Hide_HudAndPerformBlock(^{
                                                                                                      [self refreshMemberVenueDetails:aRequest.result];
                                                                                                      selectedVenueIndex = index;
                                                                                                      [self dismissSwitcherDropdown];
                                                                                                      [contentView setupWithSelectedVenue:selectedVenue];
                                                                                                      [contentView.venuesTableView reloadData];
                                                                                                  })
                                                                                              } else {
                                                                                                  [self processConnectionError];
                                                                                              }
                                                                                          }];
                                             } else {
                                                 [self processConnectionError];
                                             }
                                         }];
    }
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    switch (tableView.tag) {
        case ZFHomeView_TableViewType_Venues:
            return venuesList.count;
            
        case ZFHomeView_TableViewType_Events:
            return MIN(selectedVenue.events.count, 5);
            
        case ZFHomeView_TableViewType_News:
            return MIN(selectedVenue.news.count, 3);
            
        case ZFHomeView_TableViewType_Activities:
            return MIN(selectedVenue.activities.count, 5);
            
        default:
            return 0;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (tableView.tag) {
        case ZFHomeView_TableViewType_Venues:
            return ZFVenueCellHeight;
            
        case ZFHomeView_TableViewType_Events:
            return ZFEventCellHeight;
            
        case ZFHomeView_TableViewType_News:
            return ZFNewsCellHeight;
            
        case ZFHomeView_TableViewType_Activities:
            return ZFActivityCellHeight;
            
        default:
            return 0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell;
    switch (tableView.tag) {
        case ZFHomeView_TableViewType_Venues:
        {
            cell = [tableView dequeueReusableCellWithIdentifier:[ZFVenueCell reuseIdentifier]];
            if(!cell) {
                cell = [ZFVenueCell cell];
                [((ZFVenueCell *)cell).goButton addTarget:self
                                                   action:@selector(goButtonPressed:)
                                         forControlEvents:UIControlEventTouchUpInside];
            }
            
            ZFVenue *venue = [venuesList objectAtIndex:indexPath.row];
            [(ZFVenueCell *)cell setupWithVenue:venue
                                    isUserVenue:(CurrentUser.currentVenueId && [venue.uuid isEqualToNumber:CurrentUser.currentVenueId])
                                     isSelected:(indexPath.row == selectedVenueIndex)
                                     isLastCell:(indexPath.row == venuesList.count-1)];
            
            return cell;
        }
            
        case ZFHomeView_TableViewType_Events:
        {
            cell = [tableView dequeueReusableCellWithIdentifier:[ZFEventCell reuseIdentifier]];
            if(!cell) {
                cell = [ZFEventCell cell];
            }
            
            ZFEvents *event = [selectedVenue.events objectAtIndex:indexPath.row];
            
            BOOL separatorVisible = YES;
            NSUInteger eventsCount = MIN(selectedVenue.events.count, 5);
            if(indexPath.row < eventsCount-1) {
                ZFEvents *nextEvent = [selectedVenue.events objectAtIndex:indexPath.row+1];
                separatorVisible = ![event.eventDate isEqualToDateIgnoringTime:nextEvent.eventDate];
            } else {
                separatorVisible = NO;
            }
            
            [(ZFEventCell *)cell setupWithEvent:event isSeparatorVisible:separatorVisible];
            
            return cell;
        }
            
        case ZFHomeView_TableViewType_News:
        {
            cell = [tableView dequeueReusableCellWithIdentifier:[ZFNewsCell reuseIdentifier]];
            if(!cell) {
                cell = [ZFNewsCell cell];
            }
            
            NSUInteger newsCount = MIN(selectedVenue.news.count, 3);
            ZFNews *news = [selectedVenue.news objectAtIndex:indexPath.row];
            [(ZFNewsCell *)cell setupWithNews:news isSeparatorVisible:(indexPath.row < newsCount-1)];
            
            return cell;
        }
            
        case ZFHomeView_TableViewType_Activities:
        {
            cell = [tableView dequeueReusableCellWithIdentifier:[ZFActivityCell reuseIdentifier]];
            if(!cell) {
                cell = [ZFActivityCell cell];
            }
            
            NSUInteger activitiesCount = MIN(selectedVenue.activities.count, 5);
            ZFActivity *activity = [selectedVenue.activities objectAtIndex:indexPath.row];
            [(ZFActivityCell *)cell setupWithActivity:activity isSeparatorVisible:(indexPath.row < activitiesCount-1)];
            
            return cell;
        }
        default:
            return nil;
    }
}


#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    ZFUser *user = CurrentUser;
    return MIN(user.venueDetails.achievements.count, 9);
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

#pragma mark -
#pragma mark UICollectionViewDelegateFlowLayout

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    ZFHomeAchievementCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:[ZFHomeAchievementCollectionViewCell reuseIdentifier]
                                                                                          forIndexPath:indexPath];
    ZFAchievementLevel *achievementLevel = [CurrentUser.venueDetails.achievements objectAtIndex:indexPath.row];
    [cell setupWithImageAtUrl:achievementLevel.achievement.imageUrl];
    return cell;
}

#pragma mark - ZFHomeSortDelegate

- (void)didSortVenues:(SortButtonType)sortType {
    currentSortButtonType = sortType;
    [self sortVenuesList];
}

#pragma mark -
#pragma mark Utils

- (void)sortVenuesList {
    switch (currentSortButtonType) {
        case SortButtonTypeAZ: {
            venuesList = [venuesList sortedArrayUsingComparator:^NSComparisonResult(ZFVenue* _Nonnull venue1, ZFVenue*  _Nonnull venue2)
            {

                return [venue1.name compare:venue2.name];

            }];
            break;
        }
        case SortButtonTypeFavourites: {
            venuesList = [venuesList sortedArrayUsingComparator:^NSComparisonResult(ZFVenue* _Nonnull venue1, ZFVenue*  _Nonnull venue2)
            {
                if([venue1.name isEqualToString:@"Глобальный Циферблат"] || [venue1.name isEqualToString:@"Ziferblat World"])
                {
                    return NSOrderedDescending;
                }
                else if([venue2.name isEqualToString:@"Глобальный Циферблат"] || [venue2.name isEqualToString:@"Ziferblat World"])
                {
                    return NSOrderedAscending;
                }
                
                if (!venue1.minutesSpent && !venue2.minutesSpent) {
                    return [venue1.name compare:venue2.name];
                } else if (!venue1.minutesSpent) {
                    return NSOrderedDescending;
                } else if (!venue2.minutesSpent) {
                    return NSOrderedAscending;
                } else {
                    NSComparisonResult comparisonResult = [venue2.minutesSpent compare:venue1.minutesSpent];
                    if (comparisonResult == NSOrderedSame) {
                        return [venue1.name compare:venue2.name];
                    } else {
                        return comparisonResult;
                    }
                }
            }];
            break;
        }
        case SortButtonTypeMembers: {
            venuesList = [venuesList sortedArrayUsingComparator:^NSComparisonResult(ZFVenue* _Nonnull venue1, ZFVenue*  _Nonnull venue2)
            {
                if([venue1.name isEqualToString:@"Глобальный Циферблат"] || [venue1.name isEqualToString:@"Ziferblat World"])
                {
                    return NSOrderedDescending;
                }
                else if([venue2.name isEqualToString:@"Глобальный Циферблат"] || [venue2.name isEqualToString:@"Ziferblat World"])
                {
                    return NSOrderedAscending;
                }
                
                if (!venue1.memberCount && !venue2.memberCount) {
                    return [venue1.name compare:venue2.name];
                } else if (!venue1.memberCount) {
                    return NSOrderedDescending;
                } else if (!venue2.memberCount) {
                    return NSOrderedAscending;
                } else {
                    NSComparisonResult comparisonResult = [venue2.memberCount compare:venue1.memberCount];
                    if (comparisonResult == NSOrderedSame) {
                        return [venue1.name compare:venue2.name];
                    } else {
                        return comparisonResult;
                    }
                }
            }];
            break;
        }
        case SortButtonTypeDistance: {
            venuesList = [venuesList sortedArrayUsingComparator:^NSComparisonResult(ZFVenue* _Nonnull venue1, ZFVenue*  _Nonnull venue2)
            {
                if([venue1.name isEqualToString:@"Глобальный Циферблат"] || [venue1.name isEqualToString:@"Ziferblat World"])
                {
                    return NSOrderedDescending;
                }
                else if([venue2.name isEqualToString:@"Глобальный Циферблат"] || [venue2.name isEqualToString:@"Ziferblat World"])
                {
                    return NSOrderedAscending;
                }
                
                NSNumber *venue1Distance = [NSNumber numberWithDouble:[venue1 distanceFromCurrentLocation]];
                NSNumber *venue2Distance = [NSNumber numberWithDouble:[venue2 distanceFromCurrentLocation]];
                
                NSComparisonResult comparisonResult = [venue1Distance compare:venue2Distance];
                if (comparisonResult == NSOrderedSame) {
                    return [venue1.name compare:venue2.name];
                } else {
                    return comparisonResult;
                }
            }];
            break;
        }
        default:
            break;
    }
    [self updateSelectedVenueId];
    [contentView.venuesTableView reloadData];
}

- (void)refreshMemberVenueDetails:(ZFMemberVenueDetails *)aMemberVenueDetails {
    CurrentUser.venueDetails = aMemberVenueDetails;
    [contentView.achievementCollectionView reloadData];
}

- (void)refreshVenuesList:(NSArray *)venues {
    venuesList = venues;
    ZiferblatService.allZiferblats = [[NSArray alloc] initWithArray:venuesList];
    
    if (![LOLocationManager sharedInstance].locationServicesEnabled && CurrentUser.currentVenueId) {
        if (![CurrentUser.currentVenueId isEqualToNumber:@0]) {
            NSPredicate *venueIdPreciate = [NSPredicate predicateWithFormat:@"uuid == %@", CurrentUser.currentVenueId];
            ZFVenue *currentVenue = [[venuesList filteredArrayUsingPredicate:venueIdPreciate] firstObject];
            currentLocation = currentVenue.coordinate;
        } else {
            currentLocation = kCLLocationCoordinate2DInvalid;
        }
    }
    

    if (CLLocationCoordinate2DIsValid(currentLocation)) {
        for (ZFVenue *venue in venuesList) {
            [ZFVenue setDistanceFromCurrentLocationForVenue:venue userLocation:currentLocation];
        }
    }
    [self sortVenuesList];
    
    for(ZFVenue *venue in venuesList) {
        if(CurrentUser.favouriteVenueId && [venue.uuid isEqualToNumber:CurrentUser.favouriteVenueId]) {
            CurrentUser.favoriteVenue = venue;
        }
    }
    [contentView.venuesTableView reloadData];
}

- (void)refreshSelectedVenue:(ZFVenue *)aVenue {
    selectedVenue = aVenue;
    [[ZFUserService sharedInstance] updateSelectedVenueIdentifier:selectedVenue.uuid];
    [ZFUserService sharedInstance].loggedInUser.selectedVenue = selectedVenue;
    [contentView setupWithSelectedVenue:selectedVenue];
}

- (void)updateSelectedVenueId {
    for (NSInteger index = 0; index < venuesList.count; index++) {
        ZFVenue *venue = [venuesList objectAtIndex:index];
        if ([venue.uuid isEqual:selectedVenue.uuid]) {
            selectedVenueIndex = index;
            break;
        }
    }
}

- (void)processConnectionError
{
    Show_ConnectionError
    [UserService logout];
    [AppDelegate.navigationController popToRootViewControllerAnimated:YES];
}

- (void)pushZiferblatDetailsViewControllerWithVenue:(ZFVenue *)venue {
    ZFZiferblatDetailsViewController *ziferblatDetailsViewController = [[ZFZiferblatDetailsViewController alloc]
                                                                        initWithVenue:venue];
    [self.navigationController pushViewController:ziferblatDetailsViewController
                                         animated:YES];
}

- (void)dismissSwitcherDropdown {
    CGFloat xContentOffset = roundf(contentView.scrollView.contentOffset.x);
    CGFloat defaultOffset = roundf(contentView.scrollView.contentSize.width/2-contentView.scrollView.width/2);
    CGFloat animationDuration = (xContentOffset > defaultOffset || xContentOffset < defaultOffset) ? 0.3 : 0.0;
    
    [UIView animateWithDuration:animationDuration
                     animations:^{
                         [contentView.scrollView scrollRectToVisible:CGRectMake(defaultOffset, 0,
                                                                                contentView.scrollView.width,
                                                                                contentView.scrollView.height)
                                                            animated:NO];
                     } completion:^(BOOL finished) {
                         [contentView dismissSwitchMenuAndPerformBlock:^{
                             [contentView.menuButton setSelected:NO];
                         }];
                     }];
}

@end