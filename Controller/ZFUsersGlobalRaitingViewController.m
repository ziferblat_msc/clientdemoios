//
//  ZFUsersGlobalRaitingViewController.m
//  Ziferblat
//
//  Created by Terry Michel on 08/04/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "ZFUsersGlobalRaitingViewController.h"

@implementation ZFUsersGlobalRaitingViewController

#pragma mark - 
#pragma mark Lifecycle

- (void)loadView {
    contentView = [[ZFUsersGlobalRaitingView alloc] init];
    self.view = contentView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    TARGET_TOUCH_UP(contentView.backButton, @selector(backButtonTapped))
    TARGET_TOUCH_UP(contentView.filterLastDaysButton, @selector(filterLastDaysButtonTapped))
    TARGET_TOUCH_UP(contentView.filterAllTimeButton, @selector(filterAllTimeButtonTapped))
    TARGET_TOUCH_UP(contentView.searchButton, @selector(searchButtonTapped))
}

#pragma mark -
#pragma mark Buttons actions

- (void)backButtonTapped {
    #warning TO IMPLEMENT
}

- (void)filterLastDaysButtonTapped {
    #warning TO IMPLEMENT
}

- (void)filterAllTimeButtonTapped {
    #warning TO IMPLEMENT
}

- (void)searchButtonTapped {
    #warning TO IMPLEMENT
}

@end
