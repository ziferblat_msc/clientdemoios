//
//  ZFEventsArchiveViewController.h
//  Ziferblat
//
//  Created by Terry Michel on 14/04/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "ZFEventsArchiveView.h"

@interface ZFEventsArchiveViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>
{
    ZFEventsArchiveView *contentView;
}

@end
