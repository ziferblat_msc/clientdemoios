//
//  ZFSubscriptionSelectionViewController.m
//  Ziferblat
//
//  Created by Terry Michel on 08/04/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "ZFSubscriptionSelectionViewController.h"
#import "ZFBaseView.h"
#import "ZFButton.h"
#import "ZFSubscriptionZiferblatSelectionCell.h"
#import "ZFVenue.h"
#import "ZFPurchaseSubscriptionRequest.h"
#import "ZFSubscriptionPurchase.h"
#import "ZFGetSubscriptionForVenueRequest.h"
#import "ZFListAllVenuesRequest.h"
#import "ZFTreasuryViewController.h"
#import "ZFBackButton.h"

@implementation ZFSubscriptionSelectionViewController

#define kSubscriptionNumberDays   30

#pragma mark -
#pragma mark Lifecycle

- (instancetype)initWithType:(ZFSubscriptionType)aType {
    self = [super init];
    if(self) {
        type = aType;
        ziferblatsSelected = [NSMutableArray array];
    }
    return self;
}

- (void)loadView
{
    contentView = [[ZFBaseView alloc] init];
    self.view = contentView;
    UIImage *image = [UIImage imageNamed:@"ZFSubscriptionSelectionView"];
    contentView.scrollView.contentSize = CGSizeMake(image.size.width, contentView.height);
    contentView.backgroundImageView.frame = CGRectMake(0, 0, image.size.width, image.size.height);
    contentView.backgroundImageView.image = image;
    
    ZFBackButton *backButton = [ZFBackButton backButtonWithTitle:NSLocalizedString(@"BACK TO MY TREASURY", nil)];
    [self.view addSubview:backButton];
    TARGET_TOUCH_UP(backButton, @selector(backButtonPressed))
    contentView.backgroundImageView.top = backButton.bottom;
    
    UILabel *backLabel = [[UILabel alloc] init];
    [backLabel setText:NSLocalizedString(@"BACK TO SUBSCRIPTION", nil)];
    [backLabel setFont:FontRockWell(notIPad ? 12:29)];
    [backLabel sizeToFit];
    [backLabel setCenter:backButton.center];
    backLabel.top -= 2;
    [kScrollView addSubview:backLabel];
    
    UILabel *titlLabel = [[UILabel alloc] init];
    [titlLabel setText:NSLocalizedString(@"SUBSCRIPTIONS",nil)];
    [titlLabel setFont:FontRockWell(notIPad ? 22:53)];
    [titlLabel setTextAlignment:NSTextAlignmentCenter];
    titlLabel.frame = (notIPad ? CGRectMake(46, 40, 223, 28):CGRectMake(110, 79, 535, 70));
    [kScrollView addSubview:titlLabel];
    
    UILabel *youHaveLabel = [[UILabel alloc] init];
    [youHaveLabel setText:NSLocalizedString(@"YOU HAVE",nil)];
    [youHaveLabel setFont:FontRockWell(notIPad ? 18:43)];
    [youHaveLabel setTextAlignment:NSTextAlignmentCenter];
    youHaveLabel.frame = (notIPad ? CGRectMake(92, 90, 138, 28):CGRectMake(221, 199, 331, 70));
    [kScrollView addSubview:youHaveLabel];
    
    NSUInteger piastresToDisplay = 0;
    
    if([CurrentUser.accountBalance respondsToSelector:@selector(piastres)])
    {
        piastresToDisplay = CurrentUser.accountBalance.piastres.integerValue;
    }
    
    piastresLabel = [[UILabel alloc] init];
    [piastresLabel setText:[NSString stringWithFormat:NSLocalizedString(@"%d PIASTRES",@"Do not translate"),piastresToDisplay]];
    [piastresLabel setFont:FontRockWell(notIPad ? 21:50)];
    piastresLabel.adjustsFontSizeToFitWidth = YES;
    piastresLabel.frame = (notIPad ? CGRectMake(46, 133, 224, 29):CGRectMake(110, 302, 538, 70));
    [piastresLabel setTextAlignment:NSTextAlignmentCenter];
    [kScrollView addSubview:piastresLabel];
    
    ZFButton *unlimitedButton = [ZFButton buttonWithTitle:NSLocalizedString(@"UNLIMITED",nil)];
    unlimitedButton.left = (notIPad ? kLargePadding:230);
    unlimitedButton.top = (notIPad ? 154:370) + contentView.backgroundImageView.top;
    unlimitedButton.tag = ZFSubscriptionType_Unlimited;
    [contentView.scrollView addSubview:unlimitedButton];
    TARGET_TOUCH_UP(unlimitedButton, @selector(toggleButtonPressed:))
    
    workingButton = [ZFButton buttonWithTitle:NSLocalizedString(@"WORKING",nil)];
    workingButton.right = contentView.scrollView.width - (notIPad ? kLargePadding:230);
    workingButton.top = unlimitedButton.top;
    workingButton.tag = ZFSubscriptionType_Working;
    [kScrollView addSubview:workingButton];
    TARGET_TOUCH_UP(workingButton, @selector(toggleButtonPressed:));
    
    [unlimitedButton addButtonInToggle:workingButton];
    [workingButton addButtonInToggle:unlimitedButton];
    [unlimitedButton setSelected:(type == ZFSubscriptionType_Unlimited)];
    [workingButton setSelected:(type == ZFSubscriptionType_Working)];
    
    descriptionLabel = [[UILabel alloc] init];
    descriptionLabel.width = (notIPad ? workingButton.right - unlimitedButton.left - kPadding*2 : 568);
    descriptionLabel.top = unlimitedButton.bottom + kPadding;
    descriptionLabel.left = (notIPad ? unlimitedButton.left + kPadding: 100);
    descriptionLabel.numberOfLines = 0;
    descriptionLabel.adjustsFontSizeToFitWidth = YES;
    [kScrollView addSubview:descriptionLabel];
    
    descriptionSeparator = [UIImageView imageViewWithImageNamed:RESOURCE_DIVIDER];
    descriptionSeparator.left = kLargePadding+kPadding;
    descriptionSeparator.width = contentView.scrollView.width - (kLargePadding+kPadding)*2;
    [kScrollView addSubview:descriptionSeparator];
    
    selectZiferblatLabel = [[UILabel alloc] init];
    [selectZiferblatLabel setFont:FontRockWell(notIPad ? 13:31)];
    [selectZiferblatLabel setTextAlignment:NSTextAlignmentCenter];
    selectZiferblatLabel.numberOfLines = 0;
    [selectZiferblatLabel setText:NSLocalizedString(@"Select the Ziferblat(s) You wish\nto subscribe to:",nil)];
    [selectZiferblatLabel sizeToFit];
    [kScrollView addSubview:selectZiferblatLabel];
    [selectZiferblatLabel centerHorizontallyInSuperView];
    
    chooseAllButton = [UIButton buttonWithType:UIButtonTypeCustom];
    chooseAllButton.frame = (notIPad ? CGRectMake(0, selectZiferblatLabel.bottom+kSmallPadding, 124, 28):CGRectMake(0, selectZiferblatLabel.bottom+12, 298, 67));
    [chooseAllButton setTitle:NSLocalizedString(@"CHOOSE ALL",nil) forState:UIControlStateNormal];
    [chooseAllButton setTitle:NSLocalizedString(@"CHOOSE ALL",nil) forState:UIControlStateSelected];
    chooseAllButton.titleLabel.adjustsFontSizeToFitWidth = YES;
    chooseAllButton.titleLabel.minimumScaleFactor = 0.5;
    [chooseAllButton setTitleColor:Colour_Black forState:UIControlStateNormal];
    chooseAllButton.titleLabel.font = FontRockWell(notIPad ? 16:38);
    chooseAllButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    [chooseAllButton setBackgroundImage:[UIImage imageNamed:RESOURCE_SUBSCRIPTION_CHOOSE_ALL_BUTTON] forState:UIControlStateNormal];
    [kScrollView addSubview:chooseAllButton];
    [chooseAllButton centerHorizontallyInSuperView];
    TARGET_TOUCH_UP(chooseAllButton, @selector(chooseAllButtonPressed))
    
    subscriptionTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, chooseAllButton.bottom + (notIPad ? 5:12), contentView.scrollView.width, (notIPad ? 156:374))];
    subscriptionTableView.dataSource = self;
    subscriptionTableView.delegate = self;
    subscriptionTableView.allowsMultipleSelection = YES;
    subscriptionTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [subscriptionTableView setScrollEnabled:NO];
    [kScrollView addSubview:subscriptionTableView];
    
    getItButton = [ZFButton buttonWithTitle:NSLocalizedString(@"GET IT !",nil)];
    [kScrollView addSubview:getItButton];
    [getItButton centerHorizontallyInSuperView];
    [getItButton addTarget:self action:@selector(getItButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    
    summaryArray = [[NSMutableArray alloc]init];
    [self getSummarys];
    [self updateLayout];
    [self checkStatus];
    
    [NetworkService pollForType:ZFPollingRequestType_AccountBalance memberId:CurrentUser.uuid venueId:nil summary:YES];
    Notification_Observe(kNotification_AccountBalanceRefreshed, updateBalanceLabel)
}

- (void)dealloc {
    Notification_RemoveObserver
}

#pragma mark -
#pragma mark Layout

- (void)updateLayout {
    CGFloat fontSize = (notIPad ? 12.5:30);
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc]initWithString:@""];
    if (ziferblatsSelected.count > 0) {
        [attributedString appendAttributedString:[[NSAttributedString alloc] initWithString:NSLocalizedString(@"Pass at",nil)
                                                                                 attributes:@{NSFontAttributeName:FontRockWell(fontSize),
                                                                                              NSForegroundColorAttributeName:Colour_AppLightGray}]];
        int i = 0;
        for(ZFVenueSummary *ziferblatSummary in ziferblatsSelected) {
            if(i != 0) {
                [attributedString appendAttributedString:[[NSAttributedString alloc] initWithString:@" + "
                                                                                         attributes:@{NSFontAttributeName:FontRockWell(fontSize),
                                                                                                      NSForegroundColorAttributeName:Colour_AppLightGray}]];
            }
            [attributedString appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@" %@ ", ziferblatSummary.name]
                                                                                     attributes:@{NSFontAttributeName:FontRockWell(fontSize),
                                                                                                  NSForegroundColorAttributeName:Colour_Black}]];
            i++;
        }
        
        if (ziferblatsSelected.count > 1) {
            [attributedString appendAttributedString:[[NSAttributedString alloc] initWithString:NSLocalizedString(@"for\n",@"Subscription")
                                                                                     attributes:@{NSFontAttributeName:FontRockWell(fontSize),
                                                                                                  NSForegroundColorAttributeName:Colour_AppLightGray}]];
        } else {
            [attributedString appendAttributedString:[[NSAttributedString alloc] initWithString:NSLocalizedString(@"for",@"Subscriptuon")
                                                                                     attributes:@{NSFontAttributeName:FontRockWell(fontSize),
                                                                                                  NSForegroundColorAttributeName:Colour_AppLightGray}]];
        }
        [attributedString appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@" %d ", kSubscriptionNumberDays]
                                                                                 attributes:@{NSFontAttributeName:FontRockWell(fontSize),
                                                                                              NSForegroundColorAttributeName:Colour_Black}]];
        
        if (ziferblatsSelected.count == 1) {
            ZFVenueSummary *summary = [ziferblatsSelected objectAtIndex:0];
            [attributedString appendAttributedString:[[NSAttributedString alloc] initWithString:(type == ZFSubscriptionType_Unlimited ? NSLocalizedString(@"days for just\n",nil) : [NSString stringWithFormat:NSLocalizedString(@"days from\n%@ to %@ for just ",nil) ,[[self dateFormater] stringFromDate:summary.workingSubscriptionStartTime],[[self dateFormater] stringFromDate:summary.workingSubscriptionEndTime]])
                                                                                     attributes:@{NSFontAttributeName:FontRockWell(fontSize),
                                                                                                  NSForegroundColorAttributeName:Colour_AppLightGray}]];
        } else if(ziferblatsSelected.count > 1) {
            NSMutableArray *arrayOfStartDates = [NSMutableArray array];
            NSMutableArray *arrayOfEndDates = [NSMutableArray array];
            for (ZFVenueSummary *summary in ziferblatsSelected) {
                [arrayOfStartDates addObject:summary.workingSubscriptionStartTime];
                [arrayOfEndDates addObject:summary.workingSubscriptionEndTime];
            }
#warning Need to clarify whether the following times are correct.
            NSArray *descendingStartTimes = [arrayOfStartDates sortedArrayUsingDescriptors:@[[[NSSortDescriptor alloc] initWithKey:@"self" ascending:NO]]];
            NSArray *ascendingEndTimes = [arrayOfEndDates sortedArrayUsingDescriptors:@[[[NSSortDescriptor alloc] initWithKey:@"self" ascending:YES]]];
            
            [attributedString appendAttributedString:[[NSAttributedString alloc] initWithString:(type == ZFSubscriptionType_Unlimited ? NSLocalizedString(@"days for just ",nil) :
                                                                                                 [NSString stringWithFormat:NSLocalizedString(@"days from %@ to %@ for just ",nil) ,
                                                                                                  [[self dateFormater] stringFromDate:[descendingStartTimes firstObject]],
                                                                                                  [[self dateFormater] stringFromDate:[ascendingEndTimes firstObject]]])
                                                                                     attributes:@{NSFontAttributeName:FontRockWell(fontSize),
                                                                                                  NSForegroundColorAttributeName:Colour_AppLightGray}]];
        }
        
        NSArray *sortedArray = [NSArray arrayWithArray:ziferblatsSelected];
        if (ziferblatsSelected.count > 1) {
            sortedArray = [sortedArray sortedArrayUsingComparator:^NSComparisonResult(ZFVenueSummary *summary1, ZFVenueSummary *summary2) {
                if (type == ZFSubscriptionType_Unlimited) {
                    return summary1.unlimitedSubscriptionCost.floatValue > summary2.unlimitedSubscriptionCost.floatValue;
                } else {
                    
                    return summary1.workingSubscriptionCost.floatValue > summary2.workingSubscriptionCost.floatValue;
                    
                }
            }];
            
            if (type == ZFSubscriptionType_Unlimited) {
                
                [attributedString appendAttributedString:[[NSAttributedString alloc] initWithString:[[(ZFVenueSummary *)[sortedArray objectAtIndex:sortedArray.count - 1] unlimitedSubscriptionCost] stringValue]
                                                                                         attributes:@{NSFontAttributeName:FontRockWell(fontSize),
                                                                                                      NSForegroundColorAttributeName:Colour_Black}]];
            } else {
                [attributedString appendAttributedString:[[NSAttributedString alloc] initWithString:[[(ZFVenueSummary *)[sortedArray objectAtIndex:sortedArray.count - 1] workingSubscriptionCost] stringValue]
                                                                                         attributes:@{NSFontAttributeName:FontRockWell(fontSize),
                                                                                                      NSForegroundColorAttributeName:Colour_Black}]];
            }
        } else {
            if (type == ZFSubscriptionType_Unlimited && ziferblatsSelected.count > 0) {
                
                [attributedString appendAttributedString:[[NSAttributedString alloc] initWithString:[[(ZFVenueSummary *)[ziferblatsSelected objectAtIndex:0] unlimitedSubscriptionCost] stringValue]
                                                                                         attributes:@{NSFontAttributeName:FontRockWell(fontSize),
                                                                                                      NSForegroundColorAttributeName:Colour_Black}]];
            } else if (type == ZFSubscriptionType_Working && ziferblatsSelected.count > 0) {
                
                [attributedString appendAttributedString:[[NSAttributedString alloc] initWithString:[[(ZFVenueSummary *)[ziferblatsSelected objectAtIndex:0] workingSubscriptionCost] stringValue]
                                                                                         attributes:@{NSFontAttributeName:FontRockWell(fontSize),
                                                                                                      NSForegroundColorAttributeName:Colour_Black}]];
            }
        }
        
        [attributedString appendAttributedString:[[NSAttributedString alloc] initWithString:NSLocalizedString(@" piastres",nil)
                                                                                 attributes:@{NSFontAttributeName:FontRockWell(fontSize),
                                                                                              NSForegroundColorAttributeName:Colour_AppLightGray}]];
    }
    
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setLineSpacing:kSmallPadding];
    [paragraphStyle setAlignment:NSTextAlignmentCenter];
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [attributedString length])];
    descriptionLabel.attributedText = attributedString;
    [descriptionLabel sizeToWidthIsAttributed:YES];
    
    descriptionSeparator.top = descriptionLabel.bottom + kPadding;
    selectZiferblatLabel.top = descriptionSeparator.bottom + kPadding;
    chooseAllButton.top = selectZiferblatLabel.bottom+kSmallPadding;
    subscriptionTableView.top = chooseAllButton.bottom + kSmallPadding;
    subscriptionTableView.height = MAX([ZFSubscriptionZiferblatSelectionCell height]*ZiferblatService.allZiferblats.count, (notIPad ? 154:370));
    getItButton.top = subscriptionTableView.bottom + (notIPad ? 10:24);
    contentView.scrollView.contentSize = CGSizeMake(contentView.width, getItButton.bottom + kLargePadding);
}

#pragma mark -
#pragma mark Utils

- (NSDateFormatter *)dateFormater {
    NSDateFormatter *formater = [[NSDateFormatter alloc] init];
//    [formater setDateFormat:@"h:mm a"];
//    [formater setAMSymbol:@"am"];
//    [formater setPMSymbol:@"pm"];
    [formater setLocale:[NSLocale systemLocale]];
    formater.timeStyle = NSDateFormatterShortStyle;
    
    return formater;
}

#pragma mark -
#pragma mark Buttons actions

- (void)backButtonPressed {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)toggleButtonPressed:(UIControl *)aButton {
    if(aButton.tag != type) {
        type = aButton.tag;
        [ziferblatsSelected removeAllObjects];
        [subscriptionTableView reloadData];
        [self updateLayout];
    }
}

- (void)chooseAllButtonPressed {
    [ziferblatsSelected removeAllObjects];
    for (int i = 0; i <= summaryArray.count - 1; i++) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:0];
        ZFSubscriptionZiferblatSelectionCell *cell = (ZFSubscriptionZiferblatSelectionCell *)[subscriptionTableView cellForRowAtIndexPath:indexPath];
        [cell setWillSubscribe:YES];
        
        if(![ziferblatsSelected containsObject:[summaryArray objectAtIndex:indexPath.row]]) {
            [ziferblatsSelected addObject:[summaryArray objectAtIndex:indexPath.row]];
            [self checkStatusWithSummary:[summaryArray objectAtIndex:indexPath.row]];
        }
    }
    [self updateLayout];
}

- (void)getItButtonPressed {
    NSString *subscriptionType =  type == ZFSubscriptionType_Unlimited ? @"UNLIMITED" : @"WORKING";
    NSMutableArray *arrayOfIds = [NSMutableArray array];
    for (ZFVenueSummary *summary in ziferblatsSelected) {
        [arrayOfIds addObject:summary.uuid];
    }
    
    [ZFPurchaseSubscriptionRequest requestWithType:subscriptionType arrayOfIDs:arrayOfIds
                                                         completion:^(LORESTRequest *aRequest) {
        if(aRequest.resultStatus == LORESTRequestResultStatusSucceeded) {
            [SVProgressHUD showSuccessWithStatus:NSLocalizedString(@"You have successfully purchased a subscription",nil)];
            CurrentUser.accountBalance = aRequest.result;
            [self updateBalanceLabel];
            
            // Pop two levels up
            UIViewController *controller;
            for(controller in self.navigationController.viewControllers) {
                if([controller isKindOfClass:[ZFTreasuryViewController class]]) {
                    [self.navigationController popToViewController:controller animated:TRUE];
                    return;
                }
            }
        } else {
            [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"Could not purchase subscription",nil)];
        }
     }];
}

- (void)updateBalanceLabel
{
    NSUInteger piastresToDisplay = 0;
    
    if([CurrentUser.accountBalance respondsToSelector:@selector(piastres)])
    {
        piastresToDisplay = CurrentUser.accountBalance.piastres.integerValue;
    }
    
    piastresLabel.text = [NSString stringWithFormat:NSLocalizedString(@"%d PIASTRES", @"Do not translate"), piastresToDisplay];
}

#pragma mark -
#pragma mark TableView

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [ZFSubscriptionZiferblatSelectionCell height];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    switch (type) {
        case ZFSubscriptionType_Unlimited: {
            ZFVenueSummary *selectedVenueSummary = [summaryArray objectAtIndex:indexPath.row];
            NSNumber *cost = selectedVenueSummary.workingSubscriptionCost;
            NSPredicate *costPredicate = [NSPredicate predicateWithFormat:@"unlimitedSubscriptionCost <= %@", cost];
            [self selectVenueSummariesWithLessThanSelectedCostPredicate:costPredicate forSelectedVenueSummary:selectedVenueSummary];

            break;
        }
        case ZFSubscriptionType_Working: {
            ZFVenueSummary *selectedVenueSummary = [summaryArray objectAtIndex:indexPath.row];
            NSNumber *cost = selectedVenueSummary.workingSubscriptionCost;
            NSPredicate *costPredicate = [NSPredicate predicateWithFormat:@"workingSubscriptionCost <= %@", cost];
            [self selectVenueSummariesWithLessThanSelectedCostPredicate:costPredicate forSelectedVenueSummary:selectedVenueSummary];
            
            break;
        }
        default:
            break;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ZFSubscriptionZiferblatSelectionCell *cell = [tableView dequeueReusableCellWithIdentifier:[ZFSubscriptionZiferblatSelectionCell reuseIdentifier]];
    
    if(!cell) {
        cell = [[ZFSubscriptionZiferblatSelectionCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[ZFSubscriptionZiferblatSelectionCell reuseIdentifier]];
    }
    
    if(summaryArray.count > 0) {
        [cell setupWithZiferblatSummary:[summaryArray objectAtIndex:indexPath.row] andSubscriptionType:type];
        [cell setWillSubscribe:([ziferblatsSelected containsObject:[summaryArray objectAtIndex:indexPath.row]])];
    }
    
    
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return ZiferblatService.allZiferblats.count;
}

#pragma mark -
#pragma mark Utils

- (BOOL)containsSubscriptionAfterUpdateAtIndexPath:(NSIndexPath *)aIndexPath {
    BOOL containsSubscription = NO;
    if ([ziferblatsSelected containsObject:[summaryArray objectAtIndex:aIndexPath.row]]) {
        [subscriptionTableView deselectRowAtIndexPath:aIndexPath animated:YES];
        [ziferblatsSelected removeObject:[summaryArray objectAtIndex:aIndexPath.row]];
    } else {
        [ziferblatsSelected addObject:[summaryArray objectAtIndex:aIndexPath.row]];
        containsSubscription = YES;
    }
    [self updateLayout];
    [self checkStatusWithSummary:[summaryArray objectAtIndex:aIndexPath.row]];
    
    return containsSubscription;
}

- (void)checkStatus {
    getItButton.enabled = ziferblatsSelected.count > 0;
}

- (void)checkStatusWithSummary:(ZFVenueSummary *)summary {
    if (type == ZFSubscriptionType_Unlimited) {
        getItButton.enabled = ziferblatsSelected.count > 0 && [CurrentUser.accountBalance.piastres doubleValue] >= [summary.unlimitedSubscriptionCost doubleValue];
    } else {
        getItButton.enabled = ziferblatsSelected.count > 0 && [CurrentUser.accountBalance.piastres doubleValue] >= [summary.workingSubscriptionCost doubleValue];
    }
}

- (void)getSummarys {
    [ZFListAllVenuesRequest requestWithCompletion:^(LORESTRequest *aRequest) {
        if(aRequest.resultStatus == LORESTRequestResultStatusSucceeded) {
            summaryArray = aRequest.result;
            [subscriptionTableView reloadData];
        } else {
            [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"Could not get venue summary",nil)];
        }
    }];
}

- (void)selectVenueSummariesWithLessThanSelectedCostPredicate:(NSPredicate *)costPredicate forSelectedVenueSummary:(ZFVenueSummary *)selectedVenueSummary {
    if ([ziferblatsSelected containsObject:selectedVenueSummary]) {
        [ziferblatsSelected removeObject:selectedVenueSummary];
        NSArray *venuesWithLessThanCost = [summaryArray filteredArrayUsingPredicate:costPredicate];
        
        for (ZFVenueSummary *venue in venuesWithLessThanCost) {
            if ([ziferblatsSelected containsObject:venue]) {
                [ziferblatsSelected removeObject:venue];
            }
        }
    } else {
        [ziferblatsSelected addObject:selectedVenueSummary];
        NSArray *venuesWithLessThanCost = [summaryArray filteredArrayUsingPredicate:costPredicate];
        
        for (ZFVenueSummary *venue in venuesWithLessThanCost) {
            if (![ziferblatsSelected containsObject:venue]) {
                [ziferblatsSelected addObject:venue];
            }
        }
    }
    [self updateLayout];
    [self checkStatusWithSummary:selectedVenueSummary];
    [subscriptionTableView reloadData];
}

@end
