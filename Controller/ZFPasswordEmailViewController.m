//
//  ZFPasswordEmailViewController.m
//  Ziferblat
//
//  Created by Jose Fernandez on 30/07/2015.
//  Copyright (c) 2015 Locassa. All rights reserved.
//

#import "ZFPasswordEmailViewController.h"
#import "ZFNewPasswordViewController.h"
#import "ZFButton.h"

@interface ZFPasswordEmailViewController ()
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UILabel *topNavigationLabel;
@property (weak, nonatomic) IBOutlet UILabel *instructionsLabel;
@property (weak, nonatomic) IBOutlet UILabel *resetPasswordLabel;

@end

@implementation ZFPasswordEmailViewController {
    ZFButton *enterCodeButton;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupViewSizes];
    
    [self setupLocalizedStrings];
    [self setupEnterCodeButton];
}

- (void) setupViewSizes
{
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        self.topNavigationLabel.font = FontRockWell(26);
        self.resetPasswordLabel.font = FontRockWell(30);
        self.instructionsLabel.font = FontRockWell(26);
        
        self.topNavigationControlHeightConstraint.constant = 48;
        self.ResetPasswordLabelWidthConstraint.constant = 300;
        self.ResetPasswordLabelYConstraint.constant = 22;
        self.ResetPasswordLabelHeightConstraint.constant = 60;
        self.instructionsLabelYConstraint.constant = 120;
        self.instructionsLabelWidthConstraint.constant = 375;
    }
}

#pragma mark -
#pragma mark Setup UI

- (void)setupLocalizedStrings {
    self.instructionsLabel.text = NSLocalizedString(@"Great, the email has been sent!\nPlease check it for the secret code and follow the link", nil);
    self.topNavigationLabel.text = NSLocalizedString(@"BACK TO LOG IN PAGE", nil);
    self.resetPasswordLabel.text = NSLocalizedString(@"RESET PASSWORD", nil);
}

- (void)setupEnterCodeButton {
    enterCodeButton = [ZFButton buttonWithTitle:NSLocalizedString(@"ENTER CODE", nil) showMarks:NO];
    enterCodeButton.translatesAutoresizingMaskIntoConstraints = NO;
    [self.scrollView addSubview:enterCodeButton];
    
    NSLayoutConstraint *enterCodeButtonTopConstraint = [NSLayoutConstraint constraintWithItem:enterCodeButton
                                                                                attribute:NSLayoutAttributeTop
                                                                                relatedBy:NSLayoutRelationEqual
                                                                                   toItem:self.instructionsLabel
                                                                                attribute:NSLayoutAttributeBottom
                                                                               multiplier:1.0
                                                                                     constant:(UI_USER_INTERFACE_IDIOM() !=UIUserInterfaceIdiomPad ? 180:300)];
    NSLayoutConstraint *enterCodeButtonCenterXConstraint = [NSLayoutConstraint constraintWithItem:enterCodeButton
                                                                                    attribute:NSLayoutAttributeCenterX
                                                                                    relatedBy:NSLayoutRelationEqual
                                                                                       toItem:self.instructionsLabel
                                                                                    attribute:NSLayoutAttributeCenterX
                                                                                   multiplier:1.0
                                                                                     constant:0];
    NSLayoutConstraint *enterCodeButtonWidthConstraint = [NSLayoutConstraint constraintWithItem:enterCodeButton
                                                                                  attribute:NSLayoutAttributeWidth
                                                                                  relatedBy:NSLayoutRelationEqual
                                                                                     toItem:nil
                                                                                  attribute:NSLayoutAttributeWidth
                                                                                 multiplier:1.0
                                                                                   constant:158];
    NSLayoutConstraint *enterCodeButtonHeightConstraint = [NSLayoutConstraint constraintWithItem:enterCodeButton
                                                                                   attribute:NSLayoutAttributeHeight
                                                                                   relatedBy:NSLayoutRelationEqual
                                                                                      toItem:nil
                                                                                   attribute:NSLayoutAttributeHeight
                                                                                  multiplier:1.0
                                                                                    constant:44];
    
    [self.scrollView addConstraints:@[enterCodeButtonTopConstraint, enterCodeButtonCenterXConstraint, enterCodeButtonWidthConstraint, enterCodeButtonHeightConstraint]];
    
    TARGET_TOUCH_UP(enterCodeButton, @selector(enterCodeButtonWasPressed:))
}

#pragma mark -
#pragma mark Actions and Selectors

- (IBAction)backButtonWasPressed:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)enterCodeButtonWasPressed:(id)sender {
    ZFNewPasswordViewController *newPasswordViewController = [ZFNewPasswordViewController new];
    newPasswordViewController.email = self.email;
    [self.navigationController pushViewController:newPasswordViewController animated:YES];
}

@end
