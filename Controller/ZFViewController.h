//
//  ZFViewController.h
//  Ziferblat
//
//  Created by Boris Yurkevich on 24/12/2015.
//  Copyright © 2015 Locassa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZFViewController : UIViewController

// Set to yes if you presented this view controller
// Set to no if you pushed this view controller
@property BOOL canBeDismissed;

- (void)backButtonTapped;

@end
